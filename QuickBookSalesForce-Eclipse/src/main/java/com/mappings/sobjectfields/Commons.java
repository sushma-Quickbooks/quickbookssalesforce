package com.mappings.sobjectfields;

import java.io.StringWriter;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.quickbook.jaxb.QBXML;

public class Commons {
 static Logger log = Logger.getLogger(Commons.class.getName()); // for logging
 
 public static String marshallingSObjects(QBXML qbxmlObj){
  String reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
    +"<?qbxml version="+"\"8.0\""+"?>";
  StringWriter sw = new StringWriter();
  try {
   JAXBContext context = JAXBContext.newInstance(QBXML.class);
      Marshaller marshaller = context.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
      marshaller.marshal(qbxmlObj, sw );
  } catch (JAXBException e) {
   log.info("Account Marshalling xml ==>> "+e);
  } 
 
  log.info("Marshalling dynamic xml ==>> "+reqXML+sw.toString());
  return reqXML+sw.toString();
  
 }

}