package com.mappings.sobjectfields;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.appshark.qb.invoice.InvoiceLineRetType;
import com.appshark.qb.invoice.InvoiceRetType;
import com.constants.CommonConstants;
import com.constants.fieldconstnats.InvoiceConstantFields;
import com.constants.fieldconstnats.InvoiceLineItemsConstantFields;
import com.constants.objectconstants.SFDCObjectConstants;
import com.queryresultFormatter.QueryResultIterator;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.UpsertResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class SObjectMappings {
	
	static Logger log = Logger.getLogger(QueryResultIterator.class.getName()); // for logging
	
	public String invoiceObjectMaping(List<com.appshark.qb.invoice.InvoiceRetType> invoiceRetTypeList,
									Map<String,String> mapTxnIds,
									Map<String,SObject> mapOldListIds,
									Map<String,SObject> mapProducts,
									Map<String,String> mapTxnLineIds,
									Map<String,SObject> mapInvoices,
									Set<String> setInvoiceFields,
									Set<String> setInvoiceTxnIds,
									Set<String> setInvoiceLineItemFields,
									PartnerConnection partnerConnection,
									boolean invoiceFlag){
		String isOk = "";
		
		if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){

			List<SObject> lstInvoices = new ArrayList<SObject>();
			List<SObject> lstInvoiceItems = new ArrayList<SObject>();
			SObject[] objInvoiceLst = null;
			SObject[] objInvoiceList = null;
			SObject objInvoice;
			SObject objInvoiceItem;
			
			//iteration of invoiceRetTypeList to map sfdc invoices and qbsf invoices
			for(InvoiceRetType invoiceRetType : invoiceRetTypeList){
				log.info("invoiceRetType TxnID ==>> "+invoiceRetType.getTxnID());
				
				if((mapTxnIds != null && invoiceRetType.getTxnID() != null) 
						&& ((mapTxnIds.isEmpty() || (mapTxnIds.size()>0  && !mapTxnIds.containsKey(invoiceRetType.getTxnID().trim()))) || (mapTxnIds.size() > 0 && mapTxnIds.containsKey(invoiceRetType.getTxnID().trim()) && "false".equalsIgnoreCase(mapTxnIds.get(invoiceRetType.getTxnID().trim()))))){//mapTxnIds null check for controlling 2 way sync
					
					if((mapOldListIds != null && mapOldListIds.size() > 0) && invoiceRetType != null){ //mapOldListIds null check(account ids)
						
						for(String strLstId : mapOldListIds.keySet()){ //mapOldListIds key set
							
							if((invoiceRetType.getCustomerRef() != null && invoiceRetType.getCustomerRef().getListID() != null) && strLstId.equals(invoiceRetType.getCustomerRef().getListID().trim())){
								
								if(invoiceRetType.getTxnID() != null){
									
									if((setInvoiceFields != null && setInvoiceFields.size() > 0)/* && compFlag*/){//setInvoiceFields null check
										
										invoiceFlag = true;
										objInvoice = new SObject();
										objInvoice.setType(SFDCObjectConstants.INVOICE__C);
										objInvoice.setField("Name", "isQB");
										log.info("Account__c Insert==>> "+invoiceRetType.getCustomerRef().getListID());
										
										//Account
										if(setInvoiceFields.contains(InvoiceConstantFields.INVOICE_ACCOUNT__C)){
											objInvoice.setField(InvoiceConstantFields.INVOICE_ACCOUNT__C, mapOldListIds.get(strLstId).getId());
										}
										//TxnID
										log.info("LINE NO ==>> 513");
										if(setInvoiceFields.contains(InvoiceConstantFields.TXNID__C)){
											if(invoiceRetType.getTxnID() != null){
												log.info("***** TxnID__c ==>> "+invoiceRetType.getTxnID());
												objInvoice.setField(InvoiceConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
											}
										}
										//QB_Flag__c
										if(setInvoiceFields.contains(InvoiceConstantFields.QB_FLAG__C)){
											objInvoice.setField(InvoiceConstantFields.QB_FLAG__C,true);
										}
										//SFDC_Flag__c
										if(setInvoiceFields.contains(InvoiceConstantFields.SFDC_FLAG__C)){
											objInvoice.setField(InvoiceConstantFields.SFDC_FLAG__C,false);
										}
										//ListID__c
										if(setInvoiceFields.contains(InvoiceConstantFields.LISTID__C)){
											if(invoiceRetType.getTxnID() != null){
												objInvoice.setField(InvoiceConstantFields.LISTID__C, strLstId);
											}else{
												objInvoice.setField(InvoiceConstantFields.LISTID__C, "");
											}
										}
										//Time Created
										log.info("LINE NO ==>> 521");
										if(setInvoiceFields.contains(InvoiceConstantFields.TIME_CREATED__C)){
											if(invoiceRetType.getTimeCreated() != null){
												objInvoice.setField(InvoiceConstantFields.TIME_CREATED__C, invoiceRetType.getTimeCreated());
											}else{
												objInvoice.setField(InvoiceConstantFields.TIME_CREATED__C, "");
											}
										}
										//Time_Modified__c
										log.info("LINE NO ==>> 529");
										if(setInvoiceFields.contains(InvoiceConstantFields.TIME_MODIFIED__C)){
											if(invoiceRetType.getTimeModified() != null){
												objInvoice.setField(InvoiceConstantFields.TIME_MODIFIED__C, invoiceRetType.getTimeModified());
											}else{
												objInvoice.setField(InvoiceConstantFields.TIME_MODIFIED__C, "");
											}
										}
										//EditSequence__c
										log.info("LINE NO ==>> 537");
										if(setInvoiceFields.contains(InvoiceConstantFields.EDITSEQUENCE__C)){
											if(invoiceRetType.getEditSequence() != null){
												objInvoice.setField(InvoiceConstantFields.EDITSEQUENCE__C, invoiceRetType.getEditSequence());
											}else{
												objInvoice.setField(InvoiceConstantFields.EDITSEQUENCE__C, "");
											}
										}
										//Transaction_Number__c
										log.info("LINE NO ==>> 545");
										if(setInvoiceFields.contains(InvoiceConstantFields.TRANSACTION_NUMBER__C)){
											if(invoiceRetType.getTxnNumber() != null){
												objInvoice.setField(InvoiceConstantFields.TRANSACTION_NUMBER__C, invoiceRetType.getTxnNumber());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TRANSACTION_NUMBER__C});
											}
										}
										//CustomerRef_ListID__c
										log.info("LINE NO ==>> 553");
										if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERREF_LISTID__C)){
											if(invoiceRetType.getCustomerRef() != null && invoiceRetType.getCustomerRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.CUSTOMERREF_LISTID__C, invoiceRetType.getCustomerRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMERREF_LISTID__C});
											}
										}
										//Customer_Full_Name__c
										if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMER_FULL_NAME__C)){
											if(invoiceRetType.getCustomerRef() != null && invoiceRetType.getCustomerRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.CUSTOMER_FULL_NAME__C, invoiceRetType.getCustomerRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMER_FULL_NAME__C});
											}
										}
										//ClassRefListID__c
										log.info("LINE NO ==>> 569");
										if(setInvoiceFields.contains(InvoiceConstantFields.CLASSREFLISTID__C)){
											if(invoiceRetType.getClassRef() != null && invoiceRetType.getClassRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.CLASSREFLISTID__C, invoiceRetType.getClassRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CLASSREFLISTID__C});
											}
										}
										//ClassRefFullName__c
										log.info("LINE NO ==>> 577");
										if(setInvoiceFields.contains(InvoiceConstantFields.CLASSREFFULLNAME__C)){
											if(invoiceRetType.getClassRef() != null && invoiceRetType.getClassRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.CLASSREFFULLNAME__C, invoiceRetType.getClassRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CLASSREFFULLNAME__C});
											}
										}
										//ARAccountRef_ListID__c
										log.info("LINE NO ==>> 585");
										if(setInvoiceFields.contains(InvoiceConstantFields.ARACCOUNTREF_LISTID__C)){
											if(invoiceRetType.getARAccountRef() != null && invoiceRetType.getARAccountRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.ARACCOUNTREF_LISTID__C, invoiceRetType.getARAccountRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ARACCOUNTREF_LISTID__C});
											}
										}
										//ARAccountRef_FullName__c
										log.info("LINE NO ==>> 593");
										if(setInvoiceFields.contains(InvoiceConstantFields.ARACCOUNTREF_FULLNAME__C)){
											if(invoiceRetType.getARAccountRef() != null && invoiceRetType.getARAccountRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.ARACCOUNTREF_FULLNAME__C, invoiceRetType.getARAccountRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ARACCOUNTREF_FULLNAME__C});
											}
										}
										//TemplateRef_ListID__c
										log.info("LINE NO ==>> 601");
										if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFLISTID__C)){
											if(invoiceRetType.getTemplateRef() != null && invoiceRetType.getTemplateRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.TERMSREFLISTID__C, invoiceRetType.getTemplateRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFLISTID__C});
											}
										}
										//TemplateRef_FullName__c
										log.info("LINE NO ==>> 609");
										if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFFULLNAME__C)){
											if(invoiceRetType.getTemplateRef() != null && invoiceRetType.getTemplateRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.TERMSREFFULLNAME__C, invoiceRetType.getTemplateRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFFULLNAME__C});
											}
										}
										//Transaction_Date__c
										log.info("LINE NO ==>> 617");
										if(setInvoiceFields.contains(InvoiceConstantFields.TRANSACTION_DATE__C)){
											if(invoiceRetType.getTxnDate() != null){
												String strTransactionDate = invoiceRetType.getTxnDate();
												SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
												Date invTransactionDate;
												try {
													invTransactionDate = simpleDateFormat.parse(strTransactionDate);
													objInvoice.setField(InvoiceConstantFields.TRANSACTION_DATE__C, invTransactionDate);
												} catch (ParseException e) {
													if(e.getMessage()!=null && e.getMessage()!="")
													log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 220 ---> Exception while formatting invTransactionDate : "+ e.getMessage().toString());
												}
												
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TRANSACTION_DATE__C});
											}
										}
										//RefNumber__c
										log.info("LINE NO ==>> 625");
										if(setInvoiceFields.contains(InvoiceConstantFields.REFNUMBER__C) && invoiceRetType.getRefNumber() != null){
												objInvoice.setField(InvoiceConstantFields.REFNUMBER__C, invoiceRetType.getRefNumber());
										}
										log.info("LINE NO ==>> 633");
										//bill address
										String billAddressStreet = "";
										if(invoiceRetType.getBillAddress() != null){
											if(invoiceRetType.getBillAddress().getAddr1() != null){
												billAddressStreet = invoiceRetType.getBillAddress().getAddr1();
											}
											if(invoiceRetType.getBillAddress().getAddr2() != null){
												billAddressStreet = billAddressStreet + invoiceRetType.getBillAddress().getAddr2();
											}
											if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_STREET__C)){
												if(billAddressStreet != null && billAddressStreet.length() > 0){
													objInvoice.setField(InvoiceConstantFields.BILLING_STREET__C, billAddressStreet);
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STREET__C});
												}
											}
											if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_CITY__C)){
												if(invoiceRetType.getBillAddress().getCity() != null){
													objInvoice.setField(InvoiceConstantFields.BILLING_CITY__C, invoiceRetType.getBillAddress().getCity());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_CITY__C});
												}
											}
											if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_STATE__C)){
												if(invoiceRetType.getBillAddress().getState() != null){
													objInvoice.setField(InvoiceConstantFields.BILLING_STATE__C, invoiceRetType.getBillAddress().getState());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STATE__C});
												}
											}
											if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_POSTAL__C)){
												if(invoiceRetType.getBillAddress().getPostalCode() != null){
													objInvoice.setField(InvoiceConstantFields.BILLING_POSTAL__C, invoiceRetType.getBillAddress().getPostalCode());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_POSTAL__C});
												}
											}
											if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_COUNTRY__C)){
												if(invoiceRetType.getBillAddress().getCountry() != null){
													objInvoice.setField(InvoiceConstantFields.BILLING_COUNTRY__C, invoiceRetType.getBillAddress().getCountry());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_COUNTRY__C});
												}
											}
										}else{
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STREET__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_CITY__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STATE__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_POSTAL__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_COUNTRY__C});
										}
										//ship address
										String shipAddressStreet = "";
										if(invoiceRetType.getShipAddress() != null){
											if(invoiceRetType.getShipAddress().getAddr1() != null){
												shipAddressStreet = invoiceRetType.getShipAddress().getAddr1();
											}
											if(invoiceRetType.getShipAddress().getAddr2() != null){
												shipAddressStreet = shipAddressStreet + invoiceRetType.getShipAddress().getAddr2();
											}
											//SHIPPING_STREET__C
											if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_STREET__C)){
												if(shipAddressStreet != null && shipAddressStreet.length() > 0){
													objInvoice.setField(InvoiceConstantFields.SHIPPING_STREET__C, shipAddressStreet);
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STREET__C});
												}
											}
											//SHIPPING_CITY__C
											if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_CITY__C)){
												if(invoiceRetType.getShipAddress().getCity() != null){
													objInvoice.setField(InvoiceConstantFields.SHIPPING_CITY__C, invoiceRetType.getShipAddress().getCity());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_CITY__C});
												}
											}
											//Shipping_State__c
											if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_STATE__C)){
												if(invoiceRetType.getShipAddress().getState() != null){
													objInvoice.setField(InvoiceConstantFields.SHIPPING_STATE__C, invoiceRetType.getShipAddress().getState());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STATE__C});
												}
											}
											//SHIPPING_POSTAL__C
											if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_POSTAL__C)){
												if(invoiceRetType.getShipAddress().getPostalCode() != null){
													objInvoice.setField(InvoiceConstantFields.SHIPPING_POSTAL__C, invoiceRetType.getShipAddress().getPostalCode());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_POSTAL__C});
												}
											}
											//SHIPPING_COUNTRY__C
											if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_COUNTRY__C)){
												if(invoiceRetType.getShipAddress().getCountry() != null){
													objInvoice.setField(InvoiceConstantFields.SHIPPING_COUNTRY__C, invoiceRetType.getShipAddress().getCountry());
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_COUNTRY__C});
												}
											}
										}else{
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STREET__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_CITY__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STATE__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_POSTAL__C});
											objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_COUNTRY__C});
										}
										//Is_Pending__c
										log.info("LINE NO ==>> 668");
										if(setInvoiceFields.contains(InvoiceConstantFields.IS_PENDING__C)){
											if(invoiceRetType.getIsPending() != null){
												objInvoice.setField(InvoiceConstantFields.IS_PENDING__C, invoiceRetType.getIsPending());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_PENDING__C});
											}
										}
										//Is_Finance_Charge__c
										log.info("LINE NO ==>> 676");
										if(setInvoiceFields.contains(InvoiceConstantFields.IS_FINANCE_CHARGE__C)){
											if(invoiceRetType.getIsFinanceCharge() != null){
												objInvoice.setField(InvoiceConstantFields.IS_FINANCE_CHARGE__C, invoiceRetType.getIsFinanceCharge());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_FINANCE_CHARGE__C});
											}
										}
										//TermsRefListID__c
										log.info("LINE NO ==>> 684");
										if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFLISTID__C)){
											if(invoiceRetType.getTermsRef() != null && invoiceRetType.getTermsRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.TERMSREFLISTID__C, invoiceRetType.getTermsRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFLISTID__C});
											}
										}
										//TermsRefFullName__c
										log.info("LINE NO ==>> 692");
										if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFFULLNAME__C)){
											if(invoiceRetType.getTermsRef() != null && invoiceRetType.getTermsRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.TERMSREFFULLNAME__C, invoiceRetType.getTermsRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFFULLNAME__C});
											}
										}
										//Due_Date__c
										log.info("LINE NO ==>> 700");
										if(setInvoiceFields.contains(InvoiceConstantFields.DUE_DATE__C)){
											if(invoiceRetType.getDueDate() != null){
												String strDueDate = invoiceRetType.getDueDate();
												SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
												Date invDueDate;
												try {
													invDueDate = simpleDateFormat.parse(strDueDate);
													objInvoice.setField(InvoiceConstantFields.DUE_DATE__C, invDueDate);
												} catch (ParseException e) {
													if(e.getMessage()!=null && e.getMessage()!="")
														log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 391 ---> Exception while formatting invDueDate : "+ e.getMessage().toString());
												}
												
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.DUE_DATE__C});
											}
										}
										//Ship_Date__c
										log.info("LINE NO ==>> 708");
										if(setInvoiceFields.contains(InvoiceConstantFields.SHIP_DATE__C)){
											if(invoiceRetType.getShipDate() != null){
												String strShipDate = invoiceRetType.getShipDate();
												SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
												Date invShipDate;
												try {
													invShipDate = simpleDateFormat.parse(strShipDate);
													objInvoice.setField(InvoiceConstantFields.SHIP_DATE__C, invShipDate);
												} catch (ParseException e) {
													if(e.getMessage()!=null && e.getMessage()!="")
														log.info("Exception while formatting strShipDate:"+ e.getMessage().toString());
													log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 411 ---> Exception while formatting strShipDate : "+ e.getMessage().toString());
												}
												
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIP_DATE__C});
											}
										}
										//Subtotal__c
										log.info("LINE NO ==>> 716");
										if(setInvoiceFields.contains(InvoiceConstantFields.SUBTOTAL__C)){
											if(invoiceRetType.getSubtotal() != null){
												objInvoice.setField(InvoiceConstantFields.SUBTOTAL__C, invoiceRetType.getSubtotal());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SUBTOTAL__C});
											}
										}
										//ItemSalesTaxRefListID__c
										log.info("LINE NO ==>> 724");
										if(setInvoiceFields.contains(InvoiceConstantFields.ITEMSALESTAXREFLISTID__C)){
											if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.ITEMSALESTAXREFLISTID__C, invoiceRetType.getItemSalesTaxRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ITEMSALESTAXREFLISTID__C});
											}
										}
										//ItemSalesTaxRefFullName__c
										log.info("LINE NO ==>> 732");
										if(setInvoiceFields.contains(InvoiceConstantFields.ITEMSALESTAXREFFULLNAME__C)){
											if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.ITEMSALESTAXREFFULLNAME__C, invoiceRetType.getItemSalesTaxRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ITEMSALESTAXREFFULLNAME__C});
											}
										}
										//Sales_Tax_Percentage__c
										log.info("LINE NO ==>> 740");
										if(setInvoiceFields.contains(InvoiceConstantFields.SALES_TAX_PERCENTAGE__C)){
											if(invoiceRetType.getSalesTaxPercentage() != null){
												objInvoice.setField(InvoiceConstantFields.SALES_TAX_PERCENTAGE__C, invoiceRetType.getSalesTaxPercentage());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SALES_TAX_PERCENTAGE__C});
											}
										}
										//Sales_Tax_Total__c
										log.info("LINE NO ==>> 748");
										if(setInvoiceFields.contains(InvoiceConstantFields.SALES_TAX_TOTAL__C)){
											if(invoiceRetType.getSalesTaxTotal() != null){
												objInvoice.setField(InvoiceConstantFields.SALES_TAX_TOTAL__C, invoiceRetType.getSalesTaxTotal());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SALES_TAX_TOTAL__C});
											}
										}
										//Applied_Amount__c
										log.info("LINE NO ==>> 756");
										if(setInvoiceFields.contains(InvoiceConstantFields.APPLIED_AMOUNT__C)){
											if(invoiceRetType.getAppliedAmount() != null){
												objInvoice.setField(InvoiceConstantFields.APPLIED_AMOUNT__C, invoiceRetType.getAppliedAmount());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.APPLIED_AMOUNT__C});
											}
										}
										//Balance_Remaining__c
										log.info("LINE NO ==>> 764");
										if(setInvoiceFields.contains(InvoiceConstantFields.BALANCE_REMAINING__C)){
											if(invoiceRetType.getBalanceRemaining() != null){
												objInvoice.setField(InvoiceConstantFields.BALANCE_REMAINING__C, invoiceRetType.getBalanceRemaining());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BALANCE_REMAINING__C});
											}
										}
										//Is_Paid__c
										log.info("LINE NO ==>> 772");
										if(setInvoiceFields.contains(InvoiceConstantFields.IS_PAID__C)){
											if(invoiceRetType.getIsPaid() != null){
												objInvoice.setField(InvoiceConstantFields.IS_PAID__C, invoiceRetType.getIsPaid());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_PAID__C});
											}
										}
										//Is_To_Be_Printed__c
										log.info("LINE NO ==>> 780");
										if(setInvoiceFields.contains(InvoiceConstantFields.IS_TO_BE_PRINTED__C)){
											if(invoiceRetType.getIsToBePrinted() != null){
												objInvoice.setField(InvoiceConstantFields.IS_TO_BE_PRINTED__C, invoiceRetType.getIsToBePrinted());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_TO_BE_PRINTED__C});
											}
										}
										//CustomerSalesTaxCodeRefListID__c
										log.info("LINE NO ==>> 788");
										if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFLISTID__C)){
											if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getListID() != null){
												objInvoice.setField(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFLISTID__C, invoiceRetType.getItemSalesTaxRef().getListID());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMERSALESTAXCODEREFLISTID__C});
											}
										}
										//CustomerSalesTaxCodeRefFullName__c
										log.info("LINE NO ==>> 796");
										if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFFULLNAME__C)){
											if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getFullName() != null){
												objInvoice.setField(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFFULLNAME__C, invoiceRetType.getItemSalesTaxRef().getFullName());
											}else{
												objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMERSALESTAXCODEREFFULLNAME__C});
											}
										}
										lstInvoices.add(objInvoice);
										//Preparing Invoice Line Items
										List<InvoiceLineRetType> invoiceLineRetTypeList = invoiceRetType.getInvoiceLineRet();
										log.info("$$$ LINE NO ==>> 1016"+invoiceLineRetTypeList);
										if(invoiceLineRetTypeList != null && invoiceLineRetTypeList.size() > 0){//null check for list of invoice line items
											for(InvoiceLineRetType invoiceLineRetType : invoiceLineRetTypeList){// for loop of list of invoice line items
												if((mapTxnLineIds != null && invoiceLineRetType.getTxnLineID() != null) 
														&& ((mapTxnLineIds.isEmpty() || (mapTxnLineIds.size()>0  && !mapTxnLineIds.containsKey(invoiceLineRetType.getTxnLineID().trim()))) || (mapTxnLineIds.size() > 0 && mapTxnLineIds.containsKey(invoiceLineRetType.getTxnLineID().trim()) && "false".equalsIgnoreCase(mapTxnLineIds.get(invoiceLineRetType.getTxnLineID().trim()))))){
													if(invoiceLineRetType != null){
														setInvoiceTxnIds.add(invoiceRetType.getTxnID().trim());
														objInvoiceItem = new SObject();
														objInvoiceItem.setType(SFDCObjectConstants.INVOICE_LINE_ITEM__C);
														//name
														objInvoiceItem.setField(InvoiceLineItemsConstantFields.INVOICE_LINE_ITEMS_NAME,"isQB");
														//TxnLineID__c
														log.info("$$$ LINE NO ==>> 1273");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNLINEID__C)){
															objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, "");
															if(invoiceLineRetType.getTxnLineID() != null)
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, invoiceLineRetType.getTxnLineID().trim());
														}
														//QB_Flag__c
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.QB_FLAG__C)){
															objInvoice.setField(InvoiceLineItemsConstantFields.QB_FLAG__C,true);
														}
														//SFDC_FLAG__C
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SFDC_FLAG__C)){
															objInvoice.setField(InvoiceLineItemsConstantFields.SFDC_FLAG__C,false);
														}
														//TxnID__c
														log.info("$$$ LINE NO ==>> 1279");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNID__C)){
															objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, "");
															if(invoiceRetType.getTxnID() != null)
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
														}
														//Product__c
														log.info("$$$ LINE NO ==>> 1285");
														if(invoiceLineRetType.getItemRef() != null){
															//log.info("$$$ LINE NO ==>> 1298 : " +invoiceLineRetType.getItemRef().getListID());
															//log.info("$$$ LINE NO ==>> 1299 : " +invoiceLineRetType.getItemRef().getFullName());
															//log.info("$$$ LINE NO ==>> 1300 : " +mapProducts.get(invoiceLineRetType.getItemRef().getListID()));
															if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.PRODUCT__C)){
																if(invoiceLineRetType.getItemRef().getListID() != null && (mapProducts != null && mapProducts.get(invoiceLineRetType.getItemRef().getListID()) != null)){
																	objInvoiceItem.setField(InvoiceLineItemsConstantFields.PRODUCT__C, mapProducts.get(invoiceLineRetType.getItemRef().getListID()).getId());
																}
															}
														}
														//ExternID__c
														log.info("$$$ LINE NO ==>> 1304");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.EXTERNID__C)){
															objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, "");
															if(invoiceRetType.getTxnID() != null && (invoiceLineRetType.getItemRef() != null && invoiceLineRetType.getItemRef().getListID() != null)){
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, invoiceRetType.getTxnID().trim()+" "+invoiceLineRetType.getItemRef().getListID().trim());
															}
														}
														//Quantity__c
														log.info("$$$ LINE NO ==>> 1315");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.QUANTITY__C)){
															if(invoiceLineRetType.getQuantity() != null){
																log.info("$$$ invoiceLineRetType.getQuantity()" + invoiceLineRetType.getQuantity());
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.QUANTITY__C, invoiceLineRetType.getQuantity());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.QUANTITY__C});
															}
														}
														//Amount__c
														log.info("$$$ LINE NO ==>> 1326");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.AMOUNT__C)){
															if(invoiceLineRetType.getAmount() != null){
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.AMOUNT__C, invoiceLineRetType.getAmount());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.AMOUNT__C});
															}
														}
														//Rate__c
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.RATE__C)){
															if(invoiceLineRetType.getRate() != null){
																log.info("$$$ invoiceLineRetType.getRate()" + invoiceLineRetType.getRate());
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.RATE__C, invoiceLineRetType.getRate());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.RATE__C});
															}
														}
														//Description__c
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.DESCRIPTION__C)){
															if(invoiceLineRetType.getDesc() != null){
																log.info("$$$ invoiceLineRetType.getDesc()" + invoiceLineRetType.getDesc());
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.DESCRIPTION__C, invoiceLineRetType.getDesc());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.DESCRIPTION__C});
															}
														}
														//SalesTaxCodeRef_ListID__c
														log.info("$$$ LINE NO ==>> 1332");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C)){
															if(invoiceLineRetType.getSalesTaxCodeRef() != null && invoiceLineRetType.getSalesTaxCodeRef().getListID() != null){
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C, invoiceLineRetType.getSalesTaxCodeRef().getListID());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C});
															}
														}
														//SalesTaxCodeRef_Full_Name__c
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C)){
															if(invoiceLineRetType.getSalesTaxCodeRef() != null && invoiceLineRetType.getSalesTaxCodeRef().getFullName() != null){
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C, invoiceLineRetType.getSalesTaxCodeRef().getFullName());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C});
															}
														}
														//ClassRefListID__c
														log.info("$$$ LINE NO ==>> 1345");
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.CLASSREFLISTID__C)){
															if(invoiceLineRetType.getClassRef() != null && invoiceLineRetType.getClassRef().getListID() != null){
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.CLASSREFLISTID__C, invoiceLineRetType.getClassRef().getListID());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.CLASSREFLISTID__C});
															}
														}
														//ClassRefFullName__c
														if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.CLASSREFFULLNAME__C)){
															if(invoiceLineRetType.getClassRef() != null && invoiceLineRetType.getClassRef().getFullName() != null){
																objInvoiceItem.setField(InvoiceLineItemsConstantFields.CLASSREFFULLNAME__C, invoiceLineRetType.getClassRef().getFullName());
															}else{
																objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.CLASSREFFULLNAME__C});
															}
														}
														lstInvoiceItems.add(objInvoiceItem);
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}//end
			
			
			if(lstInvoices != null && !lstInvoices.isEmpty()){//adding list to an array
				objInvoiceLst = new SObject[lstInvoices.size()];
				for(int i = 0;i < lstInvoices.size();i++){
					objInvoiceLst[i] = lstInvoices.get(i);
				}
			}//End of lstInvoices null check
			
			//logic to upsert records to sfdc in batches to overcome sfdc limits
			if((objInvoiceLst != null && objInvoiceLst.length > 0) && invoiceFlag){
				log.info("objInvoiceLst Size ==>> "+objInvoiceLst.length);
				SObject[] invList = null;
				int indx = 0;
				int indx1 = 1;
				int indice = 0;
				if(objInvoiceLst.length > 200){//array greater than 200
					objInvoiceList = new SObject[100];
					if((objInvoiceLst.length%100) != 0){
						invList = new SObject[objInvoiceLst.length%100];
						indice = objInvoiceLst.length-(objInvoiceLst.length%100);
						log.info("INDICE VALUE ==>> "+indice);
						int j = 0;
						for(int i = 0;i < objInvoiceLst.length;i++){
							if(i >= indice){
								invList[j] = objInvoiceLst[i];
								j++;
							}
						}
					}
					for(SObject sObj : objInvoiceLst){
						indx1++;
						objInvoiceList[indx] = sObj;
						if(indx == 99){//upsert batches of 100
							log.info("objInvoiceList Size ==>> "+objInvoiceList.length);
							UpsertResult[] results;
							
							try {
								results = partnerConnection.upsert("TxnID__c",objInvoiceList);
								isOk = "ok";
								for (UpsertResult result : results){
									String accId = result.getId();
									com.sforce.soap.partner.Error[] errors = result.getErrors();
									for (com.sforce.soap.partner.Error error : errors){
										log.info("Invoice Save Error ==> "+error.toString());
									}
									log.info("INVOICE ID ==> "+accId);
								}
							} catch (ConnectionException e) {
								if(e.getMessage()!=null && e.getMessage()!="")
									log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 705 ---> Exception while upsert objInvoiceList: "+ e.getMessage().toString());
							}
						
							objInvoiceList = null;
							objInvoiceList = new SObject[100];
							indx = 0;
							continue;
						}
						//left over records after preparing batches
						if((indx1 == objInvoiceLst.length) && (objInvoiceLst.length%100) != 0){
							log.info("indx1 value ==> "+indx1 +" == NO OF BATCHES : "+objInvoiceLst.length);
							log.info("invList Size ==>> "+invList.length);
							if(invList != null && invList.length > 0){
								UpsertResult[] results;
								try {
									results = partnerConnection.upsert("TxnID__c",invList);
									isOk = "ok";
									for (UpsertResult result : results){
										String accId = result.getId();
										com.sforce.soap.partner.Error[] errors = result.getErrors();
										for (com.sforce.soap.partner.Error error : errors){
											log.info("Invoice Save Error ==> "+error.toString());
										}
										log.info("INVOICE ID ==> "+accId);
									}
								} catch (ConnectionException e) {
									if(e.getMessage()!=null && e.getMessage()!="")
										log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 733 ---> Exception while upsert invList: "+ e.getMessage().toString());
								}
								
							}
						}
						indx++;
					}
				}else{
					//records are less than 200
					UpsertResult[] results;
					try {
						results = partnerConnection.upsert("TxnID__c",objInvoiceLst);
						isOk = "ok";
						for (UpsertResult result : results){
							String accId = result.getId();
							com.sforce.soap.partner.Error[] errors = result.getErrors();
							for (com.sforce.soap.partner.Error error : errors){
								log.info("Invoice Save Error ==> "+error.toString());
							}
							log.info("INVOICE ID ==> "+accId);
						}
					} catch (ConnectionException e) {
						if(e.getMessage()!=null && e.getMessage()!="")
							log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 756 ---> Exception while upsert objInvoiceLst: "+ e.getMessage().toString());
					}
					
				}
				//Inserting Invoice Items
				if(lstInvoiceItems != null && lstInvoiceItems.size() > 0){
					log.info("lstInvoiceItems Size ==>> "+lstInvoiceItems.size());
					boolean iItemFlag = false;
					SObject[] invoiceItemList = new SObject[lstInvoiceItems.size()];
					//query to get invoice based on txn id
					String invoiceOrdQuery = "SELECT Id";
					if(setInvoiceFields.contains("TxnID__c") && setInvoiceFields.contains("RefNumber__c")){
						invoiceOrdQuery = invoiceOrdQuery+", TxnID__c,RefNumber__c FROM Invoice__c where TxnID__c in (";
						iItemFlag = true;
					}
					if(iItemFlag){
						invoiceOrdQuery = invoiceOrdQuery+"";
						String filter = "";
						int count = 1;
						//Preparing set of LISTID's
						if(setInvoiceTxnIds != null && setInvoiceTxnIds.size() > 0){
							for(String invoiceTxnId : setInvoiceTxnIds){
								if(invoiceTxnId != null){
									filter = filter+"\'"+invoiceTxnId+"\'";
									if(count != setInvoiceTxnIds.size())
										filter = filter+" ,";
								}
								count++;
							}
						}
						invoiceOrdQuery = invoiceOrdQuery+filter+")";
						log.info("invoiceOrdQuery ==>> "+invoiceOrdQuery);
						//Retrieving Invoice__c objects from Salesforce
						QueryResult qResult;
						try {
							qResult = partnerConnection.query(invoiceOrdQuery);
							
							if (qResult != null && qResult.getSize() > 0) { //resultset null check
								SObject[] records = qResult.getRecords();
								for(SObject sObj : records){
									if(sObj != null && sObj.getField("TxnID__c") != null){
										String strTxnID = sObj.getField("TxnID__c").toString().trim();
										mapInvoices.put(strTxnID, sObj);
									}
								}
							}
						} catch (ConnectionException e) {
							if(e.getMessage()!=null && e.getMessage()!="")
								log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 804 ---> Exception while invoiceOrdQuery query : "+ e.getMessage().toString());
						}
						
					}
					//preparing map for invoice and txn id
					for(int i = 0;i < lstInvoiceItems.size();i++){
						SObject invoiceItemObj = lstInvoiceItems.get(i);
						if(invoiceItemObj != null){
							if(mapInvoices != null && mapInvoices.size() > 0){
								if(invoiceItemObj.getField("TxnID__c") != null && mapInvoices.containsKey(invoiceItemObj.getField("TxnID__c").toString())){
									if(setInvoiceLineItemFields.contains("Invoice__c")){
										invoiceItemObj.setField("Invoice__c", mapInvoices.get(invoiceItemObj.getField("TxnID__c").toString()).getId());
									}
								}
							}
						}
						invoiceItemList[i] = invoiceItemObj;
					}
					//logic to prepare batches to overcome sfdc limit
					if(invoiceItemList != null && invoiceItemList.length > 0){
						log.info("invoiceItemList Size ==>> "+invoiceItemList.length);
						indx = 0;
						indx1 = 1;
						indice = 0;
						SObject[] objTempList = null;
						SObject[] invItemList = null;
						if(invoiceItemList.length > 200){//array size greater than 200
							objTempList = new SObject[100];
							if((invoiceItemList.length%100) != 0){
								invItemList = new SObject[invoiceItemList.length%100];
								indice = invoiceItemList.length-(invoiceItemList.length%100);
								log.info("INDICE VALUE ==>> "+indice);
								int j = 0;
								for(int i = 0;i < invoiceItemList.length;i++){
									if(i >= indice){
										invItemList[j] = invoiceItemList[i];
										j++;
									}
								}
							}
							for(SObject sObj : invoiceItemList){
								indx1++;
								objTempList[indx] = sObj;
								if(indx == 99){//batches of 100
									log.info("objTempList Size ==>> "+objTempList.length);
									UpsertResult[] results;
									try {
										results = partnerConnection.upsert("ExternID__c",objTempList);
										isOk = "ok";
										for (UpsertResult result : results){
											String salesItemId = result.getId();
											com.sforce.soap.partner.Error[] errors = result.getErrors();
											for (com.sforce.soap.partner.Error error : errors){
												log.info("Invoice Line Item Save Error ==> "+error.toString());
											}
											log.info("INVOICE LINE ITEM ID ==> "+salesItemId);
										}
									} catch (ConnectionException e) {
										if(e.getMessage()!=null && e.getMessage()!="")
											log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 863 ---> Exception while objTempList upsert : "+ e.getMessage().toString());

									}
									
									objTempList = null;
									objTempList = new SObject[100];
									indx = 0;
									continue;
								}
								//left over after preparing batches of 100
								if((indx1 == invoiceItemList.length) && (invoiceItemList.length%100) != 0){
									if(invItemList != null && invItemList.length > 0){
										UpsertResult[] results;
										try {
											results = partnerConnection.upsert("ExternID__c",invItemList);
											for (UpsertResult result : results){
												String salesItemId = result.getId();
												com.sforce.soap.partner.Error[] errors = result.getErrors();
												for (com.sforce.soap.partner.Error error : errors){
													log.info("Invoice Line Item Save Error ==> "+error.toString());
												}
												log.info("INVOICE LINE ITEM ID ==> "+salesItemId);
											}
										} catch (ConnectionException e) {
											if(e.getMessage()!=null && e.getMessage()!="")
												log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 888 ---> Exception while invItemList upsert : "+ e.getMessage().toString());
										}
										isOk = "ok";
										
									}
								}
								indx++;
							}
						}else{
							//array less than 200
							UpsertResult[] results;
							try {
								results = partnerConnection.upsert("ExternID__c",invoiceItemList);
								for (UpsertResult result : results){
									String accId = result.getId();
									com.sforce.soap.partner.Error[] errors = result.getErrors();
									for (com.sforce.soap.partner.Error error : errors){
										log.info("Invoice Line Item Save Error ==> "+error.toString());
									}
									log.info("INVOICE LINE ITEM ID ==> "+accId);
								}
							} catch (ConnectionException e) {
								if(e.getMessage()!=null && e.getMessage()!="")
									log.info(" Class : SObjectMapping ; Method : invoiceObjectMaping ; Line No : 911 ---> Exception while invoiceItemList upsert : "+ e.getMessage().toString());
							}
							isOk = "ok";
							
						}
					}
				}
			}
		
		}
		return isOk;
	}

}
