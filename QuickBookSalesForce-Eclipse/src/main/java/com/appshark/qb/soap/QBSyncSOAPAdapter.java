package com.appshark.qb.soap;

import java.util.logging.Logger;

import com.intuit.developer.ArrayOfString;
import com.intuit.developer.QBSyncService;
import com.intuit.developer.jaxws.Authenticate;
import com.intuit.developer.jaxws.AuthenticateResponse;
import com.intuit.developer.jaxws.ClientVersion;
import com.intuit.developer.jaxws.ClientVersionResponse;
import com.intuit.developer.jaxws.CloseConnection;
import com.intuit.developer.jaxws.CloseConnectionResponse;
import com.intuit.developer.jaxws.ConnectionError;
import com.intuit.developer.jaxws.ConnectionErrorResponse;
import com.intuit.developer.jaxws.GetLastError;
import com.intuit.developer.jaxws.GetLastErrorResponse;
import com.intuit.developer.jaxws.ReceiveResponseXML;
import com.intuit.developer.jaxws.ReceiveResponseXMLResponse;
import com.intuit.developer.jaxws.SendRequestXML;
import com.intuit.developer.jaxws.SendRequestXMLResponse;
import com.intuit.developer.jaxws.ServerVersion;
import com.intuit.developer.jaxws.ServerVersionResponse;
/**
 * The  program conatins logic to get Client server details
 * @author Appshark
 * @version 1.0
 * @since   2016-07-05
 */
public class QBSyncSOAPAdapter {

	private QBSyncService qbService = new QBSyncService();
	static Logger log = Logger.getLogger(QBSyncService.class.getName());//for logging
	/**
	 * This method is used to get client version response
	 * @param request
	 * @return ClientVersionResponse
	 */
	public ClientVersionResponse clientVersion(ClientVersion request) {
		log.info("IN clientVersion QBSyncSOAPAdapter");
		String response = qbService.clientVersion(request.getStrVersion());

		ClientVersionResponse adapterResponse = new ClientVersionResponse();
		adapterResponse.setClientVersionResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get ServerVersionResponse
	 * @param request
	 * @return ServerVersionResponse
	 */
	public ServerVersionResponse serverVersion(ServerVersion request) {
		log.info("IN serverVersion QBSyncSOAPAdapter");
		String response = qbService.serverVersion(request.getStrVersion());

		ServerVersionResponse adapterResponse = new ServerVersionResponse();
		adapterResponse.setServerVersionResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get AuthenticateResponse
	 * @param request
	 * @return AuthenticateResponse
	 */
	public AuthenticateResponse authenticate(Authenticate request) {
	
		log.info("IN authenticate QBSyncSOAPAdapter");
		ArrayOfString response = qbService.authenticate(request.getStrUserName(),
				request.getStrPassword());

		AuthenticateResponse adapterResponse = new AuthenticateResponse();
		adapterResponse.setAuthenticateResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get SendRequestXMLResponse
	 * @param request
	 * @return SendRequestXMLResponse
	 */
	public SendRequestXMLResponse sendRequestXML(SendRequestXML request) {
		log.info("IN sendRequestXML QBSyncSOAPAdapter");
		String response = qbService.sendRequestXML(request.getTicket(),
				request.getStrHCPResponse(), request.getStrCompanyFileName(), request.getQbXMLCountry(),
				request.getQbXMLMajorVers(), request.getQbXMLMinorVers());

		SendRequestXMLResponse adapterResponse = new SendRequestXMLResponse();
		adapterResponse.setSendRequestXMLResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get ReceiveResponseXMLResponse
	 * @param request
	 * @return ReceiveResponseXMLResponse
	 */
	public ReceiveResponseXMLResponse receiveResponseXML(
			ReceiveResponseXML request) {
		log.info("IN receiveResponseXML QBSyncSOAPAdapter");
		int response = qbService.receiveResponseXML(request.getTicket(),
				request.getResponse(), request.getHresult(), request.getMessage());

		ReceiveResponseXMLResponse adapterResponse = new ReceiveResponseXMLResponse();
		adapterResponse.setReceiveResponseXMLResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get ConnectionErrorResponse
	 * @param request
	 * @return ConnectionErrorResponse
	 */
	public ConnectionErrorResponse connectionError(ConnectionError request) {
		log.info("IN connectionError QBSyncSOAPAdapter");
		String response = qbService.connectionError(request.getTicket(),
				request.getHresult(), request.getMessage());

		ConnectionErrorResponse adapterResponse = new ConnectionErrorResponse();
		adapterResponse.setConnectionErrorResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get GetLastErrorResponse
	 * @param request
	 * @return GetLastErrorResponse
	 */
	public GetLastErrorResponse getLastError(GetLastError request) {
		log.info("IN getLastError QBSyncSOAPAdapter");
		String response = qbService.getLastError(request.getTicket());

		GetLastErrorResponse adapterResponse = new GetLastErrorResponse();
		adapterResponse.setGetLastErrorResult(response);

		return adapterResponse;
	}
	/**
	 * This method is used to get CloseConnectionResponse
	 * @param request
	 * @return CloseConnectionResponse
	 */
	public CloseConnectionResponse closeConnection(CloseConnection request) {
		log.info("IN closeConnection QBSyncSOAPAdapter");
		String response = qbService.closeConnection(request.getTicket());

		CloseConnectionResponse adapterResponse = new CloseConnectionResponse();
		adapterResponse.setCloseConnectionResult(response);

		return adapterResponse;
	}

}