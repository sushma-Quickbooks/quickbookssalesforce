package com.appshark.qb.soap;

import java.io.*;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.apache.commons.io.IOUtils;
import  com.appshark.qb.soap.QBSyncSOAPHandler;

@SuppressWarnings("serial")
/**
 * The  program conatins logic to parse SOAP messages
 * @author Appshark
 * @version 1.0
 * @since   2016-07-05
 */
public class SoapMsgParser extends HttpServlet {
	static Logger log = Logger.getLogger(SoapMsgParser.class.getName());
	static MessageFactory messageFactory;
	static QBSyncSOAPHandler soapHandler;

	static {
		log.info("IN SoapMsgParser static block ");

		System.setProperty("javax.xml.transform.TransformerFactory","com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
		try {
			messageFactory = MessageFactory.newInstance();
			soapHandler = new QBSyncSOAPHandler();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	/**
	 * This method is to parse SOAP messages
	 * @param HttpServletResponse,HttpServletRequest
	 * @return void
	 */
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)throws IOException {
		try {
			log.info("IN SoapMsgParser doPost ");

			boolean logResponse = false;
			MimeHeaders headers = getHeaders(req); //Getting MimeHeaders from request Object
			InputStream is = req.getInputStream(); //Get InputStream Object 
			StringWriter writer = new StringWriter();
			IOUtils.copy(is, writer);
			String requestString = writer.toString();
			log.info("requestString - "+ requestString) ;
			
			if ((requestString.contains("sendRequestXML") == false) && (requestString.contains("receiveResponseXML") == false))
			if (requestString.contains("<authenticate")) //if requires authentication
				logResponse = true;
			is = new ByteArrayInputStream(requestString.getBytes());//get stream of bytes (XML - Bytes)
			
			//Creating SOAP Object with soap headers and xml input stream
			SOAPMessage soapRequest = messageFactory.createMessage(headers, is);
			
			log.info("soapRequest - "+ soapRequest.toString()) ;
			 //parsing and handling SOAP request & responses
			SOAPMessage soapResponse = soapHandler.handleSOAPRequest(soapRequest);
			
			//set response headers
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/xml;charset=\"utf-8\"");
			
			OutputStream os = resp.getOutputStream();  //get stream of bytes (XML - Bytes)
			soapResponse.writeTo(os);
			
			//flush off the stream
			os.flush();
			resp.flushBuffer();
			os.flush();
			
			if (logResponse) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				soapResponse.writeTo(baos);
				String soapResponseStr = new String(baos.toByteArray(), "UTF-8");
				log.info("SoapResponseStr logResponse- "+ soapResponseStr) ;
			}
			
		} catch (SOAPException e) { //handling exception
			throw new IOException("Exception Occured ==>> ", e);
		}
	}

	@SuppressWarnings("unchecked")
	static MimeHeaders getHeaders(HttpServletRequest req) {
		log.info("IN MimeHeaders static ");
		Enumeration headerNames = req.getHeaderNames();
		MimeHeaders headers = new MimeHeaders();
		while (headerNames.hasMoreElements()) {
			String headerName = (String) headerNames.nextElement();
			String headerValue = req.getHeader(headerName);
			StringTokenizer values = new StringTokenizer(headerValue, ",");
			while (values.hasMoreTokens()) {
				headers.addHeader(headerName, values.nextToken().trim());
			}
		}
		return headers;
	}
}