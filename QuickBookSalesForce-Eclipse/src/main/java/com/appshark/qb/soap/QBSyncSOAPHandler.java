package com.appshark.qb.soap;

import java.util.Iterator;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SAAJResult;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.dom.DOMSource;

import com.intuit.developer.jaxws.Authenticate;
import com.intuit.developer.jaxws.ClientVersion;
import com.intuit.developer.jaxws.CloseConnection;
import com.intuit.developer.jaxws.ConnectionError;
import com.intuit.developer.jaxws.GetLastError;
import com.intuit.developer.jaxws.ReceiveResponseXML;
import com.intuit.developer.jaxws.SendRequestXML;
import com.intuit.developer.jaxws.ServerVersion;
/**
 * The  program conatins logic to handle SOAP requests
 * @author Appshark
 * @version 1.0
 * @since   2016-07-05
 */
public class QBSyncSOAPHandler {

	private static final String NAMESPACE_URI = "http://developer.intuit.com/";
	private static final QName QNAME_clientVersion = new QName(NAMESPACE_URI,"clientVersion");
	private static final QName QNAME_serverVersion = new QName(NAMESPACE_URI,"serverVersion");
	private static final QName QNAME_authenticate = new QName(NAMESPACE_URI,"authenticate");
	private static final QName QNAME_sendRequestXML = new QName(NAMESPACE_URI,"sendRequestXML");
	private static final QName QNAME_receiveResponseXML = new QName(NAMESPACE_URI, "receiveResponseXML");
	private static final QName QNAME_connectionError = new QName(NAMESPACE_URI,"connectionError");
	private static final QName QNAME_getLastError = new QName(NAMESPACE_URI,"getLastError");
	private static final QName QNAME_closeConnection = new QName(NAMESPACE_URI,"closeConnection");

	private MessageFactory messageFactory;
	private QBSyncSOAPAdapter qBSyncSOAPAdapter;

	public QBSyncSOAPHandler() throws SOAPException {
		messageFactory = MessageFactory.newInstance();
		qBSyncSOAPAdapter = new QBSyncSOAPAdapter();
	}

	/**
	 * This method is used to handle SOAP requests
	 * @param request
	 * @return SOAPMessage
	 */

	public SOAPMessage handleSOAPRequest(SOAPMessage request)
			throws SOAPException {
		SOAPBody soapBody = request.getSOAPBody();
		Iterator iterator = soapBody.getChildElements();
		Object responsePojo = null;
		while (iterator.hasNext()) {
			Object next = iterator.next();
			if (next instanceof SOAPElement) {
				SOAPElement soapElement = (SOAPElement) next;
				QName qname = soapElement.getElementQName();
				if (QNAME_authenticate.equals(qname)) { //SFDC Authentication 
					responsePojo = handle_authenticate(soapElement);
					break;
				} else if (QNAME_sendRequestXML.equals(qname)) { //Prepare request query xml and send it to Quick books for data
					responsePojo = handle_sendRequestXML(soapElement);
					break;
				} else if (QNAME_receiveResponseXML.equals(qname)) {  //Receive data from Quick book and sync it to SFDC
					responsePojo = handle_receiveResponseXML(soapElement);
					break;
				} else if (QNAME_connectionError.equals(qname)) {//gets connection errors
					responsePojo = handle_connectionError(soapElement);
					break;
				} else if (QNAME_getLastError.equals(qname)) {//gets  last errors
					responsePojo = handle_getLastError(soapElement);
					break;
				} else if (QNAME_closeConnection.equals(qname)) {//close connection
					responsePojo = handle_closeConnection(soapElement);
					break;
				} else if (QNAME_clientVersion.equals(qname)) {//client version
					responsePojo = handle_clientVersion(soapElement);
					break;
				} else if (QNAME_serverVersion.equals(qname)) {//server version
					responsePojo = handle_serverVersion(soapElement);
					break;
				}
			}
		}

		SOAPMessage soapResponse = messageFactory.createMessage();
		soapBody = soapResponse.getSOAPBody();
		if (responsePojo != null) {
			JAXB.marshal(responsePojo, new SAAJResult(soapBody));
		} else {
			SOAPFault fault = soapBody.addFault();
			fault.setFaultString("Unrecognized SOAP request.");
		}
		return soapResponse;
	}

	
	
	/**
	 * This method is used to handle client version
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_clientVersion(SOAPElement soapElement) {
		ClientVersion request = JAXB.unmarshal(new DOMSource(soapElement),
				ClientVersion.class);
		return qBSyncSOAPAdapter.clientVersion(request);
	}
	/**
	 * This method is used to handle server version
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_serverVersion(SOAPElement soapElement) {
		ServerVersion request = JAXB.unmarshal(new DOMSource(soapElement),
				ServerVersion.class);
		return qBSyncSOAPAdapter.serverVersion(request);
	}
	
	/**
	 * This method is used to handle authentication
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_authenticate(SOAPElement soapElement) {
		Authenticate request = JAXB.unmarshal(new DOMSource(soapElement),
				Authenticate.class);
		return qBSyncSOAPAdapter.authenticate(request);
	}
	/**
	 * This method is used to handle send Request xml
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_sendRequestXML(SOAPElement soapElement) {
		SendRequestXML request = JAXB.unmarshal(new DOMSource(soapElement),
				SendRequestXML.class);
		return qBSyncSOAPAdapter.sendRequestXML(request);
	}
	/**
	 * This method is used to handle receive response xml
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_receiveResponseXML(SOAPElement soapElement) {
		ReceiveResponseXML request = JAXB.unmarshal(new DOMSource(soapElement),
				ReceiveResponseXML.class);
		return qBSyncSOAPAdapter.receiveResponseXML(request);
	}
	/**
	 * This method is used to handle connection errors
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_connectionError(SOAPElement soapElement) {
		ConnectionError request = JAXB.unmarshal(new DOMSource(soapElement),
				ConnectionError.class);
		return qBSyncSOAPAdapter.connectionError(request);
	}
	/**
	 * This method is used to handle last errors
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_getLastError(SOAPElement soapElement) {
		GetLastError request = JAXB.unmarshal(new DOMSource(soapElement),
				GetLastError.class);
		return qBSyncSOAPAdapter.getLastError(request);
	}
	/**
	 * This method is used to handle close connection
	 * @param soapElement
	 * @return Object
	 */
	private Object handle_closeConnection(SOAPElement soapElement) {
		CloseConnection request = JAXB.unmarshal(new DOMSource(soapElement),
				CloseConnection.class);
		return qBSyncSOAPAdapter.closeConnection(request);
	}

}