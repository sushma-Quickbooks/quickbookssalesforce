//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.07 at 07:25:02 PM IST 
//


package com.quickbook.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItemOtherChargeAddRqType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemOtherChargeAddRqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ItemOtherChargeAdd"/>
 *       &lt;/sequence>
 *       &lt;attribute name="requestID" type="{}STRTYPE" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemOtherChargeAddRqType", propOrder = {
    "itemOtherChargeAdd"
})
public class ItemOtherChargeAddRqType {

    @XmlElement(name = "ItemOtherChargeAdd", required = true)
    protected ItemOtherChargeAdd itemOtherChargeAdd;
    @XmlAttribute(name = "requestID")
    protected String requestID;

    /**
     * Gets the value of the itemOtherChargeAdd property.
     * 
     * @return
     *     possible object is
     *     {@link ItemOtherChargeAdd }
     *     
     */
    public ItemOtherChargeAdd getItemOtherChargeAdd() {
        return itemOtherChargeAdd;
    }

    /**
     * Sets the value of the itemOtherChargeAdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemOtherChargeAdd }
     *     
     */
    public void setItemOtherChargeAdd(ItemOtherChargeAdd value) {
        this.itemOtherChargeAdd = value;
    }

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

}
