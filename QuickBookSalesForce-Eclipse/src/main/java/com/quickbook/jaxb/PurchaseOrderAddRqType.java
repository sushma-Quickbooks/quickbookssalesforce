//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.06.07 at 07:25:02 PM IST 
//


package com.quickbook.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchaseOrderAddRqType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderAddRqType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}PurchaseOrderAdd"/>
 *       &lt;/sequence>
 *       &lt;attribute name="requestID" type="{}STRTYPE" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderAddRqType", propOrder = {
    "purchaseOrderAdd"
})
public class PurchaseOrderAddRqType {

    @XmlElement(name = "PurchaseOrderAdd", required = true)
    protected PurchaseOrderAdd purchaseOrderAdd;
    @XmlAttribute(name = "requestID")
    protected String requestID;

    /**
     * Gets the value of the purchaseOrderAdd property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderAdd }
     *     
     */
    public PurchaseOrderAdd getPurchaseOrderAdd() {
        return purchaseOrderAdd;
    }

    /**
     * Sets the value of the purchaseOrderAdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderAdd }
     *     
     */
    public void setPurchaseOrderAdd(PurchaseOrderAdd value) {
        this.purchaseOrderAdd = value;
    }

    /**
     * Gets the value of the requestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Sets the value of the requestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

}
