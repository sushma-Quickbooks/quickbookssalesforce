package com.intuit.developer;

import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;

public class TestConnection {
	public PartnerConnection authenticate(String enterpriseEnvironment,String username,String password) throws ConnectionException {
		PartnerConnection connection;
		ConnectorConfig config = new ConnectorConfig();
		enterpriseEnvironment= "https://test.salesforce.com/services/Soap/u/35.0";
		config.setAuthEndpoint(enterpriseEnvironment);
		config.setUsername(username);
		config.setPassword(password);
		try {
			connection = Connector.newConnection(config);
			System.out.println("###SFDCService authenticate End###");
		} catch (ConnectionException e) {
			System.out.println("###SFDCService authenticate LINE 33 ###");
			System.out.println("Unable to authenticate as '" + username + "'"+ e);
			throw e;
		}
		return connection;
	}
	public static void main(String[] args) {
		
		try {
			TestConnection testConnection = new TestConnection();
			PartnerConnection PartnerConn = testConnection.authenticate("null",
					"cventapi@aqsquilt.com",
					"cvent@123lSGXzpM0sLQrss8sYnuizl6eb");
			System.out.println("PartnerConn ==> " + PartnerConn);
			SObject acc = new SObject();
			acc.setType("Account");
			acc.setField("Name","Test Acc 333");
			PartnerConn.create(new SObject[]{acc});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
