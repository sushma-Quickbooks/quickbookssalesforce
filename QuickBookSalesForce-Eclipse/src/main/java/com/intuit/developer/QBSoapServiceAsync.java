package com.intuit.developer;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * QBSoapServiceAsync Interface
 * @author rtatikonda
 *
 */
public interface QBSoapServiceAsync {
	void getGooglePublicKey(String name, AsyncCallback<String> callback) throws IllegalArgumentException;
	
	void getGooglePrivateKey(String name, AsyncCallback<String> callback) throws IllegalArgumentException;
}
