package com.intuit.developer;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * QBSoapService class
 * @author rtatikonda
 *
 */
@RemoteServiceRelativePath("initializeqb")
public interface QBSoapService extends RemoteService {
	
	/**
	 * getGooglePublicKey method
	 * @param name
	 * @return String
	 * @throws IllegalArgumentException
	 */
	String getGooglePublicKey(String name) throws IllegalArgumentException;
	
	/**
	 * getGooglePrivateKey method
	 * @param name
	 * @return String
	 * @throws IllegalArgumentException
	 */
	String getGooglePrivateKey(String name) throws IllegalArgumentException;
}
