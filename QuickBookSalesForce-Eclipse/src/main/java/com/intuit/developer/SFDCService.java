package com.intuit.developer;

import com.sforce.soap.partner.Connector;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import java.util.logging.Logger;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class SFDCService  extends RemoteServiceServlet implements QBSoapService{
	static Logger log = Logger.getLogger(SFDCService.class.getName());
	public PartnerConnection connection=null;
	
	/**
	 * authenticate is used to get the connection of SFDC
	 * @param enterpriseEnvironment
	 * @param username
	 * @param password
	 * @return PartnerConnection
	 * @throws ConnectionException
	 */
    public PartnerConnection authenticate(String enterpriseEnvironment,String username,String password) throws ConnectionException {
	    log.info("###SFDCService authenticate Start###");
	    log.info("###username###"+username);
	    log.info("###password###"+password);
		PartnerConnection connection;
		ConnectorConfig config = new ConnectorConfig();
		enterpriseEnvironment= "https://login.salesforce.com/services/Soap/u/34.0";
		config.setAuthEndpoint(enterpriseEnvironment);
		config.setUsername(username);
		config.setPassword(password);
		try {
			connection = Connector.newConnection(config);
			log.info("###SFDCService authenticate End###");
		} catch (ConnectionException e) {
			log.info("###SFDCService authenticate LINE 33 ###");
			System.out.println("Unable to authenticate as '" + username + "'"+ e);
			throw e;
		}
		return connection;
	}
    
    /**
     * getGooglePublicKey method
     */
    @Override
	public String getGooglePublicKey(String name)throws IllegalArgumentException {
		return "-----BEGIN CERTIFICATE-----"
			   + "MIIEbjCCA1agAwIBAgIOAUY4KJiDAAAAABeGLpQwDQYJKoZIhvcNAQEFBQAwfTEV"
			   + "MBMGA1UEAwwMQXBwc2hhcmtDZXJ0MRgwFgYDVQQLDA8wMEQ5MDAwMDAwMGpMSngx"
			   + "FzAVBgNVBAoMDlNhbGVzZm9yY2UuY29tMRYwFAYDVQQHDA1TYW4gRnJhbmNpc2Nv"
			   + "c2ZvcmNlLmNvbTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzELMAkGA1UECAwCQ0Ex"
			   + "DDAKBgNVBAYTA1VTQYIOAUY4KJiUAAAAABeGLpQwDwYDVR0TAQH/BAUwAwEB/zAN"
			   + "BgkqhkiG9w0BAQUFAAOCAQEAP86KLrAmzMnfneAUWVbvuUJ9B7gu9rWS4TVQVOiy"
			   + "1gN6S8yTAI8fGl6wYwDc2V+oZz7lsTx1UAUqKZdU1KnFyYaAMJopHtIfszealKhn"
			   + "XOMt0DUpYg3kEyHm1GnaPTyqembcA4YB3CVxz0T5QCdpW0jhYnJOVm5cfE7Mvcbh"
			   + "C2zUcE2qQHoHz3cWujKvJ7ivN4qhjoKL4ab2C4CtRZfNAlYoklU9rvF6Ixzf/yuI"
			   + "9Tz/cOeXeaCtlInDriyWIP+EVq9kKCphGpaXPADhuonGMxrGOvvNxEMBmL6qPLLO"
			   + "q9xjnv31AezT11cvTlVMTvCqoziBQzoF7Jf+gPC/FMiQUA=="
			   + "-----END CERTIFICATE-----";
		//PKCS#14
	}
    
    /**
     * getGooglePrivateKey method
     */
    @Override
	public String getGooglePrivateKey(String name)throws IllegalArgumentException {
		return "-----BEGIN CERTIFICATE-----"
				+ "MQswCQYDVQQIDAJDQTEMMAoGA1UEBhMDVVNBMB4XDTE0MDUyNjEwNTIwM1oXDTE2"
				+ "MDUyNTEwNTIwM1owfTEVMBMGA1UEAwwMQXBwc2hhcmtDZXJ0MRgwFgYDVQQLDA8w"
				+ "MEQ5MDAwMDAwMGpMSngxFzAVBgNVBAoMDlNhbGVzZm9yY2UuY29tMRYwFAYDVQQH"
				+ "DA1TYW4gRnJhbmNpc2NvMQswCQYDVQQIDAJDQTEMMAoGA1UEBhMDVVNBMIIBIjAN"
				+ "BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqjwAuWOjGfl0PA1wZacmqoLnSPM8"
				+ "azcF09sn+WjzVixdeSGGCwjmCmyhjdk/qT95GeVvEJolQaOByMuJ35rk4v9A+Og9"
				+ "BYsf9/+kT+7o17T41sFlTTVifN0BML37D9yzhHZ1Ja2zyR4DKwtULLJTttDeIbIb"
				+ "fwn4ySsUTDRcIHc5PQwu36RNmra6IBCKhz+rlUDvfHJeeLMI7XVFcsuacgcxcb4k"
				+ "W46fgUg5FYXOLppHWcOZtgwTcnddw/+oOrOdIQb4Q9M/b0UtYstGudXp/X2AOEXQ"
				+ "OLXkqoGno62AqmSAl9ZLQBP9LKmJA1kf+cYl5HrHyK8rRfM2coaZpbWCOwIDAQAB"
				+ "o4HrMIHoMB0GA1UdDgQWBBQbPGWjTO4fC+c5BBSMgOW3bERQEzCBtQYDVR0jBIGt"
				+ "MIGqgBQbPGWjTO4fC+c5BBSMgOW3bERQE6GBgaR/MH0xFTATBgNVBAMMDEFwcHNo"
				+ "YXJrQ2VydDEYMBYGA1UECwwPMDBEOTAwMDAwMDBqTEp4MRcwFQYDVQQKDA5TYWxl"
				+ "-----END CERTIFICATE-----";
		//PKCS#7
	}
}
