package com.intuit.developer;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import com.appshark.AccountingQuery.rs.AccountRetType;
import com.appshark.billquery.rs.BillRetType;
import com.appshark.billquery.rs.ExpenseLineRetType;
import com.appshark.billquery.rs.ItemLineRetType;
import com.appshark.customutil.cls.CustomerRetType;
import com.appshark.customutil.cls.QBXMLType;
import com.appshark.estimatequery.rs.EstimateLineRetType;
import com.appshark.estimatequery.rs.EstimateRetType;
import com.appshark.itemInventoryquery.rs.ItemInventoryRetType;
import com.appshark.purchaseorderquery.rs.PurchaseOrderLineRetType;
import com.appshark.purchaseorderquery.rs.PurchaseOrderRetType;
import com.appshark.qb.invoice.InvoiceLineRetType;
import com.appshark.qb.invoice.InvoiceRetType;
import com.appshark.qb.salesorder.SalesOrderLineRetType;
import com.appshark.qb.salesorder.SalesOrderRetType;
import com.appshark.vendorquery.rs.VendorRetType;
import com.constants.CommonConstants;
import com.constants.RequestXMLConstants.XMLConstants;
import com.constants.fieldconstnats.OpportunityConstantFields;
import com.constants.fieldconstnats.OpportunityLineItem;
import com.constants.objectconstants.SFDCObjectConstants;
import com.constants.queryconstants.SFDCQueryConstants;
import com.constants.fieldconstnats.AccountConstantFields;
import com.constants.fieldconstnats.AccountingConstantFields;
import com.constants.fieldconstnats.BillLineItemConstantFields;
import com.constants.fieldconstnats.BillsConstantFields;
import com.constants.fieldconstnats.ExpensesLineItemConstantFields;
import com.constants.fieldconstnats.InvoiceConstantFields;
import com.constants.fieldconstnats.InvoiceLineItemsConstantFields;
import com.constants.fieldconstnats.ItemInventoryConstantFields;
import com.constants.fieldconstnats.Product2ConstantFields;
import com.constants.fieldconstnats.PurchaseOrderConstantFields;
import com.constants.fieldconstnats.PurchaseOrderItemsConstantFields;
import com.constants.fieldconstnats.QBSynchControlConstants;
import com.constants.fieldconstnats.SFDCSynchControlConstants;
import com.constants.fieldconstnats.VendorConstantFields;
import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.Field;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.UpsertResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.util.MailService;
import com.util.QueryExecuter;
import com.util.SqlQueryBuilder;
import com.util.UtilityFunctions;
import com.queryresultFormatter.QueryResultIterator;
import com.logging.PartnerConnectionErrorLogging;


/**This program contains methods to sync data from afdc to qb and vice versa
 * QBSyncService Class
 * @author Appshark
 * @since   updated on 06-08-2016
 *
 */
@WebService
public class QBSyncService {
	static Logger log = Logger.getLogger(QBSyncService.class.getName());//for logging

	public static PartnerConnection partnerConnection = null ;//partner connection from sfdc authenticate method
	public static int percentComplete=0,percentinct=0,count=0,size=0;
	//stores the lstSyncDate from each object
	public static String qbSynchControl = null,qbAccSynchControl = null,qbInvoiceSynchControl = null,qbSalesSynchControl = null,
			qbItemSynchControl = null,qbEstimateSynchControl = null,sfdcInsertLastSyncControl=null,sfdcUpdateLastSyncControl=null,sfdcInsertInvLastSyncControl=null,qbBillSynchControl=null,qbVenSynchControl=null,qbAcctingSynchControl=null,qbPurchaseOrderSynchControl=null,qbItemInvSynchControl=null,
			sfdcUpdateInvLastSyncControl=null,sfdcUpdateInvItemLastSyncControl=null,sfdcUpdateOppItemLastSyncControl=null,sfdcInsertProductLastSyncControl=null,sfdcUpdateProductLastSyncControl=null,sfdcInsertOppLastSyncControl=null,sfdcUpdateOppLastSyncControl=null;
	public static Set<String> setAccountFields = null;//contains set of all Account sobject fields 
	public static Set<String> setInvoiceFields = null;//contains set of all Invoice sobject fields 
	public static Set<String> setInvoiceLineItemFields = null;//contains set of all InvoiceLineItems sobject fields 
	public static Set<String> setProduct2Fields = null;//contains set of all Product2 sobject fields 
	public static Set<String> setPriceBookEntryFields = null;//contains set of all PriceBookEntry sobject fields 
	public static Set<String> setOpportunityFields = null;//contains set of all Opportunity sobject fields 
	public static Set<String> setOppLineItemFields = null;//contains set of all OpportunityLineItems sobject fields 
	public static Set<String> setVendorFields = null;//contains set of all Vendor sobject fields 
	public static Set<String> setItemInvFields = null;//contains set of all Item Inventory sobject fields 
	public static Set<String> setPurchaseOrderFields = null;//contains set of all Purchase Order sobject fields 
	public static Set<String> setPurchaseLineItemFields = null;//contains set of all Purchase Order Line Item sobject fields 
	public static Set<String> setBillFields = null;//contains set of all Bill sobject fields 
	public static Set<String> setExpensesLineItemsFields = null;//contains set of all Bill Expenses sobject fields 
	public static Set<String> setBillLineItemsFields = null;//contains set of all Bill line items sobject fields 
	public static Set<String> setQBSynchFileds = null;//contains set of all QBSynchControl custom sobject fields 
	public static Set<String> setSFDCSyncFields = null;//contains set of all SFDCSyncControl custom sobject fields 
	public static Set<String> setAccountingFields = null;//contains set of all Accounting__c custom sobject fields 
	public SqlQueryBuilder sqlQueryBuilder = new SqlQueryBuilder();

	public Map<String,Object> returnedMap =  new HashMap<String,Object>(); //added by gopaal for dynamic query building
	List<String> fieldToBeUsed =null; //added by gopaal for dynamic query building
	String adminEmail="";
	
	/**
	 * This method is used to get authentication results
	 * 
	 * @param String strUserName
	 * @param String strPassword
	 * @return ArrayOfString
	 */	
	@WebMethod
	@WebResult(name = "authenticateResult", targetNamespace = "http://developer.intuit.com/")
	public com.intuit.developer.ArrayOfString authenticate(@WebParam(name = "strUserName", targetNamespace = "http://developer.intuit.com/") java.lang.String strUserName,
			@WebParam(name = "strPassword", targetNamespace = "http://developer.intuit.com/") java.lang.String strPassword) {
		log.info("#####I am at authenticate of service####");
		String[] asRtn = new String[2];
		asRtn[0] = "{F5FCCBC3-AA13-4d28-9DBF-3E571823F2BB}"; //myGUID.toString();
		asRtn[1] = "";
		SFDCService  sfdcService = null;
		com.intuit.developer.ArrayOfString asRtn2 = new com.intuit.developer.ArrayOfString(asRtn);
		try
		{
			sfdcService = new SFDCService();
			partnerConnection = sfdcService.authenticate("null",strUserName,strPassword);
			if(partnerConnection != null){
				// Connected to SF
				log.info("###Connection Success###");
			} else {
				log.info("###Connection Fail###");
				// Not Connected
				asRtn[1]="nvu";
				asRtn[0]=null;
			}
		} catch(ConnectionException ce) {
			log.info("connection Exception :"+ce);
			if(ce.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",ce.getMessage());
			}
		} catch(Exception e) {
			log.info("\n\nin authenticate call----Any other Exception :"+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return asRtn2;
	}

	/**
	 * This method is used to send requestXML querys to qbsf
	 * 
	 * @param String ticket
	 * @param String strHCPResponse
	 * @param String strCompanyFileName
	 * @param String qbXMLCountry
	 * @param String qbXMLMajorVers
	 * @param String qbXMLMinorVers
	 * @return String
	 */	
	@WebMethod
	@WebResult(name = "sendRequestXMLResult", targetNamespace = "http://developer.intuit.com/")
	public java.lang.String sendRequestXML(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket,
			@WebParam(name = "strHCPResponse", targetNamespace = "http://developer.intuit.com/") java.lang.String strHCPResponse,
			@WebParam(name = "strCompanyFileName", targetNamespace = "http://developer.intuit.com/") java.lang.String strCompanyFileName,
			@WebParam(name = "qbXMLCountry", targetNamespace = "http://developer.intuit.com/") java.lang.String qbXMLCountry,
			@WebParam(name = "qbXMLMajorVers", targetNamespace = "http://developer.intuit.com/") int qbXMLMajorVers,
			@WebParam(name = "qbXMLMinorVers", targetNamespace = "http://developer.intuit.com/") int qbXMLMinorVers) {

		String reqXML="";
		//get the last sync date from qbsyncControl object
		if(percentComplete == 0 || percentComplete == 100){
			percentComplete = 0;
			adminEmail=this.getAdminEmailFromSFDC();
			this.describeSfdcObjects();
			qbSynchControl = this.getQBSynchControl();						
		}
		//request Customer data from qbsf
		log.info("@@@ percentComplete in sendRequestXML ==>> "+percentComplete);
		if(percentComplete == 0 ){
			reqXML= XMLConstants.CUSTOMER_QUERYR_XML_PART_1 + qbSynchControl + XMLConstants.CUSTOMER_QUERYR_XML_PART_2;			
		}
		
		//request for vendor data from qbsf
		if(percentComplete == 5){
			reqXML= XMLConstants.VENDOR_QUERYR_XML_PART_1  + qbSynchControl + XMLConstants.VENDOR_QUERYR_XML_PART_2;
		}
		
		////request items data from qbsf
		if(percentComplete == 10 ){
			reqXML= XMLConstants.ITEM_QUERY_REQUEST_XML;
		}
		
		//request for item inventory from qbsf
		if(percentComplete == 15 ){
			reqXML= XMLConstants.ESTIMATE_QUERY_REQUEST_XML;
		}
		
		/*////request invoice data from qbsf
		if(percentComplete == 20 ){
			reqXML=XMLConstants.INVOICE_QUERY_REQUEST_XML_PART_1 + qbSynchControl + XMLConstants.INVOICE_QUERY_REQUEST_XML_PART_2;
		}
		
		////request salesorder data from qbsf
		if(percentComplete == 25 ){
			reqXML=XMLConstants.SALESORDER_QUERY_REQUEST_XML_PART_1  + qbSynchControl + XMLConstants.SALESORDER_QUERY_REQUEST_XML_PART_2;
		}
		
		////request estimates data from qbsf
		if(percentComplete == 30 ){
			reqXML= XMLConstants.ESTIMATE_QUERY_REQUEST_XML;
		}
		
		//request for purchase order from qbsf
		if(percentComplete == 35 ){
			reqXML= XMLConstants.PURCHASEORDER_QUERY_REQUEST_XML_PART_1  + qbSynchControl + XMLConstants.PURCHASEORDER_QUERY_REQUEST_XML_PART_2;
		}
		
		//request for Bill from qbsf
		if(percentComplete == 40 ){
			reqXML= XMLConstants.ACCOUNT_QUERY_REQUEST_XML;			
		}
		
		//request for Accounts from qbsf
		if(percentComplete == 45 ){
			reqXML= XMLConstants.BILL_QUERY_REQUEST_XML_PART_1  + qbSynchControl + XMLConstants.BILL_QUERY_REQUEST_XML_PART_2;
			
		}*/
		
		//insert Customers in qbsf with sfdc accounts data
		/*if(percentComplete == 25){
			reqXML = insertAccounts();
		}
		//update customers in qbsf with sfdc accounts data
		if(percentComplete == 30){
			reqXML = updateAccounts();
		}
		//insert items in qbsf with sfdc products data
		if(percentComplete == 35){
			reqXML = insertProducts();
		}
		//update items in qbsf with sfdc products data
		if(percentComplete == 40 ){
			reqXML = updateProducts();
		}
		//insert invoices in qbsf with sfdc invoices data
		if(percentComplete == 45){
			reqXML = this.insertInvoices();
		}
		//update invoices in qbsf with sfdc invoices
		if(percentComplete == 50){
			reqXML = this.updateInvoices();
		}
		//insert estimates in qbsf with sfdc opportunities data
		if(percentComplete == 55 ){
			reqXML = this.insertOpportunities();
		}
		//insert estimates in qbsf with sfdc opportunities data
		if(percentComplete == 60 ){
			reqXML = this.updateOpportunities();
		}*/
		return reqXML;
	}

	/**
	 * This method is used to get response from qbsf according to the request xml sent
	 * 
	 * @param String ticket
	 * @param String response
	 * @param String hresult
	 * @param String message
	 * @return int
	 */	
	@WebMethod
	@WebResult(name = "receiveResponseXMLResult", targetNamespace = "http://developer.intuit.com/")
	public int receiveResponseXML(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket,
			@WebParam(name = "response", targetNamespace = "http://developer.intuit.com/") java.lang.String response,
			@WebParam(name = "hresult", targetNamespace = "http://developer.intuit.com/") java.lang.String hresult,
			@WebParam(name = "message", targetNamespace = "http://developer.intuit.com/") java.lang.String message) {

		log.info("*****Start receiveResponseXML Method*****");
		log.info("In side receiveResponseXML method\nticket: "+ticket+"\nresponse: "+response+"\nhresult:"+hresult+"\nmessage: "+message+"\n\n Percentage complete: "+percentinct);
		log.info("@@@ percentComplete in receiveResponseXML ==>> "+percentComplete);
		switch(percentComplete){
		case 0 : 
			//create accounts in sfdc with customers data in qbsf
			log.info("@@@Switch Case 0 Entered @@@"); 
			//this.createCustomers(response);
			percentComplete = 5;
			break;
		case 5 : 
			//create vendors in sfdc with vendors data in qbsf
			log.info("@@@Switch Case 5 Entered @@@"+response);	
			//this.createVendors(response);
			percentComplete = 10;
			break;
		case 10 : 
			//create items in sfdc with invoices data in qbsf
			log.info("@@@Switch Case 10 Entered @@@"+response);
			//this.createItems(response);	
			percentComplete = 15;
			break;
		case 15 : 
			//item inventory
			log.info("@@@Switch Case 15 Entered @@@"+response);	
			//this.createItemInventorys(response);
			this.lastSynchUpdate();
			percentComplete = 100;
			break;
		/*case 20 : 
			//create invoices in sfdc with invoices data in qbsf
			log.info("@@@Switch Case 20 Entered @@@"+response);
			this.getInvoices(response);
			percentComplete = 25;
			break;
		case 25 : 
			//Sales Order
			log.info("@@@Switch Case 15 Entered @@@");
			//this.getSalesOrders(response);
			percentComplete = 30;
			break;
		case 30 : 
			//Estimates
			log.info("@@@Switch Case 15 Entered @@@");
			this.getEstimates(response);
			percentComplete = 35;
			break;
		case 35 : 
			//Purchase Order
			log.info("@@@Switch Case 15 Entered @@@");
			this.createPurchaseOrders(response);
			percentComplete = 40;
			break;
		case 40 : 
			//Bill
			log.info("@@@Switch Case 15 Entered @@@");
			this.createAccountings(response);
			percentComplete = 45;
			break;
		case 45 : 
			//Accounts
			log.info("@@@Switch Case 45 Entered @@@"+response);
			this.createBills(response);			
			//new MailService().sendMail("app.qb2sf@gmail.com","XML",response);
			log.info("qbAccSynchControl ==> "+qbAccSynchControl);
			log.info("qbInvoiceSynchControl ==> "+qbInvoiceSynchControl);
			log.info("qbSalesSynchControl ==> "+qbSalesSynchControl);
			log.info("qbItemSynchControl ==> "+qbItemSynchControl);
			log.info("qbEstimateSynchControl ==> "+qbEstimateSynchControl);
			this.lastSynchUpdate();
			percentComplete = 100;
			break;*/
		/*case 25 :
			//log.info("@@@Switch Case 25 Entered @@@");
			this.insertAccountsToSFDC(response);
			this.sfdcInsertSynchControl(sfdcInsertLastSyncControl);
			percentComplete = 30;
			break;
		case 30 :
			//log.info("@@@Switch Case 30 Entered @@@");
			this.updateAccountsToSFDC(response);
			this.sfdcUpdateSynchControl(sfdcUpdateLastSyncControl);
			percentComplete = 35;
			break;
		case 35 : 
			// Insert SFDC Products
			//log.info("@@@Switch Case 35 Entered @@@");
			this.insertProductsDetToSFDC(response);
			this.sfdcProductInsertSynchControl(sfdcInsertProductLastSyncControl);
			percentComplete = 40;
			break;
		case 40 : 
			// Update SFDC Products
			//log.info("@@@Switch Case 40 Entered @@@");    
			this.updateProductsDetToSFDC(response);
			this.sfdcProductUpdateSynchControl(sfdcUpdateProductLastSyncControl);
			percentComplete = 45;
			break;
		case 45 :
			//log.info("@@@Switch Case 45 Entered @@@");
			this.insertSFDCInvoices(response);
			this.sfdcInvoiceInsertSynchControl(sfdcInsertInvLastSyncControl);
			percentComplete = 50;
			break;
		case 50 :
			//log.info("@@@Switch Case 50 Entered @@@");
			this.updateSFDCInvoices(response);
			this.sfdcInvoiceUpdateSynchControl(sfdcUpdateInvLastSyncControl);
			this.sfdcInvoiceItemUpdateSynchControl(sfdcUpdateInvItemLastSyncControl);
			percentComplete = 55;
			break;
		case 55 : 
			//log.info("@@@Switch Case 55 Entered @@@");     
			this.insertSFDCOpportunities(response);
			this.sfdcOpportunityInsertSynchControl(sfdcInsertOppLastSyncControl);
			percentComplete = 60;
			break;
		case 60 : 
			//log.info("@@@Switch Case 60 Entered @@@");     
			this.updateSFDCOpportunities(response);
			this.sfdcOpportunityUpdateSynchControl(sfdcUpdateOppLastSyncControl);
			this.sfdcOpportunityItemUpdateSynchControl(sfdcUpdateOppItemLastSyncControl);
			percentComplete = 100;
			break;*/
		default : 
			//percentComplete = 100;
			break;
		}
		log.info("*****Compl receiveResponseXML Method*****");
		return percentComplete;
	}
	
	/**
	 * createAccountings method
	 * @param response
	 * @return String
	 * @Description This is to insert Accounts records from quickbooks to sfdc Accounting__c 
	 */
	public java.lang.String createAccountings(java.lang.String response){
		log.info("***************** QB Accountings SYNC START *****************");
		String isOk = "";//status of sfdc upsert
		boolean flag = false; //to check sql query builds correctly with Object name and fields
		Map<String,String> mapListIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		Map<String,SObject> mapParentIds = new HashMap<String,SObject>();// contains of map of listIds and SFDC_FLAG__C values
		Map<String,String> mapIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		Set<String> setListIds=new HashSet<String>();
		com.appshark.AccountingQuery.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.AccountingQuery.rs.AccountRetType> accountingRetTypeList = null;//CustomerRetType from qb

		try {  
			//getting the data from CustomerRetType jars
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.AccountingQuery.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.AccountingQuery.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.AccountingQuery.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getAccountQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getAccountQueryRs().getAccountRet() != null)){
				accountingRetTypeList = qbxml.getQBXMLMsgsRs().getAccountQueryRs().getAccountRet();
			}
		} catch (JAXBException e) {
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
			log.info("JAXBException ==>> " + e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception ==>> " + e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		try{
			if(accountingRetTypeList != null && accountingRetTypeList.size() > 0){
				log.info("partnerConnection ==>> "+partnerConnection);
				log.info("customerRetTypeList Size ==>> "+accountingRetTypeList.size());

				//Preparing Query for Account
				String accQuery = SFDCQueryConstants.ACCOUNTING_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountingConstantFields.LIST_ID__C);
				fieldToBeUsed.add(AccountingConstantFields.SFDC_FLAG__C);
				//dynamic query method that returns a map containing sqlQuery,flag,lastModifiedDate
				returnedMap = sqlQueryBuilder.buildQueryForAccounting(accQuery,setAccountingFields, SFDCObjectConstants.ACCOUNTING__C, accountingRetTypeList,fieldToBeUsed);

				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					accQuery = (String) returnedMap.get("sqlQuery");
					qbAcctingSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("accQuery ==>> "+accQuery);
					log.info("invflag ==>> "+flag);
					log.info("qbAcctingSynchControl ==>> "+qbAcctingSynchControl);
				}

				if(flag){
					//fields to prepare map 
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(AccountingConstantFields.LIST_ID__C);
					fieldToBeUsed.add(AccountingConstantFields.SFDC_FLAG__C);
					//method that returns map of listIds and SFDC_FLAG__C values
					mapListIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(accQuery), fieldToBeUsed);
				}				
			}

			//mapping of fields from quickbooks to salesforce
			if(accountingRetTypeList != null && accountingRetTypeList.size() > 0){
				List<SObject> lstAccounting = new ArrayList<SObject>();
				SObject[] objAcctLst = null;
				SObject objAcct;

				for(AccountRetType accountRetType : accountingRetTypeList){//customerRetTypeList iteration
					log.info("mapListIds.get(customerRetType.getListID().trim()" +(mapListIds.get(accountRetType.getListID().trim())));

					if((mapListIds != null ) 
							&& ((mapListIds.isEmpty() || (mapListIds.size()>0  && !mapListIds.containsKey(accountRetType.getListID().trim()))) || (mapListIds.size() > 0 && mapListIds.containsKey(accountRetType.getListID().trim()) && "false".equalsIgnoreCase(mapListIds.get(accountRetType.getListID().trim()))))){
						log.info("mapTxnIds ==>> "+mapListIds.get(accountRetType.getListID()));

						if(accountRetType != null && accountRetType.getListID() != null){
							setListIds.add(accountRetType.getListID());
							flag = true;
							objAcct = new SObject();
							//setting object type of sfdc account
							objAcct.setType(SFDCObjectConstants.ACCOUNTING__C);
							//QB_FLAG__C
							if(setAccountingFields.contains(AccountingConstantFields.QB_FLAG__C)){
								objAcct.setField(AccountConstantFields.QB_FLAG__C,true);
								log.info("QB_FLAG__C ==>> "+objAcct.getField(AccountingConstantFields.QB_FLAG__C));
							}

							//SFDC_Flag__c
							if(setAccountingFields.contains(AccountingConstantFields.SFDC_FLAG__C)){
								objAcct.setField(AccountConstantFields.SFDC_FLAG__C,false);
								log.info("SFDC_FLAG__C ==>> "+objAcct.getField(AccountingConstantFields.SFDC_FLAG__C));
							}

							//name
							if(accountRetType.getFullName() != null){
								log.info("NEW ACCOUNT FULL NAME ==>> "+accountRetType.getName());
								objAcct.setField(AccountingConstantFields.ACCOUNTING_NAME, accountRetType.getName());
							}

							//ACCOUNTING_TYPE__C
							if(setAccountingFields.contains(AccountingConstantFields.ACCOUNTING_TYPE__C)){
								if(accountRetType.getAccountType() != null && accountRetType.getAccountType() != ""){
									objAcct.setField(AccountingConstantFields.ACCOUNTING_TYPE__C, accountRetType.getAccountType());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.ACCOUNTING_TYPE__C});
								}
							}

							//BALANCE__C
							if(setAccountingFields.contains(AccountingConstantFields.BALANCE__C)){
								if(accountRetType.getBalance() != null && accountRetType.getBalance() != ""){
									objAcct.setField(AccountingConstantFields.BALANCE__C, accountRetType.getBalance());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.BALANCE__C});
								}
							}

							//CASHFLOWCLASSIFICATION__C
							if(setAccountingFields.contains(AccountingConstantFields.CASHFLOWCLASSIFICATION__C)){
								if(accountRetType.getCashFlowClassification() != null && accountRetType.getCashFlowClassification() != ""){
									objAcct.setField(AccountingConstantFields.CASHFLOWCLASSIFICATION__C, accountRetType.getCashFlowClassification());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.CASHFLOWCLASSIFICATION__C});
								}
							}

							//EDIT_SEQUENCE__C
							if(setAccountingFields.contains(AccountingConstantFields.EDIT_SEQUENCE__C)){
								if(accountRetType.getEditSequence() != null && accountRetType.getEditSequence() != ""){
									objAcct.setField(AccountingConstantFields.EDIT_SEQUENCE__C, accountRetType.getEditSequence());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.EDIT_SEQUENCE__C});
								}
							}

							/*//FULL_NAME__C
							if(setAccountingFields.contains(AccountingConstantFields.FULL_NAME__C)){
								if(accountRetType.getFullName() != null && accountRetType.getFullName() != ""){
									objAcct.setField(AccountingConstantFields.FULL_NAME__C, accountRetType.getFullName());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.FULL_NAME__C});
								}
							}*/

							//IS_ACTIVE__C
							if(setAccountingFields.contains(AccountingConstantFields.IS_ACTIVE__C)){
								if(accountRetType.getIsActive() != null && accountRetType.getIsActive() != ""){
									if("true".equalsIgnoreCase(accountRetType.getIsActive())){
										objAcct.setField(AccountingConstantFields.IS_ACTIVE__C, true);
									}else{
										objAcct.setField(AccountingConstantFields.IS_ACTIVE__C, false);
									}
								}
							}

							//LIST_ID__C
							if(setAccountingFields.contains(AccountingConstantFields.LIST_ID__C)){
								if(accountRetType.getListID() != null && accountRetType.getListID() != ""){
									objAcct.setField(AccountingConstantFields.LIST_ID__C, accountRetType.getListID());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.LIST_ID__C});
								}
							}

							/*//PARENT_FULL_NAME__C
							if(setAccountingFields.contains(AccountingConstantFields.PARENT_FULL_NAME__C)){
								if(accountRetType.getParentRef() != null && accountRetType.getParentRef().getFullName() != null && accountRetType.getParentRef().getFullName() != ""){
									objAcct.setField(AccountingConstantFields.PARENT_FULL_NAME__C, accountRetType.getParentRef().getFullName());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.PARENT_FULL_NAME__C});
								}
							}*/

							//PARENT_LIST_ID__C
							if(setAccountingFields.contains(AccountingConstantFields.PARENT_LIST_ID__C)){
								if(accountRetType.getParentRef() != null && accountRetType.getParentRef().getListID() != null && accountRetType.getParentRef().getListID() != ""){
									objAcct.setField(AccountingConstantFields.PARENT_LIST_ID__C, accountRetType.getParentRef().getListID());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.PARENT_LIST_ID__C});
								}
							}

							//SUB_LEVEL__C
							if(setAccountingFields.contains(AccountingConstantFields.SUB_LEVEL__C)){
								if(accountRetType.getSublevel() != null && accountRetType.getSublevel() != ""){
									objAcct.setField(AccountingConstantFields.SUB_LEVEL__C, accountRetType.getSublevel());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.SUB_LEVEL__C});
								}
							}

							//TIME_CREATED__C
							if(setAccountingFields.contains(AccountingConstantFields.TIME_CREATED__C)){
								if(accountRetType.getTimeCreated() != null && accountRetType.getTimeCreated() != ""){
									objAcct.setField(AccountingConstantFields.TIME_CREATED__C, accountRetType.getTimeCreated());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.TIME_CREATED__C});
								}
							}

							//TIME_CREATED__C
							if(setAccountingFields.contains(AccountingConstantFields.TIME_MODIFIED__C)){
								if(accountRetType.getTimeModified() != null && accountRetType.getTimeModified() != ""){
									objAcct.setField(AccountingConstantFields.TIME_MODIFIED__C, accountRetType.getTimeModified());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.TIME_MODIFIED__C});
								}
							}

							//TOATL_BALANCE__C
							if(setAccountingFields.contains(AccountingConstantFields.TOATL_BALANCE__C)){
								if(accountRetType.getTotalBalance() != null && accountRetType.getTotalBalance() != ""){
									objAcct.setField(AccountingConstantFields.TOATL_BALANCE__C, accountRetType.getTotalBalance());
								}else{
									objAcct.setFieldsToNull(new String[]{AccountingConstantFields.TOATL_BALANCE__C});
								}
							}

							lstAccounting.add(objAcct);
						}
					}
				}
				
				//assign list to an array
				if(lstAccounting != null && lstAccounting.size() > 0){
					objAcctLst = new SObject[lstAccounting.size()];
					lstAccounting.toArray(objAcctLst);
					log.info("objAccLst********"+objAcctLst.toString());
				}

				//logic to upsert 100 records at a time to overcome the salesforce batch limit
				if(objAcctLst!=null && objAcctLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objAcctLst,partnerConnection,AccountingConstantFields.LIST_ID__C,SFDCObjectConstants.ACCOUNTING__C);
				}

				String itemParentQuery="select id,name,List_ID__c from Accounting__c where List_ID__c IN (";
				StringBuilder filter=new StringBuilder();//to hold values
				int count1 = 1;
				List<SObject> lstItemInvparent = new ArrayList<SObject>();
				SObject[] objItemInvParentLst = null;
				SObject objItemInvenparent;
				for(AccountRetType accountRetType : accountingRetTypeList){//customerRetTypeList iteration
					if(accountRetType.getParentRef()!=null && accountRetType.getParentRef().getListID()!=null){
						//log.info("Parent Id"+itemInvenRetType.getParentRef().getListID());
						filter.append("\'"+accountRetType.getParentRef().getListID()+"\'");
						filter.append(" ,");
					}								
				}

				String filtered=filter.substring(0, filter.length()-1);
				itemParentQuery=itemParentQuery+filtered+")";
				log.info("parent query"+itemParentQuery);
				QueryResult qresult=partnerConnection.query(itemParentQuery);
				if(qresult!=null && qresult.getSize()>0){
					SObject[] records=qresult.getRecords();
					if(records!=null && records.length>0){
						for(SObject sobj:records){
							if(sobj!=null && sobj.getField(AccountingConstantFields.LIST_ID__C)!=null && sobj.getField(AccountingConstantFields.LIST_ID__C)!=""){
								log.info(" List id from map preparation---"+sobj.getField(AccountingConstantFields.LIST_ID__C));
								mapParentIds.put(sobj.getField(AccountingConstantFields.LIST_ID__C).toString(), sobj);
								//mapIds.put(sobj.getField(ItemInventoryConstantFields.LISTID__C).toString(), sobj.getField(ItemInventoryConstantFields.ID).toString());
							}								
						}
					}
				}

				String itemPQuery="select id,name,List_ID__c from Accounting__c where List_ID__c IN (";
				itemPQuery = itemPQuery+"";
				StringBuilder filter1=new StringBuilder();
				int count = 1;
				log.info("setListIds---->>"+ setListIds.size());
				//Preparing set of LISTID's
				if(setListIds != null && setListIds.size() > 0){
					for(String listId : setListIds){
						log.info("listId set---->>"+ listId);
						if(listId != null){
							log.info("listId setttttttt---->>"+ listId);
							filter1.append("\'"+listId+"\'");
							if(count != setListIds.size())
								filter1.append(" ,");
						}
						count++;
					}
				}

				itemPQuery = itemPQuery+filter1+")";
				log.info("purchaseOrdQuery ==>> "+itemPQuery);
				QueryResult queryresult=partnerConnection.query(itemPQuery);
				if(queryresult!=null && queryresult.getSize()>0){//query result null check
					SObject[] records=queryresult.getRecords();
					if(records!=null && records.length>0){
						for(SObject sobj:records){
							if(sobj!=null && sobj.getField(AccountingConstantFields.LIST_ID__C)!=null && sobj.getField(AccountingConstantFields.LIST_ID__C)!=""){
								log.info(" List id from map preparation---"+sobj.getField(AccountingConstantFields.LIST_ID__C));									
								mapIds.put(sobj.getField(AccountingConstantFields.LIST_ID__C).toString(), sobj.getField(AccountingConstantFields.ACCOUNTING_ID).toString());
							}								
						}
					}
				}

				for(AccountRetType accountRetType : accountingRetTypeList){//customerRetTypeList iteration
					if(!(accountRetType.getSublevel().equalsIgnoreCase("0"))){

						objItemInvenparent=new SObject();
						objItemInvenparent.setType(SFDCObjectConstants.ACCOUNTING__C);
						log.info("List Id--->"+accountRetType.getListID());
						log.info("Sub Level--->"+accountRetType.getSublevel().equalsIgnoreCase("0"));														
						//Parent Item
						log.info("PARENT_ITEM__C ==>> "+AccountingConstantFields.PARENT_ACCOUNTING__C);
						log.info("itemInvenRetType null ==>> "+mapIds.get(accountRetType.getListID()));

						if(mapIds!=null &&  mapIds.get(accountRetType.getListID())!=null){
							log.info("***********"+mapIds.get(accountRetType.getListID()));
							objItemInvenparent.setField(AccountingConstantFields.ACCOUNTING_ID, mapIds.get(accountRetType.getListID()));
							log.info("iD SFDC--->>"+objItemInvenparent.getField(AccountingConstantFields.ACCOUNTING_ID));								
						}

						if(setAccountingFields.contains(AccountingConstantFields.PARENT_ACCOUNTING__C)){
							log.info("null ==>> "+(accountRetType.getParentRef()!=null));
							if(accountRetType.getParentRef()!=null && mapParentIds!=null && mapParentIds.get(accountRetType.getParentRef().getListID())!=null){
								log.info("mapParentIds ==>> "+mapParentIds.get(accountRetType.getParentRef().getListID()));
								log.info("Parent List Name--->"+accountRetType.getParentRef().getFullName());
								log.info("Parent List id--->"+accountRetType.getParentRef().getListID());
								objItemInvenparent.setField(AccountingConstantFields.PARENT_ACCOUNTING__C, mapParentIds.get(accountRetType.getParentRef().getListID()).getId());
								log.info("parent item"+objItemInvenparent.getField(AccountingConstantFields.PARENT_ACCOUNTING__C));
							}
						}

						if(objItemInvenparent.getField(AccountingConstantFields.ACCOUNTING_ID) != null || mapIds.get(accountRetType.getListID())!=null){
							log.info("in if");
							lstItemInvparent.add(objItemInvenparent);
						}								
					}
				}

				if(lstItemInvparent != null && lstItemInvparent.size() > 0){//lstItemInvparent null check
					objItemInvParentLst = new SObject[lstItemInvparent.size()];
					lstItemInvparent.toArray(objItemInvParentLst);
					log.info("Parent list size********"+objItemInvParentLst);
				}

				if(objItemInvParentLst!=null && objItemInvParentLst.length>0){
					isOk = QueryExecuter.prepareAndExecuteUpdate(objItemInvParentLst,partnerConnection,SFDCObjectConstants.ACCOUNTING__C);
				}				
			}
		}
		
		catch(Exception e){
			log.info("exception from createAccountings"+e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		log.info("***************** QB Accountings SYNC END *****************"+new java.util.Date());
		return isOk;
	}

	/**
	 * createCustomers method
	 * @param response
	 * @return String
	 * @Description This is to insert customers records from quickbooks to sfdc accounts 
	 */
	public java.lang.String createCustomers(java.lang.String response){
		log.info("***************** QB CUSTOMERS SYNC START *****************");
		String isOk = "";//status of sfdc upsert
		boolean flag = false; //to check sql query builds correctly with Object name and fields
		Map<String,String> mapListIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		com.appshark.customutil.cls.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.customutil.cls.CustomerRetType> customerRetTypeList = null;//CustomerRetType from qb

		try {  
			//getting the data from CustomerRetType jars
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.customutil.cls.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.customutil.cls.QBXMLType> root = unmarshaller.unmarshal(source, QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getCustomerQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getCustomerQueryRs().getCustomerRet() != null)){
				customerRetTypeList = qbxml.getQBXMLMsgsRs().getCustomerQueryRs().getCustomerRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}

		try{
			if(customerRetTypeList != null && customerRetTypeList.size() > 0){
				log.info("partnerConnection ==>> "+partnerConnection);
				log.info("customerRetTypeList Size ==>> "+customerRetTypeList.size());

				//Preparing Query for Account
				String accQuery = SFDCQueryConstants.ACCOUNT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountConstantFields.QBLISTID__C);
				fieldToBeUsed.add(AccountConstantFields.SFDC_FLAG__C);
				//dynamic query method that returns a map containing sqlQuery,flag,lastModifiedDate
				returnedMap = sqlQueryBuilder.buildQueryForAccount(accQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, customerRetTypeList,fieldToBeUsed);

				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					accQuery = (String) returnedMap.get("sqlQuery");
					qbAccSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("accQuery ==>> "+accQuery);
					log.info("invflag ==>> "+flag);
					log.info("qbAccSynchControl ==>> "+qbAccSynchControl);
				}

				if(flag){
					//fields to prepare map 
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(AccountConstantFields.LISTID__C);
					fieldToBeUsed.add(AccountConstantFields.SFDC_FLAG__C);
					//method that returns map of listIds and SFDC_FLAG__C values
					mapListIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(accQuery), fieldToBeUsed);
				}				
			}

			//mapping of fields from quickbooks to salesforce
			if(customerRetTypeList != null && customerRetTypeList.size() > 0){
				List<SObject> lstCustomers = new ArrayList<SObject>();
				SObject[] objAccLst = null;
				SObject objAcc;
				
				for(CustomerRetType customerRetType : customerRetTypeList){//customerRetTypeList iteration
					log.info("mapListIds.get(customerRetType.getListID().trim()" +(mapListIds.get(customerRetType.getListID().trim())));
					
					if((mapListIds != null ) 
							&& ((mapListIds.isEmpty() || (mapListIds.size()>0  && !mapListIds.containsKey(customerRetType.getListID().trim()))) || (mapListIds.size() > 0 && mapListIds.containsKey(customerRetType.getListID().trim()) && "false".equalsIgnoreCase(mapListIds.get(customerRetType.getListID().trim()))))){
						log.info("mapTxnIds ==>> "+mapListIds.get(customerRetType.getListID()));
						if(customerRetType != null && customerRetType.getListID() != null){
							flag = true;
							objAcc = new SObject();
							//setting object type of sfdc account
							objAcc.setType(SFDCObjectConstants.ACCOUNT);
							//QB_FLAG__C
							if(setAccountFields.contains(AccountConstantFields.QB_FLAG__C)){
								objAcc.setField(AccountConstantFields.QB_FLAG__C,true);
								log.info("QB_FLAG__C ==>> "+objAcc.getField(AccountConstantFields.QB_FLAG__C));
							}
							
							//SFDC_Flag__c
							if(setAccountFields.contains(AccountConstantFields.SFDC_FLAG__C)){
								objAcc.setField(AccountConstantFields.SFDC_FLAG__C,false);
								log.info("SFDC_FLAG__C ==>> "+objAcc.getField(AccountConstantFields.SFDC_FLAG__C));
							}
							
							//name
							if(customerRetType.getFullName() != null){
								log.info("NEW ACCOUNT FULL NAME ==>> "+customerRetType.getName());
								objAcc.setField(AccountConstantFields.ACCOUNT_NAME, customerRetType.getName());
							}
							
							//List Id
							if(customerRetType.getListID() != null){
								if(setAccountFields.contains(AccountConstantFields.LISTID__C)){
									objAcc.setField(AccountConstantFields.LISTID__C, customerRetType.getListID().trim());
								}
								//QBListId__c
								if(setAccountFields.contains(AccountConstantFields.QBLISTID__C)){
									objAcc.setField(AccountConstantFields.QBLISTID__C, customerRetType.getListID());
								}
							}
							
							//Current_Balance__c
							if(setAccountFields.contains(AccountConstantFields.CURRENT_BALANCE__C)){
								if(customerRetType.getBalance() != null){
									objAcc.setField(AccountConstantFields.CURRENT_BALANCE__C, customerRetType.getTotalBalance());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.CURRENT_BALANCE__C});
								}
							}
							
							//fax
							if(customerRetType.getFax() != null){
								objAcc.setField(AccountConstantFields.FAX, customerRetType.getFax());
							}else{
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.FAX});
							}
							
							//phone
							if(customerRetType.getPhone() != null){
								objAcc.setField(AccountConstantFields.PHONE, customerRetType.getPhone());
							}else{
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.PHONE});
							}
							
							//Email__c
							if(setAccountFields.contains(AccountConstantFields.EMAIL__C)){
								if(customerRetType.getEmail() != null){
									objAcc.setField(AccountConstantFields.EMAIL__C, customerRetType.getEmail());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.EMAIL__C});
								}
							}
							
							//Job_Title__c
							if(setAccountFields.contains(AccountConstantFields.JOB_TITLE__C)){
								if(customerRetType.getJobTitle() != null){
									objAcc.setField(AccountConstantFields.JOB_TITLE__C, customerRetType.getJobTitle());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.JOB_TITLE__C});
								}
							}
							
							//Active__c
							if(customerRetType.getIsActive() != null){
								if(setAccountFields.contains(AccountConstantFields.ACTIVE__C)){
									String isActive = customerRetType.getIsActive().trim();
									if("true".equalsIgnoreCase(isActive)){
										objAcc.setField(AccountConstantFields.ACTIVE__C, "Yes");
									}else{
										objAcc.setField(AccountConstantFields.ACTIVE__C, "No");
									}
								}
							}
							
							//Time_Modified__c
							if(setAccountFields.contains(AccountConstantFields.TIME_MODIFIED__C)){
								if(customerRetType.getTimeModified() != null){
									objAcc.setField(AccountConstantFields.TIME_MODIFIED__C, customerRetType.getTimeModified());
								}
							}
							
							//billing address
							String billingStreet = "";
							if(customerRetType.getBillAddress() != null){
								if(customerRetType.getBillAddress().getAddr1() != null){
									billingStreet = customerRetType.getBillAddress().getAddr1();
								}
								
								if(customerRetType.getBillAddress().getAddr2() != null){
									billingStreet = billingStreet + customerRetType.getBillAddress().getAddr2();
								}
								
								if(customerRetType.getBillAddress().getAddr3() != null){
									billingStreet = billingStreet + customerRetType.getBillAddress().getAddr3();
								}
								
								if(customerRetType.getBillAddress().getAddr4() != null){
									billingStreet = billingStreet + customerRetType.getBillAddress().getAddr4();
								}
								
								if(customerRetType.getBillAddress().getAddr5() != null){
									billingStreet = billingStreet + customerRetType.getBillAddress().getAddr5();
								}
								
								//BILLINGSTREET
								if(billingStreet != null && billingStreet.length() > 0){
									objAcc.setField(AccountConstantFields.BILLINGSTREET, billingStreet);
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGSTREET});
								}
								
								//BILLINGCITY
								if(customerRetType.getBillAddress().getCity() != null){
									objAcc.setField(AccountConstantFields.BILLINGCITY, customerRetType.getBillAddress().getCity());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGCITY});
								}
								
								//BILLINGSTATE
								if(customerRetType.getBillAddress().getState() != null){
									objAcc.setField(AccountConstantFields.BILLINGSTATE, customerRetType.getBillAddress().getState());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGSTATE});
								}
								
								//BILLINGPOSTALCODE
								if(customerRetType.getBillAddress().getPostalCode() != null){
									objAcc.setField(AccountConstantFields.BILLINGPOSTALCODE, customerRetType.getBillAddress().getPostalCode());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGPOSTALCODE});
								}
								
								//BILLINGCOUNTRY
								if(customerRetType.getBillAddress().getCountry() != null){
									objAcc.setField(AccountConstantFields.BILLINGCOUNTRY, customerRetType.getBillAddress().getCountry());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGCOUNTRY});
								}
							}else{
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGSTREET});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGCITY});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGSTATE});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGPOSTALCODE});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.BILLINGCOUNTRY});
							}
							
							//shipping address
							String shippingStreet = "";
							if(customerRetType.getShipAddress() != null){
								if(customerRetType.getShipAddress().getAddr1() != null){
									shippingStreet = customerRetType.getShipAddress().getAddr1();
								}
								
								if(customerRetType.getShipAddress().getAddr2() != null){
									shippingStreet = shippingStreet + customerRetType.getShipAddress().getAddr2();
								}
								
								if(customerRetType.getShipAddress().getAddr3() != null){
									shippingStreet = shippingStreet + customerRetType.getShipAddress().getAddr3();
								}
								
								if(customerRetType.getShipAddress().getAddr4() != null){
									shippingStreet = shippingStreet + customerRetType.getShipAddress().getAddr4();
								}
								
								if(customerRetType.getShipAddress().getAddr5() != null){
									shippingStreet = shippingStreet + customerRetType.getShipAddress().getAddr5();
								}
								
								//SHIPPINGSTREET
								if(shippingStreet != null && shippingStreet.length() > 0){
									objAcc.setField(AccountConstantFields.SHIPPINGSTREET, shippingStreet);
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGSTREET});
								}
								
								//SHIPPINGCITY
								if(customerRetType.getShipAddress().getCity() != null){
									objAcc.setField(AccountConstantFields.SHIPPINGCITY, customerRetType.getShipAddress().getCity());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGCITY});
								}
								
								//SHIPPINGSTATE
								if(customerRetType.getShipAddress().getState() != null){
									objAcc.setField(AccountConstantFields.SHIPPINGSTATE, customerRetType.getShipAddress().getState());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGSTATE});
								}
								
								//SHIPPINGPOSTALCODE
								if(customerRetType.getShipAddress().getPostalCode() != null){
									objAcc.setField(AccountConstantFields.SHIPPINGPOSTALCODE, customerRetType.getShipAddress().getPostalCode());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGPOSTALCODE});
								}
								
								//SHIPPINGCOUNTRY
								if(customerRetType.getShipAddress().getCountry() != null){
									objAcc.setField(AccountConstantFields.SHIPPINGCOUNTRY, customerRetType.getShipAddress().getCountry());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGCOUNTRY});
								}
							}else{
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGSTREET});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGCITY});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGSTATE});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGPOSTALCODE});
								objAcc.setFieldsToNull(new String[]{AccountConstantFields.SHIPPINGCOUNTRY});
							}
							
							//Market_Segment__c
							if(setAccountFields.contains(AccountConstantFields.MARKET_SEGMENT__C)){
								if(customerRetType.getCustomerTypeRef() != null && customerRetType.getCustomerTypeRef().getFullName() != null){
									objAcc.setField(AccountConstantFields.MARKET_SEGMENT__C, customerRetType.getCustomerTypeRef().getFullName());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.MARKET_SEGMENT__C});
								}
							}
							
							//Sales_Rep__c
							if(setAccountFields.contains(AccountConstantFields.SALES_REP__C)){
								if(customerRetType.getItemSalesTaxRef() != null && customerRetType.getItemSalesTaxRef().getFullName() != null){
									objAcc.setField(AccountConstantFields.SALES_REP__C, customerRetType.getItemSalesTaxRef().getFullName());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.SALES_REP__C});
								}	
							}
							
							//Terms_Code__c
							if(setAccountFields.contains(AccountConstantFields.TERMS_CODE__C)){
								if(customerRetType.getTermsRef() != null && customerRetType.getTermsRef().getFullName() != null){
									objAcc.setField(AccountConstantFields.TERMS_CODE__C, customerRetType.getTermsRef().getFullName());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.TERMS_CODE__C});
								}
							}
							
							//EditSequence__c
							if(setAccountFields.contains(AccountConstantFields.EDITSEQUENCE__C)){
								if(customerRetType.getEditSequence() != null){
									objAcc.setField(AccountConstantFields.EDITSEQUENCE__C, customerRetType.getEditSequence());
								}
							}
							
							//COMPANY_NAME__C
							if(setAccountFields.contains(AccountConstantFields.COMPANY_NAME__C)){
								if(customerRetType.getCompanyName() != null){
									objAcc.setField(AccountConstantFields.COMPANY_NAME__C, customerRetType.getCompanyName());
								}else{
									objAcc.setFieldsToNull(new String[]{AccountConstantFields.COMPANY_NAME__C});
								}
							}
							lstCustomers.add(objAcc);
						}
					}
				}
				
				//assign list to an array
				if(lstCustomers != null && lstCustomers.size() > 0){
					objAccLst = new SObject[lstCustomers.size()];
					lstCustomers.toArray(objAccLst);
					log.info("objAccLst********"+objAccLst.toString());
				}
				
				//logic to upsert 100 records at a time to overcome the salesforce batch limit
				if(objAccLst!=null && objAccLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objAccLst,partnerConnection,AccountConstantFields.LISTID__C,SFDCObjectConstants.ACCOUNT);
				}																
			}
		} catch (ConnectionException e) {
			log.info("Exception while Inserting Customers :"+ e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		log.info("***************** QB CUSTOMERS SYNC END *****************");
		return isOk;
	}
	
	/**
	 * createVendors method
	 * @param response
	 * @return String
	 * @Description This is to insert vendrs records from quickbooks to sfdc vendors 
	 */
	public java.lang.String createVendors(java.lang.String response){
		log.info("***************** QB Vendors SYNC START *****************");
		String isOk = "";//status of sfdc upsert
		boolean flag = false; //to check sql query builds correctly with Object name and fields
		Map<String,String> mapListIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		com.appshark.vendorquery.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.vendorquery.rs.VendorRetType> vendorRetTypeList = null;//CustomerRetType from qb

		try {  
			//getting the data from CustomerRetType jars
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.vendorquery.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.vendorquery.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.vendorquery.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getVendorQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getVendorQueryRs().getVendorRet() != null)){
				vendorRetTypeList = qbxml.getQBXMLMsgsRs().getVendorQueryRs().getVendorRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException from createVendors Method ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception from createVendors Method==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		try{
			if(vendorRetTypeList != null && vendorRetTypeList.size() > 0){
				log.info("partnerConnection ==>> "+partnerConnection);
				log.info("vendorRetTypeList Size ==>> "+vendorRetTypeList.size());

				//Preparing Query for Account
				String vendorQuery = SFDCQueryConstants.VENDOR_QUERY;
				log.info("vendorQuery 111--->"+vendorQuery);
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(VendorConstantFields.LISTID__C);
				fieldToBeUsed.add(VendorConstantFields.SFDC_FLAG__C);
				//dynamic query method that returns a map containing sqlQuery,flag,lastModifiedDate
				returnedMap = sqlQueryBuilder.buildQueryForVendor(vendorQuery,setVendorFields, SFDCObjectConstants.VENDOR__C, vendorRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					vendorQuery = (String) returnedMap.get("sqlQuery");
					qbVenSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("vendorQuery ==>> "+vendorQuery);
					log.info("vendorflag ==>> "+flag);
					log.info("qbVenSynchControl ==>> "+qbVenSynchControl);
				}

				if(flag){
					//fields to prepare map 
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(VendorConstantFields.LISTID__C);
					fieldToBeUsed.add(VendorConstantFields.SFDC_FLAG__C);
					//method that returns map of listIds and SFDC_FLAG__C values
					mapListIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(vendorQuery), fieldToBeUsed);
				log.info("mapListIds"+mapListIds);
				}				
			}
			//mapping of fields from quickbooks to salesforce
			log.info("vendorRetTypeList"+vendorRetTypeList.size());
			if(vendorRetTypeList != null && vendorRetTypeList.size() > 0){
				List<SObject> lstVendors = new ArrayList<SObject>();
				SObject[] objVenLst = null;
				SObject objVendor;
				
				for(VendorRetType vendorRetType : vendorRetTypeList){//customerRetTypeList iteration
					log.info("mapListIds.get(vendorRetType.getListID().trim()" +(mapListIds.get(vendorRetType.getListID().trim())));
					
					if((mapListIds != null ) 
							&& ((mapListIds.isEmpty() || (mapListIds.size()>0  && !mapListIds.containsKey(vendorRetType.getListID().trim()))) || (mapListIds.size() > 0 && mapListIds.containsKey(vendorRetType.getListID().trim()) && "false".equalsIgnoreCase(mapListIds.get(vendorRetType.getListID().trim()))))){
						log.info("mapListIds ==>> "+mapListIds.get(vendorRetType.getListID()));
						
						if(vendorRetType != null && vendorRetType.getListID() != null){
							flag = true;
							objVendor = new SObject();
							//setting object type of sfdc account
							objVendor.setType(SFDCObjectConstants.VENDOR__C);
							//QB_FLAG__C
							if(setVendorFields.contains(VendorConstantFields.QB_FLAG__C)){
								objVendor.setField(VendorConstantFields.QB_FLAG__C,true);
								log.info("QB_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.QB_FLAG__C));
							}
							
							//SFDC_Flag__c
							if(setVendorFields.contains(VendorConstantFields.SFDC_FLAG__C)){
								objVendor.setField(VendorConstantFields.SFDC_FLAG__C,false);
								log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.SFDC_FLAG__C));
							}
							
							//name
							if(vendorRetType.getName() != null && vendorRetType.getName() != ""){
								log.info("NEW ACCOUNT FULL NAME ==>> "+vendorRetType.getName());
								objVendor.setField(VendorConstantFields.VENDOR_NAME, vendorRetType.getName());
							}
							
							//balance
							if(setVendorFields.contains(VendorConstantFields.BALANCE__C)){
								if(vendorRetType.getBalance() != null && vendorRetType.getBalance() != ""){
									objVendor.setField(VendorConstantFields.BALANCE__C, vendorRetType.getBalance());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.BALANCE__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.BALANCE__C});
								}	
							}
							
							//EDITSEQUENCE__C
							if(setVendorFields.contains(VendorConstantFields.EDITSEQUENCE__C)){
								if(vendorRetType.getEditSequence() != null && vendorRetType.getEditSequence() != ""){
									objVendor.setField(VendorConstantFields.EDITSEQUENCE__C, vendorRetType.getEditSequence());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.EDITSEQUENCE__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.EDITSEQUENCE__C});
								}	
							}
							
							//EMAIL__C
							if(setVendorFields.contains(VendorConstantFields.EMAIL__C)){
								if(vendorRetType.getEmail() != null && vendorRetType.getEmail() != ""){
									objVendor.setField(VendorConstantFields.EMAIL__C, vendorRetType.getEmail());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.EMAIL__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.EMAIL__C});
								}	
							}
							
							//FAX__C
							if(setVendorFields.contains(VendorConstantFields.FAX__C)){
								if(vendorRetType.getFax() != null && vendorRetType.getFax() != ""){
									objVendor.setField(VendorConstantFields.FAX__C, vendorRetType.getFax());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.FAX__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.FAX__C});
								}	
							}
							
							//FIRSTNAME__C
							if(setVendorFields.contains(VendorConstantFields.FIRSTNAME__C)){
								if(vendorRetType.getFirstName() != null && vendorRetType.getFirstName() != ""){
									objVendor.setField(VendorConstantFields.FIRSTNAME__C, vendorRetType.getFirstName());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.FIRSTNAME__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.FIRSTNAME__C});
								}	
							}
							
							//LASTNAME__C
							if(setVendorFields.contains(VendorConstantFields.LASTNAME__C)){
								if(vendorRetType.getLastName() != null && vendorRetType.getLastName() != ""){
									objVendor.setField(VendorConstantFields.LASTNAME__C, vendorRetType.getLastName());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.LASTNAME__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.LASTNAME__C});
								}	
							}
							
							//LISTID__C
							if(setVendorFields.contains(VendorConstantFields.LISTID__C)){
								if(vendorRetType.getListID() != null && vendorRetType.getListID() != ""){
									objVendor.setField(VendorConstantFields.LISTID__C, vendorRetType.getListID());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.LISTID__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.LISTID__C});
								}	
							}
							
							//PHONE__C
							if(setVendorFields.contains(VendorConstantFields.PHONE__C)){
								if(vendorRetType.getPhone() != null && vendorRetType.getPhone() != ""){
									objVendor.setField(VendorConstantFields.PHONE__C, vendorRetType.getPhone());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.PHONE__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.PHONE__C});
								}	
							}
							
							//SALUTATION__C
							if(setVendorFields.contains(VendorConstantFields.SALUTATION__C)){
								if(vendorRetType.getSalutation() != null && vendorRetType.getSalutation() != ""){
									objVendor.setField(VendorConstantFields.SALUTATION__C, vendorRetType.getSalutation());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.SALUTATION__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.SALUTATION__C});
								}	
							}
							
							//TIME_CREATED__C
							if(setVendorFields.contains(VendorConstantFields.TIME_CREATED__C)){
								if(vendorRetType.getTimeCreated() != null && vendorRetType.getTimeCreated() != ""){
									objVendor.setField(VendorConstantFields.TIME_CREATED__C, vendorRetType.getTimeCreated());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.TIME_CREATED__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.TIME_CREATED__C});
								}	
							}
							
							//TIME_MODIFIED__C
							if(setVendorFields.contains(VendorConstantFields.TIME_MODIFIED__C)){
								if(vendorRetType.getTimeModified() != null && vendorRetType.getTimeModified() != ""){
									objVendor.setField(VendorConstantFields.TIME_MODIFIED__C, vendorRetType.getTimeModified());
									log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.TIME_MODIFIED__C));
								}else{
									objVendor.setFieldsToNull(new String[]{VendorConstantFields.TIME_MODIFIED__C});
								}	
							}
							
							if(vendorRetType.getVendorAddress()!=null){
								//VENDOR_ADDRESS1__C
								if(setVendorFields.contains(VendorConstantFields.VENDOR_ADDRESS1__C)){
									if(vendorRetType.getVendorAddress().getAddr1() != null && vendorRetType.getVendorAddress().getAddr1() != ""){
										objVendor.setField(VendorConstantFields.VENDOR_ADDRESS1__C, vendorRetType.getVendorAddress().getAddr1());
										log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.VENDOR_ADDRESS1__C));
									}else{
										objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_ADDRESS1__C});
									}	
								}
								
								//VENDOR_ADDRESS2__C
								if(setVendorFields.contains(VendorConstantFields.VENDOR_ADDRESS2__C)){
									if(vendorRetType.getVendorAddress().getAddr2() != null && vendorRetType.getVendorAddress().getAddr2() != ""){
										objVendor.setField(VendorConstantFields.VENDOR_ADDRESS2__C, vendorRetType.getVendorAddress().getAddr2());
										log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.VENDOR_ADDRESS2__C));
									}else{
										objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_ADDRESS2__C});
									}	
								}
								
								//VENDOR_ADDRESS3__C
								if(setVendorFields.contains(VendorConstantFields.VENDOR_ADDRESS3__C)){
									if(vendorRetType.getVendorAddress().getAddr3() != null && vendorRetType.getVendorAddress().getAddr3() != ""){
										objVendor.setField(VendorConstantFields.VENDOR_ADDRESS3__C, vendorRetType.getVendorAddress().getAddr3());
										log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.VENDOR_ADDRESS3__C));
									}else{
										objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_ADDRESS3__C});
									}	
								}
								
								//VENDOR_CITY__C
								if(setVendorFields.contains(VendorConstantFields.VENDOR_CITY__C)){
									if(vendorRetType.getVendorAddress().getCity() != null && vendorRetType.getVendorAddress().getCity() != ""){
										objVendor.setField(VendorConstantFields.VENDOR_CITY__C, vendorRetType.getVendorAddress().getCity());
									}else{
										objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_CITY__C});
									}	
								}
								
								//VENDOR_STATE__C
								if(setVendorFields.contains(VendorConstantFields.VENDOR_STATE__C)){
									if(vendorRetType.getVendorAddress().getState() != null && vendorRetType.getVendorAddress().getState() != ""){
										objVendor.setField(VendorConstantFields.VENDOR_STATE__C, vendorRetType.getVendorAddress().getState());
										log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.VENDOR_STATE__C));
									}else{
										objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_STATE__C});
									}	
								}
								
								//VENDOR_POSTAL_CODE__C
								if(setVendorFields.contains(VendorConstantFields.VENDOR_POSTAL_CODE__C)){
									if(vendorRetType.getVendorAddress().getPostalCode() != null && vendorRetType.getVendorAddress().getPostalCode() != ""){
										objVendor.setField(VendorConstantFields.VENDOR_POSTAL_CODE__C, vendorRetType.getVendorAddress().getPostalCode());
										log.info("SFDC_FLAG__C ==>> "+objVendor.getField(VendorConstantFields.VENDOR_POSTAL_CODE__C));
									}else{
										objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_POSTAL_CODE__C});
									}	
								}
							}else{
								objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_ADDRESS1__C});
								objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_ADDRESS2__C});
								objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_ADDRESS3__C});
								objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_CITY__C});
								objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_STATE__C});
								objVendor.setFieldsToNull(new String[]{VendorConstantFields.VENDOR_POSTAL_CODE__C});
							}
							lstVendors.add(objVendor);
						}
					}
				}
				
				//assign list to an array
				if(lstVendors != null && lstVendors.size() > 0){
					log.info("lstVendors********"+lstVendors);
					objVenLst = new SObject[lstVendors.size()];
					lstVendors.toArray(objVenLst);
					log.info("objVenLst********"+objVenLst.toString());
				}
				
				//logic to upsert 100 records at a time to overcome the salesforce batch limit
				if(objVenLst!=null && objVenLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objVenLst,partnerConnection,VendorConstantFields.LISTID__C,SFDCObjectConstants.VENDOR__C);
				}				
			}
		}catch(Exception e){
			log.info("Exception from createVendors Method--" +e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		log.info("***************** QB VENDORS SYNC END *****************");
		return isOk;
	}
	/**
	 * createItemInventorys method
	 * @param response
	 * @return String
	 * @Description This is to insert ItemInventorys records from quickbooks to sfdc ItemInventorys 
	 */
	public java.lang.String createItemInventorys(java.lang.String response){
		log.info("***************** QB Item Inevntorys SYNC START *****************");
		String isOk = "";//status of sfdc upsert
		boolean flag = false; //to check sql query builds correctly with Object name and fields
		Map<String,String> mapListIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		Set<String> setListIds = new HashSet<String>();// contains of map of listIds and SFDC_FLAG__C values
		Map<String,SObject> mapParentIds = new HashMap<String,SObject>();// contains of map of listIds and SFDC_FLAG__C values
		Map<String,String> mapIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		
		com.appshark.itemInventoryquery.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.itemInventoryquery.rs.ItemInventoryRetType> itemInvenRetTypeList = null;//CustomerRetType from qb

		try {  
			//getting the data from CustomerRetType jars
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.itemInventoryquery.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.itemInventoryquery.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.itemInventoryquery.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getItemInventoryQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getItemInventoryQueryRs().getItemInventoryRet() != null)){
				itemInvenRetTypeList = qbxml.getQBXMLMsgsRs().getItemInventoryQueryRs().getItemInventoryRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException from createItemInventorys Method ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception from createItemInventorys Method==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		try{
			if(itemInvenRetTypeList != null && itemInvenRetTypeList.size() > 0){
				log.info("partnerConnection ==>> "+partnerConnection);
				log.info("itemInvenRetTypeList Size ==>> "+itemInvenRetTypeList.size());

				//Preparing Query for ITEM_INVENTORY_QUERY
				String itemInvQuery = SFDCQueryConstants.ITEM_INVENTORY_QUERY;
				log.info("itemInvQuery 111--->"+itemInvQuery);
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(ItemInventoryConstantFields.LISTID__C);
				fieldToBeUsed.add(ItemInventoryConstantFields.SFDC_FLAG__C);
				//dynamic query method that returns a map containing sqlQuery,flag,lastModifiedDate
				returnedMap = sqlQueryBuilder.buildQueryForItemInventory(itemInvQuery,setItemInvFields, SFDCObjectConstants.ITEM_INVENTORY__C, itemInvenRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					itemInvQuery = (String) returnedMap.get("sqlQuery");
					qbItemInvSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("itemInvQuery ==>> "+itemInvQuery);
					log.info("ItemInvflag ==>> "+flag);
					log.info("qbItemInvSynchControl ==>> "+qbItemInvSynchControl);
				}

				if(flag){
					//fields to prepare map 
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(ItemInventoryConstantFields.LISTID__C);
					fieldToBeUsed.add(ItemInventoryConstantFields.SFDC_FLAG__C);
					//method that returns map of listIds and SFDC_FLAG__C values
					mapListIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(itemInvQuery), fieldToBeUsed);
				log.info("mapListIds"+mapListIds);
				}						
			}
			if(itemInvenRetTypeList != null && itemInvenRetTypeList.size() > 0){
				List<SObject> lstItemInventorys = new ArrayList<SObject>();
				SObject[] objItemInvLst = null;
				SObject objItemInven;
				
				for(ItemInventoryRetType itemInvenRetType : itemInvenRetTypeList){//customerRetTypeList iteration
					log.info("mapListIds.get(vendorRetType.getListID().trim()" +(mapListIds.get(itemInvenRetType.getListID().trim())));
					
					if((mapListIds != null ) 
							&& ((mapListIds.isEmpty() || (mapListIds.size()>0  && !mapListIds.containsKey(itemInvenRetType.getListID().trim()))) || (mapListIds.size() > 0 && mapListIds.containsKey(itemInvenRetType.getListID().trim()) && "false".equalsIgnoreCase(mapListIds.get(itemInvenRetType.getListID().trim()))))){
						log.info("mapListIds ==>> "+mapListIds.get(itemInvenRetType.getListID()));
						if(itemInvenRetType != null && itemInvenRetType.getListID() != null){
							setListIds.add(itemInvenRetType.getListID());
							flag = true;
							objItemInven = new SObject();
							//setting object type of sfdc Item_Inventory__c
							objItemInven.setType(SFDCObjectConstants.ITEM_INVENTORY__C);
							log.info("Sub Level--->"+itemInvenRetType.getSublevel().equalsIgnoreCase("0"));
							log.info("List Id--->"+itemInvenRetType.getListID());
								log.info("Sub Level--->"+itemInvenRetType.getSublevel().equalsIgnoreCase("0"));
								//QB_FLAG__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.QB_FLAG__C)){
									objItemInven.setField(ItemInventoryConstantFields.QB_FLAG__C,true);
									log.info("QB_FLAG__C ==>> "+objItemInven.getField(ItemInventoryConstantFields.QB_FLAG__C));
								}
								
								//SFDC_Flag__c
								if(setItemInvFields.contains(ItemInventoryConstantFields.SFDC_FLAG__C)){
									objItemInven.setField(ItemInventoryConstantFields.SFDC_FLAG__C,false);
									log.info("SFDC_FLAG__C ==>> "+objItemInven.getField(ItemInventoryConstantFields.SFDC_FLAG__C));
								}
								
								//name
								if(itemInvenRetType.getName() != null && itemInvenRetType.getName() != ""){
									log.info("NEW ACCOUNT FULL NAME ==>> "+itemInvenRetType.getName());
									objItemInven.setField(ItemInventoryConstantFields.ITEM_INVENTORY_NAME, itemInvenRetType.getName());
								}
								
								//AVERAGE_COST__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.AVERAGE_COST__C)){
									if(itemInvenRetType.getAverageCost() != null && itemInvenRetType.getAverageCost() != ""){
										objItemInven.setField(ItemInventoryConstantFields.AVERAGE_COST__C, itemInvenRetType.getAverageCost());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.AVERAGE_COST__C});
									}	
								}
								
								//EDIT_SEQUENCE__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.EDIT_SEQUENCE__C)){
									if(itemInvenRetType.getEditSequence() != null && itemInvenRetType.getEditSequence() != ""){
										objItemInven.setField(ItemInventoryConstantFields.EDIT_SEQUENCE__C, itemInvenRetType.getEditSequence());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.EDIT_SEQUENCE__C});
									}	
								}
								
								//ISACTIVE__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.ISACTIVE__C)){
									if(itemInvenRetType.getIsActive() != null && itemInvenRetType.getIsActive() != ""){
										objItemInven.setField(ItemInventoryConstantFields.ISACTIVE__C, itemInvenRetType.getIsActive());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.ISACTIVE__C});
									}	
								}
								
								//LISTID__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.LISTID__C)){
									if(itemInvenRetType.getListID() != null && itemInvenRetType.getListID() != ""){
										objItemInven.setField(ItemInventoryConstantFields.LISTID__C, itemInvenRetType.getListID());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.LISTID__C});
									}	
								}
								
								//QUANTITY_ON_HAND__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.QUANTITY_ON_HAND__C)){
									if(itemInvenRetType.getQuantityOnHand() != null && itemInvenRetType.getQuantityOnHand() != ""){
										objItemInven.setField(ItemInventoryConstantFields.QUANTITY_ON_HAND__C, itemInvenRetType.getQuantityOnHand());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.QUANTITY_ON_HAND__C});
									}	
								}
								
								//SUB_LEVEL__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.SUB_LEVEL__C)){
									if(itemInvenRetType.getSublevel() != null && itemInvenRetType.getSublevel() != ""){
										objItemInven.setField(ItemInventoryConstantFields.SUB_LEVEL__C, itemInvenRetType.getSublevel());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.SUB_LEVEL__C});
									}	
								}
								
								//QUANTITY_ON_ORDER__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.QUANTITY_ON_ORDER__C)){
									if(itemInvenRetType.getQuantityOnOrder() != null && itemInvenRetType.getQuantityOnOrder() != ""){
										objItemInven.setField(ItemInventoryConstantFields.QUANTITY_ON_ORDER__C, itemInvenRetType.getQuantityOnOrder());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.QUANTITY_ON_ORDER__C});
									}	
								}
								
								//QUANTITY_ON_SALESORDER__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.QUANTITY_ON_SALESORDER__C)){
									if(itemInvenRetType.getQuantityOnSalesOrder() != null && itemInvenRetType.getQuantityOnSalesOrder() != ""){
										objItemInven.setField(ItemInventoryConstantFields.QUANTITY_ON_SALESORDER__C, itemInvenRetType.getQuantityOnSalesOrder());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.QUANTITY_ON_SALESORDER__C});
									}	
								}
								
								//REORDER_POINT__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.REORDER_POINT__C)){
									if(itemInvenRetType.getReorderPoint() != null && itemInvenRetType.getReorderPoint() != ""){
										objItemInven.setField(ItemInventoryConstantFields.REORDER_POINT__C, itemInvenRetType.getReorderPoint());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.REORDER_POINT__C});
									}	
								}
								
								//TIME_CREATED__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.TIME_CREATED__C)){
									if(itemInvenRetType.getTimeCreated() != null && itemInvenRetType.getTimeCreated() != ""){
										objItemInven.setField(ItemInventoryConstantFields.TIME_CREATED__C, itemInvenRetType.getTimeCreated());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.TIME_CREATED__C});
									}	
								}
								
								//TIME_MODIFIED__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.TIME_MODIFIED__C)){
									if(itemInvenRetType.getTimeModified() != null && itemInvenRetType.getTimeModified() != ""){
										objItemInven.setField(ItemInventoryConstantFields.TIME_MODIFIED__C, itemInvenRetType.getTimeModified());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.TIME_MODIFIED__C});
									}	
								}
								
								//PURCHASE_COST__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.PURCHASE_COST__C)){
									if(itemInvenRetType.getPurchaseCost() != null && itemInvenRetType.getPurchaseCost() != ""){
										objItemInven.setField(ItemInventoryConstantFields.PURCHASE_COST__C, itemInvenRetType.getPurchaseCost());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.PURCHASE_COST__C});
									}	
								}
								
								//PURCHASE_DESCRIPTION__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.PURCHASE_DESCRIPTION__C)){
									if(itemInvenRetType.getPurchaseDesc() != null && itemInvenRetType.getPurchaseDesc() != ""){
										objItemInven.setField(ItemInventoryConstantFields.PURCHASE_DESCRIPTION__C, itemInvenRetType.getPurchaseDesc());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.PURCHASE_DESCRIPTION__C});
									}	
								}
								
								//SALES_PRICE__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.SALES_PRICE__C)){
									if(itemInvenRetType.getSalesPrice() != null && itemInvenRetType.getSalesPrice() != ""){
										objItemInven.setField(ItemInventoryConstantFields.SALES_PRICE__C, itemInvenRetType.getSalesPrice());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.SALES_PRICE__C});
									}	
								}
								
								//SALES_DESCRIPTION__C
								if(setItemInvFields.contains(ItemInventoryConstantFields.SALES_DESCRIPTION__C)){
									if(itemInvenRetType.getSalesDesc() != null && itemInvenRetType.getSalesDesc() != ""){
										objItemInven.setField(ItemInventoryConstantFields.SALES_DESCRIPTION__C, itemInvenRetType.getSalesDesc());
									}else{
										objItemInven.setFieldsToNull(new String[]{ItemInventoryConstantFields.SALES_DESCRIPTION__C});
									}	
								}
								
								lstItemInventorys.add(objItemInven);
							}
					   }
					}
				
				//assign list to an array
				if(lstItemInventorys != null && lstItemInventorys.size() > 0){
					objItemInvLst = new SObject[lstItemInventorys.size()];
					lstItemInventorys.toArray(objItemInvLst);
					log.info("objVenLst********"+objItemInvLst.toString());
				}
				
				//logic to upsert 100 records at a time to overcome the salesforce batch limit
				if(objItemInvLst!=null && objItemInvLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objItemInvLst,partnerConnection,ItemInventoryConstantFields.LISTID__C,SFDCObjectConstants.ITEM_INVENTORY__C);
				}
				
				String itemParentQuery="select id,name,ListID__c from Item_Inventory__c where ListID__c IN (";
				StringBuilder filter = new StringBuilder();//to hold values
				int count1 = 1;
					List<SObject> lstItemInvparent = new ArrayList<SObject>();
					SObject[] objItemInvParentLst = null;
					SObject objItemInvenparent;
					for(ItemInventoryRetType itemInvenRetType : itemInvenRetTypeList){
						if(itemInvenRetType.getParentRef()!=null && itemInvenRetType.getParentRef().getListID()!=null){
							//log.info("Parent Id"+itemInvenRetType.getParentRef().getListID());
							  filter.append("\'"+itemInvenRetType.getParentRef().getListID()+"\'");
							 filter.append(" ,");
							}								
						}
					String filtered=filter.substring(0, filter.length()-1);
					itemParentQuery=itemParentQuery+filtered+")";
					log.info("parent query"+itemParentQuery);
					QueryResult qresult=partnerConnection.query(itemParentQuery);
					if(qresult!=null && qresult.getSize()>0){
						SObject[] records=qresult.getRecords();
						if(records!=null && records.length>0){
							for(SObject sobj:records){
								if(sobj!=null && sobj.getField(ItemInventoryConstantFields.LISTID__C)!=null && sobj.getField(ItemInventoryConstantFields.LISTID__C)!=""){
									log.info(" List id from map preparation---"+sobj.getField(ItemInventoryConstantFields.LISTID__C));
									mapParentIds.put(sobj.getField(ItemInventoryConstantFields.LISTID__C).toString(), sobj);
									//mapIds.put(sobj.getField(ItemInventoryConstantFields.LISTID__C).toString(), sobj.getField(ItemInventoryConstantFields.ID).toString());
								}								
							}
						}
					}
					
					String itemPQuery="select id,name,ListID__c from Item_Inventory__c where ListID__c IN (";
					itemPQuery = itemPQuery+"";
					StringBuilder filter1 = new StringBuilder();
					int count = 1;
						log.info("setListIds---->>"+ setListIds.size());
					//Preparing set of LISTID's
					if(setListIds != null && setListIds.size() > 0){
						for(String listId : setListIds){
							log.info("listId set---->>"+ listId);
							if(listId != null){
								log.info("listId setttttttt---->>"+ listId);
								 filter1.append("\'"+listId+"\'");
								if(count != setListIds.size())
									filter1.append(" ,");
							}
							count++;
						}
					}
					
					itemPQuery = itemPQuery+filter1+")";
					log.info("purchaseOrdQuery ==>> "+itemPQuery);
					QueryResult queryresult=partnerConnection.query(itemPQuery);
					if(queryresult!=null && queryresult.getSize()>0){
						log.info(" queryresult.getSize()---"+queryresult.getSize());
						SObject[] records=queryresult.getRecords();
						if(records!=null && records.length>0){
							log.info(" records---"+records.length);
							for(SObject sobj:records){								
								if(sobj!=null && sobj.getField(ItemInventoryConstantFields.LISTID__C)!=null && sobj.getField(ItemInventoryConstantFields.LISTID__C)!=""){
									log.info(" List id from map preparation---"+sobj.getField(ItemInventoryConstantFields.LISTID__C));									
									mapIds.put(sobj.getField(ItemInventoryConstantFields.LISTID__C).toString(), sobj.getField(ItemInventoryConstantFields.ID).toString());
								}								
							}
						}
					}
					
					for(ItemInventoryRetType itemInvenRetType : itemInvenRetTypeList){
						if(!(itemInvenRetType.getSublevel().equalsIgnoreCase("0"))){
							objItemInvenparent=new SObject();
							objItemInvenparent.setType(SFDCObjectConstants.ITEM_INVENTORY__C);
							log.info("List Id--->"+itemInvenRetType.getListID());
							log.info("Sub Level--->"+itemInvenRetType.getSublevel().equalsIgnoreCase("0"));														
							//Parent Item
							log.info("mapIds.size() ==>> "+mapIds.size());
							log.info("itemInvenRetType null ==>> "+mapIds.get(itemInvenRetType.getListID()));
							log.info("Name ==>> "+itemInvenRetType.getName());
							if(mapIds!=null &&  mapIds.get(itemInvenRetType.getListID())!=null){
								log.info("***********"+mapIds.get(itemInvenRetType.getListID()));
								objItemInvenparent.setField(ItemInventoryConstantFields.ID, mapIds.get(itemInvenRetType.getListID()));
								log.info("iD SFDC--->>"+objItemInvenparent.getField(ItemInventoryConstantFields.ID));								
							}

							if(setItemInvFields.contains(ItemInventoryConstantFields.PARENT_ITEM__C)){
								log.info("null ==>> "+(itemInvenRetType.getParentRef()!=null));
								if(itemInvenRetType.getParentRef()!=null && mapParentIds!=null && mapParentIds.get(itemInvenRetType.getParentRef().getListID())!=null){
									log.info("mapParentIds ==>> "+mapParentIds.get(itemInvenRetType.getParentRef().getListID()));
									log.info("Parent List Name--->"+itemInvenRetType.getParentRef().getFullName());
									log.info("Parent List id--->"+itemInvenRetType.getParentRef().getListID());
									objItemInvenparent.setField(ItemInventoryConstantFields.PARENT_ITEM__C, mapParentIds.get(itemInvenRetType.getParentRef().getListID()).getId());
									log.info("parent item"+objItemInvenparent.getField(ItemInventoryConstantFields.PARENT_ITEM__C));
								}
							}

							if(objItemInvenparent.getField(ItemInventoryConstantFields.ID) != null || mapIds.get(itemInvenRetType.getListID())!=null){
								log.info("in if");
								lstItemInvparent.add(objItemInvenparent);
							}
						}
					}
					
					if(lstItemInvparent != null && lstItemInvparent.size() > 0){
						objItemInvParentLst = new SObject[lstItemInvparent.size()];
						lstItemInvparent.toArray(objItemInvParentLst);
						log.info("Parent list size********"+objItemInvParentLst);
					}
					
					if(objItemInvParentLst!=null && objItemInvParentLst.length>0){
						isOk = QueryExecuter.prepareAndExecuteUpdate(objItemInvParentLst,partnerConnection,SFDCObjectConstants.ITEM_INVENTORY__C);
					}
				}		
		}catch(Exception e){
			log.info("exception from createItemInventorys Method"+e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		log.info("***************** QB Item Inevntorys SYNC END *****************");
		return isOk;		
	}
	/**
	 * createPurchaseOrders method
	 * @param response
	 * @return String
	 * @Description This is to insert purchase orders records from quickbooks to sfdc Purchase_Orders__c 
	 */
	public java.lang.String createPurchaseOrders(java.lang.String response){
		log.info("***************** QB PurchaseOrders SYNC START *****************");
		String isOk = "";//status of sfdc upsert
		boolean flag = false; //to check sql query builds correctly with Object name and fields
		boolean productFlag=false;
		Map<String,String> mapTxnIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		Map<String,SObject> mapAccListIds = new HashMap<String, SObject>();//maps purchase order line item to its Account
		Map<String,SObject> mapProducts = new HashMap<String, SObject>();//map of products
		Map<String,SObject> mapVendorIds = new HashMap<String, SObject>();//to map vendor as look up
		Map<String,SObject> mapPurchases = new HashMap<String, SObject>();//map of purchase Orders
		Set<String> setPurchaseTxnIds=new HashSet<String>();
		com.appshark.purchaseorderquery.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.purchaseorderquery.rs.PurchaseOrderRetType> purchaseOrderRetTypeList = null;//CustomerRetType from qb

		try {  
			//getting the data from CustomerRetType jars
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.purchaseorderquery.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.purchaseorderquery.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.purchaseorderquery.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getPurchaseOrderQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getPurchaseOrderQueryRs().getPurchaseOrderRet() != null)){
				purchaseOrderRetTypeList = qbxml.getQBXMLMsgsRs().getPurchaseOrderQueryRs().getPurchaseOrderRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException from createPurchaseOrders Method ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception from createPurchaseOrders Method==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		try{
			if(purchaseOrderRetTypeList != null && purchaseOrderRetTypeList.size() > 0){ //purchaseOrderRetTypeList null check
				log.info("partnerConnection ==>> "+partnerConnection);
				log.info("itemInvenRetTypeList Size ==>> "+purchaseOrderRetTypeList.size());

				//Preparing Query for Purchase Order
				String purchaseQuery = SFDCQueryConstants.PURCHASE_ORDER_QUERY;
				log.info("purchaseQuery 111--->"+purchaseQuery);
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(PurchaseOrderConstantFields.TXN_ID__C);
				fieldToBeUsed.add(PurchaseOrderConstantFields.SFDC_FLAG__C);
				//dynamic query method that returns a map containing sqlQuery,flag,lastModifiedDate
				Map<String,Object> returnedMap = sqlQueryBuilder.buildQueryForPurchaseOrder(purchaseQuery,setPurchaseOrderFields, SFDCObjectConstants.PURCHASE_ORDER__C, purchaseOrderRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					purchaseQuery = (String) returnedMap.get("sqlQuery");
					qbPurchaseOrderSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("purchaseQuery ==>> "+purchaseQuery);
					log.info("Purchaseflag ==>> "+flag);
					log.info("qbPurchaseOrderSynchControl ==>> "+qbPurchaseOrderSynchControl);
				}

				if(flag){
					//fields to prepare map 
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(PurchaseOrderConstantFields.TXN_ID__C);
					fieldToBeUsed.add(PurchaseOrderConstantFields.SFDC_FLAG__C);
					//method that returns map of listIds and SFDC_FLAG__C values
					mapTxnIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(purchaseQuery), fieldToBeUsed);
					log.info("mapListIds"+mapTxnIds);
				}	

				//Preparing Query for vendor__c
				String venQuery = SFDCQueryConstants.VENDOR_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(VendorConstantFields.SFDC_FLAG__C);
				fieldToBeUsed.add(VendorConstantFields.LISTID__C);
				returnedMap = sqlQueryBuilder.buildQueryForPurchaseVendor(venQuery,setVendorFields, SFDCObjectConstants.VENDOR__C, purchaseOrderRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					venQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+venQuery);
					log.info("invflag ==>> "+flag);
				}

				if(flag){
					//Retrieving Account objects from Salesforce
					mapVendorIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(venQuery), VendorConstantFields.LISTID__C);
				}
				log.info("mapVendorIds ==>> "+mapVendorIds.size());
				//Preparing Query for Product2
				String productQuery = "SELECT Id, Name";
				if(setProduct2Fields.contains("ListID__c")){
					productQuery = productQuery+", ListID__c FROM Product2";
					productFlag = true;
				}

				if(productFlag){
					QueryResult qResult = partnerConnection.query(productQuery);
					if (qResult != null && qResult.getSize() > 0) {
						SObject[] records = qResult.getRecords();
						if(records!=null && records.length>0){
							log.info("SFDC PRODUCT RECORDS LENGTH ==>> "+records.length);
							for(SObject sObj : records){
								if(sObj != null && sObj.getField("ListID__c") != null){//ListID__c null check
									//log.info("PRODUCT2 SOBJECT ID FROM QUERY ==>> "+sObj.getId()+" <==> "+sObj.getField("ListID__c"));
									mapProducts.put(sObj.getField("ListID__c").toString(), sObj);
								}
							}	
						}

					}
				}

				//Preparing Query for Account
				String accQuery = SFDCQueryConstants.PARENT_ACCOUNT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountConstantFields.QBLISTID__C);
				fieldToBeUsed.add(AccountConstantFields.LISTID__C);
				log.info("fieldToBeUsed");
				Map<String,Object> returnedMapAcc = sqlQueryBuilder.buildQueryForPurchaseItemAccount(accQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, purchaseOrderRetTypeList,fieldToBeUsed);
				log.info("returnedMapAcc"+returnedMapAcc);
				if(returnedMapAcc != null && returnedMapAcc.size() > 0){
					flag = (boolean) returnedMapAcc.get("flag");
					accQuery = (String) returnedMapAcc.get("sqlQuery");
					log.info("accountQuery ==>> "+accQuery);
					log.info("invflag ==>> "+flag);

					log.info("accQuery ==>> "+accQuery);
					if(flag){
						//Retrieving Account objects from Salesforce
						mapAccListIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(accQuery), AccountConstantFields.LISTID__C);	
						log.info("mapAccListIds"+mapAccListIds.size());	
					}				
				}
			}
			
			//mapping of fields from quickbooks to salesforce
			if(purchaseOrderRetTypeList != null && purchaseOrderRetTypeList.size() > 0){
				List<SObject> lstPurchaseOrder = new ArrayList<SObject>();
				List<SObject> lstPurchaseItem = new ArrayList<SObject>();
				SObject[] objpurchaseLst = null;
				SObject[] objpurchaseItemLst = null;
				SObject objPurchaseOrder;
				SObject objPurchaseItem;
				//iteration of purchase orders from qb
				for(PurchaseOrderRetType purchaseOrderRetType : purchaseOrderRetTypeList){//customerRetTypeList iteration
					log.info("mapTxnIds.get(vendorRetType.getListID().trim()" +(mapTxnIds.get(purchaseOrderRetType.getTxnID().trim())));
					
					if((mapTxnIds != null ) 
							&& ((mapTxnIds.isEmpty() || (mapTxnIds.size()>0  && !mapTxnIds.containsKey(purchaseOrderRetType.getTxnID().trim()))) || (mapTxnIds.size() > 0 && mapTxnIds.containsKey(purchaseOrderRetType.getTxnID().trim()) && "false".equalsIgnoreCase(mapTxnIds.get(purchaseOrderRetType.getTxnID().trim()))))){
						log.info("mapListIds ==>> "+mapTxnIds.get(purchaseOrderRetType.getTxnID()));
						
						if(purchaseOrderRetType != null && purchaseOrderRetType.getTxnID() != null){//TxnID() null check
							flag = true;
							objPurchaseOrder = new SObject();
							//setting object type of sfdc account
							objPurchaseOrder.setType(SFDCObjectConstants.PURCHASE_ORDER__C);//setting type of sobject to PURCHASE_ORDER__C
							
							//QB_FLAG__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.QB_FLAG__C)){
								objPurchaseOrder.setField(PurchaseOrderConstantFields.QB_FLAG__C,true);
								log.info("QB_FLAG__C ==>> "+objPurchaseOrder.getField(PurchaseOrderConstantFields.QB_FLAG__C));
							}
							
							//SFDC_Flag__c
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.SFDC_FLAG__C)){
								objPurchaseOrder.setField(PurchaseOrderConstantFields.SFDC_FLAG__C,false);
								log.info("SFDC_FLAG__C ==>> "+objPurchaseOrder.getField(PurchaseOrderConstantFields.SFDC_FLAG__C));
							}
							
							//name
							if(purchaseOrderRetType.getRefNumber()!=null && purchaseOrderRetType.getRefNumber()!=""){
								objPurchaseOrder.setField(PurchaseOrderConstantFields.PURCHASE_ORDER_NAME, "isQB");	
							}
							
							//DUE_DATE__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.DUE_DATE__C)){
								if(purchaseOrderRetType.getDueDate() != null && purchaseOrderRetType.getDueDate() != ""){
									String strDueDate =purchaseOrderRetType.getDueDate();
									SimpleDateFormat txnDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
									Date dueDate = txnDateFormat.parse(strDueDate);
									log.info("***** txnDate ==>> "+dueDate);
									objPurchaseOrder.setField(PurchaseOrderConstantFields.DUE_DATE__C, dueDate);
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.DUE_DATE__C});
								}	
							}
							
							//EDITSEQUENCE__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.EDITSEQUENCE__C)){
								if(purchaseOrderRetType.getEditSequence() != null && purchaseOrderRetType.getEditSequence() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.EDITSEQUENCE__C, purchaseOrderRetType.getEditSequence());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.EDITSEQUENCE__C});
								}	
							}
							
							//EXPECTED_DATE__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.EXPECTED_DATE__C)){
								if(purchaseOrderRetType.getExpectedDate() != null && purchaseOrderRetType.getExpectedDate() != ""){
									String strExpDate =purchaseOrderRetType.getExpectedDate();
									SimpleDateFormat txnDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
									Date expDate = txnDateFormat.parse(strExpDate);
									log.info("***** txnDate ==>> "+expDate);
									objPurchaseOrder.setField(PurchaseOrderConstantFields.EXPECTED_DATE__C, expDate);
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.EXPECTED_DATE__C});
								}	
							}
							
							//IS_FULLY_RECEIVED__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.IS_FULLY_RECEIVED__C)){
								if(purchaseOrderRetType.getIsFullyReceived() != null && purchaseOrderRetType.getIsFullyReceived() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.IS_FULLY_RECEIVED__C, purchaseOrderRetType.getIsFullyReceived());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.IS_FULLY_RECEIVED__C});
								}	
							}
							
							//IS_MANUALLY_CLOSED
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.IS_MANUALLY_CLOSED)){
								if(purchaseOrderRetType.getIsManuallyClosed() != null && purchaseOrderRetType.getIsManuallyClosed() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.IS_MANUALLY_CLOSED, purchaseOrderRetType.getIsManuallyClosed());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.IS_MANUALLY_CLOSED});
								}	
							}
							
							//MEMO__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.MEMO__C)){
								if(purchaseOrderRetType.getMemo() != null && purchaseOrderRetType.getMemo() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.MEMO__C, purchaseOrderRetType.getMemo());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.MEMO__C});
								}	
							}
							
							//REFERRENCE_NUMBER__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.REFERRENCE_NUMBER__C)){
								if(purchaseOrderRetType.getRefNumber() != null && purchaseOrderRetType.getRefNumber() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.REFERRENCE_NUMBER__C, purchaseOrderRetType.getRefNumber());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.REFERRENCE_NUMBER__C});
								}	
							}
							
							if(purchaseOrderRetType.getShipAddress()!=null){
								//SHIPPING_ADDRESS1__C
								if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.SHIPPING_ADDRESS1__C)){
									if(purchaseOrderRetType.getShipAddress().getAddr1() != null && purchaseOrderRetType.getShipAddress().getAddr1() != ""){
										objPurchaseOrder.setField(PurchaseOrderConstantFields.SHIPPING_ADDRESS1__C, purchaseOrderRetType.getShipAddress().getAddr1());
									}else{
										objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_ADDRESS1__C});
									}	
								}
								
								//SHIPPING_ADDRESS2__C
								if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.SHIPPING_ADDRESS2__C)){
									if(purchaseOrderRetType.getShipAddress().getAddr2() != null && purchaseOrderRetType.getShipAddress().getAddr2() != ""){
										objPurchaseOrder.setField(PurchaseOrderConstantFields.SHIPPING_ADDRESS2__C, purchaseOrderRetType.getShipAddress().getAddr2());
									}else{
										objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_ADDRESS2__C});
									}	
								}
								
								//SHIPPING__CITY__C
								if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.SHIPPING__CITY__C)){
									if(purchaseOrderRetType.getShipAddress().getCity() != null && purchaseOrderRetType.getShipAddress().getCity() != ""){
										objPurchaseOrder.setField(PurchaseOrderConstantFields.SHIPPING__CITY__C, purchaseOrderRetType.getShipAddress().getCity());
									}else{
										objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING__CITY__C});
									}	
								}
								
								//SHIPPING_STATE__C
								if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.SHIPPING_STATE__C)){
									if(purchaseOrderRetType.getShipAddress().getState() != null && purchaseOrderRetType.getShipAddress().getState() != ""){
										objPurchaseOrder.setField(PurchaseOrderConstantFields.SHIPPING_STATE__C, purchaseOrderRetType.getShipAddress().getState());
									}else{
										objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_STATE__C});
									}	
								}
								
								//SHIPPING_POSTAL_CODE__C
								if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.SHIPPING_POSTAL_CODE__C)){
									if(purchaseOrderRetType.getShipAddress().getPostalCode() != null && purchaseOrderRetType.getShipAddress().getPostalCode() != ""){
										objPurchaseOrder.setField(PurchaseOrderConstantFields.SHIPPING_POSTAL_CODE__C, purchaseOrderRetType.getShipAddress().getPostalCode());
									}else{
										objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_POSTAL_CODE__C});
									}	
								}
							}
							else{
								objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_ADDRESS1__C});
								objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_ADDRESS2__C});
								objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING__CITY__C});
								objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_STATE__C});
								objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.SHIPPING_POSTAL_CODE__C});
							}
							//TIME_CREATED__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TIME_CREATED__C)){
								if(purchaseOrderRetType.getTimeCreated() != null && purchaseOrderRetType.getTimeCreated() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.TIME_CREATED__C, purchaseOrderRetType.getTimeCreated());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.TIME_CREATED__C});		
								}	
							}
							
							//TIME_MODIFIED__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TIME_MODIFIED__C)){
								if(purchaseOrderRetType.getTimeModified() != null && purchaseOrderRetType.getTimeModified() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.TIME_MODIFIED__C, purchaseOrderRetType.getTimeModified());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.TIME_MODIFIED__C});
								}	
							}
							
							//TOTAL_AMOUNT__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TOTAL_AMOUNT__C)){
								if(purchaseOrderRetType.getTotalAmount() != null && purchaseOrderRetType.getTotalAmount() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.TOTAL_AMOUNT__C, purchaseOrderRetType.getTotalAmount());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.TOTAL_AMOUNT__C});
								}	
							}
							
							//TRANSACTION_DATE__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TRANSACTION_DATE__C)){
								if(purchaseOrderRetType.getTxnDate() != null && purchaseOrderRetType.getTxnDate() != ""){
									String strTxnDate =purchaseOrderRetType.getTxnDate();
									SimpleDateFormat txnDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
									Date txnDate = txnDateFormat.parse(strTxnDate);
									log.info("***** txnDate ==>> "+txnDate);
									objPurchaseOrder.setField(PurchaseOrderConstantFields.TRANSACTION_DATE__C, txnDate);
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.TRANSACTION_DATE__C});
								}	
							}
							
							//TRANSACTION_NUMBER__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TRANSACTION_NUMBER__C)){
								if(purchaseOrderRetType.getTxnNumber() != null && purchaseOrderRetType.getTxnNumber() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.TRANSACTION_NUMBER__C, purchaseOrderRetType.getTxnNumber());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.TRANSACTION_NUMBER__C});
								}	
							}
							
							//TXN_ID__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TXN_ID__C)){
								if(purchaseOrderRetType.getTxnID() != null && purchaseOrderRetType.getTxnID() != ""){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.TXN_ID__C, purchaseOrderRetType.getTxnID());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.TXN_ID__C});
								}	
							}
							
							//VENDOR__C
							if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.VENDOR__C)){
								if(purchaseOrderRetType.getVendorRef() != null && mapVendorIds!=null && mapVendorIds.get(purchaseOrderRetType.getVendorRef().getListID())!=null){
									objPurchaseOrder.setField(PurchaseOrderConstantFields.VENDOR__C, mapVendorIds.get(purchaseOrderRetType.getVendorRef().getListID()).getId());
								}else{
									objPurchaseOrder.setFieldsToNull(new String[]{PurchaseOrderConstantFields.VENDOR__C});
								}	
							}
							
							lstPurchaseOrder.add(objPurchaseOrder);
							
							List<PurchaseOrderLineRetType> lstPurchaseOrderLineRet=purchaseOrderRetType.getPurchaseOrderLineRet();
							if(lstPurchaseOrderLineRet!=null && lstPurchaseOrderLineRet.size()>0){// purchase order line items null check
								for(PurchaseOrderLineRetType purchaseOrderLineRet:lstPurchaseOrderLineRet){
									setPurchaseTxnIds.add(purchaseOrderRetType.getTxnID().trim());
									objPurchaseItem = new SObject();
									objPurchaseItem.setType(SFDCObjectConstants.PURCHASE_ORDER_LINE_ITEM__C);//sets sobject type to PURCHASE_ORDER_LINE_ITEM__C
									
									//name
									objPurchaseItem.setField(PurchaseOrderItemsConstantFields.PURCHASEORDER_LINE_ITEM_NAME, "isQB");
									log.info("QB_FLAG__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.PURCHASEORDER_LINE_ITEM_NAME));
									
									//QB_FLAG__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.QB_FLAG__C)){
										objPurchaseItem.setField(PurchaseOrderItemsConstantFields.QB_FLAG__C,true);
										log.info("QB_FLAG__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.QB_FLAG__C));
									}
									
									//SFDC_Flag__c
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.SFDC_FLAG__C)){
										objPurchaseItem.setField(PurchaseOrderItemsConstantFields.SFDC_FLAG__C,false);
										log.info("SFDC_FLAG__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.SFDC_FLAG__C));
									}
									
									//AMOUNT__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.AMOUNT__C)){
										if(purchaseOrderLineRet.getAmount() != null && purchaseOrderLineRet.getAmount() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.AMOUNT__C, purchaseOrderLineRet.getAmount());
											log.info("AMOUNT__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.AMOUNT__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.AMOUNT__C});
										}	
									}
									
									//EXTERN_ID__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.EXTERN_ID__C)){
										if(purchaseOrderLineRet.getItemRef() != null && purchaseOrderRetType.getTxnID()!=null && purchaseOrderRetType.getRefNumber()!=null){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.EXTERN_ID__C, purchaseOrderLineRet.getItemRef().getListID()+" "+purchaseOrderRetType.getRefNumber());
											log.info("AMOUNT__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.EXTERN_ID__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.EXTERN_ID__C});
										}	
									}
									
									//DESCRIPTION__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.DESCRIPTION__C)){
										if(purchaseOrderLineRet.getDesc() != null && purchaseOrderLineRet.getDesc() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.DESCRIPTION__C, purchaseOrderLineRet.getDesc());
											log.info("DESCRIPTION__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.DESCRIPTION__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.DESCRIPTION__C});
										}	
									}
									
									//IS_BILLED__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.IS_BILLED__C)){
										if(purchaseOrderLineRet.getIsBilled() != null && purchaseOrderLineRet.getIsBilled() != ""){
											if("true".equalsIgnoreCase(purchaseOrderLineRet.getIsBilled())){
												objPurchaseItem.setField(PurchaseOrderItemsConstantFields.IS_BILLED__C, true);
												log.info("IS_BILLED__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.IS_BILLED__C));
											}else{
												objPurchaseItem.setField(PurchaseOrderItemsConstantFields.IS_BILLED__C, false);
											}	
										}	
									}
									
									//IS_MANUALLY_CLOSED
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.IS_MANUALLY_CLOSED)){
										if(purchaseOrderLineRet.getIsManuallyClosed() != null && purchaseOrderLineRet.getIsManuallyClosed() != ""){
											if("true".equalsIgnoreCase(purchaseOrderLineRet.getIsManuallyClosed())){
												objPurchaseItem.setField(PurchaseOrderItemsConstantFields.IS_MANUALLY_CLOSED, true);
												log.info("IS_MANUALLY_CLOSED ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.IS_MANUALLY_CLOSED));
											}else{
												objPurchaseItem.setField(PurchaseOrderItemsConstantFields.IS_MANUALLY_CLOSED, false);
											}	
										}	
									}
									
									//QUANTITY__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.QUANTITY__C)){
										if(purchaseOrderLineRet.getQuantity() != null && purchaseOrderLineRet.getQuantity() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.QUANTITY__C, purchaseOrderLineRet.getQuantity());
											log.info("QUANTITY__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.QUANTITY__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.QUANTITY__C});
										}	
									}
									
									//RATE__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.RATE__C)){
										if(purchaseOrderLineRet.getRate() != null && purchaseOrderLineRet.getRate() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.RATE__C, purchaseOrderLineRet.getRate());
											log.info("RATE__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.RATE__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.RATE__C});
										}	
									}
									
									//RECEIVED_QUANTITY__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.RECEIVED_QUANTITY__C)){
										if(purchaseOrderLineRet.getReceivedQuantity() != null && purchaseOrderLineRet.getReceivedQuantity() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.RECEIVED_QUANTITY__C, purchaseOrderLineRet.getReceivedQuantity());
											log.info("RECEIVED_QUANTITY__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.RECEIVED_QUANTITY__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.RECEIVED_QUANTITY__C});
										}	
									}
									
									//TXN_ID__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.TXN_ID__C)){
										if(purchaseOrderRetType.getTxnID() != null && purchaseOrderRetType.getTxnID() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.TXN_ID__C, purchaseOrderRetType.getTxnID());
											log.info("TXN_ID__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.TXN_ID__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.TXN_ID__C});
										}	
									}
									
									//TXNLINEID__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.TXNLINEID__C)){
										if(purchaseOrderLineRet.getTxnLineID() != null && purchaseOrderLineRet.getTxnLineID() != ""){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.TXNLINEID__C, purchaseOrderLineRet.getTxnLineID());
											log.info("TXNLINEID__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.TXNLINEID__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.TXNLINEID__C});
										}	
									}
									
									//PRODUCT__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.PRODUCT__C)){
										if(purchaseOrderLineRet.getItemRef() != null && mapProducts!=null && mapProducts.get(purchaseOrderLineRet.getItemRef().getListID())!=null){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.PRODUCT__C, mapProducts.get(purchaseOrderLineRet.getItemRef().getListID()).getId());
											log.info("PRODUCT__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.PRODUCT__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.PRODUCT__C});
										}	
									}
									
									//ACCOUNT__C
									if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.ACCOUNT__C)){
										if(purchaseOrderLineRet.getCustomerRef() != null && mapAccListIds!=null && mapAccListIds.get(purchaseOrderLineRet.getCustomerRef().getListID())!=null){
											objPurchaseItem.setField(PurchaseOrderItemsConstantFields.ACCOUNT__C, mapAccListIds.get(purchaseOrderLineRet.getCustomerRef().getListID()).getId());
											log.info("ACCOUNT__C ==>> "+objPurchaseItem.getField(PurchaseOrderItemsConstantFields.ACCOUNT__C));
										}else{
											objPurchaseItem.setFieldsToNull(new String[]{PurchaseOrderItemsConstantFields.ACCOUNT__C});
										}	
									}
									
									lstPurchaseItem.add(objPurchaseItem);
								}
							}
							
						}
					}
				}
				
				//assign list to an array
				if(lstPurchaseOrder != null && lstPurchaseOrder.size() > 0){
					objpurchaseLst = new SObject[lstPurchaseOrder.size()];
					lstPurchaseOrder.toArray(objpurchaseLst);
					log.info("objVenLst********"+objpurchaseLst.toString());
				}
				
				//logic to upsert 100 records at a time to overcome the salesforce batch limit
				if(objpurchaseLst!=null && objpurchaseLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objpurchaseLst,partnerConnection,PurchaseOrderConstantFields.TXN_ID__C,SFDCObjectConstants.PURCHASE_ORDER__C);
					//Inserting Purchase_Order_Line_Item__C
					if(lstPurchaseItem != null && lstPurchaseItem.size() > 0){
						log.info("lstPurchaseItem Size ==>> "+lstPurchaseItem.size());
						boolean iItemFlag = false;
						SObject[] purchaseItemList = new SObject[lstPurchaseItem.size()];
						//query to get invoice based on txn id
						String purchaseOrdQuery = "SELECT Id";
						if(setPurchaseOrderFields.contains(PurchaseOrderConstantFields.TXN_ID__C) && setPurchaseOrderFields.contains(PurchaseOrderConstantFields.REFERRENCE_NUMBER__C)){
							purchaseOrdQuery = purchaseOrdQuery+", TxnID__c,Reference_Number__c FROM Purchase_Order__c where TxnID__c in (";
							iItemFlag = true;
						}
						
						if(iItemFlag){
							purchaseOrdQuery = purchaseOrdQuery+"";
							StringBuilder filter=new StringBuilder();
							int count = 1;
							//Preparing set of LISTID's
							if(setPurchaseTxnIds != null && !setPurchaseTxnIds.isEmpty()){
								for(String purchaseTxnId : setPurchaseTxnIds){
									if(purchaseTxnId != null){
										filter.append("\'"+purchaseTxnId+"\'");
										if(count != setPurchaseTxnIds.size())
											filter.append(" ,");
									}
									count++;
								}
							}
							
							purchaseOrdQuery = purchaseOrdQuery+filter+")";
							log.info("purchaseOrdQuery ==>> "+purchaseOrdQuery);
							//Retrieving Invoice__c objects from Salesforce
							QueryResult qResult = partnerConnection.query(purchaseOrdQuery);
							if (qResult != null && qResult.getSize() > 0) { //resultset null check
								SObject[] records = qResult.getRecords();
								for(SObject sObj : records){
									log.info("sobj"+ sObj.getField("TxnID__c"));
									if(sObj != null && sObj.getField("TxnID__c") != null){
										String strTxnID = sObj.getField("TxnID__c").toString().trim();
										mapPurchases.put(strTxnID, sObj);
									}
								}
							}
						}
						
						//preparing map for invoice and txn id
						for(int i = 0;i < lstPurchaseItem.size();i++){
							SObject purchaseItemObj = lstPurchaseItem.get(i);
							log.info("purchaseItemObj"+purchaseItemObj);
							if(purchaseItemObj != null){
								log.info("mapPurchases"+mapPurchases);
								if(mapPurchases != null && mapPurchases.size() > 0){
									log.info("purchaseItemObj.getField(TxnID__c)" +mapPurchases.containsKey(purchaseItemObj.getField(PurchaseOrderItemsConstantFields.TXN_ID__C)));
									if(purchaseItemObj.getField(PurchaseOrderItemsConstantFields.TXN_ID__C) != null && mapPurchases.containsKey(purchaseItemObj.getField(PurchaseOrderItemsConstantFields.TXN_ID__C).toString())){
										log.info("true");
										if(setPurchaseLineItemFields.contains(PurchaseOrderItemsConstantFields.PURCHASE_ORDER__C)){
											purchaseItemObj.setField(PurchaseOrderItemsConstantFields.PURCHASE_ORDER__C, mapPurchases.get(purchaseItemObj.getField(PurchaseOrderItemsConstantFields.TXN_ID__C).toString()).getId());
											log.info("purchaseItemObj fieldsss"+ purchaseItemObj.getField(PurchaseOrderItemsConstantFields.PURCHASE_ORDER__C));
										}
									}
								}
							}
							purchaseItemList[i] = purchaseItemObj;
						}
						
						//logic to prepare batches to overcome sfdc limit
						if(purchaseItemList != null && purchaseItemList.length > 0){
							log.info("invoiceItemList Size ==>> "+purchaseItemList.length);
							isOk = QueryExecuter.prepareAndExecute(purchaseItemList,partnerConnection,PurchaseOrderItemsConstantFields.EXTERN_ID__C,SFDCObjectConstants.PURCHASE_ORDER_LINE_ITEM__C);
						}
					}
				}
			}
		}catch(Exception e){
			log.info("Exception occured in createPurchaseorders method"+e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		
		log.info("***************** QB PurchaseOrders SYNC END *****************");
		return isOk;
	}
	
	/**
	 * createBills method
	 * @param response
	 * @return String
	 * @Description This is to insert Bills,bill line items and expense line items records from quickbooks to sfdc vendors 
	 */
	public java.lang.String createBills(java.lang.String response){
		log.info("***************** QB Bills SYNC START *****************");
		String isOk = "";//status of sfdc upsert
		boolean flag = false; //to check sql query builds correctly with Object name and fields
		boolean productFlag=false;
		Map<String,String> mapTxnIds = new HashMap<String,String>();// contains of map of listIds and SFDC_FLAG__C values
		Map<String,SObject> mapVendorIds = new HashMap<String, SObject>();//to map vendor as look up
		Map<String,SObject> mapAccountingIds = new HashMap<String, SObject>();//to map Accounting as look up
		Map<String,SObject> mapAcctExpIds = new HashMap<String, SObject>();//to map Accounting as look up for expenses
		Map<String,SObject> mapProducts = new HashMap<String, SObject>();//to map Product2 as look up
		Map<String,SObject> mapAccListIds = new HashMap<String, SObject>();//to map Account as look up
		Map<String,SObject> mapBills = new HashMap<String, SObject>();//to map Bills as look up
		Map<String,SObject> mapBillsExp = new HashMap<String, SObject>();//to map Bills as look up
		com.appshark.billquery.rs.QBXMLType qbxml = null;//QBXMLType
		Set<String> setBillIds=new HashSet<String>();
		Set<String> setBillExpIds=new HashSet<String>();
		List<com.appshark.billquery.rs.BillRetType> billRetTypeList = null;//CustomerRetType from qb

		try {  
			//getting the data from CustomerRetType jars
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.billquery.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.billquery.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.billquery.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getBillQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getBillQueryRs().getBillRet() != null)){
				billRetTypeList = qbxml.getQBXMLMsgsRs().getBillQueryRs().getBillRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException from createBills Method ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception from createBills Method==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		try{
			if(billRetTypeList != null && billRetTypeList.size() > 0){
				log.info("partnerConnection ==>> "+partnerConnection);
				log.info("billRetTypeList Size ==>> "+billRetTypeList.size());

				//Preparing Query for Account
				String billQuery = SFDCQueryConstants.BILL_QUERY;
				log.info("vendorQuery 111--->"+billQuery);
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(BillsConstantFields.TXNID__C);
				fieldToBeUsed.add(BillsConstantFields.SFDC_FLAG__C);
				//dynamic query method that returns a map containing sqlQuery,flag,lastModifiedDate
				returnedMap = sqlQueryBuilder.buildQueryForBill(billQuery,setBillFields, SFDCObjectConstants.BILL__C, billRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					billQuery = (String) returnedMap.get("sqlQuery");
					qbBillSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("billQuery ==>> "+billQuery);
					log.info("billflag ==>> "+flag);
					log.info("qbBillSynchControl ==>> "+qbBillSynchControl);
				}

				if(flag){
					//fields to prepare map 
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(BillsConstantFields.TXNID__C);
					fieldToBeUsed.add(BillsConstantFields.SFDC_FLAG__C);
					//method that returns map of listIds and SFDC_FLAG__C values
					mapTxnIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(billQuery), fieldToBeUsed);
				log.info("mapTxnIds"+mapTxnIds);
				}
				
				//Preparing Query for vendor__c
				String venQuery = SFDCQueryConstants.VENDOR_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(VendorConstantFields.SFDC_FLAG__C);
				fieldToBeUsed.add(VendorConstantFields.LISTID__C);
				returnedMap = sqlQueryBuilder.buildQueryForBillVendor(venQuery,setVendorFields, SFDCObjectConstants.VENDOR__C, billRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					venQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+venQuery);
					log.info("invflag ==>> "+flag);
				}

				if(flag){
					//Retrieving Account objects from Salesforce
					mapVendorIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(venQuery), VendorConstantFields.LISTID__C);
				}
				
				//Preparing Query for Accounting__c
				String AcctQuery = SFDCQueryConstants.ACCOUNTING_PARENT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountingConstantFields.ACCOUNTING_ID);
				fieldToBeUsed.add(AccountingConstantFields.LIST_ID__C);
				returnedMap = sqlQueryBuilder.buildQueryForBillAccounting(AcctQuery,setAccountingFields, SFDCObjectConstants.ACCOUNTING__C, billRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					AcctQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+AcctQuery);
					log.info("invflag ==>> "+flag);
				}

				if(flag){
					//Retrieving Account objects from Salesforce
					mapAccountingIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(AcctQuery), AccountingConstantFields.LIST_ID__C);
				}
				
				//Preparing Query for Accounting__c
				String AcctingQuery = SFDCQueryConstants.ACCOUNTING_PARENT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountingConstantFields.ACCOUNTING_ID);
				fieldToBeUsed.add(AccountingConstantFields.LIST_ID__C);
				returnedMap = sqlQueryBuilder.buildQueryForExpenseAccounting(AcctingQuery,setAccountingFields, SFDCObjectConstants.ACCOUNTING__C, billRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					AcctingQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+AcctingQuery);
					log.info("invflag ==>> "+flag);
				}

				if(flag){
					//Retrieving Account objects from Salesforce
					mapAcctExpIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(AcctingQuery), AccountingConstantFields.LIST_ID__C);
				}
				log.info("mapAcctExpIds ==>> "+mapAcctExpIds.size());
				//Preparing Query for Product2
				String productQuery = "SELECT Id, Name";
				if(setProduct2Fields.contains("ListID__c")){
					productQuery = productQuery+", ListID__c FROM Product2";
					productFlag = true;
				}

				if(productFlag){
					QueryResult qResult = partnerConnection.query(productQuery);
					if (qResult != null && qResult.getSize() > 0) {
						SObject[] records = qResult.getRecords();
						if(records!=null && records.length>0){
							log.info("SFDC PRODUCT RECORDS LENGTH ==>> "+records.length);
							for(SObject sObj : records){
								if(sObj != null && sObj.getField("ListID__c") != null){//ListID__c null check
									//log.info("PRODUCT2 SOBJECT ID FROM QUERY ==>> "+sObj.getId()+" <==> "+sObj.getField("ListID__c"));
									mapProducts.put(sObj.getField("ListID__c").toString(), sObj);
								}
							}	
						}
					}
				}
				
				//Preparing Query for Account
				String accQuery = SFDCQueryConstants.PARENT_ACCOUNT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountConstantFields.QBLISTID__C);
				fieldToBeUsed.add(AccountConstantFields.LISTID__C);
				log.info("fieldToBeUsed");
				Map<String,Object> returnedMapAcc = sqlQueryBuilder.buildQueryForBillItemAccount(accQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, billRetTypeList,fieldToBeUsed);
				log.info("returnedMapAcc"+returnedMapAcc);
				if(returnedMapAcc != null && returnedMapAcc.size() > 0){
					flag = (boolean) returnedMapAcc.get("flag");
					accQuery = (String) returnedMapAcc.get("sqlQuery");
					log.info("accountQuery ==>> "+accQuery);
					log.info("invflag ==>> "+flag);

					log.info("accQuery ==>> "+accQuery);
					if(flag){
						//Retrieving Account objects from Salesforce
						mapAccListIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(accQuery), AccountConstantFields.LISTID__C);	
						log.info("mapAccListIds"+mapAccListIds.size());	
					}				
				}
			}
			
			//mapping of fields from quickbooks to salesforce
			if(billRetTypeList != null && billRetTypeList.size() > 0){
				List<SObject> lstBills = new ArrayList<SObject>();
				List<SObject> lstBillItems = new ArrayList<SObject>();
				List<SObject> lstBillExpItems = new ArrayList<SObject>();
				SObject[] objBillLst = null;
				SObject objBill;
				SObject objBillItem;
				SObject objBillExp;
				for(BillRetType billRetType : billRetTypeList){//customerRetTypeList iteration
					log.info("mapListIds.get(vendorRetType.getListID().trim()" +(mapTxnIds.get(billRetType.getTxnID().trim())));
					if((mapTxnIds != null ) 
							&& ((mapTxnIds.isEmpty() || (mapTxnIds.size()>0  && !mapTxnIds.containsKey(billRetType.getTxnID().trim()))) || (mapTxnIds.size() > 0 && mapTxnIds.containsKey(billRetType.getTxnID().trim()) && "false".equalsIgnoreCase(mapTxnIds.get(billRetType.getTxnID().trim()))))){
						log.info("mapListIds ==>> "+mapTxnIds.get(billRetType.getTxnID()));
						if(billRetType != null && billRetType.getTxnID() != null){
							flag = true;
							objBill = new SObject();
							//setting object type of sfdc account
							objBill.setType(SFDCObjectConstants.BILL__C);
							
							//QB_FLAG__C
							if(setBillFields.contains(BillsConstantFields.QB_FLAG__C)){
								objBill.setField(BillsConstantFields.QB_FLAG__C,true);
								log.info("QB_FLAG__C ==>> "+objBill.getField(BillsConstantFields.QB_FLAG__C));
							}
							
							//SFDC_Flag__c
							if(setBillFields.contains(BillsConstantFields.SFDC_FLAG__C)){
								objBill.setField(BillsConstantFields.SFDC_FLAG__C,false);
								log.info("SFDC_FLAG__C ==>> "+objBill.getField(BillsConstantFields.SFDC_FLAG__C));
							}
							
							//name
							if(billRetType.getTxnID() != null && billRetType.getTxnID() != ""){
								log.info("NEW ACCOUNT FULL NAME ==>> "+billRetType.getTxnID());
								objBill.setField(BillsConstantFields.BILLS_NAME,"isQB");
								log.info("NEW Bill FULL NAME ==>> "+objBill.getField(BillsConstantFields.BILLS_NAME));
							}
							
							//AMOUNT_DUE__C
							if(setBillFields.contains(BillsConstantFields.AMOUNT_DUE__C)){
								if(billRetType.getAmountDue() != null && billRetType.getAmountDue() != ""){
									objBill.setField(BillsConstantFields.AMOUNT_DUE__C, billRetType.getAmountDue());
									log.info("TXNLINEID__C ==>> "+objBill.getField(BillsConstantFields.AMOUNT_DUE__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.AMOUNT_DUE__C});
								}	
							}
							
							//APACCOUNT_LISTID__C
							if(setBillFields.contains(BillsConstantFields.APACCOUNT_LISTID__C)){
								if(billRetType.getAPAccountRef() != null && billRetType.getAPAccountRef().getListID() !=null && billRetType.getAPAccountRef().getListID() !=""){
									objBill.setField(BillsConstantFields.APACCOUNT_LISTID__C, billRetType.getAPAccountRef().getListID());
									log.info("APACCOUNT_LISTID__C ==>> "+objBill.getField(BillsConstantFields.APACCOUNT_LISTID__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.APACCOUNT_LISTID__C});
								}	
							}
							
							//APACCOUNT_TYPE__C
							if(setBillFields.contains(BillsConstantFields.APACCOUNT_TYPE__C)){
								if(billRetType.getAPAccountRef() != null && billRetType.getAPAccountRef().getFullName() !=null && billRetType.getAPAccountRef().getFullName() !=""){
									objBill.setField(BillsConstantFields.APACCOUNT_TYPE__C, billRetType.getAPAccountRef().getFullName());
									log.info("APACCOUNT_TYPE__C ==>> "+objBill.getField(BillsConstantFields.APACCOUNT_TYPE__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.APACCOUNT_TYPE__C});
								}	
							}
							
							//DUE_DATE__C
							if(setBillFields.contains(BillsConstantFields.DUE_DATE__C)){
								if(billRetType.getDueDate() != null && billRetType.getDueDate() !=""){
									String strDueDate =billRetType.getDueDate();
									SimpleDateFormat txnDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
									Date dueDate = txnDateFormat.parse(strDueDate);
									log.info("***** txnDate ==>> "+dueDate);
									objBill.setField(BillsConstantFields.DUE_DATE__C, dueDate);
									log.info("DUE_DATE__C ==>> "+objBill.getField(BillsConstantFields.DUE_DATE__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.DUE_DATE__C});
								}	
							}
							
							//REFERRENCE_NUMBER__C
							if(setBillFields.contains(BillsConstantFields.REFERRENCE_NUMBER__C)){
								if(billRetType.getRefNumber() != null && billRetType.getRefNumber() != ""){
									objBill.setField(BillsConstantFields.REFERRENCE_NUMBER__C, billRetType.getRefNumber());
									log.info("REFERRENCE_NUMBER__C ==>> "+objBill.getField(BillsConstantFields.REFERRENCE_NUMBER__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.REFERRENCE_NUMBER__C});
								}	
							}
							
							//TIME_CREATED__C
							if(setBillFields.contains(BillsConstantFields.TIME_CREATED__C)){
								if(billRetType.getTimeCreated() != null && billRetType.getTimeCreated() != ""){
									objBill.setField(BillsConstantFields.TIME_CREATED__C, billRetType.getTimeCreated());
									log.info("TIME_CREATED__C ==>> "+objBill.getField(BillsConstantFields.TIME_CREATED__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.TIME_CREATED__C});
								}	
							}
							
							//TIME_CREATED__C
							if(setBillFields.contains(BillsConstantFields.TIME_MODIFIED__C)){
								if(billRetType.getTimeModified() != null && billRetType.getTimeModified() != ""){
									objBill.setField(BillsConstantFields.TIME_MODIFIED__C, billRetType.getTimeModified());
									log.info("TIME_MODIFIED__C ==>> "+objBill.getField(BillsConstantFields.TIME_MODIFIED__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.TIME_MODIFIED__C});
								}	
							}
							
							//TRANSACTION_DATE__C
							if(setBillFields.contains(BillsConstantFields.TRANSACTION_DATE__C)){
								if(billRetType.getTxnDate() != null && billRetType.getTxnDate() !=""){
									String strTxnDate =billRetType.getTxnDate();
									SimpleDateFormat txnDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
									Date txnDate = txnDateFormat.parse(strTxnDate);
									log.info("***** txnDate ==>> "+txnDate);
									objBill.setField(BillsConstantFields.TRANSACTION_DATE__C, txnDate);
									log.info("TRANSACTION_DATE__C ==>> "+objBill.getField(BillsConstantFields.TRANSACTION_DATE__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.TRANSACTION_DATE__C});
								}	
							}
							
							//TIME_CREATED__C
							if(setBillFields.contains(BillsConstantFields.TRANSACTION_NUMBER__C)){
								if(billRetType.getTxnNumber() != null && billRetType.getTxnNumber() != ""){
									objBill.setField(BillsConstantFields.TRANSACTION_NUMBER__C, billRetType.getTxnNumber());
									log.info("TRANSACTION_NUMBER__C ==>> "+objBill.getField(BillsConstantFields.TRANSACTION_NUMBER__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.TRANSACTION_NUMBER__C});
								}	
							}
							
							//TXNID__C
							if(setBillFields.contains(BillsConstantFields.TXNID__C)){
								if(billRetType.getTxnID() != null && billRetType.getTxnID() != ""){
									objBill.setField(BillsConstantFields.TXNID__C, billRetType.getTxnID());
									log.info("TXNID__C ==>> "+objBill.getField(BillsConstantFields.TXNID__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.TXNID__C});
								}	
							}
							
							//VENDOR__C
							if(setBillFields.contains(BillsConstantFields.VENDOR__C)){
								if(billRetType.getVendorRef() != null && billRetType.getVendorRef().getListID() != null &&  mapVendorIds!=null && mapVendorIds.get(billRetType.getVendorRef().getListID())!=null){
									objBill.setField(BillsConstantFields.VENDOR__C, mapVendorIds.get(billRetType.getVendorRef().getListID()).getId());
									log.info("VENDOR__C ==>> "+objBill.getField(BillsConstantFields.VENDOR__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.VENDOR__C});
								}	
							}
							
							//ACCOUNTING__C
							if(setBillFields.contains(BillsConstantFields.ACCOUNTING__C)){
								if(billRetType.getAPAccountRef() != null && billRetType.getAPAccountRef().getListID() != null &&  mapAccountingIds!=null && mapAccountingIds.get(billRetType.getAPAccountRef().getListID())!=null){
									objBill.setField(BillsConstantFields.ACCOUNTING__C, mapAccountingIds.get(billRetType.getAPAccountRef().getListID()).getId());
									log.info("VENDOR__C ==>> "+objBill.getField(BillsConstantFields.ACCOUNTING__C));
								}else{
									objBill.setFieldsToNull(new String[]{BillsConstantFields.ACCOUNTING__C});
								}	
							}
							
							lstBills.add(objBill);
							
							List<ItemLineRetType> billLineItems=billRetType.getItemLineRet();
							if(billLineItems!=null && billLineItems.size()>0){
								for(ItemLineRetType itemLineRet:billLineItems){
									setBillIds.add(billRetType.getTxnID().trim());
									objBillItem = new SObject();
									objBillItem.setType(SFDCObjectConstants.BILL_LINE__ITEM__C);
									//name
									objBillItem.setField(BillLineItemConstantFields.BILL_LINE_ITEM_NAME, "isQB");
									log.info("QB_FLAG__C ==>> "+objBillItem.getField(BillLineItemConstantFields.BILL_LINE_ITEM_NAME));
									//QB_FLAG__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.QB_FLAG__C)){
										objBillItem.setField(BillLineItemConstantFields.QB_FLAG__C,true);
										log.info("QB_FLAG__C ==>> "+objBillItem.getField(BillLineItemConstantFields.QB_FLAG__C));
									}
									//SFDC_Flag__c
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.SFDC_FLAG__C)){
										objBillItem.setField(BillLineItemConstantFields.SFDC_FLAG__C,false);
										log.info("SFDC_FLAG__C ==>> "+objBillItem.getField(BillLineItemConstantFields.SFDC_FLAG__C));
									}
									//TXNID__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.TXNID__C)){
										if(billRetType.getTxnID() != null && billRetType.getTxnID() != ""){
											objBillItem.setField(BillLineItemConstantFields.TXNID__C, billRetType.getTxnID());
											log.info("TXNID__C ==>> "+objBillItem.getField(BillLineItemConstantFields.TXNID__C));
										}else{
											objBillItem.setFieldsToNull(new String[]{BillLineItemConstantFields.TXNID__C});
										}	
									}
									//DESCRIPTION__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.DESCRIPTION__C)){
										if(itemLineRet.getDesc() != null && itemLineRet.getDesc() != ""){
											objBillItem.setField(BillLineItemConstantFields.DESCRIPTION__C, itemLineRet.getDesc());
											log.info("DESCRIPTION__C ==>> "+objBillItem.getField(BillLineItemConstantFields.DESCRIPTION__C));
										}else{
											objBillItem.setFieldsToNull(new String[]{BillLineItemConstantFields.DESCRIPTION__C});
										}	
									}
									//QUANTITY__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.QUANTITY__C)){
										if(itemLineRet.getQuantity() != null && itemLineRet.getQuantity() != ""){
											objBillItem.setField(BillLineItemConstantFields.QUANTITY__C, itemLineRet.getQuantity());
											log.info("TXNID__C ==>> "+objBillItem.getField(BillLineItemConstantFields.QUANTITY__C));
										}else{
											objBillItem.setFieldsToNull(new String[]{BillLineItemConstantFields.QUANTITY__C});
										}	
									}
									//TXN_LINE_ID__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.TXN_LINE_ID__C)){
										if(itemLineRet.getTxnLineID() != null && itemLineRet.getTxnLineID() != ""){
											objBillItem.setField(BillLineItemConstantFields.TXN_LINE_ID__C, itemLineRet.getTxnLineID());
											log.info("TXNID__C ==>> "+objBillItem.getField(BillLineItemConstantFields.TXN_LINE_ID__C));
										}else{
											objBillItem.setFieldsToNull(new String[]{BillLineItemConstantFields.TXN_LINE_ID__C});
										}	
									}
									//EXTERN_ID__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.EXTERN_ID__C)){
										if(itemLineRet.getItemRef() != null && itemLineRet.getItemRef().getListID()!=null && billRetType.getTxnID()!=null){
											objBillItem.setField(BillLineItemConstantFields.EXTERN_ID__C, billRetType.getTxnID().trim()+" "+itemLineRet.getItemRef().getListID());
											log.info("EXTERN_ID__C Bill Items==>> "+objBillItem.getField(BillLineItemConstantFields.EXTERN_ID__C));
										}else{
											objBillItem.setFieldsToNull(new String[]{BillLineItemConstantFields.EXTERN_ID__C});
										}	
									}
									//PRODUCT__C
									if(setBillLineItemsFields.contains(BillLineItemConstantFields.PRODUCT__C)){
										if(itemLineRet.getItemRef() != null && mapProducts!=null && mapProducts.get(itemLineRet.getItemRef().getListID())!=null){
											objBillItem.setField(BillLineItemConstantFields.PRODUCT__C, mapProducts.get(itemLineRet.getItemRef().getListID()).getId());
											log.info("PRODUCT__C ==>> "+objBillItem.getField(BillLineItemConstantFields.PRODUCT__C));
										}else{
											objBillItem.setFieldsToNull(new String[]{BillLineItemConstantFields.PRODUCT__C});
										}	
									}	
									lstBillItems.add(objBillItem);
									List<ExpenseLineRetType> expenseItemLst=billRetType.getExpenseLineRet();
									if(expenseItemLst!=null && expenseItemLst.size()>0){
										for(ExpenseLineRetType expenseLineRet:expenseItemLst){
											setBillExpIds.add(billRetType.getTxnID().trim());
											objBillExp = new SObject();
											objBillExp.setType(SFDCObjectConstants.EXPENSE_LINE_ITEM__C);
											//name
											objBillExp.setField(ExpensesLineItemConstantFields.EXPENSESLINE_ITEM_NAME, "isQB");
											log.info("QB_FLAG__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.EXPENSESLINE_ITEM_NAME));
											//QB_FLAG__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.QB_FLAG__C)){
												objBillExp.setField(ExpensesLineItemConstantFields.QB_FLAG__C,true);
												log.info("QB_FLAG__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.QB_FLAG__C));
											}
											//SFDC_Flag__c
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.SFDC_FLAG__C)){
												objBillExp.setField(ExpensesLineItemConstantFields.SFDC_FLAG__C,false);
												log.info("SFDC_FLAG__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.SFDC_FLAG__C));
											}	
											//TXN_LINE_ID__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.TXN_LINE_ID__C)){
												if(expenseLineRet.getTxnLineID() != null && expenseLineRet.getTxnLineID() != ""){
													objBillExp.setField(ExpensesLineItemConstantFields.TXN_LINE_ID__C, expenseLineRet.getTxnLineID());
													log.info("TXNID__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.TXN_LINE_ID__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.TXN_LINE_ID__C});
												}	
											}
											//TXN_ID__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.TXN_ID__C)){
												if(billRetType.getTxnID() != null && billRetType.getTxnID() != ""){
													objBillExp.setField(ExpensesLineItemConstantFields.TXN_ID__C, billRetType.getTxnID());
													log.info("TXN_ID__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.TXN_ID__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.TXN_ID__C});
												}	
											}
											//ACCOUNT_REF_LISTID__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.ACCOUNT_REF_LISTID__C)){
												if(expenseLineRet.getAccountRef() != null && expenseLineRet.getAccountRef().getListID() != null && expenseLineRet.getAccountRef().getListID()!=""){
													objBillExp.setField(ExpensesLineItemConstantFields.ACCOUNT_REF_LISTID__C, expenseLineRet.getAccountRef().getListID());
													log.info("ACCOUNT_REF_LISTID__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.ACCOUNT_REF_LISTID__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.ACCOUNT_REF_LISTID__C});
												}	
											}
											/*//ACCOUNT_REF_NAME__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.ACCOUNT_REF_NAME__C)){
												if(expenseLineRet.getAccountRef() != null && expenseLineRet.getAccountRef().getFullName() != null && expenseLineRet.getAccountRef().getFullName()!=""){
													objBillExp.setField(ExpensesLineItemConstantFields.ACCOUNT_REF_NAME__C, expenseLineRet.getAccountRef().getFullName());
													log.info("ACCOUNT_REF_NAME__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.ACCOUNT_REF_NAME__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.ACCOUNT_REF_NAME__C});
												}	
											}*/
											//AMOUNT__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.AMOUNT__C)){
												if(expenseLineRet.getAmount() != null && expenseLineRet.getAmount() != ""){
													objBillExp.setField(ExpensesLineItemConstantFields.AMOUNT__C, expenseLineRet.getAmount());
													log.info("AMOUNT__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.AMOUNT__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.AMOUNT__C});
												}	
											}
											//BILLABLE_STATUS__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.BILLABLE_STATUS__C)){
												if(expenseLineRet.getBillableStatus() != null && expenseLineRet.getBillableStatus() != ""){
													objBillExp.setField(ExpensesLineItemConstantFields.BILLABLE_STATUS__C, expenseLineRet.getBillableStatus());
													log.info("BILLABLE_STATUS__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.BILLABLE_STATUS__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.BILLABLE_STATUS__C});
												}	
											}
											//EXTERN_ID__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.EXTERN_ID__C)){
												if(expenseLineRet.getAccountRef() != null && expenseLineRet.getAccountRef().getListID() != null && billRetType.getTxnID()!=null){
													objBillExp.setField(ExpensesLineItemConstantFields.EXTERN_ID__C, billRetType.getTxnID().trim()+" "+expenseLineRet.getAccountRef().getListID());
													log.info("EXTERN_ID__C Expenses==>> "+objBillExp.getField(ExpensesLineItemConstantFields.EXTERN_ID__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.EXTERN_ID__C});
												}	
											}
											//ACCOUNTING__C
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.ACCOUNTING__C)){
												log.info("Accounting__c"+ExpensesLineItemConstantFields.ACCOUNTING__C);
												if(expenseLineRet.getAccountRef() != null && expenseLineRet.getAccountRef().getListID() != null &&  mapAcctExpIds!=null && mapAcctExpIds.get(expenseLineRet.getAccountRef().getListID())!=null){
													objBillExp.setField(ExpensesLineItemConstantFields.ACCOUNTING__C, mapAcctExpIds.get(expenseLineRet.getAccountRef().getListID()).getId());
													log.info("ACCOUNTING__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.ACCOUNTING__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.ACCOUNTING__C});
												}	
											}
											//Account
											if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.ACCOUNT__C)){
												if(expenseLineRet.getCustomerRef() != null && expenseLineRet.getCustomerRef().getListID() != null &&  mapAccListIds!=null && mapAccListIds.get(expenseLineRet.getCustomerRef().getListID())!=null){
													objBillExp.setField(ExpensesLineItemConstantFields.ACCOUNT__C, mapAccListIds.get(expenseLineRet.getCustomerRef().getListID()).getId());
													log.info("ACCOUNT__C ==>> "+objBillExp.getField(ExpensesLineItemConstantFields.ACCOUNT__C));
												}else{
													objBillExp.setFieldsToNull(new String[]{ExpensesLineItemConstantFields.ACCOUNT__C});
												}	
											}
											lstBillExpItems.add(objBillExp);
										}
									}
								}
							}								
						}
					}
				}
				//assign list to an array
				if(lstBills != null && lstBills.size() > 0){
					log.info("lstBills********"+lstBills);
					objBillLst = new SObject[lstBills.size()];
					lstBills.toArray(objBillLst);
					log.info("objBillLst ********"+objBillLst.toString());

				}
				//logic to upsert 100 records at a time to overcome the salesforce batch limit
				if(objBillLst!=null && objBillLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objBillLst,partnerConnection,BillsConstantFields.TXNID__C,SFDCObjectConstants.BILL__C);
					//Inserting Bill Line Items
					if(lstBillItems != null && lstBillItems.size() > 0){
						log.info("lstBillItems Size ==>> "+lstBillItems.size());
						boolean iItemFlag = false;
						SObject[] billItemList = new SObject[lstBillItems.size()];
						//query to get invoice based on txn id
						String billOrdQuery = "SELECT Id";
						if(setBillFields.contains(BillsConstantFields.TXNID__C) && setBillFields.contains(BillsConstantFields.REFERRENCE_NUMBER__C)){
							billOrdQuery = billOrdQuery+", TxnID__c,Referrence_Number__c FROM Bill__c where TxnID__c in (";
							iItemFlag = true;
						}
						if(iItemFlag){
							billOrdQuery = billOrdQuery+"";
							StringBuilder filter = new StringBuilder();
							int count = 1;
							//Preparing set of LISTID's
							if(setBillIds != null && setBillIds.size() > 0){
								for(String purchaseTxnId : setBillIds){
									if(purchaseTxnId != null){
										filter.append("\'"+purchaseTxnId+"\'");
										if(count != setBillIds.size())
											filter.append(" ,");
									}
									count++;
								}
							}
							billOrdQuery = billOrdQuery+filter+")";
							log.info("purchaseOrdQuery ==>> "+billOrdQuery);
							//Retrieving Invoice__c objects from Salesforce
							QueryResult qResult = partnerConnection.query(billOrdQuery);
							if (qResult != null && qResult.getSize() > 0) { //resultset null check
								SObject[] records = qResult.getRecords();
								for(SObject sObj : records){
									log.info("sobj"+ sObj.getField("TxnID__c"));
									if(sObj != null && sObj.getField("TxnID__c") != null){
										String strTxnID = sObj.getField("TxnID__c").toString().trim();
										mapBills.put(strTxnID, sObj);
									}
								}
							}
						}
						//preparing map for invoice and txn id
						for(int i = 0;i < lstBillItems.size();i++){
							SObject billItemObj = lstBillItems.get(i);
							log.info("billItemObj"+billItemObj);
							if(billItemObj != null){
								if(mapBills != null && mapBills.size() > 0){
									log.info("billItemObj.getField(TxnID__c)" +mapBills.containsKey(billItemObj.getField(BillLineItemConstantFields.TXNID__C)));
									if(billItemObj.getField(BillLineItemConstantFields.TXNID__C) != null && mapBills.containsKey(billItemObj.getField(BillLineItemConstantFields.TXNID__C).toString())){
										log.info("true");
										if(setBillLineItemsFields.contains(BillLineItemConstantFields.BILL__C)){
											billItemObj.setField(BillLineItemConstantFields.BILL__C, mapBills.get(billItemObj.getField(BillLineItemConstantFields.TXNID__C).toString()).getId());
											log.info("billItemObj fieldsss"+ billItemObj.getField(BillLineItemConstantFields.BILL__C));
										}
									}
								}
							}
							billItemList[i] = billItemObj;
						}
						//logic to prepare batches to overcome sfdc limit
						if(billItemList != null && billItemList.length > 0){
							log.info("billItemList Size ==>> "+billItemList.length);
							isOk = QueryExecuter.prepareAndExecute(billItemList,partnerConnection,BillLineItemConstantFields.EXTERN_ID__C,SFDCObjectConstants.BILL_LINE__ITEM__C);
						}
				}	
					//Inserting Expenses
					if(lstBillExpItems != null && lstBillExpItems.size() > 0){
						log.info("lstBillItems Size ==>> "+lstBillExpItems.size());
						boolean iItemFlag = false;
						SObject[] billExpList = new SObject[lstBillExpItems.size()];
						//query to get invoice based on txn id
						String billOrdQuery = "SELECT Id";
						if(setBillFields.contains(BillsConstantFields.TXNID__C) && setBillFields.contains(BillsConstantFields.REFERRENCE_NUMBER__C)){
							billOrdQuery = billOrdQuery+", TxnID__c,Referrence_Number__c FROM Bill__c where TxnID__c in (";
							iItemFlag = true;
						}
						if(iItemFlag){
							billOrdQuery = billOrdQuery+"";
							StringBuilder filter = new StringBuilder();
							int count = 1;
							//Preparing set of LISTID's
							if(setBillExpIds != null && setBillExpIds.size() > 0){
								for(String purchaseTxnId : setBillExpIds){
									if(purchaseTxnId != null){
										filter.append("\'"+purchaseTxnId+"\'");
										if(count != setBillExpIds.size())
											 filter.append(" ,");
									}
									count++;
								}
							}
							billOrdQuery = billOrdQuery+filter+")";
							log.info("purchaseOrdQuery ==>> "+billOrdQuery);
							//Retrieving Invoice__c objects from Salesforce
							QueryResult qResult = partnerConnection.query(billOrdQuery);
							if (qResult != null && qResult.getSize() > 0) { //resultset null check
								SObject[] records = qResult.getRecords();
								for(SObject sObj : records){
									log.info("sobj"+ sObj.getField(ExpensesLineItemConstantFields.TXN_ID__C));
									if(sObj != null && sObj.getField(ExpensesLineItemConstantFields.TXN_ID__C) != null){
										String strTxnID = sObj.getField(ExpensesLineItemConstantFields.TXN_ID__C).toString().trim();
										mapBillsExp.put(strTxnID, sObj);
									}
								}
							}
						}
						//preparing map for invoice and txn id
						for(int i = 0;i < lstBillExpItems.size();i++){
							SObject billExpObj = lstBillExpItems.get(i);
							log.info("billItemObj"+billExpObj);
							if(billExpObj != null){
								log.info("mapBills"+mapBillsExp);
								if(mapBillsExp != null && mapBillsExp.size() > 0){
									log.info("billItemObj.getField(TxnID__c)" +mapBillsExp.containsKey(billExpObj.getField(ExpensesLineItemConstantFields.TXN_ID__C)));
									if(billExpObj.getField(ExpensesLineItemConstantFields.TXN_ID__C) != null && mapBillsExp.containsKey(billExpObj.getField(ExpensesLineItemConstantFields.TXN_ID__C).toString())){
										log.info("true");
										if(setExpensesLineItemsFields.contains(ExpensesLineItemConstantFields.TXN_ID__C)){
											billExpObj.setField(ExpensesLineItemConstantFields.BILL__C, mapBillsExp.get(billExpObj.getField(ExpensesLineItemConstantFields.TXN_ID__C).toString()).getId());
											log.info("billItemObj fieldsss"+ billExpObj.getField(ExpensesLineItemConstantFields.TXN_ID__C));
										}
									}
								}
							}
							billExpList[i] = billExpObj;
						}
						//logic to prepare batches to overcome sfdc limit
						if(billExpList != null && billExpList.length > 0){
							log.info("billExpList Size ==>> "+billExpList.length);
							isOk = QueryExecuter.prepareAndExecute(billExpList,partnerConnection,ExpensesLineItemConstantFields.EXTERN_ID__C,SFDCObjectConstants.EXPENSE_LINE_ITEM__C);
						}
				}	
			}
			}
		}catch(Exception e){
			log.info("Exception occured in createBills method"+e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		log.info("***************** QB Bills SYNC END *****************");
		return isOk;
	}
	/**
	 * getInvoices method
	 * @param response
	 * @return String
	 * @Description This is to insert invoices records from quickbooks to sfdc invoices custom object 
	 */
	public java.lang.String getInvoices(java.lang.String response){
		log.info("***************** QB INVOICES SYNC START *****************");
		//variable declaration
		String isOk = "";// status of upsert records
		boolean flag = false;//to check sql query builds correctly with Account Object name and fields
		boolean invoiceFlag = false;//to check sql query builds correctly with invoice Object name and fields
		boolean productFlag = false;//to check sql query builds correctly with product Object name and fields
		boolean invflag=false;//to check sql query builds correctly with Object name and fields
		boolean invoiceItemFlag=false;//to check sql query builds correctly with Object name and fields
		Map<String,SObject> mapOldListIds = new HashMap<String, SObject>();//map of sobject account listIds
		Map<String,SObject> mapInvoices = new HashMap<String, SObject>();//map of invoices
		Map<String,SObject> mapProducts = new HashMap<String, SObject>();//map of products
		Map<String,String> mapTxnIds = new HashMap<String,String>();//map of txnIds
		Map<String,String> mapTxnLineIds = new HashMap<String,String>();//map of txn line ids
		Set<String> setInvoiceTxnIds = new HashSet<String>();//set of invoice txn ids
		com.appshark.qb.invoice.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.qb.invoice.InvoiceRetType> invoiceRetTypeList = null;
		//getting the data from invoiceRetTypeList jars
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.qb.invoice.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.qb.invoice.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.qb.invoice.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getInvoiceQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getInvoiceQueryRs().getInvoiceRet() != null)){
				invoiceRetTypeList = qbxml.getQBXMLMsgsRs().getInvoiceQueryRs().getInvoiceRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}

		try{
			//invoiceRetTypeList null check
			if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){
				//preparing query for invoice
				String invQuery = SFDCQueryConstants.INVOICE_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(InvoiceConstantFields.SFDC_FLAG__C);
				fieldToBeUsed.add(InvoiceConstantFields.TXNID__C);
				//building query
				Map<String,Object> returnedMap = sqlQueryBuilder.buildQueryForInvoice(invQuery,setInvoiceFields, SFDCObjectConstants.INVOICE__C, invoiceRetTypeList,fieldToBeUsed);
				//extracting return result after query building
				if(returnedMap != null && returnedMap.size() > 0){
					invflag = (boolean) returnedMap.get("flag");
					invQuery = (String) returnedMap.get("sqlQuery");
					qbInvoiceSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("accQuery ==>> "+invQuery);
					log.info("invflag ==>> "+invflag);
				}

				//check flag and prepare a map from query which was returnd from query builder
				if(invflag){	
					//Retrieving Invoice objects from Salesforce
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(InvoiceConstantFields.TXNID__C);
					fieldToBeUsed.add(InvoiceConstantFields.SFDC_FLAG__C);
					mapTxnIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(invQuery), fieldToBeUsed);					
				}
				//query to get invoice lien items based on txnline id
				String invItemQuery = SFDCQueryConstants.INVOICE_LINE_ITEM_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(InvoiceLineItemsConstantFields.SFDC_FLAG__C);
				fieldToBeUsed.add(InvoiceLineItemsConstantFields.TXNLINEID__C);
				returnedMap = sqlQueryBuilder.buildQueryInvoiceItems(invItemQuery,setInvoiceLineItemFields, SFDCObjectConstants.INVOICE_LINE_ITEM__C, invoiceRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					invoiceItemFlag = (boolean) returnedMap.get("flag");
					invItemQuery = (String) returnedMap.get("sqlQuery");					
					log.info("invItemQuery ==>> "+invItemQuery);
					log.info("invflag ==>> "+invoiceItemFlag);
				}				
				//Retrieving Invoice_Line_Item__c objects from Salesforce
				if(invflag){
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(InvoiceLineItemsConstantFields.TXNLINEID__C);
					fieldToBeUsed.add(InvoiceLineItemsConstantFields.SFDC_FLAG__C);
					mapTxnLineIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(invItemQuery), fieldToBeUsed);

				}
				log.info("mapTxnIds ==>> "+mapTxnIds.size());
				//Preparing Query for Product2
				String productQuery = "SELECT Id, Name";
				if(setProduct2Fields.contains("ListID__c")){
					productQuery = productQuery+", ListID__c FROM Product2";
					productFlag = true;
				}

				if(productFlag){
					QueryResult qResult = partnerConnection.query(productQuery);
					if (qResult != null && qResult.getSize() > 0) {
						SObject[] records = qResult.getRecords();
						if(records!=null && records.length>0){
							log.info("SFDC PRODUCT RECORDS LENGTH ==>> "+records.length);
							for(SObject sObj : records){
								if(sObj != null && sObj.getField("ListID__c") != null){//ListID__c null check
									//log.info("PRODUCT2 SOBJECT ID FROM QUERY ==>> "+sObj.getId()+" <==> "+sObj.getField("ListID__c"));
									mapProducts.put(sObj.getField("ListID__c").toString(), sObj);
								}
							}	
						}

					}
				}

				//Preparing Query for Account
				String accQuery = SFDCQueryConstants.ACCOUNT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountConstantFields.QBLISTID__C);
				fieldToBeUsed.add(AccountConstantFields.LISTID__C);
				returnedMap = sqlQueryBuilder.buildQueryForInvoiceAccount(accQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, invoiceRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					accQuery = (String) returnedMap.get("sqlQuery");
					log.info("accountQuery ==>> "+accQuery);
					log.info("invflag ==>> "+flag);
				}

				if(flag){
					//Retrieving Account objects from Salesforce
					mapOldListIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(accQuery), AccountConstantFields.LISTID__C);
				}
				//preparing list of TimeModified
				log.info("mapOldListIds ==>> "+mapOldListIds.size());
				//invoiceRetTypeList null check
				if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){
					List<SObject> lstInvoices = new ArrayList<SObject>();
					List<SObject> lstInvoiceItems = new ArrayList<SObject>();
					SObject[] objInvoiceLst = null;
					SObject[] objInvoiceList = null;
					SObject objInvoice;
					SObject objInvoiceItem;
					//iteration of invoiceRetTypeList to map sfdc invoices and qbsf invoices
					for(InvoiceRetType invoiceRetType : invoiceRetTypeList){
						log.info("invoiceRetType TxnID ==>> "+invoiceRetType.getTxnID());
						if((mapTxnIds != null && invoiceRetType.getTxnID() != null) 
								&& ((mapTxnIds.isEmpty() || (mapTxnIds.size()>0  && !mapTxnIds.containsKey(invoiceRetType.getTxnID().trim()))) || (mapTxnIds.size() > 0 && mapTxnIds.containsKey(invoiceRetType.getTxnID().trim()) && "false".equalsIgnoreCase(mapTxnIds.get(invoiceRetType.getTxnID().trim()))))){//mapTxnIds null check for controlling 2 way sync
							if((mapOldListIds != null && mapOldListIds.size() > 0) && invoiceRetType != null){ //mapOldListIds null check(account ids)
								for(String strLstId : mapOldListIds.keySet()){ //mapOldListIds key set
									if((invoiceRetType.getCustomerRef() != null && invoiceRetType.getCustomerRef().getListID() != null) && strLstId.equals(invoiceRetType.getCustomerRef().getListID().trim())){
										if(invoiceRetType.getTxnID() != null){
											if((setInvoiceFields != null && setInvoiceFields.size() > 0)/* && compFlag*/){//setInvoiceFields null check
												invoiceFlag = true;
												objInvoice = new SObject();
												objInvoice.setType(SFDCObjectConstants.INVOICE__C);
												//QB_Flag__c
												if(setInvoiceFields.contains(InvoiceConstantFields.QB_FLAG__C)){
													objInvoice.setField(InvoiceConstantFields.QB_FLAG__C,true);
												}
												//SFDC_Flag__c
												if(setInvoiceFields.contains(InvoiceConstantFields.SFDC_FLAG__C)){
													objInvoice.setField(InvoiceConstantFields.SFDC_FLAG__C,false);
												}
												//Name
												if(invoiceRetType.getRefNumber()!=null && invoiceRetType.getRefNumber()!=""){
													objInvoice.setField("Name", "Inv-"+invoiceRetType.getRefNumber());
												}												
												//Account
												if(setInvoiceFields.contains(InvoiceConstantFields.INVOICE_ACCOUNT__C)){
													objInvoice.setField(InvoiceConstantFields.INVOICE_ACCOUNT__C, mapOldListIds.get(strLstId).getId());
													log.info("Account__c Insert==>> "+invoiceRetType.getCustomerRef().getListID());
												}
												//TxnID
												log.info("LINE NO ==>> 513");
												if(setInvoiceFields.contains(InvoiceConstantFields.TXNID__C)){
													if(invoiceRetType.getTxnID() != null){
														log.info("***** TxnID__c ==>> "+invoiceRetType.getTxnID());
														objInvoice.setField(InvoiceConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
													}
												}

												//ListID__c
												if(setInvoiceFields.contains(InvoiceConstantFields.LISTID__C)){
													if(invoiceRetType.getTxnID() != null){
														objInvoice.setField(InvoiceConstantFields.LISTID__C, strLstId);
													}else{
														objInvoice.setField(InvoiceConstantFields.LISTID__C, "");
													}
												}
												//Time Created
												log.info("LINE NO ==>> 521");
												if(setInvoiceFields.contains(InvoiceConstantFields.TIME_CREATED__C)){
													if(invoiceRetType.getTimeCreated() != null){
														objInvoice.setField(InvoiceConstantFields.TIME_CREATED__C, invoiceRetType.getTimeCreated());
													}else{
														objInvoice.setField(InvoiceConstantFields.TIME_CREATED__C, "");
													}
												}
												//Time_Modified__c
												log.info("LINE NO ==>> 529");
												if(setInvoiceFields.contains(InvoiceConstantFields.TIME_MODIFIED__C)){
													if(invoiceRetType.getTimeModified() != null){
														objInvoice.setField(InvoiceConstantFields.TIME_MODIFIED__C, invoiceRetType.getTimeModified());
													}else{
														objInvoice.setField(InvoiceConstantFields.TIME_MODIFIED__C, "");
													}
												}
												//EditSequence__c
												log.info("LINE NO ==>> 537");
												if(setInvoiceFields.contains(InvoiceConstantFields.EDITSEQUENCE__C)){
													if(invoiceRetType.getEditSequence() != null){
														objInvoice.setField(InvoiceConstantFields.EDITSEQUENCE__C, invoiceRetType.getEditSequence());
													}else{
														objInvoice.setField(InvoiceConstantFields.EDITSEQUENCE__C, "");
													}
												}
												//Transaction_Number__c
												log.info("LINE NO ==>> 545");
												if(setInvoiceFields.contains(InvoiceConstantFields.TRANSACTION_NUMBER__C)){
													if(invoiceRetType.getTxnNumber() != null){
														objInvoice.setField(InvoiceConstantFields.TRANSACTION_NUMBER__C, invoiceRetType.getTxnNumber());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TRANSACTION_NUMBER__C});
													}
												}
												//CustomerRef_ListID__c
												log.info("LINE NO ==>> 553");
												if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERREF_LISTID__C)){
													if(invoiceRetType.getCustomerRef() != null && invoiceRetType.getCustomerRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.CUSTOMERREF_LISTID__C, invoiceRetType.getCustomerRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMERREF_LISTID__C});
													}
												}
												//Customer_Full_Name__c
												if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMER_FULL_NAME__C)){
													if(invoiceRetType.getCustomerRef() != null && invoiceRetType.getCustomerRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.CUSTOMER_FULL_NAME__C, invoiceRetType.getCustomerRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMER_FULL_NAME__C});
													}
												}
												//ClassRefListID__c
												log.info("LINE NO ==>> 569");
												if(setInvoiceFields.contains(InvoiceConstantFields.CLASSREFLISTID__C)){
													if(invoiceRetType.getClassRef() != null && invoiceRetType.getClassRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.CLASSREFLISTID__C, invoiceRetType.getClassRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CLASSREFLISTID__C});
													}
												}
												//ClassRefFullName__c
												log.info("LINE NO ==>> 577");
												if(setInvoiceFields.contains(InvoiceConstantFields.CLASSREFFULLNAME__C)){
													if(invoiceRetType.getClassRef() != null && invoiceRetType.getClassRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.CLASSREFFULLNAME__C, invoiceRetType.getClassRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CLASSREFFULLNAME__C});
													}
												}
												//ARAccountRef_ListID__c
												log.info("LINE NO ==>> 585");
												if(setInvoiceFields.contains(InvoiceConstantFields.ARACCOUNTREF_LISTID__C)){
													if(invoiceRetType.getARAccountRef() != null && invoiceRetType.getARAccountRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.ARACCOUNTREF_LISTID__C, invoiceRetType.getARAccountRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ARACCOUNTREF_LISTID__C});
													}
												}
												//ARAccountRef_FullName__c
												log.info("LINE NO ==>> 593");
												if(setInvoiceFields.contains(InvoiceConstantFields.ARACCOUNTREF_FULLNAME__C)){
													if(invoiceRetType.getARAccountRef() != null && invoiceRetType.getARAccountRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.ARACCOUNTREF_FULLNAME__C, invoiceRetType.getARAccountRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ARACCOUNTREF_FULLNAME__C});
													}
												}
												//TemplateRef_ListID__c
												log.info("LINE NO ==>> 601");
												if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFLISTID__C)){
													if(invoiceRetType.getTemplateRef() != null && invoiceRetType.getTemplateRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.TERMSREFLISTID__C, invoiceRetType.getTemplateRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFLISTID__C});
													}
												}
												//TemplateRef_FullName__c
												log.info("LINE NO ==>> 609");
												if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFFULLNAME__C)){
													if(invoiceRetType.getTemplateRef() != null && invoiceRetType.getTemplateRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.TERMSREFFULLNAME__C, invoiceRetType.getTemplateRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFFULLNAME__C});
													}
												}
												//Transaction_Date__c
												log.info("LINE NO ==>> 617");
												if(setInvoiceFields.contains(InvoiceConstantFields.TRANSACTION_DATE__C)){
													if(invoiceRetType.getTxnDate() != null){
														String strTransactionDate = invoiceRetType.getTxnDate();
														SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
														Date invTransactionDate = simpleDateFormat.parse(strTransactionDate);
														objInvoice.setField(InvoiceConstantFields.TRANSACTION_DATE__C, invTransactionDate);
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TRANSACTION_DATE__C});
													}
												}
												//RefNumber__c
												log.info("LINE NO ==>> 625");
												if(setInvoiceFields.contains(InvoiceConstantFields.REFNUMBER__C)){
													if(invoiceRetType.getRefNumber() != null){
														objInvoice.setField(InvoiceConstantFields.REFNUMBER__C, invoiceRetType.getRefNumber());
													}
												}
												log.info("LINE NO ==>> 633");
												//bill address
												String billAddressStreet = "";
												if(invoiceRetType.getBillAddress() != null){
													if(invoiceRetType.getBillAddress().getAddr1() != null){
														billAddressStreet = invoiceRetType.getBillAddress().getAddr1();
													}
													if(invoiceRetType.getBillAddress().getAddr2() != null){
														billAddressStreet = billAddressStreet + invoiceRetType.getBillAddress().getAddr2();
													}
													//BILLING_STREET__C
													if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_STREET__C)){
														if(billAddressStreet != null && billAddressStreet.length() > 0){
															objInvoice.setField(InvoiceConstantFields.BILLING_STREET__C, billAddressStreet);
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STREET__C});
														}
													}
													//BILLING_CITY__C
													if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_CITY__C)){
														if(invoiceRetType.getBillAddress().getCity() != null){
															objInvoice.setField(InvoiceConstantFields.BILLING_CITY__C, invoiceRetType.getBillAddress().getCity());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_CITY__C});
														}
													}
													//BILLING_STATE__C
													if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_STATE__C)){
														if(invoiceRetType.getBillAddress().getState() != null){
															objInvoice.setField(InvoiceConstantFields.BILLING_STATE__C, invoiceRetType.getBillAddress().getState());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STATE__C});
														}
													}
													//BILLING_POSTAL__C
													if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_POSTAL__C)){
														if(invoiceRetType.getBillAddress().getPostalCode() != null){
															objInvoice.setField(InvoiceConstantFields.BILLING_POSTAL__C, invoiceRetType.getBillAddress().getPostalCode());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_POSTAL__C});
														}
													}
													//BILLING_COUNTRY__C
													if(setInvoiceFields.contains(InvoiceConstantFields.BILLING_COUNTRY__C)){
														if(invoiceRetType.getBillAddress().getCountry() != null){
															objInvoice.setField(InvoiceConstantFields.BILLING_COUNTRY__C, invoiceRetType.getBillAddress().getCountry());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_COUNTRY__C});
														}
													}
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STREET__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_CITY__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_STATE__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_POSTAL__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BILLING_COUNTRY__C});
												}
												//ship address
												String shipAddressStreet = "";
												if(invoiceRetType.getShipAddress() != null){
													if(invoiceRetType.getShipAddress().getAddr1() != null){
														shipAddressStreet = invoiceRetType.getShipAddress().getAddr1();
													}
													if(invoiceRetType.getShipAddress().getAddr2() != null){
														shipAddressStreet = shipAddressStreet + invoiceRetType.getShipAddress().getAddr2();
													}
													//SHIPPING_STREET__C
													if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_STREET__C)){
														if(shipAddressStreet != null && shipAddressStreet.length() > 0){
															objInvoice.setField(InvoiceConstantFields.SHIPPING_STREET__C, shipAddressStreet);
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STREET__C});
														}
													}
													//SHIPPING_CITY__C
													if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_CITY__C)){
														if(invoiceRetType.getShipAddress().getCity() != null){
															objInvoice.setField(InvoiceConstantFields.SHIPPING_CITY__C, invoiceRetType.getShipAddress().getCity());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_CITY__C});
														}
													}
													//Shipping_State__c
													if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_STATE__C)){
														if(invoiceRetType.getShipAddress().getState() != null){
															objInvoice.setField(InvoiceConstantFields.SHIPPING_STATE__C, invoiceRetType.getShipAddress().getState());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STATE__C});
														}
													}
													//SHIPPING_POSTAL__C
													if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_POSTAL__C)){
														if(invoiceRetType.getShipAddress().getPostalCode() != null){
															objInvoice.setField(InvoiceConstantFields.SHIPPING_POSTAL__C, invoiceRetType.getShipAddress().getPostalCode());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_POSTAL__C});
														}
													}
													//SHIPPING_COUNTRY__C
													if(setInvoiceFields.contains(InvoiceConstantFields.SHIPPING_COUNTRY__C)){
														if(invoiceRetType.getShipAddress().getCountry() != null){
															objInvoice.setField(InvoiceConstantFields.SHIPPING_COUNTRY__C, invoiceRetType.getShipAddress().getCountry());
														}else{
															objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_COUNTRY__C});
														}
													}
												}else{
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STREET__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_CITY__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_STATE__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_POSTAL__C});
													objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIPPING_COUNTRY__C});
												}
												//Is_Pending__c
												log.info("LINE NO ==>> 668");
												if(setInvoiceFields.contains(InvoiceConstantFields.IS_PENDING__C)){
													if(invoiceRetType.getIsPending() != null){
														objInvoice.setField(InvoiceConstantFields.IS_PENDING__C, invoiceRetType.getIsPending());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_PENDING__C});
													}
												}
												//Is_Finance_Charge__c
												log.info("LINE NO ==>> 676");
												if(setInvoiceFields.contains(InvoiceConstantFields.IS_FINANCE_CHARGE__C)){
													if(invoiceRetType.getIsFinanceCharge() != null){
														objInvoice.setField(InvoiceConstantFields.IS_FINANCE_CHARGE__C, invoiceRetType.getIsFinanceCharge());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_FINANCE_CHARGE__C});
													}
												}
												//TermsRefListID__c
												log.info("LINE NO ==>> 684");
												if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFLISTID__C)){
													if(invoiceRetType.getTermsRef() != null && invoiceRetType.getTermsRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.TERMSREFLISTID__C, invoiceRetType.getTermsRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFLISTID__C});
													}
												}
												//TermsRefFullName__c
												log.info("LINE NO ==>> 692");
												if(setInvoiceFields.contains(InvoiceConstantFields.TERMSREFFULLNAME__C)){
													if(invoiceRetType.getTermsRef() != null && invoiceRetType.getTermsRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.TERMSREFFULLNAME__C, invoiceRetType.getTermsRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.TERMSREFFULLNAME__C});
													}
												}
												//Due_Date__c
												log.info("LINE NO ==>> 700");
												if(setInvoiceFields.contains(InvoiceConstantFields.DUE_DATE__C)){
													if(invoiceRetType.getDueDate() != null){
														String strDueDate = invoiceRetType.getDueDate();
														SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
														Date invDueDate = simpleDateFormat.parse(strDueDate);
														objInvoice.setField(InvoiceConstantFields.DUE_DATE__C, invDueDate);
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.DUE_DATE__C});
													}
												}
												//Ship_Date__c
												log.info("LINE NO ==>> 708");
												if(setInvoiceFields.contains(InvoiceConstantFields.SHIP_DATE__C)){
													if(invoiceRetType.getShipDate() != null){
														String strShipDate = invoiceRetType.getShipDate();
														SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
														Date invShipDate = simpleDateFormat.parse(strShipDate);
														objInvoice.setField(InvoiceConstantFields.SHIP_DATE__C, invShipDate);
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SHIP_DATE__C});
													}
												}
												//Subtotal__c
												log.info("LINE NO ==>> 716");
												if(setInvoiceFields.contains(InvoiceConstantFields.SUBTOTAL__C)){
													if(invoiceRetType.getSubtotal() != null){
														objInvoice.setField(InvoiceConstantFields.SUBTOTAL__C, invoiceRetType.getSubtotal());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SUBTOTAL__C});
													}
												}
												//ItemSalesTaxRefListID__c
												log.info("LINE NO ==>> 724");
												if(setInvoiceFields.contains(InvoiceConstantFields.ITEMSALESTAXREFLISTID__C)){
													if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.ITEMSALESTAXREFLISTID__C, invoiceRetType.getItemSalesTaxRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ITEMSALESTAXREFLISTID__C});
													}
												}
												//ItemSalesTaxRefFullName__c
												log.info("LINE NO ==>> 732");
												if(setInvoiceFields.contains(InvoiceConstantFields.ITEMSALESTAXREFFULLNAME__C)){
													if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.ITEMSALESTAXREFFULLNAME__C, invoiceRetType.getItemSalesTaxRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.ITEMSALESTAXREFFULLNAME__C});
													}
												}
												//Sales_Tax_Percentage__c
												log.info("LINE NO ==>> 740");
												if(setInvoiceFields.contains(InvoiceConstantFields.SALES_TAX_PERCENTAGE__C)){
													if(invoiceRetType.getSalesTaxPercentage() != null){
														objInvoice.setField(InvoiceConstantFields.SALES_TAX_PERCENTAGE__C, invoiceRetType.getSalesTaxPercentage());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SALES_TAX_PERCENTAGE__C});
													}
												}
												//Sales_Tax_Total__c
												log.info("LINE NO ==>> 748");
												if(setInvoiceFields.contains(InvoiceConstantFields.SALES_TAX_TOTAL__C)){
													if(invoiceRetType.getSalesTaxTotal() != null){
														objInvoice.setField(InvoiceConstantFields.SALES_TAX_TOTAL__C, invoiceRetType.getSalesTaxTotal());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.SALES_TAX_TOTAL__C});
													}
												}
												//Applied_Amount__c
												log.info("LINE NO ==>> 756");
												if(setInvoiceFields.contains(InvoiceConstantFields.APPLIED_AMOUNT__C)){
													if(invoiceRetType.getAppliedAmount() != null){
														objInvoice.setField(InvoiceConstantFields.APPLIED_AMOUNT__C, invoiceRetType.getAppliedAmount());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.APPLIED_AMOUNT__C});
													}
												}
												//Balance_Remaining__c
												log.info("LINE NO ==>> 764");
												if(setInvoiceFields.contains(InvoiceConstantFields.BALANCE_REMAINING__C)){
													if(invoiceRetType.getBalanceRemaining() != null){
														objInvoice.setField(InvoiceConstantFields.BALANCE_REMAINING__C, invoiceRetType.getBalanceRemaining());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.BALANCE_REMAINING__C});
													}
												}
												//Is_Paid__c
												log.info("LINE NO ==>> 772");
												if(setInvoiceFields.contains(InvoiceConstantFields.IS_PAID__C)){
													if(invoiceRetType.getIsPaid() != null){
														objInvoice.setField(InvoiceConstantFields.IS_PAID__C, invoiceRetType.getIsPaid());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_PAID__C});
													}
												}
												//Is_To_Be_Printed__c
												log.info("LINE NO ==>> 780");
												if(setInvoiceFields.contains(InvoiceConstantFields.IS_TO_BE_PRINTED__C)){
													if(invoiceRetType.getIsToBePrinted() != null){
														objInvoice.setField(InvoiceConstantFields.IS_TO_BE_PRINTED__C, invoiceRetType.getIsToBePrinted());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.IS_TO_BE_PRINTED__C});
													}
												}
												//CustomerSalesTaxCodeRefListID__c
												log.info("LINE NO ==>> 788");
												if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFLISTID__C)){
													if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getListID() != null){
														objInvoice.setField(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFLISTID__C, invoiceRetType.getItemSalesTaxRef().getListID());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMERSALESTAXCODEREFLISTID__C});
													}
												}
												//CustomerSalesTaxCodeRefFullName__c
												log.info("LINE NO ==>> 796");
												if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFFULLNAME__C)){
													if(invoiceRetType.getItemSalesTaxRef() != null && invoiceRetType.getItemSalesTaxRef().getFullName() != null){
														objInvoice.setField(InvoiceConstantFields.CUSTOMERSALESTAXCODEREFFULLNAME__C, invoiceRetType.getItemSalesTaxRef().getFullName());
													}else{
														objInvoice.setFieldsToNull(new String[]{InvoiceConstantFields.CUSTOMERSALESTAXCODEREFFULLNAME__C});
													}
												}
												lstInvoices.add(objInvoice);
												//Preparing Invoice Line Items
												List<InvoiceLineRetType> invoiceLineRetTypeList = invoiceRetType.getInvoiceLineRet();
												log.info("$$$ LINE NO ==>> 1016"+invoiceLineRetTypeList);
												if(invoiceLineRetTypeList != null && invoiceLineRetTypeList.size() > 0){//null check for list of invoice line items
													for(InvoiceLineRetType invoiceLineRetType : invoiceLineRetTypeList){// for loop of list of invoice line items
														if((mapTxnLineIds != null && invoiceLineRetType.getTxnLineID() != null) 
																&& ((mapTxnLineIds.isEmpty() || (mapTxnLineIds.size()>0  && !mapTxnLineIds.containsKey(invoiceLineRetType.getTxnLineID().trim()))) || (mapTxnLineIds.size() > 0 && mapTxnLineIds.containsKey(invoiceLineRetType.getTxnLineID().trim()) && "false".equalsIgnoreCase(mapTxnLineIds.get(invoiceLineRetType.getTxnLineID().trim()))))){
															if(invoiceLineRetType != null){
																setInvoiceTxnIds.add(invoiceRetType.getTxnID().trim());
																objInvoiceItem = new SObject();
																objInvoiceItem.setType(SFDCObjectConstants.INVOICE_LINE_ITEM__C);
																//QB_Flag__c
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.QB_FLAG__C)){
																	objInvoice.setField(InvoiceLineItemsConstantFields.QB_FLAG__C,true);
																}
																
																//SFDC_FLAG__C
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SFDC_FLAG__C)){
																	objInvoice.setField(InvoiceLineItemsConstantFields.SFDC_FLAG__C,false);
																}
																
																//name
																	objInvoiceItem.setField(InvoiceLineItemsConstantFields.INVOICE_LINE_ITEMS_NAME,"isQB");	
																
																//TxnLineID__c
																log.info("$$$ LINE NO ==>> 1273");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNLINEID__C)){
																	objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, "");
																	if(invoiceLineRetType.getTxnLineID() != null)
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, invoiceLineRetType.getTxnLineID().trim());
																}

																//TxnID__c
																log.info("$$$ LINE NO ==>> 1279");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNID__C)){
																	objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, "");
																	if(invoiceRetType.getTxnID() != null)
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
																}
																
																//Product__c
																log.info("$$$ LINE NO ==>> 1285");
																if(invoiceLineRetType.getItemRef() != null){
																	//log.info("$$$ LINE NO ==>> 1298 : " +invoiceLineRetType.getItemRef().getListID());
																	//log.info("$$$ LINE NO ==>> 1299 : " +invoiceLineRetType.getItemRef().getFullName());
																	//log.info("$$$ LINE NO ==>> 1300 : " +mapProducts.get(invoiceLineRetType.getItemRef().getListID()));
																	if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.PRODUCT__C)){
																		if(invoiceLineRetType.getItemRef().getListID() != null && (mapProducts != null && mapProducts.get(invoiceLineRetType.getItemRef().getListID()) != null)){
																			objInvoiceItem.setField(InvoiceLineItemsConstantFields.PRODUCT__C, mapProducts.get(invoiceLineRetType.getItemRef().getListID()).getId());
																		}
																	}
																}
																//ExternID__c
																log.info("$$$ LINE NO ==>> 1304");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.EXTERNID__C)){
																	objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, "");
																	if(invoiceRetType.getTxnID() != null && (invoiceLineRetType.getItemRef() != null && invoiceLineRetType.getItemRef().getListID() != null)){
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, invoiceRetType.getTxnID().trim()+" "+invoiceLineRetType.getItemRef().getListID().trim());
																	}
																}
																//Quantity__c
																log.info("$$$ LINE NO ==>> 1315");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.QUANTITY__C)){
																	if(invoiceLineRetType.getQuantity() != null){
																		log.info("$$$ invoiceLineRetType.getQuantity()" + invoiceLineRetType.getQuantity());
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.QUANTITY__C, invoiceLineRetType.getQuantity());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.QUANTITY__C});
																	}
																}
																//Amount__c
																log.info("$$$ LINE NO ==>> 1326");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.AMOUNT__C)){
																	if(invoiceLineRetType.getAmount() != null){
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.AMOUNT__C, invoiceLineRetType.getAmount());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.AMOUNT__C});
																	}
																}
																//Rate__c
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.RATE__C)){
																	if(invoiceLineRetType.getRate() != null){
																		log.info("$$$ invoiceLineRetType.getRate()" + invoiceLineRetType.getRate());
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.RATE__C, invoiceLineRetType.getRate());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.RATE__C});
																	}
																}
																//Description__c
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.DESCRIPTION__C)){
																	if(invoiceLineRetType.getDesc() != null){
																		log.info("$$$ invoiceLineRetType.getDesc()" + invoiceLineRetType.getDesc());
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.DESCRIPTION__C, invoiceLineRetType.getDesc());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.DESCRIPTION__C});
																	}
																}
																//SalesTaxCodeRef_ListID__c
																log.info("$$$ LINE NO ==>> 1332");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C)){
																	if(invoiceLineRetType.getSalesTaxCodeRef() != null && invoiceLineRetType.getSalesTaxCodeRef().getListID() != null){
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C, invoiceLineRetType.getSalesTaxCodeRef().getListID());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C});
																	}
																}
																//SalesTaxCodeRef_Full_Name__c
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C)){
																	if(invoiceLineRetType.getSalesTaxCodeRef() != null && invoiceLineRetType.getSalesTaxCodeRef().getFullName() != null){
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C, invoiceLineRetType.getSalesTaxCodeRef().getFullName());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C});
																	}
																}
																//ClassRefListID__c
																log.info("$$$ LINE NO ==>> 1345");
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.CLASSREFLISTID__C)){
																	if(invoiceLineRetType.getClassRef() != null && invoiceLineRetType.getClassRef().getListID() != null){
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.CLASSREFLISTID__C, invoiceLineRetType.getClassRef().getListID());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.CLASSREFLISTID__C});
																	}
																}
																//ClassRefFullName__c
																if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.CLASSREFFULLNAME__C)){
																	if(invoiceLineRetType.getClassRef() != null && invoiceLineRetType.getClassRef().getFullName() != null){
																		objInvoiceItem.setField(InvoiceLineItemsConstantFields.CLASSREFFULLNAME__C, invoiceLineRetType.getClassRef().getFullName());
																	}else{
																		objInvoiceItem.setFieldsToNull(new String[]{InvoiceLineItemsConstantFields.CLASSREFFULLNAME__C});
																	}
																}
																lstInvoiceItems.add(objInvoiceItem);
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					// adding list to an array
					if(lstInvoices != null && lstInvoices.size() > 0){//adding list to an array
						objInvoiceLst = new SObject[lstInvoices.size()];
						lstInvoices.toArray(objInvoiceLst);
						log.info("objAccLst********"+objInvoiceLst.toString());
					}
					//logic to upsert records to sfdc in batches to overcome sfdc limits
					if((objInvoiceLst != null && objInvoiceLst.length > 0) && invoiceFlag){						
						isOk = QueryExecuter.prepareAndExecute(objInvoiceLst,partnerConnection,InvoiceConstantFields.TXNID__C,SFDCObjectConstants.INVOICE__C);
						//Inserting Invoice Items
						if(lstInvoiceItems != null && lstInvoiceItems.size() > 0){
							log.info("lstInvoiceItems Size ==>> "+lstInvoiceItems.size());
							boolean iItemFlag = false;
							SObject[] invoiceItemList = new SObject[lstInvoiceItems.size()];
							//query to get invoice based on txn id
							String invoiceOrdQuery = "SELECT Id";
							if(setInvoiceFields.contains("TxnID__c") && setInvoiceFields.contains("RefNumber__c")){
								invoiceOrdQuery = invoiceOrdQuery+", TxnID__c,RefNumber__c FROM Invoice__c where TxnID__c in (";
								iItemFlag = true;
							}
							if(iItemFlag){
								invoiceOrdQuery = invoiceOrdQuery+"";
								StringBuilder filter = new StringBuilder();
								int count = 1;
								//Preparing set of LISTID's
								if(setInvoiceTxnIds != null && setInvoiceTxnIds.size() > 0){
									for(String invoiceTxnId : setInvoiceTxnIds){
										if(invoiceTxnId != null){
											filter.append("\'"+invoiceTxnId+"\'");
											if(count != setInvoiceTxnIds.size())
												 filter.append(" ,");
										}
										count++;
									}
								}
								invoiceOrdQuery = invoiceOrdQuery+filter+")";
								log.info("invoiceOrdQuery ==>> "+invoiceOrdQuery);
								//Retrieving Invoice__c objects from Salesforce
								QueryResult qResult = partnerConnection.query(invoiceOrdQuery);
								if (qResult != null && qResult.getSize() > 0) { //resultset null check
									SObject[] records = qResult.getRecords();
									for(SObject sObj : records){
										if(sObj != null && sObj.getField("TxnID__c") != null){
											String strTxnID = sObj.getField("TxnID__c").toString().trim();
											mapInvoices.put(strTxnID, sObj);
										}
									}
								}
							}
							//preparing map for invoice and txn id
							for(int i = 0;i < lstInvoiceItems.size();i++){
								SObject invoiceItemObj = lstInvoiceItems.get(i);
								if(invoiceItemObj != null){
									if(mapInvoices != null && mapInvoices.size() > 0){
										if(invoiceItemObj.getField("TxnID__c") != null && mapInvoices.containsKey(invoiceItemObj.getField("TxnID__c").toString())){
											if(setInvoiceLineItemFields.contains("Invoice__c")){
												invoiceItemObj.setField("Invoice__c", mapInvoices.get(invoiceItemObj.getField("TxnID__c").toString()).getId());
											}
										}
									}
								}
								invoiceItemList[i] = invoiceItemObj;
							}
							//logic to prepare batches to overcome sfdc limit
							if(invoiceItemList != null && invoiceItemList.length > 0){
								log.info("invoiceItemList Size ==>> "+invoiceItemList.length);
								isOk = QueryExecuter.prepareAndExecute(invoiceItemList,partnerConnection,InvoiceLineItemsConstantFields.EXTERNID__C,SFDCObjectConstants.INVOICE_LINE_ITEM__C);
							}
						}
					}
				}
			}
		} catch (Exception e) {//ConnectionException e			
				log.info("Exception while Inserting Invoices :"+ e.getMessage());
				if(e.getMessage()!=null){
					new MailService().sendMail(adminEmail,"Exception",e.getMessage());
				}
		}
		log.info("***************** QB INVOICES SYNC END *****************");
		return isOk;
	}
	/*not using **/
	public java.lang.String getSalesOrders(java.lang.String response){
		log.info("***************** QB SALES ORDERS SYNC START *****************");
		String isOk = "";
		boolean flag = false;
		boolean salesFlag = false;
		List<String> qbSyncDateList = new ArrayList<String>();
		String[] qbSyncDateArray = null;
		Map<String,SObject> mapOldListIds = new HashMap<String, SObject>();
		Map<String,SObject> mapSalesOrders = new HashMap<String, SObject>();
		Set<String> setAccFields = new HashSet<String>();
		Set<String> setSalesOrdFields = new HashSet<String>();
		Set<String> setSalesItemFields = new HashSet<String>();
		Set<String> setSalesOrderTxnIds = new HashSet<String>();
		com.appshark.qb.salesorder.QBXMLType qbxml = null;
		List<com.appshark.qb.salesorder.SalesOrderRetType> salesOrderRetTypeList = null;
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.qb.salesorder.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.qb.salesorder.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.qb.salesorder.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getSalesOrderQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getSalesOrderQueryRs().getSalesOrderRet() != null)){
				salesOrderRetTypeList = qbxml.getQBXMLMsgsRs().getSalesOrderQueryRs().getSalesOrderRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
		}catch (Exception e) {
			log.info("Exception ==>> " + e.toString());
		}
		try{
			if(salesOrderRetTypeList != null && salesOrderRetTypeList.size() > 0){
				//Retrieving Account Fields
				DescribeSObjectResult describeSObjectResult = partnerConnection.describeSObject("Account");
				if(describeSObjectResult != null && (describeSObjectResult.getFields() != null && describeSObjectResult.getFields().length > 0)){
					Field[] fields = describeSObjectResult.getFields();
					for(Field objField : fields){
						setAccFields.add(objField.getName());
					}
				}
				log.info("Account Fields Set ==>> "+setAccFields);
				//Retrieving SalesOrder__c Fields
				DescribeSObjectResult desSObjectResult = partnerConnection.describeSObject("SalesOrder__c");
				if(desSObjectResult != null && "SalesOrder__c".equals(desSObjectResult.getName())){
					if(desSObjectResult.getFields() != null && desSObjectResult.getFields().length > 0){
						Field[] invFields = desSObjectResult.getFields();
						for(Field objField : invFields){
							setSalesOrdFields.add(objField.getName());
						}
					}
					log.info("SalesOrder__c Fields Set ==>> "+setSalesOrdFields);
				}
				//Retrieving SalesOrderLineItem__c Fields
				DescribeSObjectResult desSObjResult = partnerConnection.describeSObject("SalesOrderLineItem__c");
				if(desSObjResult != null && "SalesOrderLineItem__c".equals(desSObjResult.getName())){
					if(desSObjResult.getFields() != null && desSObjResult.getFields().length > 0){
						Field[] invFields = desSObjResult.getFields();
						for(Field objField : invFields){
							setSalesItemFields.add(objField.getName());
						}
					}
					log.info("SalesOrderLineItem__c Fields Set ==>> "+setSalesOrdFields);
				}
				//Preparing Query for Account
				String accQuery = "SELECT Id, Name";
				if(setAccFields.contains("QBListId__c") && setAccFields.contains("ListID__c")){
					accQuery = accQuery+", ListID__c, QBListId__c, ShippingStreet, ShippingPostalCode FROM Account where QBListId__c in (";
					flag = true;
				}
				if(flag){
					accQuery = accQuery+"";
					StringBuilder filter = new StringBuilder();
					int count = 1;
					//Preparing set of LISTID's
					for(SalesOrderRetType salesOrderRetType : salesOrderRetTypeList){
						if(salesOrderRetType.getCustomerRef() != null && salesOrderRetType.getCustomerRef().getListID() != null){
							filter.append("\'"+salesOrderRetType.getCustomerRef().getListID()+"\'");
							if(count != salesOrderRetTypeList.size())
								filter.append(" ,");
						}
						count++;
					}
					accQuery = accQuery+filter+")";
					log.info("accQuery ==>> "+accQuery);
					//Retrieving Account objects from Salesforce
					QueryResult qResult = partnerConnection.query(accQuery);
					if (qResult != null && qResult.getSize() > 0) {
						SObject[] records = qResult.getRecords();
						log.info("SFDC ACC RECORDS LENGTH ==>> "+records.length);
						for(SObject sObj : records){
							if(sObj != null && sObj.getField("ListID__c") != null){
								//log.info("SOBJECT ID FROM QUERY ==>> "+sObj.getId());
								mapOldListIds.put(sObj.getField("ListID__c").toString(), sObj);
							}
						}
					}
				}
				log.info("mapOldListIds ==>> "+mapOldListIds);
				for(SalesOrderRetType salesOrderRetType : salesOrderRetTypeList){
					if(salesOrderRetType != null && salesOrderRetType.getTimeModified() != null){
						qbSyncDateList.add(salesOrderRetType.getTimeModified());
					}
				}
				log.info("Invoice TimeModified List ==>> "+qbSyncDateList.size());
				if(qbSyncDateList != null && qbSyncDateList.size() > 0){
					int cnt = 0;
					qbSyncDateArray = new String[qbSyncDateList.size()];
					for(String strTimeModified : qbSyncDateList){
						qbSyncDateArray[cnt] = strTimeModified;
						cnt++;
					}
				}
				if(qbSyncDateArray != null && qbSyncDateArray.length > 1){
					for(int i = 0;i < qbSyncDateArray.length;i++){
						for(int j = 1;j < (qbSyncDateArray.length-i);j++){
							Calendar cal = UtilityFunctions.getDateTimeByCalendar(qbSyncDateArray[j-1]);
							Calendar cal1 = UtilityFunctions.getDateTimeByCalendar(qbSyncDateArray[j]);
							if(cal.after(cal1)){
								String tmp = qbSyncDateArray[j-1];
								qbSyncDateArray[j-1] = qbSyncDateArray[j];
								qbSyncDateArray[j] = tmp;
							}
						}
					}
					log.info("Sorted Array ==> [");
					for(int i = 0;i < qbSyncDateArray.length;i++){
						log.info(qbSyncDateArray[i]+",");
					}
				}
				if(qbSyncDateArray != null && qbSyncDateArray.length > 0){
					qbSalesSynchControl = qbSyncDateArray[qbSyncDateArray.length-1];
				}
			}
			if(salesOrderRetTypeList != null && salesOrderRetTypeList.size() > 0){
				List<SObject> lstSorders = new ArrayList<SObject>();
				List<SObject> lstSorderItems = new ArrayList<SObject>();
				SObject[] objSalesOrderLst = null;
				SObject[] objSalesOrderList = null;
				SObject objSalesOrder;
				SObject objSalesItem;
				for(SalesOrderRetType salesOrderRetType : salesOrderRetTypeList){
					//log.info("invoiceRetType TxnID ==>> "+salesOrderRetType.getTxnID());
					//log.info("SR LISTID ==>> "+salesOrderRetType.getCustomerRef().getListID());
					if((mapOldListIds != null && mapOldListIds.size() > 0) && salesOrderRetType != null){
						for(String strListId : mapOldListIds.keySet()){
							if((salesOrderRetType.getCustomerRef() != null && salesOrderRetType.getCustomerRef().getListID() != null) && strListId.trim().equals(salesOrderRetType.getCustomerRef().getListID().trim())){
								if(salesOrderRetType.getTxnID() != null){
									if((setSalesOrdFields != null && setSalesOrdFields.size() > 0)/* && compFlag*/){
										salesFlag = true;
										objSalesOrder = new SObject();
										objSalesOrder.setType("SalesOrder__c");
										log.info("Account__c Insert==>> "+salesOrderRetType.getCustomerRef().getListID());
										if(setSalesOrdFields.contains("Account__c")){
											objSalesOrder.setField("Account__c", mapOldListIds.get(strListId).getId());
										}
										log.info("LINE NO ==>> 513");
										if(setSalesOrdFields.contains("TxnID__c")){
											if(salesOrderRetType.getTxnID() != null){
												objSalesOrder.setField("TxnID__c", salesOrderRetType.getTxnID().trim());
											}
										}
										if(setSalesOrdFields.contains("ListID__c")){
											if(salesOrderRetType.getTxnID() != null){
												objSalesOrder.setField("ListID__c", strListId);
											}else{
												objSalesOrder.setField("ListID__c", "");
											}
										}
										if(setSalesOrdFields.contains("TotalAmount__c")){
											if(salesOrderRetType.getTimeCreated() != null){
												objSalesOrder.setField("TotalAmount__c", salesOrderRetType.getTotalAmount());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TotalAmount__c"});
											}
										}
										log.info("LINE NO ==>> 521");
										if(setSalesOrdFields.contains("TimeCreated__c")){
											if(salesOrderRetType.getTimeCreated() != null){
												objSalesOrder.setField("TimeCreated__c", salesOrderRetType.getTimeCreated());
											}
										}
										log.info("LINE NO ==>> 529");
										if(setSalesOrdFields.contains("TimeModified__c")){
											if(salesOrderRetType.getTimeModified() != null){
												objSalesOrder.setField("TimeModified__c", salesOrderRetType.getTimeModified());
											}
										}
										log.info("LINE NO ==>> 537");
										if(setSalesOrdFields.contains("EditSequence__c")){
											if(salesOrderRetType.getEditSequence() != null){
												objSalesOrder.setField("EditSequence__c", salesOrderRetType.getEditSequence());
											}
										}
										log.info("LINE NO ==>> 545");
										if(setSalesOrdFields.contains("TxnNumber__c")){
											if(salesOrderRetType.getTxnNumber() != null){
												objSalesOrder.setField("TxnNumber__c", salesOrderRetType.getTxnNumber());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TxnNumber__c"});
											}
										}
										log.info("LINE NO ==>> 553");
										if(setSalesOrdFields.contains("CustomerRefListID__c")){
											if(salesOrderRetType.getCustomerRef() != null && salesOrderRetType.getCustomerRef().getListID() != null){
												objSalesOrder.setField("CustomerRefListID__c", salesOrderRetType.getCustomerRef().getListID());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"CustomerRefListID__c"});
											}
										}
										if(setSalesOrdFields.contains("CustomerRefFullName__c")){
											if(salesOrderRetType.getCustomerRef() != null && salesOrderRetType.getCustomerRef().getFullName() != null){
												objSalesOrder.setField("CustomerRefFullName__c", salesOrderRetType.getCustomerRef().getFullName());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"CustomerRefFullName__c"});
											}
										}
										log.info("LINE NO ==>> 569");
										if(setSalesOrdFields.contains("ClassRefListID__c")){
											if(salesOrderRetType.getClassRef() != null && salesOrderRetType.getClassRef().getListID() != null){
												objSalesOrder.setField("ClassRefListID__c", salesOrderRetType.getClassRef().getListID());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"ClassRefListID__c"});
											}
										}
										log.info("LINE NO ==>> 577");
										if(setSalesOrdFields.contains("ClassRefFullName__c")){
											if(salesOrderRetType.getClassRef() != null && salesOrderRetType.getClassRef().getFullName() != null){
												objSalesOrder.setField("ClassRefFullName__c", salesOrderRetType.getClassRef().getFullName());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"ClassRefFullName__c"});
											}
										}
										log.info("LINE NO ==>> 601");
										if(setSalesOrdFields.contains("TemplateRefListID__c")){
											if(salesOrderRetType.getTemplateRef() != null && salesOrderRetType.getTemplateRef().getListID() != null){
												objSalesOrder.setField("TemplateRefListID__c", salesOrderRetType.getTemplateRef().getListID());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TemplateRefListID__c"});
											}
										}
										log.info("LINE NO ==>> 609");
										if(setSalesOrdFields.contains("TemplateRefFullName__c")){
											if(salesOrderRetType.getTemplateRef() != null && salesOrderRetType.getTemplateRef().getFullName() != null){
												objSalesOrder.setField("TemplateRefFullName__c", salesOrderRetType.getTemplateRef().getFullName());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TemplateRefFullName__c"});
											}
										}
										log.info("LINE NO ==>> 617");
										if(setSalesOrdFields.contains("TxnDate__c")){
											if(salesOrderRetType.getTxnDate() != null){
												objSalesOrder.setField("TxnDate__c", salesOrderRetType.getTxnDate());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TxnDate__c"});
											}
										}
										log.info("LINE NO ==>> 625");
										if(setSalesOrdFields.contains("RefNumber__c")){
											if(salesOrderRetType.getRefNumber() != null){
												objSalesOrder.setField("RefNumber__c", salesOrderRetType.getRefNumber());
											}
										}
										log.info("LINE NO ==>> 633");
										//bill address
										String billAddressStreet = "";
										if(salesOrderRetType.getBillAddress() != null){
											if(salesOrderRetType.getBillAddress().getAddr1() != null){
												billAddressStreet = salesOrderRetType.getBillAddress().getAddr1();
											}
											if(salesOrderRetType.getBillAddress().getAddr2() != null){
												billAddressStreet = billAddressStreet + salesOrderRetType.getBillAddress().getAddr2();
											}
											if(setSalesOrdFields.contains("Billing_Street__c")){
												if(billAddressStreet != null && billAddressStreet.length() > 0){
													objSalesOrder.setField("Billing_Street__c", billAddressStreet);
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"Billing_Street__c"});
												}
											}
											if(setSalesOrdFields.contains("BillAddressCity__c")){
												if(salesOrderRetType.getBillAddress().getCity() != null){
													objSalesOrder.setField("BillAddressCity__c", salesOrderRetType.getBillAddress().getCity());
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"BillAddressCity__c"});
												}
											}
											if(setSalesOrdFields.contains("BillAddressState__c")){
												if(salesOrderRetType.getBillAddress().getState() != null){
													objSalesOrder.setField("BillAddressState__c", salesOrderRetType.getBillAddress().getState());
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"BillAddressState__c"});
												}
											}
											if(setSalesOrdFields.contains("BillAddressPostalCode__c")){
												if(salesOrderRetType.getBillAddress().getPostalCode() != null){
													objSalesOrder.setField("BillAddressPostalCode__c", salesOrderRetType.getBillAddress().getPostalCode());
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"BillAddressPostalCode__c"});
												}
											}
										}else{
											objSalesOrder.setFieldsToNull(new String[]{"Billing_Street__c"});
											objSalesOrder.setFieldsToNull(new String[]{"BillAddressCity__c"});
											objSalesOrder.setFieldsToNull(new String[]{"BillAddressState__c"});
											objSalesOrder.setFieldsToNull(new String[]{"BillAddressPostalCode__c"});
										}
										//ship address
										String shipAddressStreet = "";
										if(salesOrderRetType.getShipAddress() != null){
											if(salesOrderRetType.getShipAddress().getAddr1() != null){
												shipAddressStreet = salesOrderRetType.getShipAddress().getAddr1();
											}
											if(salesOrderRetType.getShipAddress().getAddr2() != null){
												shipAddressStreet = shipAddressStreet + salesOrderRetType.getShipAddress().getAddr2();
											}
											if(setSalesOrdFields.contains("Shipping_Street__c")){
												if(shipAddressStreet != null && shipAddressStreet.length() > 0){
													objSalesOrder.setField("Shipping_Street__c", shipAddressStreet);
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"Shipping_Street__c"});
												}
											}
											if(setSalesOrdFields.contains("ShipAddressCity__c")){
												if(salesOrderRetType.getShipAddress().getCity() != null){
													objSalesOrder.setField("ShipAddressCity__c", salesOrderRetType.getShipAddress().getCity());
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"ShipAddressCity__c"});
												}
											}
											if(setSalesOrdFields.contains("ShipAddressState__c")){
												if(salesOrderRetType.getShipAddress().getState() != null){
													objSalesOrder.setField("ShipAddressState__c", salesOrderRetType.getShipAddress().getState());
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"ShipAddressState__c"});
												}
											}
											if(setSalesOrdFields.contains("ShipAddressPostalCode__c")){
												if(salesOrderRetType.getShipAddress().getPostalCode() != null){
													objSalesOrder.setField("ShipAddressPostalCode__c", salesOrderRetType.getShipAddress().getPostalCode());
												}else{
													objSalesOrder.setFieldsToNull(new String[]{"ShipAddressPostalCode__c"});
												}
											}
										}else{
											objSalesOrder.setFieldsToNull(new String[]{"Shipping_Street__c"});
											objSalesOrder.setFieldsToNull(new String[]{"ShipAddressCity__c"});
											objSalesOrder.setFieldsToNull(new String[]{"ShipAddressState__c"});
											objSalesOrder.setFieldsToNull(new String[]{"ShipAddressPostalCode__c"});
										}
										log.info("LINE NO ==>> 684");
										if(setSalesOrdFields.contains("TermsRefListID__c")){
											if(salesOrderRetType.getTermsRef() != null && salesOrderRetType.getTermsRef().getListID() != null){
												objSalesOrder.setField("TermsRefListID__c", salesOrderRetType.getTermsRef().getListID());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TermsRefListID__c"});
											}
										}
										log.info("LINE NO ==>> 692");
										if(setSalesOrdFields.contains("TermsRefFullName__c")){
											if(salesOrderRetType.getTermsRef() != null && salesOrderRetType.getTermsRef().getFullName() != null){
												objSalesOrder.setField("TermsRefFullName__c", salesOrderRetType.getTermsRef().getFullName());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"TermsRefFullName__c"});
											}
										}
										log.info("LINE NO ==>> 700");
										if(setSalesOrdFields.contains("DueDate__c")){
											if(salesOrderRetType.getDueDate() != null){
												objSalesOrder.setField("DueDate__c", salesOrderRetType.getDueDate());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"DueDate__c"});
											}
										}
										log.info("LINE NO ==>> 708");
										if(setSalesOrdFields.contains("ShipDate__c")){
											if(salesOrderRetType.getShipDate() != null){
												objSalesOrder.setField("ShipDate__c", salesOrderRetType.getShipDate());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"ShipDate__c"});
											}
										}
										log.info("LINE NO ==>> 716");
										if(setSalesOrdFields.contains("Subtotal__c")){
											if(salesOrderRetType.getSubtotal() != null){
												objSalesOrder.setField("Subtotal__c", salesOrderRetType.getSubtotal());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"Subtotal__c"});
											}
										}
										log.info("LINE NO ==>> 724");
										if(setSalesOrdFields.contains("ItemSalesTaxRefListID__c")){
											if(salesOrderRetType.getItemSalesTaxRef() != null && salesOrderRetType.getItemSalesTaxRef().getListID() != null){
												objSalesOrder.setField("ItemSalesTaxRefListID__c", salesOrderRetType.getItemSalesTaxRef().getListID());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"ItemSalesTaxRefListID__c"});
											}
										}
										log.info("LINE NO ==>> 732");
										if(setSalesOrdFields.contains("ItemSalesTaxRefFullName__c")){
											if(salesOrderRetType.getItemSalesTaxRef() != null && salesOrderRetType.getItemSalesTaxRef().getFullName() != null){
												objSalesOrder.setField("ItemSalesTaxRefFullName__c", salesOrderRetType.getItemSalesTaxRef().getFullName());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"ItemSalesTaxRefFullName__c"});
											}
										}
										log.info("LINE NO ==>> 740");
										if(setSalesOrdFields.contains("SalesTaxPercentage__c")){
											if(salesOrderRetType.getSalesTaxPercentage() != null){
												objSalesOrder.setField("SalesTaxPercentage__c", salesOrderRetType.getSalesTaxPercentage());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"SalesTaxPercentage__c"});
											}
										}
										log.info("LINE NO ==>> 748");
										if(setSalesOrdFields.contains("SalesTaxTotal__c")){
											if(salesOrderRetType.getSalesTaxTotal() != null){
												objSalesOrder.setField("SalesTaxTotal__c", salesOrderRetType.getSalesTaxTotal());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"SalesTaxTotal__c"});
											}
										}
										log.info("LINE NO ==>> 780");
										if(setSalesOrdFields.contains("IsToBePrinted__c")){
											if(salesOrderRetType.getIsToBePrinted() != null){
												objSalesOrder.setField("IsToBePrinted__c", salesOrderRetType.getIsToBePrinted());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"IsToBePrinted__c"});
											}
										}
										log.info("LINE NO ==>> 788");
										if(setSalesOrdFields.contains("CustomerSalesTaxCodeRefListID__c")){
											if(salesOrderRetType.getItemSalesTaxRef() != null && salesOrderRetType.getItemSalesTaxRef().getListID() != null){
												objSalesOrder.setField("CustomerSalesTaxCodeRefListID__c", salesOrderRetType.getItemSalesTaxRef().getListID());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"CustomerSalesTaxCodeRefListID__c"});
											}
										}
										log.info("LINE NO ==>> 796");
										if(setSalesOrdFields.contains("CustomerSalesTaxCodeRefFullName__c")){
											if(salesOrderRetType.getItemSalesTaxRef() != null && salesOrderRetType.getItemSalesTaxRef().getFullName() != null){
												objSalesOrder.setField("CustomerSalesTaxCodeRefFullName__c", salesOrderRetType.getItemSalesTaxRef().getFullName());
											}else{
												objSalesOrder.setFieldsToNull(new String[]{"CustomerSalesTaxCodeRefFullName__c"});
											}
										}
										lstSorders.add(objSalesOrder);
										//Preparing Sales Line Items
										List<SalesOrderLineRetType> salesOrderLineRetTypeList = salesOrderRetType.getSalesOrderLineRet();
										log.info("$$$ LINE NO ==>> 1560"+salesOrderLineRetTypeList);
										if(salesOrderLineRetTypeList != null && salesOrderLineRetTypeList.size() > 0){
											for(SalesOrderLineRetType salesOrderLineRetType : salesOrderLineRetTypeList){
												if(salesOrderLineRetType != null){
													if(salesOrderRetType.getTxnID() != null){
														setSalesOrderTxnIds.add(salesOrderRetType.getTxnID().trim());
													}
													objSalesItem = new SObject();
													objSalesItem.setType("SalesOrderLineItem__c");
													objSalesItem.setField("TxnLineID__c", "");
													if(setSalesItemFields.contains("TxnLineID__c")){
														if(salesOrderLineRetType.getTxnLineID() != null)
															objSalesItem.setField("TxnLineID__c", salesOrderLineRetType.getTxnLineID().trim());
													}
													objSalesItem.setField("TxnID__c", "");
													if(setSalesItemFields.contains("TxnID__c")){
														if(salesOrderRetType.getTxnID() != null)
															objSalesItem.setField("TxnID__c", salesOrderRetType.getTxnID().trim());
													}
													if(setSalesItemFields.contains("ItemRefListID__c")){
														if(salesOrderLineRetType.getItemRef() != null && salesOrderLineRetType.getItemRef().getListID() != null){
															objSalesItem.setField("ItemRefListID__c", salesOrderLineRetType.getItemRef().getListID());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"ItemRefListID__c"});
														}
													}
													if(setSalesItemFields.contains("ItemRefFullName__c")){
														if(salesOrderLineRetType.getItemRef() != null && salesOrderLineRetType.getItemRef().getFullName() != null){
															objSalesItem.setField("ItemRefFullName__c", salesOrderLineRetType.getItemRef().getFullName());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"ItemRefFullName__c"});
														}
													}
													if(setSalesItemFields.contains("Desc__c")){
														if(salesOrderLineRetType.getDesc() != null){
															objSalesItem.setField("Desc__c", salesOrderLineRetType.getDesc());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"Desc__c"});
														}
													}
													if(setSalesItemFields.contains("Quantity__c")){
														if(salesOrderLineRetType.getQuantity() != null){
															objSalesItem.setField("Quantity__c", salesOrderLineRetType.getQuantity());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"Quantity__c"});
														}
													}
													if(setSalesItemFields.contains("Rate__c")){
														if(salesOrderLineRetType.getRate() != null){
															objSalesItem.setField("Rate__c", salesOrderLineRetType.getRate());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"Rate__c"});
														}
													}
													if(setSalesItemFields.contains("Amount__c")){
														if(salesOrderLineRetType.getAmount() != null){
															objSalesItem.setField("Amount__c", salesOrderLineRetType.getAmount());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"Amount__c"});
														}
													}
													if(setSalesItemFields.contains("SalesTaxCodeRefListID__c")){
														if(salesOrderLineRetType.getSalesTaxCodeRef() != null && salesOrderLineRetType.getSalesTaxCodeRef().getListID() != null){
															objSalesItem.setField("SalesTaxCodeRefListID__c", salesOrderLineRetType.getSalesTaxCodeRef().getListID());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"SalesTaxCodeRefListID__c"});
														}
													}
													if(setSalesItemFields.contains("SalesTaxCodeRefFullName__c")){
														if(salesOrderLineRetType.getSalesTaxCodeRef() != null && salesOrderLineRetType.getSalesTaxCodeRef().getFullName() != null){
															objSalesItem.setField("SalesTaxCodeRefFullName__c", salesOrderLineRetType.getSalesTaxCodeRef().getFullName());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"SalesTaxCodeRefFullName__c"});
														}
													}
													if(setSalesItemFields.contains("Invoiced__c")){
														if(salesOrderLineRetType.getInvoiced() != null){
															objSalesItem.setField("Invoiced__c", salesOrderLineRetType.getInvoiced());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"Invoiced__c"});
														}
													}
													if(setSalesItemFields.contains("IsManuallyClosed__c")){
														if(salesOrderLineRetType.getIsManuallyClosed() != null){
															objSalesItem.setField("IsManuallyClosed__c", salesOrderLineRetType.getIsManuallyClosed());
														}else{
															objSalesItem.setFieldsToNull(new String[]{"IsManuallyClosed__c"});
														}
													}
													lstSorderItems.add(objSalesItem);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				int cnt = 0;
				if(lstSorders != null && lstSorders.size() > 0){
					objSalesOrderLst = new SObject[lstSorders.size()];
					for(SObject sObj : lstSorders){
						objSalesOrderLst[cnt] = sObj;
						cnt++;
					}
				}
				if((objSalesOrderLst != null && objSalesOrderLst.length > 0) && salesFlag){
					log.info("objSalesOrderLst Size ==>> "+objSalesOrderLst.length);
					SObject[] sOrderList = null;
					int indx = 0;
					int indx1 = 1;
					int indice = 0;
					if(objSalesOrderLst.length > 200){
						objSalesOrderList = new SObject[100];
						if((objSalesOrderLst.length%100) != 0){
							sOrderList = new SObject[objSalesOrderLst.length%100];
							indice = objSalesOrderLst.length-(objSalesOrderLst.length%100);
							log.info("INDICE VALUE ==>> "+indice);
							int j = 0;
							for(int i = 0;i < objSalesOrderLst.length;i++){
								if(i >= indice){
									sOrderList[j] = objSalesOrderLst[i];
									j++;
								}
							}
						}
						for(SObject sObj : objSalesOrderLst){
							indx1++;
							objSalesOrderList[indx] = sObj;
							if(indx == 99){
								log.info("objSalesOrderList Size ==>> "+objSalesOrderList.length);
								UpsertResult[] results = partnerConnection.upsert("TxnID__c",objSalesOrderList);
								isOk = "ok";
								for (UpsertResult result : results){
									String accId = result.getId();
									com.sforce.soap.partner.Error[] errors = result.getErrors();
									for (com.sforce.soap.partner.Error error : errors){
										log.info("Sales Order Save Error ==> "+error.toString());
									}
									log.info("SALES ORDER ID ==> "+accId);
								}
								objSalesOrderList = null;
								objSalesOrderList = new SObject[100];
								indx = 0;
								continue;
							}
							if((indx1 == objSalesOrderLst.length) && (objSalesOrderLst.length%100) != 0){
								log.info("indx1 value ==> "+indx1 +" == NO OF BATCHES : "+objSalesOrderLst.length);
								log.info("sOrderList Size ==>> "+sOrderList.length);
								if(sOrderList != null && sOrderList.length > 0){
									UpsertResult[] results = partnerConnection.upsert("TxnID__c",sOrderList);
									isOk = "ok";
									for (UpsertResult result : results){
										String accId = result.getId();
										com.sforce.soap.partner.Error[] errors = result.getErrors();
										for (com.sforce.soap.partner.Error error : errors){
											log.info("Sales Order Save Error ==> "+error.toString());
										}
										log.info("SALES ORDER ID ==> "+accId);
									}
								}
							}
							indx++;
						}
					}else{
						UpsertResult[] results = partnerConnection.upsert("TxnID__c",objSalesOrderLst);
						isOk = "ok";
						for (UpsertResult result : results){
							String accId = result.getId();
							com.sforce.soap.partner.Error[] errors = result.getErrors();
							for (com.sforce.soap.partner.Error error : errors){
								log.info("Invoice Save Error ==> "+error.toString());
							}
							log.info("SALES ORDER ID ==> "+accId);
						}
					}
					//Inserting Sales Order Items
					if(lstSorderItems != null && lstSorderItems.size() > 0){
						log.info("lstSorderItems Size ==>> "+lstSorderItems.size());
						boolean sItemFlag = false;
						SObject[] salesItemList = new SObject[lstSorderItems.size()];
						//**************************
						String salesOrdQuery = "SELECT Id";
						if(setSalesItemFields.contains("TxnID__c")){
							salesOrdQuery = salesOrdQuery+", TxnID__c FROM SalesOrder__c where TxnID__c in (";
							sItemFlag = true;
						}
						if(sItemFlag){
							salesOrdQuery = salesOrdQuery+"";
							StringBuilder filter = new StringBuilder();
							int count = 1;
							//Preparing set of LISTID's
							if(setSalesOrderTxnIds != null && setSalesOrderTxnIds.size() > 0){
								for(String salesTxnId : setSalesOrderTxnIds){
									if(salesTxnId != null){
										filter.append("\'"+salesTxnId+"\'");
										if(count != setSalesOrderTxnIds.size())
											filter.append(" ,");
									}
									count++;
								}
							}
							salesOrdQuery = salesOrdQuery+filter+")";
							log.info("accQuery ==>> "+salesOrdQuery);
							//Retrieving SalesOrder__c objects from Salesforce
							QueryResult qResult = partnerConnection.query(salesOrdQuery);
							if (qResult != null && qResult.getSize() > 0) {
								SObject[] records = qResult.getRecords();
								for(SObject sObj : records){
									if(sObj != null && sObj.getField("TxnID__c") != null){
										String strTxnID = sObj.getField("TxnID__c").toString().trim();
										mapSalesOrders.put(strTxnID, sObj);
									}
								}
							}
						}
						for(int i = 0;i < lstSorderItems.size();i++){
							SObject salesItemObj = lstSorderItems.get(i);
							if(salesItemObj != null){
								if(mapSalesOrders != null && mapSalesOrders.size() > 0){
									if(salesItemObj.getField("TxnID__c") != null && mapSalesOrders.containsKey(salesItemObj.getField("TxnID__c").toString())){
										if(setSalesItemFields.contains("SalesOrder__c")){
											salesItemObj.setField("SalesOrder__c", mapSalesOrders.get(salesItemObj.getField("TxnID__c").toString()).getId());
										}
									}
								}
							}
							salesItemList[i] = salesItemObj;
						}
						if(salesItemList != null && salesItemList.length > 0){
							log.info("salesItemList Size ==>> "+salesItemList.length);
							indx = 0;
							indx1 = 1;
							indice = 0;
							SObject[] objTempList = null;
							SObject[] salItemList = null;
							if(salesItemList.length > 200){
								objTempList = new SObject[100];
								if((salesItemList.length%100) != 0){
									salItemList = new SObject[salesItemList.length%100];
									indice = salesItemList.length-(salesItemList.length%100);
									log.info("INDICE VALUE ==>> "+indice);
									int j = 0;
									for(int i = 0;i < salesItemList.length;i++){
										if(i >= indice){
											salItemList[j] = salesItemList[i];
											j++;
										}
									}
								}
								for(SObject sObj : salesItemList){
									indx1++;
									objTempList[indx] = sObj;
									if(indx == 99){
										log.info("objTempList Size ==>> "+objTempList.length);
										UpsertResult[] results = partnerConnection.upsert("TxnLineID__c",objTempList);
										isOk = "ok";
										for (UpsertResult result : results){
											String salesItemId = result.getId();
											com.sforce.soap.partner.Error[] errors = result.getErrors();
											for (com.sforce.soap.partner.Error error : errors){
												log.info("Sales Line Item Save Error ==> "+error.toString());
											}
											log.info("SALES LINE ITEM ID ==> "+salesItemId);
										}
										objTempList = null;
										objTempList = new SObject[100];
										indx = 0;
										continue;
									}
									if((indx1 == salesItemList.length) && (salesItemList.length%100) != 0){
										if(salItemList != null && salItemList.length > 0){
											UpsertResult[] results = partnerConnection.upsert("TxnLineID__c",salItemList);
											isOk = "ok";
											for (UpsertResult result : results){
												String salesItemId = result.getId();
												com.sforce.soap.partner.Error[] errors = result.getErrors();
												for (com.sforce.soap.partner.Error error : errors){
													log.info("Sales Line Item Save Error ==> "+error.toString());
												}
												log.info("SALES LINE ITEM ID ==> "+salesItemId);
											}
										}
									}
									indx++;
								}
							}else{
								UpsertResult[] results = partnerConnection.upsert("TxnLineID__c",salesItemList);
								isOk = "ok";
								for (UpsertResult result : results){
									String accId = result.getId();
									com.sforce.soap.partner.Error[] errors = result.getErrors();
									for (com.sforce.soap.partner.Error error : errors){
										log.info("Sales Line Item Save Error ==> "+error.toString());
									}
									log.info("SALES LINE ITEM ID ==> "+accId);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {//ConnectionException e
			log.info("Exception while Inserting Sales Orders :"+ e.getMessage());
		}
		log.info("***************** QB SALES ORDERS SYNC END *****************");
		return isOk;
	}

	/**
	 * @qbSynchControl method
	 * @return:void
	 * @params:string
	 * this method is to insert the last modified date in QBSynchControl__c custom object
	 */	
	public void qbSynchControl(String qbLastSynch){
		try{
			//preparing array to insert reords
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.QBSYNCHCONTROL__C);
			//SynchId__c
			if(setQBSynchFileds.contains(QBSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(QBSynchControlConstants.SYNCID__C, "100");
			}
			//QBLastSynch__c
			if(setQBSynchFileds.contains(QBSynchControlConstants.QBSYNCH__C)){
				if(qbLastSynch!=null && qbLastSynch!=""){
					objSynchControl.setField(QBSynchControlConstants.QBSYNCH__C, qbLastSynch);
				}
			}
			//objSynchControl null check to upsert
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(QBSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.QBSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in qbSynchControl method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
	}

	/**
	 * @sfdcInsertSynchControl method
	 * @return:void
	 * @params:string sfdcInsertLastSynch
	 * this method is to insert the last modified date in SFDCSynchControl__c custom object
	 * */
	public void sfdcInsertSynchControl(String sfdcInsertLastSynch){
		try{
			//preparing array to upsert records
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//SFDCLastSynch__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCLASTSYNCH__C)){
				if(sfdcInsertLastSynch!=null && sfdcInsertLastSynch!=""){
					objSynchControl.setField(SFDCSynchControlConstants.SFDCLASTSYNCH__C, sfdcInsertLastSynch);
				}
			}
			//objSynchControl null check to upsert records
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcInsertSynchControl method ==> "+e);
		}
	}

	/**
	 * @sfdcProductInsertSynchControl method
	 * @return:void
	 * @params:string sfdcInsertLastSynch
	 * this method is to insert the last modified date of products in SFDCSynchControl__c custom object
	 * */
	public void sfdcProductInsertSynchControl(String ProductInsertLastSync){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//Product_Insert_Last_Sync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.PRODUCT_INSERT_LAST_SYNC__C)){
				if(ProductInsertLastSync!=null && ProductInsertLastSync!=""){
					objSynchControl.setField(SFDCSynchControlConstants.PRODUCT_INSERT_LAST_SYNC__C, ProductInsertLastSync);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcProductInsertSynchControl method ==> "+e);
		}
	}

	/**
	 * @sfdcProductUpdateSynchControl method
	 * @return:void
	 * @params:string ProductUpdateLastSync
	 * this method is to insert the last modified date of products in SFDCSynchControl__c custom object
	 * */
	public void sfdcProductUpdateSynchControl(String ProductUpdateLastSync){
		try{
			////preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//Product_Update_Last_Sync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.PRODUCT_UPDATE_LAST_SYNC__C)){
				if(ProductUpdateLastSync!=null && ProductUpdateLastSync!=""){
					objSynchControl.setField(SFDCSynchControlConstants.PRODUCT_UPDATE_LAST_SYNC__C, ProductUpdateLastSync);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcProductUpdateSynchControl method ==> "+e);

		}
	}

	/**
	 * @sfdcOpportunityInsertSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date of Opportunity in SFDCSynchControl__c custom object
	 * */
	public void sfdcOpportunityInsertSynchControl(String opportunityInsertLastSync){
		try{
			////preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//Opportunity_Insert_Last_Sync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.OPPORTUNITY_INSERT_LAST_SYNC)){
				if(opportunityInsertLastSync!=null && opportunityInsertLastSync!=""){
					objSynchControl.setField(SFDCSynchControlConstants.OPPORTUNITY_INSERT_LAST_SYNC, opportunityInsertLastSync);
				}	
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcOpportunityInsertSynchControl method ==> "+e);
		}
	}

	/**
	 * @sfdcOpportunityUpdateSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date of Opportunity in SFDCSynchControl__c custom object
	 * */
	public void sfdcOpportunityUpdateSynchControl(String opportunityUpdateLastSync){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//Opportunity_Update_Last_Sync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.OPPORTUNITY_UPDATE_LAST_SYNC)){
				if(opportunityUpdateLastSync!=null && opportunityUpdateLastSync!=""){
					objSynchControl.setField(SFDCSynchControlConstants.OPPORTUNITY_UPDATE_LAST_SYNC, opportunityUpdateLastSync);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);	
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcOpportunityUpdateSynchControl method ==> "+e);
		}
	}
	/**
	 * @sfdcOpportunityItemUpdateSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date of Opportunity line item in SFDCSynchControl__c custom object
	 * */
	/*Added on 05/30/2016 to save the last sync date of opportunity items*/
	public void sfdcOpportunityItemUpdateSynchControl(String opportunityItemUpdateLastSync){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//Opportunity_Item_Update_Last_Sync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.OPPORTUNITY_ITEM_UPDATE_LAST_SYNC)){
				if(opportunityItemUpdateLastSync!=null && opportunityItemUpdateLastSync!=""){
					objSynchControl.setField(SFDCSynchControlConstants.OPPORTUNITY_ITEM_UPDATE_LAST_SYNC, opportunityItemUpdateLastSync);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcOpportunityUpdateSynchControl method ==> "+e);
		}
	}
	/**
	 * @sfdcUpdateSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date  line item in SFDCSynchControl__c custom object
	 * */
	public void sfdcUpdateSynchControl(String sfdcUpdateLastSynch){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//SFDCUpdateLastSynch__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCUPDATELASTSYNC__C)){
				if(sfdcUpdateLastSynch!=null && sfdcUpdateLastSynch!=""){
					objSynchControl.setField(SFDCSynchControlConstants.SFDCUPDATELASTSYNC__C, sfdcUpdateLastSynch);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcUpdateSynchControl method ==> "+e);
		}
	}

	/**
	 * @sfdcInvoiceInsertSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date  of invoice in SFDCSynchControl__c custom object
	 * */
	public void sfdcInvoiceInsertSynchControl(String sfdcInvInsertLastSynch){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//SFDCUpdateLastSynch__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCINVOICEINSERTLASTSYNC__C)){
				if(sfdcInvInsertLastSynch!=null && sfdcInvInsertLastSynch!=""){
					objSynchControl.setField(SFDCSynchControlConstants.SFDCINVOICEINSERTLASTSYNC__C, sfdcInvInsertLastSynch);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcInvoiceInsertSynchControl method ==> "+e);
		}
	}
	/**
	 * @sfdcInvoiceItemUpdateSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date  of invoice line item in SFDCSynchControl__c custom object
	 * */
	/*Added on 05/30/2016 get save the last sync date of opportunity items*/
	public void sfdcInvoiceItemUpdateSynchControl(String sfdcInvItemUpdateLastSynch){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//Invoice_Item_Update_Last_Sync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.INVOICE_ITEM_UPDATE_LAST_SYNC__C)){
				if(sfdcInvItemUpdateLastSynch!=null && sfdcInvItemUpdateLastSynch!=""){
					objSynchControl.setField(SFDCSynchControlConstants.INVOICE_ITEM_UPDATE_LAST_SYNC__C, sfdcInvItemUpdateLastSynch);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcInvItemUpdateLastSynch method ==> "+e);
		}
	}

	/**
	 * @sfdcInvoiceUpdateSynchControl method
	 * @return:void
	 * @params:string opportunityInsertLastSync
	 * this method is to insert the last modified date  of invoice line item in SFDCSynchControl__c custom object
	 * */
	public void sfdcInvoiceUpdateSynchControl(String sfdcInvUpdateLastSynch){
		try{
			//preparing array by mapping fields
			SObject objSynchControl = new SObject();
			objSynchControl.setType(SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			//SynchId__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SYNCID__C)){
				objSynchControl.setField(SFDCSynchControlConstants.SYNCID__C, "300");
			}
			//SFDCInvoiceUpdateLastSync__c
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCINVOICEUPDATELASTSYNC__C)){
				if(sfdcInvUpdateLastSynch!=null && sfdcInvUpdateLastSynch!=""){
					objSynchControl.setField(SFDCSynchControlConstants.SFDCINVOICEUPDATELASTSYNC__C, sfdcInvUpdateLastSynch);
				}
			}
			//objSynchControl null check
			if(objSynchControl!=null){
				UpsertResult[] results = partnerConnection.upsert(SFDCSynchControlConstants.SYNCID__C,new SObject[]{objSynchControl});
				PartnerConnectionErrorLogging.logUpsertErrors(results,SFDCObjectConstants.SFDCSYNCHCONTROL__C);
			}

		}catch(Exception e){
			log.info("Exception Occurred in sfdcInvoiceUpdateSynchControl method ==> "+e);
		}
	}
	/**
	 * @getQBSynchControl method
	 * @return:string
	 * @params:
	 * this method is to get the last sync date from QBSynchControl__c
	 * */
	public String getQBSynchControl(){
		boolean flag = false;//checks sfdc query building
		String qbSynchControl = "";//lastModifed date
		try{
			//query to get the last sync date
			String synchQuery = "SELECT Id";
			if(setQBSynchFileds.contains(QBSynchControlConstants.QBSYNCH__C)){
				log.info("synchQuery ifff ==>> "+ synchQuery);
				synchQuery = synchQuery+", QBLastSynch__c,SynchId__c FROM QBSynchControl__c";
				flag = true;
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				if (qResult!=null && qResult.getSize() > 0) {//qResult from query
					SObject[] records = qResult.getRecords();
					qbSynchControl = records[0].getField(QBSynchControlConstants.QBSYNCH__C).toString();
				}
			}
		}catch(Exception e){
			log.info("Exception Occurred in getQBSynchControl zero parameters method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return qbSynchControl;
	}

	/**
	 * @lastSynchUpdate method
	 * @return:void
	 * @params:
	 * adding the last sync date
	 * */
	public void lastSynchUpdate(){
		List<String> syncList = new ArrayList<String>();//contains last modified date of all objects from qb
		String[] strSynchArr = null;
		if(qbAccSynchControl != null){
			syncList.add(qbAccSynchControl);
		}
		if(qbVenSynchControl != null){
			syncList.add(qbVenSynchControl);
		}
		if(qbInvoiceSynchControl != null){
			syncList.add(qbInvoiceSynchControl);
		}
		if(qbSalesSynchControl != null){
			syncList.add(qbSalesSynchControl);
		}
		if(qbItemSynchControl != null){
			syncList.add(qbItemSynchControl);
		}
		if(qbItemInvSynchControl != null){
			syncList.add(qbItemInvSynchControl);
		}
		if(qbEstimateSynchControl != null){
			syncList.add(qbEstimateSynchControl);
		}
		if(qbPurchaseOrderSynchControl != null){
			syncList.add(qbPurchaseOrderSynchControl);
		}
		if(qbBillSynchControl != null){
			syncList.add(qbBillSynchControl);
		}
		if(qbAcctingSynchControl != null){
			syncList.add(qbAcctingSynchControl);
		}
		
		if(syncList != null && syncList.size() > 0){
			strSynchArr = new String[syncList.size()];
			for(int i = 0;i < syncList.size();i++){
				strSynchArr[i] = syncList.get(i);
			}
		}
		if(strSynchArr != null && strSynchArr.length > 0){
			for(int i = 0;i < strSynchArr.length;i++){
				for(int j = 1;j < (strSynchArr.length-i);j++){
					Calendar cal = UtilityFunctions.getDateTimeByCalendar(strSynchArr[j-1]);
					Calendar cal1 = UtilityFunctions.getDateTimeByCalendar(strSynchArr[j]);
					if(cal.after(cal1)){
						String tmp = strSynchArr[j-1];
						strSynchArr[j-1] = strSynchArr[j];
						strSynchArr[j] = tmp;
					}
				}
			}
			this.qbSynchControl(strSynchArr[strSynchArr.length-1]);
		}
	}
	/**
	 * insertAccounts method
	 * @param 
	 * @return String
	 * @Description This is to insert accounts records from sfdc to qbsf customers
	 */
	public String insertAccounts(){
		String reqXML = "";//request xml query to update accounts in qb
		//get sfdc accounts data 
		List<SObject> accountList = getSFDCAccountsData();//list of accounts data
		if(accountList != null && accountList.size() > 0){//accountList null check
			log.info("accountList ==>> "+accountList);
			log.info("accountList size ==>> "+accountList.size());
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"12.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">";
			for(SObject sObject : accountList){  //accountList iteration to insert records to qbsf
				//ACCOUNT_NAME
				if(sObject.getField(AccountConstantFields.ACCOUNT_NAME) != null){
					log.info("sObject.getField ==>> "+sObject.getField(AccountConstantFields.ACCOUNT_NAME));
					log.info("sObject.getField1 ==>> "+sObject.getField(AccountConstantFields.ACCOUNT_NAME).toString());
					String accountName = sObject.getField(AccountConstantFields.ACCOUNT_NAME).toString();
					if(accountName != null && accountName.length() > 0 && accountName.contains(":")){
						accountName = accountName.replaceAll(":", " ");
					}
					reqXML = reqXML +"<CustomerAddRq requestID=\""+System.currentTimeMillis()+"\""+">"
							+ "<CustomerAdd>"
							+"<Name>"+accountName+"</Name>";
					//ACTIVE__C
					if(sObject.getField(AccountConstantFields.ACTIVE__C) !=null){
						String isActive = sObject.getField(AccountConstantFields.ACTIVE__C).toString().trim();
						if("Yes".equals(isActive)){
							reqXML = reqXML + "<IsActive>true</IsActive>";
						}else if("No".equals(isActive)){
							reqXML = reqXML + "<IsActive>false</IsActive>";
						}
					}
					//COMPANY_NAME__C
					if(sObject.getField(AccountConstantFields.COMPANY_NAME__C) !=null){
						reqXML = reqXML + "<CompanyName>"+sObject.getField(AccountConstantFields.COMPANY_NAME__C)+"</CompanyName>";
					}else{
						reqXML = reqXML + "<CompanyName></CompanyName>";
					}
					reqXML = reqXML + "<Salutation></Salutation>";
					reqXML = reqXML + "<FirstName></FirstName>"
							+"<MiddleName></MiddleName>"
							+"<LastName></LastName>";
					//JOB_TITLE__C
					if(sObject.getField(AccountConstantFields.JOB_TITLE__C) !=null){
						reqXML = reqXML + "<JobTitle>"+sObject.getField(AccountConstantFields.JOB_TITLE__C)+"</JobTitle>";
					}else{
						reqXML = reqXML + "<JobTitle></JobTitle>";
					}
					reqXML = reqXML + "<BillAddress>";
					//BILLINGSTREET
					if(sObject.getField(AccountConstantFields.BILLINGSTREET) != null){
						String billingStreet = (String)sObject.getField(AccountConstantFields.BILLINGSTREET);
						if(billingStreet.trim().length() > 40){
							billingStreet = billingStreet.substring(0, 39);
						}
						reqXML = reqXML + "<Addr1>"+billingStreet+"</Addr1>";
					}else{
						reqXML = reqXML + "<Addr1></Addr1>";
					}
					//BILLINGCITY
					if(sObject.getField(AccountConstantFields.BILLINGCITY) !=null){
						reqXML = reqXML + "<City>"+sObject.getField(AccountConstantFields.BILLINGCITY)+"</City>";
					}else{
						reqXML = reqXML + "<City></City>";
					}
					//BILLINGSTATE
					if(sObject.getField(AccountConstantFields.BILLINGSTATE) != null){
						reqXML = reqXML + "<State>"+sObject.getField(AccountConstantFields.BILLINGSTATE)+"</State>";
					}else{
						reqXML = reqXML + "<State></State>";
					}
					//BILLINGPOSTALCODE
					if(sObject.getField(AccountConstantFields.BILLINGPOSTALCODE) != null){
						reqXML = reqXML + "<PostalCode>"+sObject.getField(AccountConstantFields.BILLINGPOSTALCODE)+"</PostalCode>";
					}else{
						reqXML = reqXML + "<PostalCode></PostalCode>";
					}
					//BILLINGCOUNTRY
					if(sObject.getField(AccountConstantFields.BILLINGCOUNTRY) != null){
						reqXML = reqXML + "<Country>"+sObject.getField(AccountConstantFields.BILLINGCOUNTRY)+"</Country>";
					}else{
						reqXML = reqXML + "<Country></Country>";
					}
					reqXML = reqXML + "</BillAddress>";
					//SHIPPINGSTREET
					if(sObject.getField(AccountConstantFields.SHIPPINGSTREET) != null){
						reqXML = reqXML + "<ShipToAddress>";
						reqXML = reqXML + "<Name>Ship To Addr</Name>";
						if(sObject.getField(AccountConstantFields.SHIPPINGSTREET) != null){
							String shippingStreet = (String)sObject.getField(AccountConstantFields.SHIPPINGSTREET);
							if(shippingStreet.trim().length() > 40){
								shippingStreet = shippingStreet.substring(0, 39);
							}
							reqXML = reqXML + "<Addr1>"+shippingStreet+"</Addr1>";
							reqXML = reqXML + "<Addr2></Addr2>";
							reqXML = reqXML + "<Addr3></Addr3>";
							reqXML = reqXML + "<Addr4></Addr4>";
							reqXML = reqXML + "<Addr5></Addr5>";
						}else{
							reqXML = reqXML + "<Addr1></Addr1>";
							reqXML = reqXML + "<Addr2></Addr2>";
							reqXML = reqXML + "<Addr3></Addr3>";
							reqXML = reqXML + "<Addr4></Addr4>";
							reqXML = reqXML + "<Addr5></Addr5>";
						}
						//SHIPPINGCITY
						if(sObject.getField(AccountConstantFields.SHIPPINGCITY) !=null){
							reqXML = reqXML + "<City>"+sObject.getField(AccountConstantFields.SHIPPINGCITY)+"</City>";
						}else{
							reqXML = reqXML + "<City></City>";
						}
						//SHIPPINGSTATE
						if(sObject.getField(AccountConstantFields.SHIPPINGSTATE) != null){
							reqXML = reqXML + "<State>"+sObject.getField(AccountConstantFields.SHIPPINGSTATE)+"</State>";
						}else{
							reqXML = reqXML + "<State></State>";
						}
						//SHIPPINGPOSTALCODE
						if(sObject.getField(AccountConstantFields.SHIPPINGPOSTALCODE) != null){
							reqXML = reqXML + "<PostalCode>"+sObject.getField(AccountConstantFields.SHIPPINGPOSTALCODE)+"</PostalCode>";
						}else{
							reqXML = reqXML + "<PostalCode></PostalCode>";
						}
						//SHIPPINGCOUNTRY
						if(sObject.getField(AccountConstantFields.SHIPPINGCOUNTRY) != null){
							reqXML = reqXML + "<Country>"+sObject.getField(AccountConstantFields.SHIPPINGCOUNTRY)+"</Country>";
						}else{
							reqXML = reqXML + "<Country></Country>";
						}
						reqXML = reqXML + "<Note></Note> <DefaultShipTo >true</DefaultShipTo> </ShipToAddress>";
					}
					//PHONE
					if(sObject.getField(AccountConstantFields.PHONE) != null){
						reqXML = reqXML + "<Phone>"+sObject.getField(AccountConstantFields.PHONE)+"</Phone>";
					}else{
						reqXML = reqXML + "<Phone></Phone>";
					}
					//FAX
					if(sObject.getField(AccountConstantFields.FAX) != null){
						reqXML = reqXML + "<Fax>"+sObject.getField(AccountConstantFields.FAX)+"</Fax>";
					}else{
						reqXML = reqXML + "<Fax></Fax>";
					}
					//EMAIL__C
					if(sObject.getField(AccountConstantFields.EMAIL__C) != null){
						reqXML = reqXML + "<Email>"+sObject.getField(AccountConstantFields.EMAIL__C)+"</Email>";
					}else{
						reqXML = reqXML + "<Email></Email>";
					}
					reqXML = reqXML + "<Contact></Contact>"		 
							+"</CustomerAdd>"
							+ "</CustomerAddRq>";
				}
			}
			reqXML = reqXML + "</QBXMLMsgsRq>"
					+"</QBXML>";
		}else{
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"7.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">"
					+"</QBXMLMsgsRq>"
					+"</QBXML>";
		}
		log.info("reqXML from insert Accounts"+reqXML);
		return reqXML;

	}
	/**
	 * updateAccounts method
	 * @param 
	 * @return String
	 * @Description This is to update accounts records from sfdc to qbsf customers
	 */
	public String updateAccounts(){
		String reqXML = "";
		//get sfdc updated accounts data based on last sync date
		List<SObject> accountList = getSFDCUpdatedAccountsData();//list of accounts data
		if(accountList != null && accountList.size() > 0){//accountList null check
			log.info("accountList ==>> "+accountList);
			log.info("accountList size ==>> "+accountList.size());
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"12.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">";
			for(SObject sObject : accountList){  //iteration of accountList
				if(sObject.getField(AccountConstantFields.ACCOUNT_NAME) != null && (sObject.getField(AccountConstantFields.LISTID__C) != null && sObject.getField(AccountConstantFields.LISTID__C).toString().length() > 0) && 
						(sObject.getField(AccountConstantFields.EDITSEQUENCE__C) != null && sObject.getField(AccountConstantFields.EDITSEQUENCE__C).toString().length() > 0)){
					log.info("sObject.getField ==>> "+sObject.getField(AccountConstantFields.ACCOUNT_NAME));
					log.info("sObject.getField1 ==>> "+sObject.getField(AccountConstantFields.ACCOUNT_NAME).toString());
					String accountName = sObject.getField(AccountConstantFields.ACCOUNT_NAME).toString();
					if(accountName != null && accountName.length() > 0 && accountName.contains(":")){
						accountName = accountName.replaceAll(":", " ");
					}
					reqXML = reqXML + "<CustomerModRq requestID=\""+System.currentTimeMillis()+"\""+">"
							+ "<CustomerMod>"
							+" <ListID>"+sObject.getField(AccountConstantFields.LISTID__C)+"</ListID>"
							+" <EditSequence>"+sObject.getField(AccountConstantFields.EDITSEQUENCE__C)+"</EditSequence>"
							+"<Name>"+accountName+"</Name>";
					//ACTIVE__C
					if(sObject.getField(AccountConstantFields.ACTIVE__C) !=null){
						String isActive = sObject.getField(AccountConstantFields.ACTIVE__C).toString().trim();
						if("Yes".equals(isActive)){
							reqXML = reqXML + "<IsActive>true</IsActive>";
						}else if("No".equals(isActive)){
							reqXML = reqXML + "<IsActive>false</IsActive>";
						}
					}
					//COMPANY_NAME__C
					if(sObject.getField(AccountConstantFields.COMPANY_NAME__C) !=null){
						log.info("CompanyName__c ==>> "+sObject.getField(AccountConstantFields.COMPANY_NAME__C));
						reqXML = reqXML + "<CompanyName>"+sObject.getField(AccountConstantFields.COMPANY_NAME__C)+"</CompanyName>";
					}else{
						reqXML = reqXML + "<CompanyName></CompanyName>";
					}
					reqXML = reqXML + "<Salutation></Salutation>";
					reqXML = reqXML + "<FirstName></FirstName>"
							+"<MiddleName></MiddleName>"
							+"<LastName></LastName>";
					//JOB_TITLE__C
					if(sObject.getField(AccountConstantFields.JOB_TITLE__C) !=null){
						reqXML = reqXML + "<JobTitle>"+sObject.getField(AccountConstantFields.JOB_TITLE__C)+"</JobTitle>";
					}else{
						reqXML = reqXML + "<JobTitle></JobTitle>";
					}
					reqXML = reqXML + "<BillAddress>";
					//BILLINGSTREET
					if(sObject.getField(AccountConstantFields.BILLINGSTREET) != null){
						String billingStreet = (String)sObject.getField(AccountConstantFields.BILLINGSTREET);
						if(billingStreet.trim().length() > 40){
							billingStreet = billingStreet.substring(0, 39);
						}
						reqXML = reqXML + "<Addr1>"+billingStreet+"</Addr1>";
						reqXML = reqXML + "<Addr2></Addr2>";
						reqXML = reqXML + "<Addr3></Addr3>";
						reqXML = reqXML + "<Addr4></Addr4>";
						reqXML = reqXML + "<Addr5></Addr5>";
					}else{
						reqXML = reqXML + "<Addr1></Addr1>";
						reqXML = reqXML + "<Addr2></Addr2>";
						reqXML = reqXML + "<Addr3></Addr3>";
						reqXML = reqXML + "<Addr4></Addr4>";
						reqXML = reqXML + "<Addr5></Addr5>";
					}
					//BILLINGCITY
					if(sObject.getField(AccountConstantFields.BILLINGCITY) !=null){
						reqXML = reqXML + "<City>"+sObject.getField(AccountConstantFields.BILLINGCITY)+"</City>";
					}else{
						reqXML = reqXML + "<City></City>";
					}
					//BILLINGSTATE
					if(sObject.getField(AccountConstantFields.BILLINGSTATE) != null){
						reqXML = reqXML + "<State>"+sObject.getField(AccountConstantFields.BILLINGSTATE)+"</State>";
					}else{
						reqXML = reqXML + "<State></State>";
					}
					//BILLINGPOSTALCODE
					if(sObject.getField(AccountConstantFields.BILLINGPOSTALCODE) != null){
						reqXML = reqXML + "<PostalCode>"+sObject.getField(AccountConstantFields.BILLINGPOSTALCODE)+"</PostalCode>";
					}else{
						reqXML = reqXML + "<PostalCode></PostalCode>";
					}
					//BILLINGCOUNTRY
					if(sObject.getField(AccountConstantFields.BILLINGCOUNTRY) != null){
						reqXML = reqXML + "<Country>"+sObject.getField(AccountConstantFields.BILLINGCOUNTRY)+"</Country>";
					}else{
						reqXML = reqXML + "<Country></Country>";
					}
					reqXML = reqXML + "</BillAddress>";
					//SHIPPINGSTREET
					log.info("### Acc ShippingStreet ==>> "+sObject.getField(AccountConstantFields.SHIPPINGSTREET));
					if(sObject.getField(AccountConstantFields.SHIPPINGSTREET) != null){
						reqXML = reqXML + "<ShipToAddress>";
						reqXML = reqXML + "<Name>Ship To Addr</Name>";
						if(sObject.getField(AccountConstantFields.SHIPPINGSTREET) != null){
							String shippingStreet = (String)sObject.getField(AccountConstantFields.SHIPPINGSTREET);
							if(shippingStreet.trim().length() > 40){
								shippingStreet = shippingStreet.substring(0, 39);
							}
							reqXML = reqXML + "<Addr1>"+shippingStreet+"</Addr1>";
							reqXML = reqXML + "<Addr2></Addr2>";
							reqXML = reqXML + "<Addr3></Addr3>";
							reqXML = reqXML + "<Addr4></Addr4>";
							reqXML = reqXML + "<Addr5></Addr5>";
						}else{
							reqXML = reqXML + "<Addr1></Addr1>";
							reqXML = reqXML + "<Addr2></Addr2>";
							reqXML = reqXML + "<Addr3></Addr3>";
							reqXML = reqXML + "<Addr4></Addr4>";
							reqXML = reqXML + "<Addr5></Addr5>";
						}
						//SHIPPINGCITY
						if(sObject.getField(AccountConstantFields.SHIPPINGCITY) !=null){
							reqXML = reqXML + "<City>"+sObject.getField(AccountConstantFields.SHIPPINGCITY)+"</City>";
						}else{
							reqXML = reqXML + "<City></City>";
						}
						//SHIPPINGSTATE
						if(sObject.getField(AccountConstantFields.SHIPPINGSTATE) != null){
							reqXML = reqXML + "<State>"+sObject.getField(AccountConstantFields.SHIPPINGSTATE)+"</State>";
						}else{
							reqXML = reqXML + "<State></State>";
						}
						//SHIPPINGPOSTALCODE
						if(sObject.getField(AccountConstantFields.SHIPPINGPOSTALCODE) != null){
							reqXML = reqXML + "<PostalCode>"+sObject.getField(AccountConstantFields.SHIPPINGPOSTALCODE)+"</PostalCode>";
						}else{
							reqXML = reqXML + "<PostalCode></PostalCode>";
						}
						//SHIPPINGCOUNTRY
						if(sObject.getField(AccountConstantFields.SHIPPINGCOUNTRY) != null){
							reqXML = reqXML + "<Country>"+sObject.getField(AccountConstantFields.SHIPPINGCOUNTRY)+"</Country>";
						}else{
							reqXML = reqXML + "<Country></Country>";
						}
						reqXML = reqXML + "<Note></Note> <DefaultShipTo >true</DefaultShipTo> </ShipToAddress>";
					}
					//PHONE
					if(sObject.getField(AccountConstantFields.PHONE) != null){
						reqXML = reqXML + "<Phone>"+sObject.getField(AccountConstantFields.PHONE)+"</Phone>";
					}else{
						reqXML = reqXML + "<Phone></Phone>";
					}
					//FAX
					if(sObject.getField(AccountConstantFields.FAX) != null){
						reqXML = reqXML + "<Fax>"+sObject.getField(AccountConstantFields.FAX)+"</Fax>";
					}else{
						reqXML = reqXML + "<Fax></Fax>";
					}
					//EMAIL__C
					if(sObject.getField(AccountConstantFields.EMAIL__C) != null){
						reqXML = reqXML + "<Email>"+sObject.getField(AccountConstantFields.EMAIL__C)+"</Email>";
					}else{
						reqXML = reqXML + "<Email></Email>";
					}
					reqXML = reqXML + "<Contact></Contact>" 		 
							+"</CustomerMod>"
							+ "</CustomerModRq>";
				}
			}
			reqXML = reqXML +"</QBXMLMsgsRq>"
					+"</QBXML>";
		}else{
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"7.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">"
					+"</QBXMLMsgsRq>"
					+"</QBXML>";
		}
		return reqXML;
	}
	/**
	 * insertInvoices method
	 * @param 
	 * @return String
	 * @Description This is to insert invoices records from sfdc to qbsf invoices
	 */
	public String insertInvoices(){
		Set<String> setInvoiceIds = new HashSet<String>();//set of invoice ids
		Map<String,List<SObject>> mapInvoiceLineItem = new HashMap<String,List<SObject>>();//contains list of sobject
		String reqXML = "";//request xml to update invoices in qb

		try{
			//get sfdc invoices data
			List<SObject> invoiceList = getSFDCInvoicesData();
			if(invoiceList != null && invoiceList.size() > 0){//invoiceList null check
				for(SObject sObject : invoiceList){
					setInvoiceIds.add((String)sObject.getField("Id"));//adding sobject ids to a set
				}

				if(setInvoiceIds != null && setInvoiceIds.size() > 0){//set invoice ids null check
					String condition = "";
					//query to get invoice line items based on invoices
					String invoiceLineItemQuery = "select Id,Name,Quantity__c,Invoice__c,Product__r.Name,Product__r.ProductCode,Product__r.Rate__c,Product__r.Description from Invoice_Line_Item__c where Invoice__c in (";
					int count = 1;
					for(String invoiceId : setInvoiceIds){
						condition = condition+"'"+invoiceId+"'";
						if(count != setInvoiceIds.size()){
							condition = condition+",";
						}
						if(count == setInvoiceIds.size()){
							condition = condition+")";
							invoiceLineItemQuery = invoiceLineItemQuery+condition;
						}
						count++;
					}

					log.info("@@@ invoiceLineItemQuery ==>> "+invoiceLineItemQuery);
					QueryResult queryResult = partnerConnection.query(invoiceLineItemQuery);
					if (queryResult!=null && queryResult.getSize() > 0) {//query result null check
						SObject[] accRecords = queryResult.getRecords();
						if(accRecords != null && accRecords.length > 0){//records from query result null check
							for(SObject sObject : accRecords){//preparing map of invoices and sobject
								if(mapInvoiceLineItem.containsKey(sObject.getField("Invoice__c"))){
									mapInvoiceLineItem.get(sObject.getField("Invoice__c")).add(sObject);
								}else{
									List<SObject> invoiceLineItemList = new ArrayList<SObject>();
									invoiceLineItemList.add(sObject);
									mapInvoiceLineItem.put((String)sObject.getField("Invoice__c"), invoiceLineItemList);
								}
							}
						}
					}
				}
			}
			//preparing xml query to insert records to qbsf
			if(invoiceList != null && invoiceList.size() > 0){//invoiceList null check
				log.info("invoiceList ==>> "+invoiceList);
				log.info("invoiceList size ==>> "+invoiceList.size());
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"7.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">";
				for(SObject sObject : invoiceList){  
					com.sforce.ws.bind.XmlObject subSobject = (com.sforce.ws.bind.XmlObject)sObject.getField("Account__r");
					if(subSobject != null && subSobject.getField(InvoiceConstantFields.LISTID__C) != null){
						log.info("$$$ Acc List Id ==> "+subSobject.getField(InvoiceConstantFields.LISTID__C));
						if((subSobject != null && subSobject.getField(InvoiceConstantFields.LISTID__C) != null) && sObject.getField(InvoiceConstantFields.REFNUMBER__C) != null){

							reqXML = reqXML +"<InvoiceAddRq requestID=\""+System.currentTimeMillis()+"\""+">"
									+ "<InvoiceAdd>"
									+ "<CustomerRef>"
									+ "<ListID>"+subSobject.getField(InvoiceConstantFields.LISTID__C)+"</ListID>"
									+ "</CustomerRef>";
							//TRANSACTION_DATE__C
							if(sObject.getField(InvoiceConstantFields.TRANSACTION_DATE__C) != null){
								reqXML = reqXML + "<TxnDate>"+sObject.getField(InvoiceConstantFields.TRANSACTION_DATE__C)+"</TxnDate>";
							}else{
								reqXML = reqXML + "<TxnDate></TxnDate>";
							}
							//REFNUMBER__C
							if(sObject.getField(InvoiceConstantFields.REFNUMBER__C) != null){
								reqXML = reqXML + "<RefNumber>"+sObject.getField(InvoiceConstantFields.REFNUMBER__C)+"</RefNumber>";
							}else{
								reqXML = reqXML + "<RefNumber></RefNumber>";
							}
							//BILLING_STREET__C
							reqXML = reqXML + "<BillAddress>";
							if(sObject.getField(InvoiceConstantFields.BILLING_STREET__C) != null){
								String billingStreet = (String)sObject.getField(InvoiceConstantFields.BILLING_STREET__C);
								if(billingStreet.trim().length() > 40){
									billingStreet = billingStreet.substring(0, 39);
								}
								reqXML = reqXML + "<Addr1>"+billingStreet+"</Addr1>";
								reqXML = reqXML + "<Addr2></Addr2>";
								reqXML = reqXML + "<Addr3></Addr3>";
							}else{
								reqXML = reqXML + "<Addr1></Addr1>";
								reqXML = reqXML + "<Addr2></Addr2>";
								reqXML = reqXML + "<Addr3></Addr3>";
							}
							//BILLING_CITY__C
							if(sObject.getField(InvoiceConstantFields.BILLING_CITY__C) !=null){
								reqXML = reqXML + "<City>"+sObject.getField(InvoiceConstantFields.BILLING_CITY__C)+"</City>";
							}else{
								reqXML = reqXML + "<City></City>";
							}
							//BILLING_STATE__C
							if(sObject.getField(InvoiceConstantFields.BILLING_STATE__C) != null){
								reqXML = reqXML + "<State>"+sObject.getField(InvoiceConstantFields.BILLING_STATE__C)+"</State>";
							}else{
								reqXML = reqXML + "<State></State>";
							}
							//BILLING_POSTAL__C
							if(sObject.getField(InvoiceConstantFields.BILLING_POSTAL__C) != null){
								reqXML = reqXML + "<PostalCode>"+sObject.getField(InvoiceConstantFields.BILLING_POSTAL__C)+"</PostalCode>";
							}else{
								reqXML = reqXML + "<PostalCode></PostalCode>";
							}
							//BILLING_COUNTRY__C
							if(sObject.getField(InvoiceConstantFields.BILLING_COUNTRY__C) != null){
								reqXML = reqXML + "<Country>"+sObject.getField(InvoiceConstantFields.BILLING_COUNTRY__C)+"</Country>";
							}else{
								reqXML = reqXML + "<Country></Country>";
							}
							reqXML = reqXML + "</BillAddress>";
							reqXML = reqXML + "<ShipAddress>";
							//SHIPPING_STREET__C
							if(sObject.getField(InvoiceConstantFields.SHIPPING_STREET__C) != null){
								String shippingStreet = (String)sObject.getField(InvoiceConstantFields.SHIPPING_STREET__C);
								if(shippingStreet.trim().length() > 40){
									shippingStreet = shippingStreet.substring(0, 39);
								}
								reqXML = reqXML + "<Addr1>"+shippingStreet+"</Addr1>";
								reqXML = reqXML + "<Addr2></Addr2>";
								reqXML = reqXML + "<Addr3></Addr3>";
							}else{
								reqXML = reqXML + "<Addr1></Addr1>";
								reqXML = reqXML + "<Addr2></Addr2>";
								reqXML = reqXML + "<Addr3></Addr3>";
							}
							//SHIPPING_CITY__C
							if(sObject.getField(InvoiceConstantFields.SHIPPING_CITY__C) !=null){
								reqXML = reqXML + "<City>"+sObject.getField(InvoiceConstantFields.SHIPPING_CITY__C)+"</City>";
							}else{
								reqXML = reqXML + "<City></City>";
							}
							//SHIPPING_STATE__C
							if(sObject.getField(InvoiceConstantFields.SHIPPING_STATE__C) != null){
								reqXML = reqXML + "<State>"+sObject.getField(InvoiceConstantFields.SHIPPING_STATE__C)+"</State>";
							}else{
								reqXML = reqXML + "<State></State>";
							}
							//SHIPPING_POSTAL__C
							if(sObject.getField(InvoiceConstantFields.SHIPPING_POSTAL__C) != null){
								reqXML = reqXML + "<PostalCode>"+sObject.getField(InvoiceConstantFields.SHIPPING_POSTAL__C)+"</PostalCode>";
							}else{
								reqXML = reqXML + "<PostalCode></PostalCode>";
							}
							//SHIPPING_COUNTRY__C
							if(sObject.getField(InvoiceConstantFields.SHIPPING_COUNTRY__C) != null){
								reqXML = reqXML + "<Country>"+sObject.getField(InvoiceConstantFields.SHIPPING_COUNTRY__C)+"</Country>";
							}else{
								reqXML = reqXML + "<Country></Country>";
							}
							reqXML = reqXML + "</ShipAddress>";
							//PONUMBER__C
							if(sObject.getField(InvoiceConstantFields.PONUMBER__C) != null){
								reqXML = reqXML + "<PONumber>"+sObject.getField(InvoiceConstantFields.PONUMBER__C)+"</PONumber>";
							}else{
								reqXML = reqXML + "<PONumber></PONumber>";
							}
							//TERMSREFFULLNAME__C
							if(sObject.getField(InvoiceConstantFields.TERMSREFFULLNAME__C) != null){
								reqXML = reqXML + "<TermsRef> <FullName>"+sObject.getField(InvoiceConstantFields.TERMSREFFULLNAME__C)+"</FullName> </TermsRef>";
							}
							//DUE_DATE__C
							if(sObject.getField(InvoiceConstantFields.DUE_DATE__C) != null){
								reqXML = reqXML + "<DueDate>"+sObject.getField(InvoiceConstantFields.DUE_DATE__C)+"</DueDate>";
							}else{
								reqXML = reqXML + "<DueDate></DueDate>";
							}
							//SHIP_DATE__C
							if(sObject.getField(InvoiceConstantFields.SHIP_DATE__C) != null){
								reqXML = reqXML + "<ShipDate>"+sObject.getField(InvoiceConstantFields.SHIP_DATE__C)+"</ShipDate>";
							}else{
								reqXML = reqXML + "<ShipDate></ShipDate>";
							}
							reqXML = reqXML +"<Memo></Memo>";
							List<SObject> invoiceLineItemList = mapInvoiceLineItem.get(sObject.getField("Id"));
							if(invoiceLineItemList != null && invoiceLineItemList.size() > 0){
								for(SObject invoiceLineItem : invoiceLineItemList){//Line_Item_Names__c
									reqXML = reqXML + "<InvoiceLineAdd>"
											+"<ItemRef>";
									//Product__r ProductCode
									if(invoiceLineItem.getChild("Product__r") != null && (invoiceLineItem.getChild("Product__r").getField("ProductCode") != null && 
											invoiceLineItem.getChild("Product__r").getField("Name") != null)){
										log.info("Line No 3543 : Product Name  ==> "+invoiceLineItem.getChild("Product__r").getField("Name"));
										reqXML = reqXML + "<ListID>"+invoiceLineItem.getChild("Product__r").getField("ProductCode")+"</ListID>";
										reqXML = reqXML + "<FullName>"+invoiceLineItem.getChild("Product__r").getField("Name")+"</FullName>";
									}else{
										reqXML = reqXML + "<ListID></ListID> <FullName></FullName>";
									}
									reqXML = reqXML + "</ItemRef>";
									//Product__r Description
									if(invoiceLineItem.getChild("Product__r") != null && invoiceLineItem.getChild("Product__r").getField("Description") != null){
										reqXML = reqXML + "<Desc>"+invoiceLineItem.getChild("Product__r").getField("Description")+"</Desc>";
									}else{
										reqXML = reqXML + "<Desc></Desc>";
									}
									//QUANTITY__C
									if(invoiceLineItem.getField(InvoiceLineItemsConstantFields.QUANTITY__C) != null){
										reqXML = reqXML + "<Quantity>"+invoiceLineItem.getField(InvoiceLineItemsConstantFields.QUANTITY__C)+"</Quantity>";
									}else{
										reqXML = reqXML + "<Quantity></Quantity>";
									}
									//Product__r Rate__c
									if(invoiceLineItem.getChild("Product__r") != null && invoiceLineItem.getChild("Product__r").getField("Rate__c") != null){
										reqXML = reqXML + "<Rate>"+invoiceLineItem.getChild("Product__r").getField("Rate__c")+"</Rate>";
									}else{
										reqXML = reqXML + "<Rate></Rate>";
									}
									reqXML = reqXML + "</InvoiceLineAdd>";					
								}
							}else{
								log.info("Line No 3576 : No Invoice Line Items Found with Invoce ListId and RefNumber==> "+subSobject.getField("ListID__c")+" <<==>> "+sObject.getField("RefNumber__c"));
							}
							reqXML = reqXML + "</InvoiceAdd>"
									+ "</InvoiceAddRq>";
						}
					}
				}
				reqXML = reqXML + "</QBXMLMsgsRq>"
						+"</QBXML>";
				log.info("@@@ reqXML ==> "+reqXML);
			}else{
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"7.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">"
						+"</QBXMLMsgsRq>"
						+"</QBXML>";
			}
		}catch(Exception e){
			log.info("@@@ Exception occurred in insertInvoices method ==> "+e);			
		}
		return reqXML;
	}
	/**
	 * updateInvoices method
	 * @param 
	 * @return String
	 * @Description This is to update invoices records from sfdc to qbsf invoices
	 */
	public String updateInvoices(){
		Set<String> setInvoiceIds = new HashSet<String>();
		Map<String,List<SObject>> mapInvoiceLineItem = new HashMap<String,List<SObject>>();
		String reqXML = "";//request xml to update invoices in qb
		boolean flag = false;//checks sfdc query building
		String qbSynchControl = "";//last sync date

		try{
			//query to get SFDCSynchControl__c data
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.INVOICE_ITEM_UPDATE_LAST_SYNC__C)){
				synchQuery = synchQuery+", Invoice_Item_Update_Last_Sync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}

			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult ==>> "+qResult);
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					if(records[0].getField(SFDCSynchControlConstants.INVOICE_ITEM_UPDATE_LAST_SYNC__C) != null){//get the value of Invoice_Item_Update_Last_Sync__c
						log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.INVOICE_ITEM_UPDATE_LAST_SYNC__C));
						qbSynchControl = records[0].getField(SFDCSynchControlConstants.INVOICE_ITEM_UPDATE_LAST_SYNC__C).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//get modified sfdc invoices data
			List<SObject> invoiceList = getModifiedSFDCInvoicesData();
			if(invoiceList != null && invoiceList.size() > 0){
				for(SObject sObject : invoiceList){//prepare set of invoices id
					setInvoiceIds.add((String)sObject.getField("Id"));
				}

				if(setInvoiceIds != null && setInvoiceIds.size() > 0){
					//prepare query of invoice line items
					String invoiceLineItemQuery = "select Id,Name,Quantity__c,Invoice__c,LastModifiedDate,CreatedDate,SalesTaxCodeRef_ListID__c,SalesTaxCodeRef_Full_Name__c,Product__r.Name,Product__r.ProductCode,Product__r.Rate__c,Product__r.Description,SFDC_Flag__c from Invoice_Line_Item__c WHERE SFDC_Flag__c=true";
					//query with last sync date
					if(qbSynchControl != null && qbSynchControl.length() > 0){
						invoiceLineItemQuery = invoiceLineItemQuery + " AND (CreatedDate > "+qbSynchControl;
						invoiceLineItemQuery = invoiceLineItemQuery + " OR LastModifiedDate > "+qbSynchControl+")";
					}

					log.info("@@@ invoiceLineItemQuery ==>> "+invoiceLineItemQuery);
					QueryResult queryResult = partnerConnection.query(invoiceLineItemQuery);
					String lastModifiedDate = null;
					int index = 0;
					if (queryResult!=null && queryResult.getSize() > 0) {
						SObject[] accRecords = queryResult.getRecords();
						if(accRecords != null && accRecords.length > 0){//queryResult after executing query
							for(SObject sObject : accRecords){//preparing map of invoice and sobject
								if(mapInvoiceLineItem.containsKey(sObject.getField("Invoice__c"))){
									mapInvoiceLineItem.get(sObject.getField("Invoice__c")).add(sObject);
								}else{
									List<SObject> invoiceLineItemList = new ArrayList<SObject>();
									invoiceLineItemList.add(sObject);
									mapInvoiceLineItem.put((String)sObject.getField("Invoice__c"), invoiceLineItemList);
								}
								//add LastModifiedDate to a set
								if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
									log.info("#####"+sObject.getField("LastModifiedDate"));
									//sfdcUpdateSyncDateSet.add(sObject.getField("LastModifiedDate").toString());
									lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
								}
							}
						}
					}
				}
			}
			//Sort dates			
			//invoiceList null check and to prepare xml query to update invoices 
			if(invoiceList != null && invoiceList.size() > 0){
				log.info("invoiceList ==>> "+invoiceList);
				log.info("invoiceList size ==>> "+invoiceList.size());
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"7.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">";
				for(SObject sObject : invoiceList){  
					com.sforce.ws.bind.XmlObject subSobject = (com.sforce.ws.bind.XmlObject)sObject.getField("Account__r");
					if(subSobject != null && subSobject.getField(InvoiceConstantFields.LISTID__C) != null){
						log.info("$$$ Acc List Id ==> "+subSobject.getField(InvoiceConstantFields.LISTID__C));
						if((subSobject != null && subSobject.getField(InvoiceConstantFields.LISTID__C) != null) && sObject.getField(InvoiceConstantFields.REFNUMBER__C) != null){
							List<SObject> invoiceLineItemList = mapInvoiceLineItem.get(sObject.getField("Id"));
							if(invoiceLineItemList != null && invoiceLineItemList.size() > 0){
								reqXML = reqXML +"<InvoiceModRq requestID=\""+System.currentTimeMillis()+"\""+">"
										+ "<InvoiceMod>"
										+ "<TxnID >"+sObject.getField(InvoiceConstantFields.TXNID__C)+"</TxnID>"
										+ "<EditSequence >"+sObject.getField(InvoiceConstantFields.EDITSEQUENCE__C)+"</EditSequence>"
										+ "<CustomerRef>"
										+ "<ListID>"+subSobject.getField(InvoiceConstantFields.LISTID__C)+"</ListID>"
										+ "</CustomerRef>";
								//TRANSACTION_DATE__C
								if(sObject.getField(InvoiceConstantFields.TRANSACTION_DATE__C) != null){
									log.info("sObject.getField(InvoiceConstantFields.TRANSACTION_DATE__C)"+sObject.getField(InvoiceConstantFields.TRANSACTION_DATE__C));
									reqXML = reqXML + "<TxnDate>"+sObject.getField(InvoiceConstantFields.TRANSACTION_DATE__C)+"</TxnDate>";
								}else{
									reqXML = reqXML + "<TxnDate></TxnDate>";
								}
								//Billing_Street__c
								reqXML = reqXML + "<BillAddress>";
								if(sObject.getField("Billing_Street__c") != null){
									log.info("$$$ BillingStreet__c ==> "+sObject.getField(InvoiceConstantFields.BILLING_STREET__C));
									String billingStreet = (String)sObject.getField(InvoiceConstantFields.BILLING_STREET__C);
									if(billingStreet.trim().length() > 40){
										billingStreet = billingStreet.substring(0, 39);
									}
									reqXML = reqXML + "<Addr1>"+billingStreet+"</Addr1>";
									reqXML = reqXML + "<Addr2></Addr2>";
									reqXML = reqXML + "<Addr3></Addr3>";
								}else{
									reqXML = reqXML + "<Addr1></Addr1>";
									reqXML = reqXML + "<Addr2></Addr2>";
									reqXML = reqXML + "<Addr3></Addr3>";
								}
								//Billing_City__c
								if(sObject.getField(InvoiceConstantFields.BILLING_CITY__C) !=null){
									reqXML = reqXML + "<City>"+sObject.getField(InvoiceConstantFields.BILLING_CITY__C)+"</City>";
								}else{
									reqXML = reqXML + "<City></City>";
								}
								//Billing_State__c
								if(sObject.getField(InvoiceConstantFields.BILLING_STATE__C) != null){
									reqXML = reqXML + "<State>"+sObject.getField(InvoiceConstantFields.BILLING_STATE__C)+"</State>";
								}else{
									reqXML = reqXML + "<State></State>";
								}
								//BILLING_POSTAL__C
								if(sObject.getField(InvoiceConstantFields.BILLING_POSTAL__C) != null){
									reqXML = reqXML + "<PostalCode>"+sObject.getField(InvoiceConstantFields.BILLING_POSTAL__C)+"</PostalCode>";
								}else{
									reqXML = reqXML + "<PostalCode></PostalCode>";
								}
								//Billing_Country__c
								if(sObject.getField(InvoiceConstantFields.BILLING_COUNTRY__C) != null){
									reqXML = reqXML + "<Country>"+sObject.getField(InvoiceConstantFields.BILLING_COUNTRY__C)+"</Country>";
								}else{
									reqXML = reqXML + "<Country></Country>";
								}
								reqXML = reqXML + "</BillAddress>";
								reqXML = reqXML + "<ShipAddress>";
								//ShippingStreet__c
								if(sObject.getField(InvoiceConstantFields.SHIPPING_STREET__C) != null){
									log.info("$$$ ShippingStreet__c ==> "+sObject.getField(InvoiceConstantFields.SHIPPING_STREET__C));
									String shippingStreet = (String)sObject.getField(InvoiceConstantFields.SHIPPING_STREET__C);
									if(shippingStreet.trim().length() > 40){
										shippingStreet = shippingStreet.substring(0, 39);
									}
									reqXML = reqXML + "<Addr1>"+shippingStreet+"</Addr1>";
									reqXML = reqXML + "<Addr2></Addr2>";
									reqXML = reqXML + "<Addr3></Addr3>";
								}else{
									reqXML = reqXML + "<Addr1></Addr1>";
									reqXML = reqXML + "<Addr2></Addr2>";
									reqXML = reqXML + "<Addr3></Addr3>";
								}
								//Shipping_City__c
								if(sObject.getField(InvoiceConstantFields.SHIPPING_CITY__C) !=null){
									reqXML = reqXML + "<City>"+sObject.getField(InvoiceConstantFields.SHIPPING_CITY__C)+"</City>";
								}else{
									reqXML = reqXML + "<City></City>";
								}
								//Shipping_State__c
								if(sObject.getField(InvoiceConstantFields.SHIPPING_STATE__C) != null){
									reqXML = reqXML + "<State>"+sObject.getField(InvoiceConstantFields.SHIPPING_STATE__C)+"</State>";
								}else{
									reqXML = reqXML + "<State></State>";
								}
								//Shipping_Postal__c
								if(sObject.getField(InvoiceConstantFields.SHIPPING_POSTAL__C) != null){
									reqXML = reqXML + "<PostalCode>"+sObject.getField(InvoiceConstantFields.SHIPPING_POSTAL__C)+"</PostalCode>";
								}else{
									reqXML = reqXML + "<PostalCode></PostalCode>";
								}
								//SHIPPING_COUNTRY__C
								if(sObject.getField(InvoiceConstantFields.SHIPPING_COUNTRY__C) != null){
									reqXML = reqXML + "<Country>"+sObject.getField(InvoiceConstantFields.SHIPPING_COUNTRY__C)+"</Country>";
								}else{
									reqXML = reqXML + "<Country></Country>";
								}
								reqXML = reqXML + "</ShipAddress>";
								//PONUMBER__C
								if(sObject.getField(InvoiceConstantFields.PONUMBER__C) != null){
									reqXML = reqXML + "<PONumber>"+sObject.getField(InvoiceConstantFields.PONUMBER__C)+"</PONumber>";
								}else{
									reqXML = reqXML + "<PONumber></PONumber>";
								}
								//Due_Date__c
								if(sObject.getField(InvoiceConstantFields.DUE_DATE__C) != null){
									reqXML = reqXML + "<DueDate>"+sObject.getField(InvoiceConstantFields.DUE_DATE__C)+"</DueDate>";
								}else{
									reqXML = reqXML + "<DueDate></DueDate>";
								}
								//SHIP_DATE__C
								if(sObject.getField(InvoiceConstantFields.SHIP_DATE__C) != null){
									reqXML = reqXML + "<ShipDate>"+sObject.getField(InvoiceConstantFields.SHIP_DATE__C)+"</ShipDate>";
								}else{
									reqXML = reqXML + "<ShipDate></ShipDate>";
								}
								reqXML = reqXML +"<Memo></Memo>";
								if(invoiceLineItemList != null && invoiceLineItemList.size() > 0){
									for(SObject invoiceLineItem : invoiceLineItemList){
										//Product__r ProductCode
										if(invoiceLineItem.getChild("Product__r") != null && (invoiceLineItem.getChild("Product__r").getField("ProductCode") != null && invoiceLineItem.getChild("Product__r").getField("Name") != null)){
											reqXML = reqXML + "<InvoiceLineMod>"
													+"<TxnLineID>-1</TxnLineID>"
													+"<ItemRef>";
											//Product__r ProductCode
											if(invoiceLineItem.getChild("Product__r") != null && invoiceLineItem.getChild("Product__r").getField("ProductCode") != null){
												reqXML = reqXML + "<ListID>"+invoiceLineItem.getChild("Product__r").getField("ProductCode")+"</ListID>";
											}
											//Product__r Name
											if(invoiceLineItem.getChild("Product__r") != null && invoiceLineItem.getChild("Product__r").getField("Name") != null){
												reqXML = reqXML + "<FullName>"+invoiceLineItem.getChild("Product__r").getField("Name")+"</FullName>";
											}else{
												reqXML = reqXML + "<FullName></FullName>";
											}
											reqXML = reqXML + "</ItemRef>";
											//Product__r Description
											if(invoiceLineItem.getChild("Product__r") != null && invoiceLineItem.getChild("Product__r").getField("Description") != null){
												reqXML = reqXML + "<Desc>"+invoiceLineItem.getChild("Product__r").getField("Description")+"</Desc>";
											}else{
												reqXML = reqXML + "<Desc></Desc>";
											}
											//Quantity__c
											if(invoiceLineItem.getField(InvoiceLineItemsConstantFields.QUANTITY__C) != null){
												reqXML = reqXML + "<Quantity>"+invoiceLineItem.getField(InvoiceLineItemsConstantFields.QUANTITY__C)+"</Quantity>";
											}else{
												reqXML = reqXML + "<Quantity></Quantity>";
											}
											//Product__r Rate
											if(invoiceLineItem.getChild("Product__r") != null && invoiceLineItem.getChild("Product__r").getField("Rate__c") != null){
												reqXML = reqXML + "<Rate>"+invoiceLineItem.getChild("Product__r").getField("Rate__c")+"</Rate>";
											}else{
												reqXML = reqXML + "<Rate></Rate>";
											}
											//SalesTaxCodeRef_ListID__c
											if(invoiceLineItem.getField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C) != null){//SalesTaxCodeRef_ListID__c,SalesTaxCodeRef_Full_Name__c
												reqXML = reqXML + "<SalesTaxCodeRef> <ListID>"+invoiceLineItem.getField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C)+"</ListID>";
												reqXML = reqXML + "<FullName>"+invoiceLineItem.getField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C)+"</FullName> </SalesTaxCodeRef>";
											}
											reqXML = reqXML + "</InvoiceLineMod>";			
										}
									}
								}else{
									log.info("Line No 3863 : No Invoice Line Items Found with Invoce ListId and RefNumber==> "+subSobject.getField("ListID__c")+" <<==>> "+sObject.getField("RefNumber__c"));
								}
								reqXML = reqXML + "</InvoiceMod>"
										+ "</InvoiceModRq>";
							}
						}
					}
				}
				reqXML = reqXML + "</QBXMLMsgsRq>"
						+"</QBXML>";
				log.info("@@@ reqXML ==> "+reqXML);
			}else{
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"7.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">"
						+"</QBXMLMsgsRq>"
						+"</QBXML>";
			}
		}catch(Exception e){
			log.info("@@@ Exception Occurred in updateInvoices method ==> "+e);
		}
		return reqXML;
	}
	/**
	 * getSFDCInvoicesData method
	 * @param 
	 * @return String
	 * @Description This is to get sfdc invoices data
	 */
	public List<SObject> getSFDCInvoicesData(){
		List<SObject> invoiceList = null;//results from query
		boolean flag = false;//checks sfdc query building
		String qbSynchControl = "";//last sync date
		try{
			//query to get SFDCInvoiceInsertLastSync__c value
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCINVOICEINSERTLASTSYNC__C)){
				synchQuery = synchQuery+", SFDCInvoiceInsertLastSync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				log.info("qResult ==>> "+qResult);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					if(records[0].getField(SFDCSynchControlConstants.SFDCINVOICEINSERTLASTSYNC__C) != null){//adds the SFDCInvoiceInsertLastSync__c value to a  string
						log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.SFDCINVOICEINSERTLASTSYNC__C));
						qbSynchControl = records[0].getField(SFDCSynchControlConstants.SFDCINVOICEINSERTLASTSYNC__C).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//query to get inoice data based on last modified and created dates
			String accQuery = "SELECT ID,Name,TxnID__c,EditSequence__c,Account__r.ListID__c,Transaction_Date__c,RefNumber__c,Billing_Street__c,Billing_City__c,Billing_State__c,Billing_Postal__c,Billing_Country__c,Shipping_Street__c,Shipping_City__c,Shipping_State__c,Shipping_Postal__c,Shipping_Country__c,PONumber__c,Due_Date__c,Ship_Date__c,LastModifiedDate FROM Invoice__c";
			if(qbSynchControl != null && qbSynchControl.length() > 0){
				accQuery = accQuery + " WHERE (CreatedDate > "+qbSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+qbSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] accRecords = queryResult.getRecords();
				if(accRecords != null && accRecords.length > 0){
					invoiceList = new ArrayList<SObject>();
					for(SObject sObject : accRecords){
						if((sObject.getField("TxnID__c") == null || "".equals(sObject.getField("TxnID__c"))) && 
								(sObject.getField("EditSequence__c") == null || "".equals(sObject.getField("EditSequence__c")))){
							log.info("@@@ TxnID__c ==>> "+sObject.getField("TxnID__c"));
							log.info("@@@ Inv EditSequence__c ==>> "+sObject.getField("EditSequence__c"));
							invoiceList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//sfdcInsertSyncDateSet.add(sObject.getField("LastModifiedDate").toString());
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
							}
						}
						index++;
					}
				}
			}
			log.info("### invoiceList size ==> "+invoiceList.size());			
			if(lastModifiedDate != null && lastModifiedDate!=null){
				sfdcInsertInvLastSyncControl = lastModifiedDate;
				log.info("sfdcInsertInvLastSyncControl ==> "+sfdcInsertInvLastSyncControl);
			}
			//}
		}catch(Exception e){
			log.info("@@@ Exception Occurred in getSFDCInvoicesData method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return invoiceList;
	}
	/**
	 * getSFDCOpportunityData method
	 * @param 
	 * @return String
	 * @Description This is to get sfdc opportunities data
	 */
	public List<SObject> getSFDCOpportunityData(){
		List<SObject> opportunityList = null;//results from query
		boolean flag = false;//checks sfdc query building
		String sfdcSynchControl = "";//last sync date
		try{
			//query to get Opportunity_Insert_Last_Sync__c value
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.OPPORTUNITY_INSERT_LAST_SYNC)){
				synchQuery = synchQuery+", Opportunity_Insert_Last_Sync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				log.info("qResult ==>> "+qResult);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					if(records[0].getField("Opportunity_Insert_Last_Sync__c") != null){
						log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_INSERT_LAST_SYNC));
						sfdcSynchControl = records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_INSERT_LAST_SYNC).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//opportunity query based on last modified and created dates
			String accQuery = "SELECT Id,TxnID__c,EditSequence__c,Account.ListID__c,Name,Ref_Number__c,CloseDate,StageName,Description,LastModifiedDate,Address__c,City__c,State__c,Country__c,Postal_Code__c FROM Opportunity WHERE StageName = 'Closed Won'";
			if(sfdcSynchControl != null && sfdcSynchControl.length() > 0){
				accQuery = accQuery + " AND (CreatedDate > "+sfdcSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+sfdcSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {
				SObject[] oppRecords = queryResult.getRecords();
				if(oppRecords != null && oppRecords.length > 0){
					opportunityList = new ArrayList<SObject>();
					for(SObject sObject : oppRecords){
						if((sObject.getField("TxnID__c") == null || "".equals(sObject.getField("TxnID__c"))) && 
								(sObject.getField("EditSequence__c") == null || "".equals(sObject.getField("EditSequence__c")))){
							log.info("@@@ TxnID__c ==>> "+sObject.getField("TxnID__c"));
							log.info("@@@ Inv EditSequence__c ==>> "+sObject.getField("EditSequence__c"));
							opportunityList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//sfdcInsertSyncDateSet.add(sObject.getField("LastModifiedDate").toString());
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
							}
						}
						index++;
					}
				}
			}
			log.info("### opportunityList size ==> "+opportunityList.size());
			if(lastModifiedDate != null && lastModifiedDate!=null){
				sfdcInsertOppLastSyncControl = lastModifiedDate;
				log.info("sfdcInsertOppLastSyncControl ==> "+sfdcInsertOppLastSyncControl);
			}
			//}
		}catch(Exception e){
			log.info("@@@ Exception Occurred in getSFDCOpportunityData method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return opportunityList;
	}
	/**
	 * getModifiedSFDCOpportunityData method
	 * @param 
	 * @return String
	 * @Description This is to get modified sfdc opportunities data
	 */
	public List<SObject> getModifiedSFDCOpportunityData(){
		List<SObject> opportunityList = null;//results from query
		boolean flag = false;//checks sfdc query building
		String sfdcSynchControl = "";//last sync date
		try{
			//query to get Opportunity_Update_Last_Sync__c field value
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.OPPORTUNITY_UPDATE_LAST_SYNC)){
				synchQuery = synchQuery+", Opportunity_Update_Last_Sync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				log.info("qResult ==>> "+qResult);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_UPDATE_LAST_SYNC));
					if(records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_UPDATE_LAST_SYNC) != null){
						sfdcSynchControl = records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_UPDATE_LAST_SYNC).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//opportunity query bawsed on LastModifiedDate and CreatedDate
			String accQuery = "SELECT Id,TxnID__c,EditSequence__c,Account.ListID__c,Name,Ref_Number__c,CloseDate,StageName,Description,LastModifiedDate,Address__c,City__c,State__c,Country__c,Postal_Code__c,SFDC_Flag__c FROM Opportunity WHERE StageName = 'Closed Won' AND SFDC_Flag__c=true" ;
			if(sfdcSynchControl != null && sfdcSynchControl.length() > 0){
				accQuery = accQuery + " AND (CreatedDate > "+sfdcSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+sfdcSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			log.info("queryResult"+queryResult);
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] oppRecords = queryResult.getRecords();
				if(oppRecords != null && oppRecords.length > 0){
					opportunityList = new ArrayList<SObject>();
					for(SObject sObject : oppRecords){
						if((sObject.getField("TxnID__c") != null && sObject.getField("TxnID__c").toString().trim().length() > 0) && 
								(sObject.getField("EditSequence__c") != null && sObject.getField("EditSequence__c").toString().trim().length() > 0)){
							log.info("@@@ TxnID__c ==>> "+sObject.getField("TxnID__c"));
							log.info("@@@ Inv EditSequence__c ==>> "+sObject.getField("EditSequence__c"));
							opportunityList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().trim().length() > 0){
								//sfdcInsertSyncDateSet.add(sObject.getField("LastModifiedDate").toString());
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
							}
						}
						index++;
					}
				}
			}			
			if(lastModifiedDate != null && lastModifiedDate!=""){
				sfdcUpdateOppLastSyncControl = lastModifiedDate;
				log.info("sfdcUpdateOppLastSyncControl ==> "+sfdcUpdateOppLastSyncControl);
			}
			//}
		}catch(Exception e){
			log.info("@@@ Exception Occurred in getModifiedSFDCOpportunityData method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return opportunityList;
	}
	
	/**
	 * getModifiedSFDCInvoicesData method
	 * @param 
	 * @return String
	 * @Description This is to get modified sfdc invoices data
	 */
	public List<SObject> getModifiedSFDCInvoicesData(){
		List<SObject> invoiceList = null;//list of invoice from query results
		boolean flag = false;//checks sfdc query building
		String qbSynchControl = "";//last sync date
		try{
			//query to get SFDCInvoiceUpdateLastSync__c from SFDCSynchControl__c custom object
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains("SFDCInvoiceUpdateLastSync__c")){
				synchQuery = synchQuery+", SFDCInvoiceUpdateLastSync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult ==>> "+qResult);
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					if(records[0].getField("SFDCInvoiceUpdateLastSync__c") != null){
						log.info("records[0].getField ==>> "+records[0].getField("SFDCInvoiceUpdateLastSync__c"));
						qbSynchControl = records[0].getField("SFDCInvoiceUpdateLastSync__c").toString();//contains value of SFDCInvoiceUpdateLastSync__c
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//account query to get data based on LastModifiedDate and CreatedDate
			String accQuery = "SELECT ID,Name,TxnID__c,EditSequence__c,Account__r.ListID__c,Transaction_Date__c,RefNumber__c,Billing_Street__c,Billing_City__c,Billing_State__c,Billing_Postal__c,Billing_Country__c,Shipping_Street__c,Shipping_City__c,Shipping_State__c,Shipping_Postal__c,Shipping_Country__c,PONumber__c,Due_Date__c,Ship_Date__c,LastModifiedDate,SFDC_Flag__c FROM Invoice__c WHERE SFDC_Flag__c=true";
			if(qbSynchControl != null && qbSynchControl.length() > 0){
				accQuery = accQuery + " AND (CreatedDate > "+qbSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+qbSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;//contains sorted lastModifiedDate  
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] accRecords = queryResult.getRecords();
				if(accRecords != null && accRecords.length > 0){//records from query result
					invoiceList = new ArrayList<SObject>();
					for(SObject sObject : accRecords){
						if((sObject.getField("TxnID__c") != null && sObject.getField("TxnID__c").toString().length() > 0) && 
								(sObject.getField("EditSequence__c") != null && sObject.getField("EditSequence__c").toString().length() > 0)){
							log.info("@@@ TxnID__c ==>> "+sObject.getField("TxnID__c"));
							log.info("@@@ Inv EditSequence__c ==>> "+sObject.getField("EditSequence__c"));
							invoiceList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//method that returns sorted lastModifiedDate
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
							}
						}
						index++;
					}
				}
			}
			//log.info("### invoiceList size ==> "+invoiceList.size());
			// Sort dats
			sfdcUpdateInvLastSyncControl = lastModifiedDate;//sfdcUpdateSyncDateSet.get(sfdcUpdateSyncDateSet.size()-1);
			log.info("sfdcUpdateInvLastSyncControl ==> "+sfdcUpdateInvLastSyncControl);


		}catch(Exception e){
			log.info("@@@ Exception Occurred in getModifiedSFDCInvoicesData method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return invoiceList;
	}
	/**
	 * getSFDCAccountsData method
	 * @param 
	 * @return String
	 * @Description This is to get sfdc accounts data
	 */
	public List<SObject> getSFDCAccountsData(){
		List<SObject> accountList = null;//results from account query
		boolean flag = false;//checks sfdc query building
		String qbSynchControl = "";//last sync date
		try{
			//query to get SFDCInvoiceUpdateLastSync__c field value
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCLASTSYNCH__C)){
				synchQuery = synchQuery+", SFDCLastSynch__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult ==>> "+qResult);
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.SFDCLASTSYNCH__C));
					if(records[0].getField(SFDCSynchControlConstants.SFDCLASTSYNCH__C) != null){
						qbSynchControl = records[0].getField(SFDCSynchControlConstants.SFDCLASTSYNCH__C).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//account query based on LastModifiedDate & CreatedDate
			String accQuery = "SELECT ID,Name,Company_Name__c,ListID__c,EditSequence__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,Phone,LastModifiedDate,Active__c,Job_Title__c,Fax,Email__c FROM Account";
			if(qbSynchControl != null && qbSynchControl.length() > 0){
				accQuery = accQuery + " WHERE (CreatedDate > "+qbSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+qbSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] accRecords = queryResult.getRecords();
				if(accRecords != null && accRecords.length > 0){
					accountList = new ArrayList<SObject>();
					for(SObject sObject : accRecords){
						if((sObject.getField("ListID__c") == null || "".equals(sObject.getField("ListID__c"))) && 
								(sObject.getField("EditSequence__c") == null || "".equals(sObject.getField("EditSequence__c")))){
							accountList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//method that returns sorted lastModifiedDate
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
							}
						}
						index++;
					}
				}
			}
			if(lastModifiedDate != null && lastModifiedDate!=null){
				sfdcInsertLastSyncControl = lastModifiedDate;
				log.info("sfdcInsertLastSyncControl ==> "+sfdcInsertLastSyncControl);
			}
			//}
		}catch(Exception e){
			log.info("@@@ Exception Occurred in getSFDCAccountsData method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return accountList;
	}
	/**
	 * getSFDCUpdatedAccountsData method
	 * @param 
	 * @return String
	 * @Description This is to get updated sfdc accounts data
	 */
	public List<SObject> getSFDCUpdatedAccountsData(){
		List<SObject> accountList = null;//results from account query
		boolean flag = false;//checks sfdc query building
		String qbSynchControl = "";//last sync date
		try{
			//query to get SFDCInvoiceUpdateLastSync__c field value
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.SFDCUPDATELASTSYNC__C)){
				synchQuery = synchQuery+", SFDCUpdateLastSynch__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult ==>> "+qResult);
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.SFDCUPDATELASTSYNC__C));
					if(records[0].getField(SFDCSynchControlConstants.SFDCUPDATELASTSYNC__C) != null){
						qbSynchControl = records[0].getField(SFDCSynchControlConstants.SFDCUPDATELASTSYNC__C).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			// account query based on CreatedDate & LastModifiedDate
			String accQuery = "SELECT ID,Name,Company_Name__c,ListID__c,EditSequence__c,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,Phone,LastModifiedDate,Active__c,Job_Title__c,Fax,Email__c,SFDC_Flag__c FROM Account WHERE SFDC_Flag__c=true";
			if(qbSynchControl != null && qbSynchControl.length() > 0){
				accQuery = accQuery + " AND (CreatedDate > "+qbSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+qbSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] accRecords = queryResult.getRecords();
				if(accRecords != null && accRecords.length > 0){
					accountList = new ArrayList<SObject>();
					for(SObject sObject : accRecords){
						if((sObject.getField("ListID__c") != null && sObject.getField("ListID__c").toString().length() > 0) && 
								(sObject.getField("EditSequence__c") != null && sObject.getField("EditSequence__c").toString().length() > 0)){
							accountList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//method that returns sorted lastModifiedDate
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
							}
						}
						index++;
					}
				}
			}
			//lastModifiedDate null check
			if(lastModifiedDate != null && lastModifiedDate!=null){
				sfdcUpdateLastSyncControl = lastModifiedDate;
				log.info("sfdcUpdateLastSyncControl ==> "+sfdcUpdateLastSyncControl);
			}
			//}
		}catch(Exception e){
			log.info("@@@ Exception Occurred in getSFDCUpdatedAccountsData method ==> "+e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return accountList;
	}

	/**
	 * insertAccountsToSFDC method
	 * @param response
	 * @return String
	 * insert sfdc accounts with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String insertAccountsToSFDC(java.lang.String response){
		log.info("***************** ACCOUNTS DETAILS INSERT SYNC START *****************");
		String isOk = "";//status from updated results
		boolean flag = false;//checks sfdc query building
		boolean accflag=false;//checks sfdc query building
		com.qb.appshark.QBXMLType qbxml = null;//QBXMLType
		List<com.qb.appshark.CustomerAddRsType> customerRetTypeList = null;//CustomerAddRsType from qb
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.qb.appshark.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.qb.appshark.QBXMLType> root = unmarshaller.unmarshal(source, com.qb.appshark.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getCustomerAddRs() != null)){
				customerRetTypeList = qbxml.getQBXMLMsgsRs().getCustomerAddRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
		}

		try{
			if(customerRetTypeList != null && customerRetTypeList.size() > 0){//customerRetTypeList null check
				List<SObject> lstCustomers = new ArrayList<SObject>();//list of mapped sobjects
				SObject[] objAccLst = null;//sobject array to update to sfdc
				SObject objAcc;//sobject to map fields
				Map<String,String> mapAccountNames = new HashMap<String, String>();//map of account names and ids
				//account query with name
				if(customerRetTypeList != null && customerRetTypeList.size() > 0){
					//Preparing Query for Account
					String accountQuery = SFDCQueryConstants.ACCOUNT_QUERY_NAME;
					//fields to be checked conditionally for dynamic query building
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(AccountConstantFields.ID);
					fieldToBeUsed.add(AccountConstantFields.ACCOUNT_NAME);
					log.info("accQuery ==>> "+accountQuery);
					returnedMap = sqlQueryBuilder.buildQueryForAccountNamesInsert(accountQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, customerRetTypeList,fieldToBeUsed);
					if(returnedMap != null && returnedMap.size() > 0){
						accflag = (boolean) returnedMap.get("flag");
						accountQuery = (String) returnedMap.get("sqlQuery");
						log.info("accQuery ==>> "+accountQuery);
						log.info("invflag ==>> "+accflag);
					}

					if(accflag){
						fieldToBeUsed = new ArrayList<String>();
						fieldToBeUsed.add(AccountConstantFields.ACCOUNT_NAME);
						fieldToBeUsed.add(AccountConstantFields.ID);
						mapAccountNames = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(accountQuery), fieldToBeUsed);
					}
				}
				//insert account data in sfdc 
				for(com.qb.appshark.CustomerAddRsType customerAddRsType : customerRetTypeList){
					com.qb.appshark.CustomerRetType customerRetType = customerAddRsType.getCustomerRet();
					if(customerRetType != null && customerRetType.getListID() != null){
						if(mapAccountNames != null && mapAccountNames.size() > 0){//mapAccountNames null check
							if(mapAccountNames.containsKey(customerRetType.getFullName())){
								flag = true;
								objAcc = new SObject();
								objAcc.setType(SFDCObjectConstants.ACCOUNT);
								if(customerRetType.getListID() != null){
									//id
									objAcc.setField(AccountConstantFields.ID, mapAccountNames.get(customerRetType.getFullName()));
									//LISTID__C
									if(setAccountFields.contains(AccountConstantFields.LISTID__C)){
										objAcc.setField(AccountConstantFields.LISTID__C, customerRetType.getListID());
									}
									//QBLISTID__C
									if(setAccountFields.contains(AccountConstantFields.QBLISTID__C)){
										objAcc.setField(AccountConstantFields.QBLISTID__C, customerRetType.getListID());
									}
								}
								//EDITSEQUENCE__C
								if(customerRetType.getEditSequence() != null){
									if(setAccountFields.contains(AccountConstantFields.EDITSEQUENCE__C)){
										objAcc.setField(AccountConstantFields.EDITSEQUENCE__C, customerRetType.getEditSequence());
									}
								}
								lstCustomers.add(objAcc);
							}
						}
					}		
				}
				//lust to array
				if(lstCustomers != null && lstCustomers.size() > 0){
					objAccLst = new SObject[lstCustomers.size()];
					lstCustomers.toArray(objAccLst);
					log.info("objAccLst********"+objAccLst.toString());
				}
				//logic to prepare batches of 100 to overcome sfdc limits while update
				if((objAccLst != null && objAccLst.length > 0) && flag){
					isOk = QueryExecuter.prepareAndExecuteUpdate(objAccLst,partnerConnection,SFDCObjectConstants.ACCOUNT);					
				}
			}
		} catch (ConnectionException e) {
			log.info("Exception while Inserting Customers :"+ e);
		}
		log.info("***************** ACCOUNTS DETAILS INSERT SYNC END *****************");
		return isOk;
	}

	/**
	 * updateAccountsToSFDC method
	 * @param response
	 * @return String
	 * update sfdc accounts with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String updateAccountsToSFDC(java.lang.String response){
		log.info("***************** ACCOUNTS DETAILS UPDATE SYNC START *****************");
		String isOk = "";//status from updated results
		boolean flag = false;//checks sfdc query building
		boolean accflag=false;//checks sfdc query building
		com.appshark.customermodrs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.customermodrs.CustomerModRsType> customerRetTypeList = null;
		// jar files content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.customermodrs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.customermodrs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.customermodrs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getCustomerModRs() != null)){
				customerRetTypeList = qbxml.getQBXMLMsgsRs().getCustomerModRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}
		//customerRetTypeList null check
		try{
			if(customerRetTypeList != null && customerRetTypeList.size() > 0){
				List<SObject> lstCustomers = new ArrayList<SObject>();//contains mapped sobject data
				SObject[] objAccLst = null;//to update to sfdc
				SObject objAcc;//sobject for mapping of fields
				Map<String,String> mapAccountNames = new HashMap<String, String>();//map of account names and account id
				//account query
				//account query with name
				if(customerRetTypeList != null && customerRetTypeList.size() > 0){
					//Preparing Query for Account
					String accountQuery = SFDCQueryConstants.ACCOUNT_QUERY_NAME;
					//fields to be checked conditionally for dynamic query building
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(AccountConstantFields.ID);
					fieldToBeUsed.add(AccountConstantFields.ACCOUNT_NAME);
					//method that returns sqlQuery,flag
					returnedMap = sqlQueryBuilder.buildQueryForAccountNamesUpdate(accountQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, customerRetTypeList,fieldToBeUsed);
					if(returnedMap != null && returnedMap.size() > 0){
						accflag = (boolean) returnedMap.get("flag");
						accountQuery = (String) returnedMap.get("sqlQuery");
						log.info("accQuery ==>> "+accountQuery);
						log.info("invflag ==>> "+accflag);
					}

					if(accflag){
						fieldToBeUsed = new ArrayList<String>();
						fieldToBeUsed.add(AccountConstantFields.ACCOUNT_NAME);
						fieldToBeUsed.add(AccountConstantFields.ID);
						mapAccountNames = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(accountQuery), fieldToBeUsed);
					}
				}

				//customerRetTypeList iteration to map sfdc fields with qbsf data
				for(com.appshark.customermodrs.CustomerModRsType customerModRsType : customerRetTypeList){
					com.appshark.customermodrs.CustomerRetType customerRetType = customerModRsType.getCustomerRet();
					if(customerRetType != null && customerRetType.getListID() != null){//customerRetType null check
						if(mapAccountNames != null && mapAccountNames.size() > 0){
							if(mapAccountNames.containsKey(customerRetType.getFullName())){//if qbsf customer name is in mapAccountNames
								flag = true;
								objAcc = new SObject();
								objAcc.setType(SFDCObjectConstants.ACCOUNT);
								if(customerRetType.getListID() != null){
									//id
									objAcc.setField(AccountConstantFields.ID, mapAccountNames.get(customerRetType.getFullName()));
									//LISTID__C
									if(setAccountFields.contains(AccountConstantFields.LISTID__C)){
										objAcc.setField(AccountConstantFields.LISTID__C, customerRetType.getListID());
									}
									//QBListId__c
									if(setAccountFields.contains(AccountConstantFields.QBLISTID__C)){
										objAcc.setField(AccountConstantFields.QBLISTID__C, customerRetType.getListID());
									}
									//SFDC_Update_Flag__c
									log.info("setAccountFields" + setAccountFields);
									if(setAccountFields.contains(AccountConstantFields.SFDC_UPDATE_FLAG__C)){
										objAcc.setField(AccountConstantFields.SFDC_UPDATE_FLAG__C,true);
										log.info("SFDC_Update_Flag__c" +objAcc.getField(AccountConstantFields.SFDC_UPDATE_FLAG__C));
									}
									//SFDC_FLAG__C
									log.info("setAccountFields" + setAccountFields);
									if(setAccountFields.contains(AccountConstantFields.SFDC_FLAG__C)){
										objAcc.setField(AccountConstantFields.SFDC_FLAG__C,true);
										log.info("SFDC_Update_Flag__c" +objAcc.getField(AccountConstantFields.SFDC_FLAG__C));
									}
								}
								//EDITSEQUENCE__C
								if(customerRetType.getEditSequence() != null){
									if(setAccountFields.contains(AccountConstantFields.EDITSEQUENCE__C)){
										objAcc.setField(AccountConstantFields.EDITSEQUENCE__C, customerRetType.getEditSequence());
									}
								}
								lstCustomers.add(objAcc);
							}
						}
					}		
				}
				//lstCustomers to an array
				if(lstCustomers != null && lstCustomers.size() > 0){
					objAccLst = new SObject[lstCustomers.size()];
					lstCustomers.toArray(objAccLst);
					log.info("objAccLst********"+objAccLst.toString());
				}
				// logic to prepare records in batches of 100 to overcome sfdc limit while update
				if((objAccLst != null && objAccLst.length > 0) && flag){
					log.info("objAccLst Size ==>> "+objAccLst.length);
					isOk = QueryExecuter.prepareAndExecuteUpdate(objAccLst,partnerConnection,SFDCObjectConstants.ACCOUNT);					
				}
			}
		} catch (ConnectionException e) {
			log.info("Exception while Inserting Customers :"+ e);
		}
		log.info("***************** ACCOUNTS DETAILS UPDATE SYNC END *****************");
		return isOk;
	}
	/**
	 * insertAccountsToSFDC method
	 * @param response
	 * @return String
	 * insert sfdc Invoices with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String insertSFDCInvoices(java.lang.String response){
		log.info("***************** INVOICES DETAILS INSERT SYNC START *****************");
		String isOk = "";//status after update to sfdc
		boolean invoiceFlag = false;//checks sfdc query building
		boolean invflag=false;//checks sfdc query building
		Set<String> setInvoiceTxnIds = new HashSet<String>();//set of invoice txn ids
		Map<String,List<SObject>> mapInvoiceLineItem = new HashMap<String,List<SObject>>();//map of invoice line items
		com.appshark.invoice.add.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.invoice.add.rs.InvoiceAddRsType> invoiceRetTypeList = null;//InvoiceAddRsType from qb
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.invoice.add.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.invoice.add.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.invoice.add.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getInvoiceAddRs() != null)){
				invoiceRetTypeList = qbxml.getQBXMLMsgsRs().getInvoiceAddRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}

		try{
			if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){//invoiceRetTypeList null check
				List<SObject> lstInvoices = new ArrayList<SObject>();//contains mapped sobject data of invoices
				List<SObject> lstInvoiceItems = new ArrayList<SObject>();//contains mapped sobject invoice line items
				SObject[] objAccLst = null;//sobject array to update to sfdc
				SObject objAcc;//sobject to map fields for invoice
				SObject objInvoiceItem;//sobject to map fields for invoice line items
				Set<String> setInvoiceIds = new HashSet<String>();//set of invoice ids to verify for 

				Map<String,String> maprefNumbers = new HashMap<String, String>();
				//invoice query with RefNumber__c
				if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){
					//preparing query for invoice
					String invQuery = SFDCQueryConstants.INVOICE_QUERY_INSERT;
					//fields to be checked conditionally for dynamic query building
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(InvoiceConstantFields.REFNUMBER__C);
					fieldToBeUsed.add(InvoiceConstantFields.ID);
					//building query
					Map<String,Object> returnedMap = sqlQueryBuilder.buildQueryForInvoiceInsert(invQuery,setInvoiceFields, SFDCObjectConstants.INVOICE__C, invoiceRetTypeList,fieldToBeUsed);
					//extracting return result after query building
					if(returnedMap != null && returnedMap.size() > 0){
						invflag = (boolean) returnedMap.get("flag");
						invQuery = (String) returnedMap.get("sqlQuery");
						log.info("accQuery ==>> "+invQuery);
						log.info("invflag ==>> "+invflag);
					}

					//check flag and prepare a map from query which was returnd from query builder
					if(invflag){	
						//Retrieving Account objects from Salesforce
						fieldToBeUsed = new ArrayList<String>();
						fieldToBeUsed.add(InvoiceConstantFields.REFNUMBER__C);
						fieldToBeUsed.add(InvoiceConstantFields.ID);
						maprefNumbers = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(invQuery), fieldToBeUsed);	
						setInvoiceIds=QueryResultIterator.prepareSetFromQueryResult(partnerConnection.query(invQuery), "Id");
					}
					//Invoice Line Items Data Preparation
					if(setInvoiceIds != null && setInvoiceIds.size() > 0){
						String condition = "";
						String invLineItemQuery = "select Id,Name,Quantity__c,Invoice__c,Product__r.Name,Product__r.ProductCode,Product__r.Rate__c from Invoice_Line_Item__c where Invoice__c in (";
						int cnt = 1;
						for(String invoiceId : setInvoiceIds){
							condition = condition+"'"+invoiceId+"'";
							if(cnt != setInvoiceIds.size()){
								condition = condition+",";
							}
							if(cnt == setInvoiceIds.size()){
								condition = condition+")";
								invLineItemQuery = invLineItemQuery+condition;
							}
							cnt++;
						}
						log.info("@@@ invoiceLineItemQuery ==>> "+invLineItemQuery);
						if(condition != null && condition.length() > 0){
							QueryResult queryRslt = partnerConnection.query(invLineItemQuery);
							if (queryRslt != null && queryRslt.getSize() > 0) {//query result null check
								SObject[] accRecords = queryRslt.getRecords();
								if(accRecords != null && accRecords.length > 0){//records from query result
									for(SObject sObject : accRecords){
										if(mapInvoiceLineItem.containsKey(sObject.getField("Invoice__c"))){//prepare map of invoice and sobject
											mapInvoiceLineItem.get(sObject.getField("Invoice__c")).add(sObject);
										}else{
											List<SObject> invoiceLineItemList = new ArrayList<SObject>();
											invoiceLineItemList.add(sObject);
											mapInvoiceLineItem.put((String)sObject.getField("Invoice__c"), invoiceLineItemList);
										}
									}
								}
							}
						}
					}
				}
				//invoiceRetTypeList iteration to map sfdc fields with qbsf data
				for(com.appshark.invoice.add.rs.InvoiceAddRsType invoiceAddRsType : invoiceRetTypeList){
					com.appshark.invoice.add.rs.InvoiceRetType  invoiceRetType = invoiceAddRsType.getInvoiceRet();
					if((maprefNumbers != null && maprefNumbers.size() > 0) && (invoiceRetType != null && invoiceRetType.getCustomerRef().getListID() != null)){
						if((invoiceRetType.getRefNumber() != null && maprefNumbers.containsKey(invoiceRetType.getRefNumber())) && invoiceRetType.getTxnID() != null){
							String invoiceId = maprefNumbers.get(invoiceRetType.getRefNumber());
							log.info("invoiceRetType TxnID ==>> "+invoiceRetType.getTxnID());
							if(setInvoiceFields != null && setInvoiceFields.size() > 0){//setInvoiceFields null check
								log.info("setInvoiceFields.size ==>> "+setInvoiceFields.size());
								invoiceFlag = true;
								objAcc = new SObject();
								objAcc.setType(SFDCObjectConstants.INVOICE__C);
								//id
								objAcc.setField(InvoiceConstantFields.ID, maprefNumbers.get(invoiceRetType.getRefNumber()));
								//TxnID__c
								if(setInvoiceFields.contains(InvoiceConstantFields.TXNID__C)){
									if(invoiceRetType.getTxnID() != null){
										objAcc.setField(InvoiceConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
									}
								}
								//ListID__c
								if(setInvoiceFields.contains(InvoiceConstantFields.LISTID__C)){
									if(invoiceRetType.getTxnID() != null){
										objAcc.setField(InvoiceConstantFields.LISTID__C, invoiceRetType.getCustomerRef().getListID());
									}else{
										objAcc.setField(InvoiceConstantFields.LISTID__C, "");
									}
								}
								//TIME_CREATED__C
								if(setInvoiceFields.contains(InvoiceConstantFields.TIME_CREATED__C)){
									if(invoiceRetType.getTimeCreated() != null){
										objAcc.setField(InvoiceConstantFields.TIME_CREATED__C, invoiceRetType.getTimeCreated());
									}else{
										objAcc.setField(InvoiceConstantFields.TIME_CREATED__C, "");
									}
								}
								//Time_Modified__c
								if(setInvoiceFields.contains(InvoiceConstantFields.TIME_MODIFIED__C)){
									if(invoiceRetType.getTimeModified() != null){
										objAcc.setField(InvoiceConstantFields.TIME_MODIFIED__C, invoiceRetType.getTimeModified());
									}else{
										objAcc.setField(InvoiceConstantFields.TIME_MODIFIED__C, "");
									}
								}
								//EditSequence__c
								if(setInvoiceFields.contains(InvoiceConstantFields.EDITSEQUENCE__C)){
									if(invoiceRetType.getEditSequence() != null){
										objAcc.setField(InvoiceConstantFields.EDITSEQUENCE__C, invoiceRetType.getEditSequence());
									}else{
										objAcc.setField(InvoiceConstantFields.EDITSEQUENCE__C, "");
									}
								}
								//TRANSACTION_NUMBER__C
								if(setInvoiceFields.contains(InvoiceConstantFields.TRANSACTION_NUMBER__C)){
									if(invoiceRetType.getTxnNumber() != null){
										objAcc.setField(InvoiceConstantFields.TRANSACTION_NUMBER__C, invoiceRetType.getTxnNumber());
									}else{
										objAcc.setField(InvoiceConstantFields.TRANSACTION_NUMBER__C, "");
									}
								}
								//CustomerRef_ListID__c
								if(invoiceRetType.getCustomerRef() != null){
									if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMERREF_LISTID__C)){
										if(invoiceRetType.getCustomerRef().getListID() != null){
											objAcc.setField(InvoiceConstantFields.CUSTOMERREF_LISTID__C, invoiceRetType.getCustomerRef().getListID());
										}else{
											objAcc.setField(InvoiceConstantFields.CUSTOMERREF_LISTID__C, "");
										}
									}
									//Customer_Full_Name__c
									if(setInvoiceFields.contains(InvoiceConstantFields.CUSTOMER_FULL_NAME__C)){
										if(invoiceRetType.getCustomerRef().getFullName() != null){
											objAcc.setField(InvoiceConstantFields.CUSTOMER_FULL_NAME__C, invoiceRetType.getCustomerRef().getFullName());
										}else{
											objAcc.setField(InvoiceConstantFields.CUSTOMER_FULL_NAME__C, "");
										}
									}
								}
								lstInvoices.add(objAcc);
								//Preparing Invoice Line Items
								Map<String,String> mapInvLineItemIds = new HashMap<String,String>(); 
								List<SObject> invoiceLineItemList = mapInvoiceLineItem.get(invoiceId);
								if(invoiceLineItemList != null && invoiceLineItemList.size() > 0){
									for(SObject invLineItem : invoiceLineItemList){
										mapInvLineItemIds.put((String)invLineItem.getChild("Product__r").getField("ProductCode"),(String)invLineItem.getField("Id"));
									}
								}
								List<com.appshark.invoice.add.rs.InvoiceLineRetType> invoiceLineRetTypeList = invoiceRetType.getInvoiceLineRet();
								log.info("$$$ LINE NO ==>> 4687"+invoiceLineRetTypeList);
								if((invoiceLineRetTypeList != null && invoiceLineRetTypeList.size() > 0) && mapInvLineItemIds != null && mapInvLineItemIds.size() > 0){
									for(com.appshark.invoice.add.rs.InvoiceLineRetType invoiceLineRetType : invoiceLineRetTypeList){
										setInvoiceTxnIds.add(invoiceRetType.getTxnID().trim());
										objInvoiceItem = new SObject();
										objInvoiceItem.setType(SFDCObjectConstants.INVOICE_LINE_ITEM__C);
										//INVOICE_LINE_ITEMS_ID
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.INVOICE_LINE_ITEMS_ID)){
											if(invoiceLineRetType.getItemRef() != null && invoiceLineRetType.getItemRef().getListID() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.INVOICE_LINE_ITEMS_ID, mapInvLineItemIds.get((String)invoiceLineRetType.getItemRef().getListID()));
										}
										//TXNLINEID__C
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNLINEID__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, "");
											if(invoiceLineRetType.getTxnLineID() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, invoiceLineRetType.getTxnLineID().trim());
										}
										//TxnID__c
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNID__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, "");
											if(invoiceRetType.getTxnID() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
										}
										//ExternID__c
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.EXTERNID__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, "");
											if(invoiceRetType.getTxnID() != null && (invoiceLineRetType.getItemRef() != null && invoiceLineRetType.getItemRef().getListID() != null))
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, invoiceRetType.getTxnID().trim()+" "+invoiceLineRetType.getItemRef().getListID().trim());
										}
										//AMOUNT__C
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.AMOUNT__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.AMOUNT__C, "");
											if(invoiceLineRetType.getAmount() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.AMOUNT__C, invoiceLineRetType.getAmount());
										}
										//SalesTaxCodeRef_ListID__c
										if(invoiceLineRetType.getSalesTaxCodeRef() != null){
											if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C)){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C, "");
												if(invoiceLineRetType.getSalesTaxCodeRef().getListID() != null)
													objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C, invoiceLineRetType.getSalesTaxCodeRef().getListID());
											}
											//SalesTaxCodeRef_Full_Name__c
											if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C)){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C, "");
												if(invoiceLineRetType.getSalesTaxCodeRef().getFullName() != null)
													objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C, invoiceLineRetType.getSalesTaxCodeRef().getFullName());
											}
										}
										lstInvoiceItems.add(objInvoiceItem);
									}
								}
							}
						}
					}		
				}
				//list to array
				if(lstInvoices != null && lstInvoices.size() > 0){
					objAccLst = new SObject[lstInvoices.size()];
					lstInvoices.toArray(objAccLst);
					log.info("objAccLst********"+objAccLst.toString());
				}
				//logic to prepare batches of records to overcome the limit of sfdc update
				if((objAccLst != null && objAccLst.length > 0)){
					isOk = QueryExecuter.prepareAndExecuteUpdate(objAccLst,partnerConnection,SFDCObjectConstants.INVOICE__C);

					//Inserting Invoice Items
					if(lstInvoiceItems != null && lstInvoiceItems.size() > 0){
						log.info("lstInvoiceItems Size ==>> "+lstInvoiceItems.size());
						SObject[] invoiceItemList = new SObject[lstInvoiceItems.size()];
						lstInvoiceItems.toArray(invoiceItemList);
						log.info("objAccLst********"+invoiceItemList.toString());
						//logic to prepare batches of 100 to overcome the limits of sfdc update
						if(invoiceItemList != null && invoiceItemList.length > 0){
							log.info("invoiceItemList Size ==>> "+invoiceItemList.length);
							isOk = QueryExecuter.prepareAndExecuteUpdate(invoiceItemList,partnerConnection,SFDCObjectConstants.INVOICE_LINE_ITEM__C);							
						}
					}
				}
			}
		} catch (Exception e) {
			log.info("Exception while Inserting Invoices :"+ e.getMessage().toString());
		}
		log.info("***************** INVOICES DETAILS INSERT SYNC END *****************");
		return isOk;
	}
	/**
	 * updateSFDCInvoices method
	 * @param response
	 * @return String
	 * update sfdc Invoices with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String updateSFDCInvoices(java.lang.String response){
		log.info("***************** INVOICES DETAILS UPDATE SYNC START *****************");
		String isOk = "";//status of upserted results
		boolean invoiceFlag = false;//checks sql query building for invoice
		boolean invflag=false;//checks sql query building for line items
		Set<String> setInvoiceTxnIds = new HashSet<String>();//set of invoice txn ids
		Map<String,List<SObject>> mapInvoiceLineItem = new HashMap<String,List<SObject>>();
		com.appshark.invoice.mod.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.invoice.mod.rs.InvoiceModRsType> invoiceRetTypeList = null;
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.invoice.mod.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.invoice.mod.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.invoice.mod.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getInvoiceModRs() != null)){
				invoiceRetTypeList = qbxml.getQBXMLMsgsRs().getInvoiceModRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}

		try{
			if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){
				List<SObject> lstInvoices = new ArrayList<SObject>();
				List<SObject> lstInvoiceItems = new ArrayList<SObject>();
				SObject[] objAccLst = null;
				SObject[] objAccList = null;
				SObject objAcc;
				SObject objInvoiceItem;
				Set<String> setInvoiceIds = new HashSet<String>();
				Map<String,String> maptxnIDs = new HashMap<String, String>();
				String invoiceQuery = "SELECT Id,Name,RefNumber__c,TxnID__c from Invoice__c";
				if(invoiceRetTypeList != null && invoiceRetTypeList.size() > 0){
					invoiceQuery = invoiceQuery+" WHERE TxnID__c in (";
					String txnID = "";
					int count = 1;
					for(com.appshark.invoice.mod.rs.InvoiceModRsType invoiceModRsType : invoiceRetTypeList){
						com.appshark.invoice.mod.rs.InvoiceRetType  invoiceRetType = invoiceModRsType.getInvoiceRet();
			    		if(invoiceRetType != null && invoiceRetType.getTxnID() != null){
			    			txnID = txnID+"'"+invoiceRetType.getTxnID().trim()+"'";
			    			if(count != invoiceRetTypeList.size())
			    				txnID = txnID+",";
			    		}
			    		count++;
					}
					if(txnID != null && txnID.trim().endsWith(",")){
						txnID = txnID.substring(0, txnID.trim().length()-1);
					}
					invoiceQuery = invoiceQuery+txnID+")";
					log.info("insertInvoicesToSFDC invoiceQuery ==> "+invoiceQuery);
					if(txnID != null && txnID.length() > 0){
						QueryResult queryResult = partnerConnection.query(invoiceQuery);
						if (queryResult != null && (queryResult.getSize() > 0 && (queryResult.getRecords() != null && queryResult.getRecords().length > 0))) {
							SObject[] accRecords = queryResult.getRecords();
							if(accRecords != null && accRecords.length > 0){
								for(SObject sObject : accRecords){
									if(sObject != null){
										maptxnIDs.put((String)sObject.getField("TxnID__c"), (String)sObject.getField("Id"));
										setInvoiceIds.add((String)sObject.getField("Id"));
									}
								}
							}
						}
				    }
					//Invoice Line Items Data Preparation
					if(setInvoiceIds != null && setInvoiceIds.size() > 0){
						String condition = "";
						String invLineItemQuery = "select Id,Name,Quantity__c,Invoice__c,Product__r.Name,Product__r.ProductCode,Product__r.Rate__c from Invoice_Line_Item__c where Invoice__c in (";
						int cnt = 1;
						for(String invoiceId : setInvoiceIds){
							condition = condition+"'"+invoiceId+"'";
							if(cnt != setInvoiceIds.size()){
								condition = condition+",";
							}
							if(cnt == setInvoiceIds.size()){
								condition = condition+")";
								invLineItemQuery = invLineItemQuery+condition;
							}
							cnt++;
						}
						log.info("@@@ invoiceLineItemQuery ==>> "+invLineItemQuery);
						if(condition != null && condition.length() > 0){
							QueryResult queryRslt = partnerConnection.query(invLineItemQuery);
							if (queryRslt != null && queryRslt.getSize() > 0) {
								SObject[] accRecords = queryRslt.getRecords();
								if(accRecords != null && accRecords.length > 0){
									for(SObject sObject : accRecords){
										if(mapInvoiceLineItem.containsKey(sObject.getField("Invoice__c"))){
											mapInvoiceLineItem.get(sObject.getField("Invoice__c")).add(sObject);
										}else{
											List<SObject> invoiceLineItemList = new ArrayList<SObject>();
											invoiceLineItemList.add(sObject);
											mapInvoiceLineItem.put((String)sObject.getField("Invoice__c"), invoiceLineItemList);
										}
									}
								}
							}
						}
					}
				}
				//iteration of invoiceRetTypeList to map sfdc invoice fields with qbsf data
				for(com.appshark.invoice.mod.rs.InvoiceModRsType invoiceModRsType : invoiceRetTypeList){
					com.appshark.invoice.mod.rs.InvoiceRetType  invoiceRetType = invoiceModRsType.getInvoiceRet();
					if((maptxnIDs != null && maptxnIDs.size() > 0) && (invoiceRetType != null && invoiceRetType.getCustomerRef().getListID() != null)){
						if((invoiceRetType.getTxnID() != null && maptxnIDs.containsKey(invoiceRetType.getTxnID()))){
							String invoiceId = maptxnIDs.get(invoiceRetType.getTxnID());
							log.info("invoiceRetType TxnID ==>> "+invoiceRetType.getTxnID());
							if(setInvoiceFields != null && setInvoiceFields.size() > 0){//setInvoiceFields null check
								log.info("setInvoiceFields.size ==>> "+setInvoiceFields.size());
								invoiceFlag = true;
								objAcc = new SObject();
								objAcc.setType(SFDCObjectConstants.INVOICE__C);
								//id
								log.info("invoiceId******"+invoiceId);
								objAcc.setField(InvoiceConstantFields.ID, invoiceId);
								//EditSequence__c
								log.info("### EditSequence__c ==>> "+invoiceRetType.getEditSequence());
								if(setInvoiceFields.contains(InvoiceConstantFields.EDITSEQUENCE__C)){
									if(invoiceRetType.getEditSequence() != null){
										objAcc.setField(InvoiceConstantFields.EDITSEQUENCE__C, invoiceRetType.getEditSequence());

									}else{
										objAcc.setField(InvoiceConstantFields.EDITSEQUENCE__C, "");
									}
								}
								//SFDC_Update_Flag__c
								if(invoiceRetType.getTxnID() != null){
									if(setInvoiceFields.contains(InvoiceConstantFields.SFDC_UPDATE_FLAG__C)){
										objAcc.setField(InvoiceConstantFields.SFDC_UPDATE_FLAG__C,true);
										log.info("SFDC_Update_Flag__c" +objAcc.getField(InvoiceConstantFields.SFDC_UPDATE_FLAG__C));
									}
								}
								//SFDC_Update_Flag__c
								if(invoiceRetType.getTxnID() != null){
									if(setInvoiceFields.contains(InvoiceConstantFields.SFDC_FLAG__C)){
										objAcc.setField(InvoiceConstantFields.SFDC_FLAG__C,false);
										log.info("SFDC_Update_Flag__c" +objAcc.getField(InvoiceConstantFields.SFDC_FLAG__C));
									}
								}
								lstInvoices.add(objAcc);
								//Preparing Invoice Line Items
								Map<String,String> mapInvLineItemIds = new HashMap<String,String>(); 
								List<SObject> invoiceLineItemList = mapInvoiceLineItem.get(invoiceId);
								if(invoiceLineItemList != null && invoiceLineItemList.size() > 0){
									for(SObject invLineItem : invoiceLineItemList){
										mapInvLineItemIds.put((String)invLineItem.getChild("Product__r").getField("ProductCode"),(String)invLineItem.getField("Id"));
									}
								}
								List<com.appshark.invoice.mod.rs.InvoiceLineRetType> invoiceLineRetTypeList = invoiceRetType.getInvoiceLineRet();
								log.info("$$$ LINE NO ==>> 5812"+invoiceLineRetTypeList);
								if((invoiceLineRetTypeList != null && invoiceLineRetTypeList.size() > 0) && mapInvLineItemIds != null && mapInvLineItemIds.size() > 0){
									for(com.appshark.invoice.mod.rs.InvoiceLineRetType invoiceLineRetType : invoiceLineRetTypeList){
										setInvoiceTxnIds.add(invoiceRetType.getTxnID().trim());
										objInvoiceItem = new SObject();
										objInvoiceItem.setType(SFDCObjectConstants.INVOICE_LINE_ITEM__C);
										//id
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.INVOICE_LINE_ITEMS_ID)){
											if(invoiceLineRetType.getItemRef() != null && invoiceLineRetType.getItemRef().getListID() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.INVOICE_LINE_ITEMS_ID, mapInvLineItemIds.get((String)invoiceLineRetType.getItemRef().getListID()));
										}
										//TxnLineID__c
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNLINEID__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, "");
											if(invoiceLineRetType.getTxnLineID() != null){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNLINEID__C, invoiceLineRetType.getTxnLineID().trim());
											}

										}
										//SFDC_Update_Flag__c
										if(invoiceLineRetType.getTxnLineID() != null){
											if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SFDC_UPDATE_FLAG__C)){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.SFDC_UPDATE_FLAG__C,true);
												log.info("SFDC_Update_Flag__c" +objInvoiceItem.getField(InvoiceLineItemsConstantFields.SFDC_UPDATE_FLAG__C));
											}
										}
										//SFDC_FLAG__C
										if(invoiceLineRetType.getTxnLineID() != null){
											if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SFDC_FLAG__C)){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.SFDC_FLAG__C,false);
												log.info("SFDC_Update_Flag__c" +objInvoiceItem.getField(InvoiceLineItemsConstantFields.SFDC_FLAG__C));
											}
										}
										//TXNID__C
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.TXNID__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, "");
											if(invoiceRetType.getTxnID() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.TXNID__C, invoiceRetType.getTxnID().trim());
										}
										//EXTERNID__C
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.EXTERNID__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, "");
											if(invoiceRetType.getTxnID() != null && (invoiceLineRetType.getItemRef() != null && invoiceLineRetType.getItemRef().getListID() != null))
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.EXTERNID__C, invoiceRetType.getTxnID().trim()+" "+invoiceLineRetType.getItemRef().getListID().trim());
										}
										//AMOUNT__C
										if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.AMOUNT__C)){
											objInvoiceItem.setField(InvoiceLineItemsConstantFields.AMOUNT__C, "");
											if(invoiceLineRetType.getAmount() != null)
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.AMOUNT__C, invoiceLineRetType.getAmount());
										}
										//SalesTaxCodeRef_ListID__c
										if(invoiceLineRetType.getSalesTaxCodeRef() != null){
											if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C)){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C, "");
												if(invoiceLineRetType.getSalesTaxCodeRef().getListID() != null)
													objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_LISTID__C, invoiceLineRetType.getSalesTaxCodeRef().getListID());
											}
											//SalesTaxCodeRef_Full_Name__c
											if(setInvoiceLineItemFields.contains(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C)){
												objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C, "");
												if(invoiceLineRetType.getSalesTaxCodeRef().getFullName() != null)
													objInvoiceItem.setField(InvoiceLineItemsConstantFields.SALESTAXCODEREF_FULL_NAME__C, invoiceLineRetType.getSalesTaxCodeRef().getFullName());
											}
										}
										lstInvoiceItems.add(objInvoiceItem);
									}
								}
							}
						}
					}		
				}
				//list to array
				if(lstInvoices != null && lstInvoices.size() > 0){
					objAccLst = new SObject[lstInvoices.size()];
					lstInvoices.toArray(objAccLst);
					log.info("objAccLst********"+objAccLst.toString());
				}
				//logic to prepare batches of 100 to overcome the logic ofsfdc update
				if((objAccLst != null && objAccLst.length > 0) && invoiceFlag){
					log.info("objAccLst Size ==>> "+objAccLst.length);
					isOk = QueryExecuter.prepareAndExecuteUpdate(objAccLst,partnerConnection,SFDCObjectConstants.INVOICE__C);					
					//Inserting Invoice Items
					if(lstInvoiceItems != null && lstInvoiceItems.size() > 0){
						log.info("lstInvoiceItems Size ==>> "+lstInvoiceItems.size());
						SObject[] invoiceItemList = new SObject[lstInvoiceItems.size()];
						lstInvoiceItems.toArray(invoiceItemList);
						log.info("objAccLst********"+invoiceItemList.toString());
						//logic to prepare batches of 100 to over come the limit of sfdc of update
						if(invoiceItemList != null && invoiceItemList.length > 0){
							isOk = QueryExecuter.prepareAndExecuteUpdate(invoiceItemList,partnerConnection,SFDCObjectConstants.INVOICE_LINE_ITEM__C);							
						}
					}
				}
			}
		} catch (Exception e) {
			log.info("Exception while Inserting Invoices :"+ e.getMessage());
		}
		log.info("***************** INVOICES DETAILS UPDATE SYNC END *****************");
		return isOk;
	}

	/**
	 * createItems method for insert Items from QuickBooks to SFDC Products
	 * @param response
	 * @return String
	 * @Description this method is used to insert items from qbsf to sfdc products
	 */
	public java.lang.String createItems(java.lang.String response){
		log.info("***************** QB ITEMS SYNC START *****************");
		String isOk = "";//status from upserted records
		boolean flag = false;//checks sql query building
		boolean Proflag=false;//checks sql query building
		Map<String,String> mapListIds = new HashMap<String,String>();//map of listIds and SFDC_FLAG__C
		com.appshark.item.query.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.item.query.rs.ItemServiceRetType> itemServiceRetTypeList = null;//ItemServiceRetType
		//get data from generated jars
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.item.query.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.item.query.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.item.query.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getItemQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getItemQueryRs().getItemServiceRet() != null)){
				itemServiceRetTypeList = qbxml.getQBXMLMsgsRs().getItemQueryRs().getItemServiceRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}

		try{
			if(itemServiceRetTypeList != null && itemServiceRetTypeList.size() > 0){//itemServiceRetTypeList null check
				log.info("itemServiceRetTypeList Size ==>> "+itemServiceRetTypeList.size());			
				//product query to get records based on list id
				String proQuery = SFDCQueryConstants.PRODUCT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(Product2ConstantFields.QB_FLAG__C);
				fieldToBeUsed.add(Product2ConstantFields.LISTID__C);
				returnedMap = sqlQueryBuilder.buildQueryForProduct(proQuery,setProduct2Fields, SFDCObjectConstants.PRODUCT2, itemServiceRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					Proflag = (boolean) returnedMap.get("flag");
					proQuery = (String) returnedMap.get("sqlQuery");
					qbItemSynchControl = (String)returnedMap.get("lastModifiedDate");
					log.info("accQuery ==>> "+proQuery);
					log.info("invflag ==>> "+Proflag);
				}

				if(Proflag){
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(Product2ConstantFields.LISTID__C);
					fieldToBeUsed.add(Product2ConstantFields.SFDC_FLAG__C);
					mapListIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(proQuery), fieldToBeUsed);
				}
				log.info("mapListIds ==>> "+mapListIds.size());				
			}
			//START PREPARING QB DATA FOR SYNC
			if(itemServiceRetTypeList != null && itemServiceRetTypeList.size() > 0){
				List<SObject> lstItems = new ArrayList<SObject>();
				SObject[] objItemLst = null;
				SObject objItem;
				for(com.appshark.item.query.rs.ItemServiceRetType itemServiceRetType : itemServiceRetTypeList){
					if(itemServiceRetType.getListID() != null && (itemServiceRetType.getSalesOrPurchase() != null 
							&& itemServiceRetType.getSalesOrPurchase().getPrice() != null)){//itemServiceRetType null check
						if((mapListIds != null ) 
								&& ((mapListIds.isEmpty() || (mapListIds.size()>0  && !mapListIds.containsKey(itemServiceRetType.getListID().trim()))) || (mapListIds.size() > 0 && mapListIds.containsKey(itemServiceRetType.getListID().trim()) && "false".equalsIgnoreCase(mapListIds.get(itemServiceRetType.getListID().trim()))))){
							flag = true;
							objItem = new SObject();
							objItem.setType(SFDCObjectConstants.PRODUCT2);
							//name
							if(itemServiceRetType.getName() != null){
								log.info("NEW ITEM NAME ==>> "+itemServiceRetType.getName());
								objItem.setField(com.constants.fieldconstnats.Product2ConstantFields.PRODUCT2_NAME, itemServiceRetType.getName());
							}
							//PRODUCTCODE
							if(itemServiceRetType.getListID() != null){
								if(setProduct2Fields.contains(Product2ConstantFields.PRODUCTCODE)){
									objItem.setField(Product2ConstantFields.PRODUCTCODE, itemServiceRetType.getListID().trim());
								}
								if(setProduct2Fields.contains(Product2ConstantFields.LISTID__C)){
									objItem.setField(Product2ConstantFields.LISTID__C, itemServiceRetType.getListID().trim());
								}
							}
							//QB_Flag__c
							if(setAccountFields.contains(Product2ConstantFields.QB_FLAG__C)){
								objItem.setField(Product2ConstantFields.QB_FLAG__C,true);
							}
							//SFDC_Flag__c
							if(setAccountFields.contains(Product2ConstantFields.SFDC_FLAG__C)){
								objItem.setField(Product2ConstantFields.SFDC_FLAG__C,false);
							}
							//Edit_Sequence__c
							if(itemServiceRetType.getEditSequence() != null){
								if(setProduct2Fields.contains(Product2ConstantFields.EDIT_SEQUENCE__C)){
									objItem.setField(Product2ConstantFields.EDIT_SEQUENCE__C, itemServiceRetType.getEditSequence().trim());
								}
							}
							//IsActive
							if(itemServiceRetType.getIsActive() != null){
								if(setProduct2Fields.contains(Product2ConstantFields.ISACTIVE)){
									if("true".equalsIgnoreCase(itemServiceRetType.getIsActive().trim())){
										objItem.setField(Product2ConstantFields.ISACTIVE, true);
									}
									if("false".equalsIgnoreCase(itemServiceRetType.getIsActive().trim())){
										objItem.setField(Product2ConstantFields.ISACTIVE, false);
									}
								}
							}
							//Rate__c
							if(setProduct2Fields.contains(Product2ConstantFields.RATE__C)){
								if(itemServiceRetType.getSalesOrPurchase() != null && itemServiceRetType.getSalesOrPurchase().getPrice() != null){
									objItem.setField(Product2ConstantFields.RATE__C, itemServiceRetType.getSalesOrPurchase().getPrice());
								}else{
									objItem.setFieldsToNull(new String[]{Product2ConstantFields.RATE__C});
								}
							}
							//flag__c
							if(setProduct2Fields.contains(Product2ConstantFields.FLAG__C)){
								objItem.setField(Product2ConstantFields.FLAG__C,true);
							}
							//Description
							if(setProduct2Fields.contains(Product2ConstantFields.DESCRIPTION)){
								if(itemServiceRetType.getSalesOrPurchase() != null && itemServiceRetType.getSalesOrPurchase().getDesc() != null){
									objItem.setField(Product2ConstantFields.DESCRIPTION, itemServiceRetType.getSalesOrPurchase().getDesc());
								}else{
									objItem.setFieldsToNull(new String[]{Product2ConstantFields.DESCRIPTION});
								}
							}	
							lstItems.add(objItem);						
						}
					}
				}
				//adding list of items to an array
				if(lstItems != null && lstItems.size() > 0){
					objItemLst = new SObject[lstItems.size()];
					lstItems.toArray(objItemLst);
					log.info("objAccLst********"+objItemLst.toString());
				}
				//logic to prepare array in batches of 200
				if(objItemLst!=null && objItemLst.length>0){
					isOk = QueryExecuter.prepareAndExecute(objItemLst,partnerConnection,Product2ConstantFields.LISTID__C,SFDCObjectConstants.PRODUCT2);
				}			
			}
		}catch (ConnectionException e) {
			log.info("Exception while Inserting Items :"+ e);
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		log.info("***************** QB ITEMS SYNC END *****************");		
		return isOk;
	}

	/**
	 * insertProducts method
	 * @param 
	 * @return String
	 * @Description This is to insert products records from sfdc to qbsf items
	 */
	public String insertProducts(){
		String reqXML = "";//request xml query
		//get sfdc products data
		List<SObject> productList = getSFDCProductData();
		if(productList != null && productList.size() > 0){//productList null check
			log.info("productList ==>> "+productList);
			log.info("productList size ==>> "+productList.size());
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"8.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">";
			for(SObject sObject : productList){  //productList iteration to insert records to qbsf
				//PRODUCT2_NAME
				if(sObject.getField(Product2ConstantFields.PRODUCT2_NAME) != null){
					log.info("sObject.getField ==>> "+sObject.getField(Product2ConstantFields.PRODUCT2_NAME));
					log.info("sObject.getField1 ==>> "+sObject.getField(Product2ConstantFields.PRODUCT2_NAME).toString());
					reqXML = reqXML +"<ItemServiceAddRq requestID=\""+System.currentTimeMillis()+"\""+">"
							+ "<ItemServiceAdd>"
							+"<Name>"+sObject.getField(Product2ConstantFields.PRODUCT2_NAME)+"</Name>";
					reqXML = reqXML + "<SalesOrPurchase>";
					//Description
					if(sObject.getField(Product2ConstantFields.DESCRIPTION) !=null && sObject.getField(Product2ConstantFields.DESCRIPTION).toString().length() > 0){
						reqXML = reqXML + "<Desc>"+sObject.getField(Product2ConstantFields.DESCRIPTION)+"</Desc>";
					}else{
						reqXML = reqXML + "<Desc></Desc>";
					}
					//RATE__C
					if(sObject.getField(Product2ConstantFields.RATE__C) !=null){
						reqXML = reqXML + "<Price>"+sObject.getField(Product2ConstantFields.RATE__C)+"</Price>";
					}else{
						reqXML = reqXML + "<Price></Price>";
					}
					reqXML = reqXML + "<AccountRef>";
					reqXML = reqXML + "<FullName>Payroll Expenses</FullName>";
					reqXML = reqXML + "</AccountRef> </SalesOrPurchase>"
							+"</ItemServiceAdd>"
							+ "</ItemServiceAddRq>";
				}
			}
			reqXML = reqXML + "</QBXMLMsgsRq>"
					+"</QBXML>";
		}else{
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"8.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">"
					+"</QBXMLMsgsRq>"
					+"</QBXML>";
		}
		return reqXML;
	}
	/**
	 * getSFDCProductData method
	 * @param 
	 * @return String
	 * @Description This is to get products records from sfdc
	 */
	public List<SObject> getSFDCProductData(){
		List<SObject> productList = null;//list of products from products query
		boolean flag = false;//checks sql query building
		String productInsertSynchControl = "";//last synch date of products
		try{
			//query to get Product_Insert_Last_Sync__c field
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.PRODUCT_INSERT_LAST_SYNC__C)){
				synchQuery = synchQuery+", Product_Insert_Last_Sync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				log.info("qResult ==>> "+qResult);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();					
					if(records[0].getField(SFDCSynchControlConstants.PRODUCT_INSERT_LAST_SYNC__C) != null){
						log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.PRODUCT_INSERT_LAST_SYNC__C));
						productInsertSynchControl = records[0].getField(SFDCSynchControlConstants.PRODUCT_INSERT_LAST_SYNC__C).toString();
					}
					log.info("productInsertSynchControl ==>> "+flag);
				}
			}
			//query to get products data based on CreatedDate & LastModifiedDate
			String accQuery = "SELECT ID,Name,IsActive,ProductCode,Description,Edit_Sequence__c,ListID__c,Rate__c,CreatedDate,LastModifiedDate FROM Product2";
			if(productInsertSynchControl != null && productInsertSynchControl.length() > 0){
				accQuery = accQuery + " WHERE (CreatedDate > "+productInsertSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+productInsertSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] accRecords = queryResult.getRecords();
				if(accRecords != null && accRecords.length > 0){
					productList = new ArrayList<SObject>();
					for(SObject sObject : accRecords){
						if((sObject.getField("ListID__c") == null || "".equals(sObject.getField("ListID__c"))) && 
								(sObject.getField("Edit_Sequence__c") == null || "".equals(sObject.getField("Edit_Sequence__c")))){
							productList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//method that returns sorted lastModifiedDate
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);

							}
						}
						index++;
					}
				}
			}
			//lastModifiedDate null check
			if(lastModifiedDate != null && lastModifiedDate!=""){
				sfdcInsertProductLastSyncControl = lastModifiedDate;
				log.info("sfdcInsertProductLastSyncControl ==> "+sfdcInsertProductLastSyncControl);
			}
			//}
		}catch(Exception e){
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		return productList;
	}

	/**
	 * insertProductsDetToSFDC method
	 * @param response
	 * @return String
	 * insert sfdc Products with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String insertProductsDetToSFDC(java.lang.String response){
		log.info("***************** PRODUCTS DETAILS INSERT SYNC START *****************");
		String isOk = "";
		boolean flag = false;
		boolean proflag=false;
		com.appshark.item.service.add.rs.QBXMLType qbxml = null;
		List<com.appshark.item.service.add.rs.ItemServiceAddRsType> itemServiceAddRsTypeList = null;
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.item.service.add.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.item.service.add.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.item.service.add.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getItemServiceAddRs() != null)){
				itemServiceAddRsTypeList = qbxml.getQBXMLMsgsRs().getItemServiceAddRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}

		try{
			if(itemServiceAddRsTypeList != null && itemServiceAddRsTypeList.size() > 0){//itemServiceAddRsTypeList null check
				List<SObject> lstProducts = new ArrayList<SObject>();//list of products
				SObject[] objProductLst = null;//sobject array to update to sfdc
				SObject objProduct;//sobject to map fields
				Map<String,String> mapProductNames = new HashMap<String, String>();
				//Preparing Query for Account
				String proQuery = SFDCQueryConstants.PRODUCT_QUERY_NAME;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(Product2ConstantFields.ID);
				fieldToBeUsed.add(Product2ConstantFields.PRODUCT2_NAME);
				returnedMap = sqlQueryBuilder.buildQueryForProductNamesInsert(proQuery,setProduct2Fields, SFDCObjectConstants.PRODUCT2, itemServiceAddRsTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					proflag = (boolean) returnedMap.get("flag");
					proQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+proQuery);
					log.info("invflag ==>> "+proflag);
				}

				if(proflag){
					//fields used to prepare map
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(Product2ConstantFields.PRODUCT2_NAME);
					fieldToBeUsed.add(Product2ConstantFields.ID);
					mapProductNames = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(proQuery), fieldToBeUsed);
				}
				//iterate qbsf data to map the data with sfdc fields
				for(com.appshark.item.service.add.rs.ItemServiceAddRsType itemServiceAddRsType : itemServiceAddRsTypeList){
					if(itemServiceAddRsType != null &&  
							(itemServiceAddRsType.getItemServiceRet() != null && itemServiceAddRsType.getItemServiceRet().size() > 0)){
						com.appshark.item.service.add.rs.ItemServiceRetType itemServiceRetType = itemServiceAddRsType.getItemServiceRet().get(0);
						if(itemServiceRetType != null && itemServiceRetType.getListID() != null){//itemServiceAddRsType null check
							if(mapProductNames != null && mapProductNames.size() > 0){
								if(mapProductNames.containsKey(itemServiceRetType.getFullName())){//item name is in mapProductNames
									flag = true;
									objProduct = new SObject();
									objProduct.setType(SFDCObjectConstants.PRODUCT2);
									//
									if(itemServiceRetType.getListID() != null){
										//id
										objProduct.setField(Product2ConstantFields.ID, mapProductNames.get(itemServiceRetType.getFullName()));
										//LISTID__C
										if(setProduct2Fields.contains(Product2ConstantFields.LISTID__C)){
											objProduct.setField(Product2ConstantFields.LISTID__C, itemServiceRetType.getListID());
										}
										//ProductCode
										if(setProduct2Fields.contains(Product2ConstantFields.PRODUCTCODE)){
											objProduct.setField(Product2ConstantFields.PRODUCTCODE, itemServiceRetType.getListID());
										}
									}
									//Edit_Sequence__c
									if(itemServiceRetType.getEditSequence() != null){
										if(setProduct2Fields.contains(Product2ConstantFields.EDIT_SEQUENCE__C)){
											objProduct.setField(Product2ConstantFields.EDIT_SEQUENCE__C, itemServiceRetType.getEditSequence());
										}
									}
									//ISACTIVE
									if(itemServiceRetType.getIsActive() != null){
										if(setProduct2Fields.contains(Product2ConstantFields.ISACTIVE)){
											if("true".equalsIgnoreCase(itemServiceRetType.getIsActive().trim())){
												objProduct.setField(Product2ConstantFields.ISACTIVE, true);
											}else if("false".equalsIgnoreCase(itemServiceRetType.getIsActive().trim())){
												objProduct.setField(Product2ConstantFields.ISACTIVE, false);
											}
										}
									}
									lstProducts.add(objProduct);
								}
							}
						}
					}
				}
				//list to array
				if(lstProducts != null && lstProducts.size() > 0){
					objProductLst = new SObject[lstProducts.size()];
					lstProducts.toArray(objProductLst);
					log.info("objAccLst********"+objProductLst.toString());
				}
				//logic to prepare batches of 100 to overcome limit of sfdc while update
				if((objProductLst != null && objProductLst.length > 0) && flag){
					isOk = QueryExecuter.prepareAndExecuteUpdate(objProductLst,partnerConnection,SFDCObjectConstants.PRODUCT2);
					log.info("objProductLst Size ==>> "+objProductLst.length);					
				}
			}
		} catch (ConnectionException e) {
			log.info("Exception while Inserting Products to SFDC :"+ e);
		}
		log.info("***************** PRODUCTS DETAILS INSERT SYNC END *****************");
		return isOk;
	}

	/**
	 * updateProducts method
	 * @param 
	 * @return String
	 * @Description This is to update products records from sfdc to qbsf items
	 */
	public String updateProducts(){
		String reqXML = "";//contains the request xml query
		//get sfdc updated products
		List<SObject> productList = getModifiedSFDCProductData();
		if(productList != null && productList.size() > 0){//productList null check
			log.info("productList ==>> "+productList);
			log.info("productList size ==>> "+productList.size());
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"8.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">";
			for(SObject sObject : productList){  		//productList iteration to update records to qbsf

				if(sObject.getField(Product2ConstantFields.LISTID__C) != null && sObject.getField(Product2ConstantFields.EDIT_SEQUENCE__C) != null && sObject.getField(Product2ConstantFields.PRODUCT2_NAME)!=null){
					log.info("sObject.getField ==>> "+sObject.getField(Product2ConstantFields.PRODUCT2_NAME));
					log.info("sObject.getField1 ==>> "+sObject.getField(Product2ConstantFields.PRODUCT2_NAME).toString());
					reqXML = reqXML +"<ItemServiceModRq requestID=\""+System.currentTimeMillis()+"\""+">"
							+ "<ItemServiceMod>"
							+"<ListID>"+sObject.getField(Product2ConstantFields.LISTID__C)+"</ListID>"
							+"<EditSequence>"+sObject.getField(Product2ConstantFields.EDIT_SEQUENCE__C)+"</EditSequence>"
							+"<Name>"+sObject.getField(Product2ConstantFields.PRODUCT2_NAME)+"</Name>";
					reqXML = reqXML + "<SalesOrPurchaseMod>";
					//DESCRIPTION
					if(sObject.getField(Product2ConstantFields.DESCRIPTION) !=null && sObject.getField(Product2ConstantFields.DESCRIPTION).toString().length() > 0){
						reqXML = reqXML + "<Desc>"+sObject.getField(Product2ConstantFields.DESCRIPTION)+"</Desc>";
					}else{
						reqXML = reqXML + "<Desc></Desc>";
					}
					//RATE__C
					if(sObject.getField(Product2ConstantFields.RATE__C) !=null){
						reqXML = reqXML + "<Price>"+sObject.getField(Product2ConstantFields.RATE__C)+"</Price>";
					}else{
						reqXML = reqXML + "<Price></Price>";
					}
					reqXML = reqXML + "<AccountRef>";
					reqXML = reqXML + "<FullName>Payroll Expenses</FullName>";
					reqXML = reqXML + "</AccountRef> </SalesOrPurchaseMod>"
							+"</ItemServiceMod>"
							+ "</ItemServiceModRq>";
				}
			}
			reqXML = reqXML + "</QBXMLMsgsRq>"
					+"</QBXML>";		
		}else{
			reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
					+"<?qbxml version="+"\"8.0\""+"?>"
					+"<QBXML>"
					+"<QBXMLMsgsRq onError=\"stopOnError\">"
					+"</QBXMLMsgsRq>"
					+"</QBXML>";
		}
		return reqXML;
	}
	/**
	 * getModifiedSFDCProductData method
	 * @param 
	 * @return String
	 * @Description This is to get products records from sfdc
	 */
	public List<SObject> getModifiedSFDCProductData(){
		List<SObject> productList = null;//productList from results of product query
		boolean flag = false;
		String productUpdateSynchControl = "";//lastModifed date of products
		try{
			//query to get Product_Update_Last_Sync__c value
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.PRODUCT_UPDATE_LAST_SYNC__C)){
				synchQuery = synchQuery+", Product_Update_Last_Sync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				log.info("qResult ==>> "+qResult);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();

					if(records[0].getField(SFDCSynchControlConstants.PRODUCT_UPDATE_LAST_SYNC__C) != null){
						log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.PRODUCT_UPDATE_LAST_SYNC__C));
						productUpdateSynchControl = records[0].getField(SFDCSynchControlConstants.PRODUCT_UPDATE_LAST_SYNC__C).toString();
					}
					log.info("productInsertSynchControl ==>> "+flag);
				}
			}
			//product query based on CreatedDate & LastModifiedDate
			String accQuery = "SELECT ID,Name,IsActive,ProductCode,Description,Edit_Sequence__c,ListID__c,Rate__c,CreatedDate,LastModifiedDate,SFDC_Flag__c FROM Product2 WHERE SFDC_Flag__c=true";
			if(productUpdateSynchControl != null && productUpdateSynchControl.length() > 0){
				accQuery = accQuery + " AND (CreatedDate > "+productUpdateSynchControl;
				accQuery = accQuery + " OR LastModifiedDate > "+productUpdateSynchControl+")";
			}
			log.info("@@@ accQuery ==>> "+accQuery);
			QueryResult queryResult = partnerConnection.query(accQuery);
			String lastModifiedDate = null;
			int index = 0;
			if (queryResult!=null && queryResult.getSize() > 0) {//queryResult null check
				SObject[] accRecords = queryResult.getRecords();
				if(accRecords != null && accRecords.length > 0){
					productList = new ArrayList<SObject>();
					for(SObject sObject : accRecords){
						if((sObject.getField("ListID__c") != null && sObject.getField("ListID__c").toString().trim().length() > 0) && 
								(sObject.getField("Edit_Sequence__c") != null && sObject.getField("Edit_Sequence__c").toString().trim().length() > 0)){
							productList.add(sObject);
							if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
								//sfdcUpdateSyncDateSet.add(sObject.getField("LastModifiedDate").toString());
								lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);

							}
						}
					}
				}
			}			
			if(lastModifiedDate != null && lastModifiedDate!=null){
				sfdcUpdateProductLastSyncControl = lastModifiedDate;
				log.info("sfdcUpdateProductLastSyncControl ==> "+sfdcUpdateProductLastSyncControl);
			}
			//}
		}catch(Exception e){
			//Exception
		}
		return productList;
	}

	/**
	 * insertProductsDetToSFDC method
	 * @param response
	 * @return String
	 * insert sfdc Products with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String updateProductsDetToSFDC(java.lang.String response){
		log.info("***************** PRODUCTS DETAILS UPDATE SYNC START *****************");
		String isOk = "";//status of updated records
		boolean flag = false;//checks sql query building
		boolean proflag=false;//checks sql query building
		com.appshark.item.service.mod.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.item.service.mod.rs.ItemServiceModRsType> itemServiceModRsTypeList = null;//ItemServiceModRsType
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.item.service.mod.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.item.service.mod.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.item.service.mod.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getItemServiceModRs() != null)){
				itemServiceModRsTypeList = qbxml.getQBXMLMsgsRs().getItemServiceModRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}
		try{
			if(itemServiceModRsTypeList != null && itemServiceModRsTypeList.size() > 0){//itemServiceModRsTypeList nullc ehck
				List<SObject> lstProducts = new ArrayList<SObject>();
				SObject[] objProductLst = null;
				SObject objProduct;
				Map<String,String> mapProductNames = new HashMap<String, String>();
				//Preparing Query for Account
				String proQuery = SFDCQueryConstants.PRODUCT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(Product2ConstantFields.LISTID__C);
				fieldToBeUsed.add(Product2ConstantFields.ID);
				returnedMap = sqlQueryBuilder.buildQueryForProductNamesUpdate(proQuery,setProduct2Fields, SFDCObjectConstants.PRODUCT2, itemServiceModRsTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					proflag = (boolean) returnedMap.get("flag");
					proQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+proQuery);
					log.info("invflag ==>> "+proflag);
				}

				if(proflag){
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(Product2ConstantFields.LISTID__C);
					fieldToBeUsed.add(Product2ConstantFields.ID);
					mapProductNames = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(proQuery), fieldToBeUsed);
				}
				//iteration of qbsf data to map the data to sfdc fields
				for(com.appshark.item.service.mod.rs.ItemServiceModRsType itemServiceModRsType : itemServiceModRsTypeList){
					if(itemServiceModRsType != null && 
							(itemServiceModRsType.getItemServiceRet() != null && itemServiceModRsType.getItemServiceRet().size() > 0)){
						com.appshark.item.service.mod.rs.ItemServiceRetType itemServiceRetType = itemServiceModRsType.getItemServiceRet().get(0);
						if(itemServiceRetType != null && itemServiceRetType.getListID() != null){//itemServiceRetType null check
							if(mapProductNames != null && mapProductNames.size() > 0){
								if(mapProductNames.containsKey(itemServiceRetType.getListID())){
									flag = true;
									objProduct = new SObject();
									objProduct.setType(SFDCObjectConstants.PRODUCT2);
									if(itemServiceRetType.getListID() != null){
										//id
										objProduct.setField(Product2ConstantFields.ID, mapProductNames.get(itemServiceRetType.getListID()));
										//LISTID__C
										if(setProduct2Fields.contains(Product2ConstantFields.LISTID__C)){
											objProduct.setField(Product2ConstantFields.LISTID__C, itemServiceRetType.getListID());
										}
										//ProductCode
										if(setProduct2Fields.contains(Product2ConstantFields.PRODUCT_CODE)){
											objProduct.setField(Product2ConstantFields.PRODUCT_CODE, itemServiceRetType.getListID());
										}
										//SFDC_UPDATE_FLAG__C
										log.info("setProduct2Fields" + setProduct2Fields);
										if(setProduct2Fields.contains(Product2ConstantFields.SFDC_UPDATE_FLAG__C)){
											objProduct.setField(Product2ConstantFields.SFDC_UPDATE_FLAG__C,true);
											log.info("SFDC_Update_Flag__c" +objProduct.getField(Product2ConstantFields.SFDC_UPDATE_FLAG__C));
										}
										//SFDC_FLAG__C
										log.info("setProduct2Fields" + setProduct2Fields);
										if(setProduct2Fields.contains(Product2ConstantFields.SFDC_FLAG__C)){
											objProduct.setField(Product2ConstantFields.SFDC_FLAG__C,true);
											log.info("SFDC_Update_Flag__c" +objProduct.getField(Product2ConstantFields.SFDC_FLAG__C));
										}
									}
									//EDIT_SEQUENCE__C
									if(itemServiceRetType.getEditSequence() != null){
										if(setProduct2Fields.contains(Product2ConstantFields.EDIT_SEQUENCE__C)){
											objProduct.setField(Product2ConstantFields.EDIT_SEQUENCE__C, itemServiceRetType.getEditSequence());
										}
									}
									lstProducts.add(objProduct);
								}
							}
						}
					}
				}
				//list to array
				if(lstProducts != null && lstProducts.size() > 0){
					objProductLst = new SObject[lstProducts.size()];
					lstProducts.toArray(objProductLst);
					log.info("objAccLst********"+objProductLst.toString());
				}
				//logic to prepare batches of 100 to overcome limit of sfdc update
				if((objProductLst != null && objProductLst.length > 0) && flag){
					log.info("objProductLst Size ==>> "+objProductLst.length);
					isOk = QueryExecuter.prepareAndExecuteUpdate(objProductLst,partnerConnection,SFDCObjectConstants.PRODUCT2);					
				}
			}
		} catch (ConnectionException e) {
			log.info("Exception while Inserting Products to SFDC :"+ e);
		}
		log.info("***************** PRODUCTS DETAILS UPDATE SYNC END *****************");
		return isOk;
	}
	/**
	 * insertOpportunities method
	 * @param 
	 * @return String
	 * @Description This is to insert Opportunities records from sfdc to qbsf estimates
	 */
	public String insertOpportunities(){
		Map<String,List<SObject>> mapOppLineItem = new HashMap<String,List<SObject>>();
		Set<String> setOpportunityIds = new HashSet<String>();
		String reqXML = "";

		try{
			//get sfdc opportunity data
			List<SObject> OpportunityList = getSFDCOpportunityData();
			if(OpportunityList != null && OpportunityList.size() > 0){//OpportunityList null check and sets opportunity ids to a set
				for(SObject sObject : OpportunityList){
					setOpportunityIds.add((String)sObject.getField("Id"));
				}
				//setOpportunityIds null check
				if(setOpportunityIds != null && setOpportunityIds.size() > 0){
					String condition = "";
					//query to get opportunity line items
					String oppLineItemQuery = "SELECT Id,OpportunityId,ProductCode,Quantity, ListPrice, PriceBookEntry.UnitPrice, PricebookEntry.Name,Description, PricebookEntry.product2.Family FROM OpportunityLineItem where OpportunityId in (";
					int count = 1;
					for(String opportunityId : setOpportunityIds){
						condition = condition+"'"+opportunityId+"'";
						if(count != setOpportunityIds.size()){
							condition = condition+",";
						}
						if(count == setOpportunityIds.size()){
							condition = condition+")";
							oppLineItemQuery = oppLineItemQuery+condition;
						}
						count++;
					}
					log.info("@@@ oppLineItemQuery ==>> "+oppLineItemQuery);
					QueryResult objQueryResult = partnerConnection.query(oppLineItemQuery);
					log.info("@@@ objQueryResult ==>> "+objQueryResult);
					if (objQueryResult!=null && objQueryResult !=null) {//objQueryResult null check
						SObject[] oppLineItemRecords = objQueryResult.getRecords();
						if(oppLineItemRecords != null && oppLineItemRecords.length > 0){//oppLineItemRecords from query result
							log.info("@@@ oppLineItemRecords length ==>> "+oppLineItemRecords.length);
							for(SObject sObject : oppLineItemRecords){//preparing map for opportunity id and sobject
								if(mapOppLineItem.containsKey(sObject.getField("OpportunityId"))){
									mapOppLineItem.get(sObject.getField("OpportunityId")).add(sObject);
								}else{
									List<SObject> invoiceLineItemList = new ArrayList<SObject>();
									invoiceLineItemList.add(sObject);
									mapOppLineItem.put((String)sObject.getField("OpportunityId"), invoiceLineItemList);
								}
							}
							log.info("@@@ mapOppLineItem size ==>> "+mapOppLineItem.size());
						}
					}
				}
			}
			//OpportunityList null check to prepare query to insert opportunities
			if(OpportunityList != null && OpportunityList.size() > 0){
				log.info("OpportunityList ==>> "+OpportunityList);
				log.info("OpportunityList size ==>> "+OpportunityList.size());
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"12.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">";
				for(SObject sObject : OpportunityList){ 
					log.info("$$$ Acc List Id ==> "+sObject.getChild("Account").getField("ListID__c"));//OpportunityList iteration 
					if(sObject.getChild("Account") != null 
							&& sObject.getChild("Account").getField("ListID__c") != null){
						log.info("$$$ Acc List Id ==> "+sObject.getChild("Account").getField("ListID__c"));
						//Ref_Number__c
						if(sObject.getField(OpportunityConstantFields.REF_NUMBER__C) != null){
							reqXML = reqXML +"<EstimateAddRq requestID=\""+System.currentTimeMillis()+"\""+">"
									+"<EstimateAdd>" 
									+"<CustomerRef>"
									+"<ListID>"+sObject.getChild("Account").getField("ListID__c")+"</ListID>"
									+"</CustomerRef>"; 
							reqXML = reqXML +"<TxnDate>"+sObject.getField(OpportunityConstantFields.CLOSEDATE)+"</TxnDate>";
							reqXML = reqXML +"<RefNumber>"+sObject.getField(OpportunityConstantFields.REF_NUMBER__C)+"</RefNumber>" 
									+"<BillAddress>";

							//ADDRESS__C
							if(sObject.getField(OpportunityConstantFields.ADDRESS__C) != null){
								reqXML = reqXML +"<Addr1>"+sObject.getField(OpportunityConstantFields.ADDRESS__C)+"</Addr1>";
							}else{
								reqXML = reqXML +"<Addr1></Addr1>";
							}
							log.info("reqXML"+reqXML);
							//City__c
							if(sObject.getField(OpportunityConstantFields.CITY__C) != null){
								reqXML = reqXML +"<City>"+sObject.getField(OpportunityConstantFields.CITY__C)+"</City>";
							}else{
								reqXML = reqXML +"<City></City>";
							}
							//State__c
							if(sObject.getField(OpportunityConstantFields.CITY__C) != null){
								reqXML = reqXML +"<State>"+sObject.getField(OpportunityConstantFields.CITY__C)+"</State>";
							}else{
								reqXML = reqXML +"<State></State>";
							}
							log.info("reqXML"+reqXML);
							//POSTAL_CODE__C
							if(sObject.getField(OpportunityConstantFields.POSTAL_CODE__C) != null){
								reqXML = reqXML +"<PostalCode>"+sObject.getField(OpportunityConstantFields.POSTAL_CODE__C)+"</PostalCode>";
							}else{
								reqXML = reqXML +"<PostalCode></PostalCode>";
							}
							log.info("reqXML"+reqXML);
							//COUNTRY__C
							if(sObject.getField(OpportunityConstantFields.COUNTRY__C) != null){
								reqXML = reqXML +"<Country>"+sObject.getField(OpportunityConstantFields.COUNTRY__C)+"</Country>";
							}else{
								reqXML = reqXML +"<Country></Country>";
							}
							log.info("reqXML"+reqXML);
							reqXML = reqXML +"</BillAddress>";
							log.info("reqXML"+reqXML);
							List<SObject> OpportunityItemList = mapOppLineItem.get(sObject.getField("Id"));
							if(OpportunityItemList != null && OpportunityItemList.size() > 0){
								for(SObject sObj : OpportunityItemList){
									if(sObj.getField("ProductCode") != null){
										reqXML = reqXML + "<EstimateLineAdd>" 
												+"<ItemRef>" 
												+"<ListID>"+sObj.getField("ProductCode")+"</ListID>"
												+"</ItemRef>"; 
										log.info("reqXML"+reqXML);
										//DESCRIPTION
										if(sObj.getField(OpportunityLineItem.DESCRIPTION) != null){
											reqXML = reqXML + "<Desc>"+sObj.getField(OpportunityLineItem.DESCRIPTION)+"</Desc>";
										}else{
											reqXML = reqXML + "<Desc></Desc>";
										}
										log.info("reqXML"+reqXML);
										//QUANTITY
										if(sObj.getField(OpportunityLineItem.QUANTITY) != null){
											reqXML = reqXML + "<Quantity>"+sObj.getField(OpportunityLineItem.QUANTITY)+"</Quantity>";
										}else{
											reqXML = reqXML + "<Quantity></Quantity>";
										}
										log.info("reqXML"+reqXML);
										//LIST_PRICE
										if(sObj.getField(OpportunityLineItem.LIST_PRICE) != null){
											reqXML = reqXML + "<Rate>"+sObj.getField(OpportunityLineItem.LIST_PRICE)+"</Rate>";
										}else{
											reqXML = reqXML + "<Rate></Rate>";
										}
										log.info("reqXML"+reqXML);
										reqXML = reqXML + "</EstimateLineAdd>";
									}
									log.info("reqXML"+reqXML);
								}
							}else{
								log.info("Opportunity Object did not have Line Items");
							}
							reqXML = reqXML + "</EstimateAdd>"
									+ "</EstimateAddRq>";
							log.info("reqXML"+reqXML);
						}
					}
				}
				log.info("reqXML"+reqXML);
				reqXML = reqXML + "</QBXMLMsgsRq>"
						+"</QBXML>";
				log.info("@@@ reqXML ==> "+reqXML);
			}else{
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"12.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">"
						+"</QBXMLMsgsRq>"
						+"</QBXML>";
			}
		}catch(Exception e){
			//Exception
		}
		return reqXML;
	}
	/**
	 * insertSFDCOpportunities method
	 * @param response
	 * @return String
	 * insert sfdc Opportunities with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String insertSFDCOpportunities(java.lang.String response){
		log.info("***************** OPPORTUNITY DETAILS INSERT SYNC START *****************");
		String isOk = "";//status of upserted records
		boolean opportunityFlag = false;//checks sql query building is done properly
		boolean oppFlag=false;//checks sql query building is done properly
		Set<String> setOpportunityIds = new HashSet<String>();//set of opportunity ids
		Map<String,List<SObject>> mapOppLineItem = new HashMap<String,List<SObject>>();//map of opportunity ids and opportunity line item sobject
		com.appshark.estimate.add.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.estimate.add.rs.EstimateAddRsType> estimateAddRsTypeList = null;//EstimateAddRsType
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.estimate.add.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.estimate.add.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.estimate.add.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getEstimateAddRs() != null)){
				estimateAddRsTypeList = qbxml.getQBXMLMsgsRs().getEstimateAddRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}

		try{
			if(estimateAddRsTypeList != null && estimateAddRsTypeList.size() > 0){//estimateAddRsTypeList null check
				List<SObject> lstOpportunities = new ArrayList<SObject>();
				List<SObject> lstOpportunityItems = new ArrayList<SObject>();
				SObject[] objOpportunityLst = null;
				SObject objOpportunity;
				SObject objOpportunityItem;
				Map<String,String> maprefNumbers = new HashMap<String, String>();
				//query of opportunity with ref number
				if((estimateAddRsTypeList != null && estimateAddRsTypeList.size() > 0)){
					//preparing query for estimate
					String opportunityQuery = SFDCQueryConstants.ESTIMATE_QUERY_INSERT;
					//fields to be checked conditionally for dynamic query building
					fieldToBeUsed= new ArrayList<String>();
					fieldToBeUsed.add(OpportunityConstantFields.REF_NUMBER__C);
					fieldToBeUsed.add(OpportunityConstantFields.OPPORTUNITY_ID);
					log.info("fieldToBeUsed"+fieldToBeUsed);
					//building query
					Map<String,Object> returnedMap = sqlQueryBuilder.buildQueryForEsimateInsert(opportunityQuery,setOpportunityFields, SFDCObjectConstants.OPPORTUNITY, estimateAddRsTypeList,fieldToBeUsed);
					//extracting return result after query building
					if(returnedMap != null && returnedMap.size() > 0){
						oppFlag = (boolean) returnedMap.get("flag");
						opportunityQuery = (String) returnedMap.get("sqlQuery");
						log.info("accQuery ==>> "+opportunityQuery);
						log.info("invflag ==>> "+oppFlag);
					}

					//check flag and prepare a map from query which was returnd from query builder
					if(oppFlag){	
						//Retrieving Account objects from Salesforce
						fieldToBeUsed = new ArrayList<String>();
						fieldToBeUsed.add(OpportunityConstantFields.REF_NUMBER__C);
						fieldToBeUsed.add(OpportunityConstantFields.OPPORTUNITY_ID);
						maprefNumbers = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(opportunityQuery), fieldToBeUsed);	
						setOpportunityIds=QueryResultIterator.prepareSetFromQueryResult(partnerConnection.query(opportunityQuery), "Id");
					}
					//Estimate Line Items Data Preparation
					if(setOpportunityIds != null && setOpportunityIds.size() > 0){
						String condition = "";
						String oppLineItemQuery = "SELECT Id,OpportunityId,ProductCode,Quantity, ListPrice, PriceBookEntry.UnitPrice, PricebookEntry.Name,Description, PricebookEntry.product2.Family FROM OpportunityLineItem where OpportunityId in (";
						int cnt = 1;
						for(String OpportunityId : setOpportunityIds){
							condition = condition+"'"+OpportunityId+"'";
							if(cnt != setOpportunityIds.size())
								condition = condition+",";
							if(cnt == setOpportunityIds.size()){
								condition = condition+")";
							}	
							cnt++;
						}
						if(condition != null && condition.length() > 0){
							oppLineItemQuery = oppLineItemQuery + condition;
							log.info("@@@ oppLineItemQuery ==>> "+oppLineItemQuery);
							QueryResult queryRslt = partnerConnection.query(oppLineItemQuery);
							if (queryRslt != null && queryRslt.getSize() > 0) {//result from query
								SObject[] oppLineItemRecords = queryRslt.getRecords();
								if(oppLineItemRecords != null && oppLineItemRecords.length > 0){//records from query results
									for(SObject sObject : oppLineItemRecords){
										if(sObject != null && sObject.getField("OpportunityId") != null){
											if(mapOppLineItem.containsKey(sObject.getField("OpportunityId"))){
												mapOppLineItem.get(sObject.getField("OpportunityId")).add(sObject);//prepare map of opportunity id and sobject
											}else{
												List<SObject> oppLineItemList = new ArrayList<SObject>();
												oppLineItemList.add(sObject);
												mapOppLineItem.put((String)sObject.getField("OpportunityId"), oppLineItemList);
											}
										}
									}
								}
							}
						}
					}
				}
				//iteration of estimateAddRsTypeList to map qbsf data with sfdc fields
				for(com.appshark.estimate.add.rs.EstimateAddRsType estimateAddRsType : estimateAddRsTypeList){
					if(estimateAddRsType != null && (estimateAddRsType.getEstimateRet() != null && estimateAddRsType.getEstimateRet().size() > 0)){
						com.appshark.estimate.add.rs.EstimateRetType estimateRetType = estimateAddRsType.getEstimateRet().get(0);
						if((maprefNumbers != null && maprefNumbers.size() > 0) && (estimateRetType != null && (estimateRetType.getCustomerRef() != null && estimateRetType.getCustomerRef().getListID() != null))){
							if((estimateRetType.getRefNumber() != null && maprefNumbers.containsKey(estimateRetType.getRefNumber())) && estimateRetType.getTxnID() != null){
								String productId = maprefNumbers.get(estimateRetType.getRefNumber());
								log.info("invoiceRetType TxnID ==>> "+estimateRetType.getTxnID());
								if(setOpportunityFields != null && setOpportunityFields.size() > 0){
									log.info("setOpportunityFields.size ==>> "+setOpportunityFields.size());
									opportunityFlag = true;
									objOpportunity = new SObject();
									objOpportunity.setType(SFDCObjectConstants.OPPORTUNITY);
									//OPPORTUNITY_ID
									objOpportunity.setField(OpportunityConstantFields.OPPORTUNITY_ID, maprefNumbers.get(estimateRetType.getRefNumber()));
									//TxnID__c
									if(setOpportunityFields.contains(OpportunityConstantFields.TXNID__C)){
										if(estimateRetType.getTxnID() != null){
											objOpportunity.setField(OpportunityConstantFields.TXNID__C, estimateRetType.getTxnID().trim());
										}
									}
									//EditSequence__c
									if(setOpportunityFields.contains(OpportunityConstantFields.EDITSEQUENCE__C)){
										if(estimateRetType.getEditSequence() != null){
											objOpportunity.setField(OpportunityConstantFields.EDITSEQUENCE__C, estimateRetType.getEditSequence());
										}else{
											objOpportunity.setField(OpportunityConstantFields.EDITSEQUENCE__C, "");
										}
									}
									lstOpportunities.add(objOpportunity);
									//Preparing opportunity Line Items
									Map<String,String> mapOppLineItemIds = new HashMap<String,String>(); 
									List<SObject> oppLineItemList = mapOppLineItem.get(productId);
									if(oppLineItemList != null && oppLineItemList.size() > 0){
										for(SObject oppLineItem : oppLineItemList){
											mapOppLineItemIds.put((String)oppLineItem.getField("ProductCode"),(String)oppLineItem.getField("Id"));
										}
									}
									List<com.appshark.estimate.add.rs.EstimateLineRetType> estimateLineRetTypeList = estimateRetType.getEstimateLineRet();
									log.info("$$$ LINE NO ==>> 4687"+estimateLineRetTypeList);
									if((estimateLineRetTypeList != null && estimateLineRetTypeList.size() > 0) && (mapOppLineItemIds != null && mapOppLineItemIds.size() > 0)){
										for(com.appshark.estimate.add.rs.EstimateLineRetType estimateLineRetType : estimateLineRetTypeList){
											objOpportunityItem = new SObject();
											objOpportunityItem.setType(SFDCObjectConstants.OPPORTUNITYLINEITEM);
											//OPPORTUNITYLINEITEM_ID
											if(setOppLineItemFields.contains(OpportunityLineItem.OPPORTUNITYLINEITEM_ID)){
												if(estimateLineRetType.getItemRef() != null && estimateLineRetType.getItemRef().getListID() != null)
													objOpportunityItem.setField(OpportunityLineItem.OPPORTUNITYLINEITEM_ID, mapOppLineItemIds.get((String)estimateLineRetType.getItemRef().getListID()));
											}
											//TXN_LINEID__C
											if(setOppLineItemFields.contains(OpportunityLineItem.TXN_LINEID__C)){
												objOpportunityItem.setField(OpportunityLineItem.TXN_LINEID__C, "");
												if(estimateLineRetType.getTxnLineID() != null)
													objOpportunityItem.setField(OpportunityLineItem.TXN_LINEID__C, estimateLineRetType.getTxnLineID().trim());
											}
											//EXTERNID__C
											if(setOppLineItemFields.contains(OpportunityLineItem.EXTERNID__C)){
												objOpportunityItem.setField(OpportunityLineItem.EXTERNID__C, "");
												if(estimateRetType.getTxnID() != null && (estimateLineRetType.getItemRef() != null && estimateLineRetType.getItemRef().getListID() != null))
													objOpportunityItem.setField(OpportunityLineItem.EXTERNID__C, estimateRetType.getTxnID().trim()+" "+estimateLineRetType.getItemRef().getListID().trim());
											}
											lstOpportunityItems.add(objOpportunityItem);
										}
									}
								}
							}
						}
					}
				}
				//list to array
				if(lstOpportunities != null && lstOpportunities.size() > 0){
					objOpportunityLst = new SObject[lstOpportunities.size()];
					lstOpportunities.toArray(objOpportunityLst);
					log.info("objAccLst********"+objOpportunityLst.toString());
				}
				//logic to over come the limit of sfdc update by preparing batches of 100
				if((objOpportunityLst != null && objOpportunityLst.length > 0) && opportunityFlag){
					log.info("objOpportunityLst Size ==>> "+objOpportunityLst.length);
					isOk = QueryExecuter.prepareAndExecuteUpdate(objOpportunityLst,partnerConnection,SFDCObjectConstants.OPPORTUNITY);				
					//Inserting opportunity line Items
					if(lstOpportunityItems != null && lstOpportunityItems.size() > 0){
						log.info("lstOpportunityItems Size ==>> "+lstOpportunityItems.size());
						SObject[] opportunityItemList = new SObject[lstOpportunityItems.size()];
						//**************************
						lstOpportunityItems.toArray(opportunityItemList);
						log.info("objAccLst********"+opportunityItemList.toString());
						//logic to overcome the limits of sfdc update by preparing batches of 100
						if(opportunityItemList != null && opportunityItemList.length > 0){
							log.info("opportunityItemList Size ==>> "+opportunityItemList.length);
							isOk = QueryExecuter.prepareAndExecuteUpdate(opportunityItemList,partnerConnection,SFDCObjectConstants.OPPORTUNITYLINEITEM);							
						}
					}
				}
			}
		} catch (Exception e) {
			log.info("Exception while Inserting Opportunity :"+ e.getMessage());
		}
		log.info("***************** OPPORTUNITY DETAILS INSERT SYNC END *****************");
		return isOk;
	}
	/**
	 * updateOpportunities method
	 * @param 
	 * @return String
	 * @Description This is to update Opportunities records from sfdc to qbsf estimates
	 */
	public String updateOpportunities(){
		Map<String,List<SObject>> mapOppLineItem = new HashMap<String,List<SObject>>();//map of opportunity id and opportunity line item
		Set<String> setOpportunityIds = new HashSet<String>();//set of opportunity ids
		String reqXML = "";//contains request xml query
		boolean flag = false;//checks query building
		String qbSynchControl = "";//lastModifiedDate

		try{
			//query to get SFDCSynchControl__c
			String synchQuery = "SELECT Id";
			if(setSFDCSyncFields.contains(SFDCSynchControlConstants.OPPORTUNITY_ITEM_UPDATE_LAST_SYNC)){
				synchQuery = synchQuery+", Opportunity_Item_Update_Last_Sync__c,SynchId__c FROM SFDCSynchControl__c";
				flag = true;
				log.info("flag in IF ==>> "+flag);
			}
			if(flag){
				log.info("synchQuery ==>> "+synchQuery);
				QueryResult qResult = partnerConnection.query(synchQuery);
				if (qResult!=null && qResult.getSize() > 0) {//qResult null check
					log.info("qResult ==>> "+qResult);
					log.info("qResult.getSize() ==>> "+qResult.getSize());
					SObject[] records = qResult.getRecords();
					if(records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_ITEM_UPDATE_LAST_SYNC) != null){//get value of Opportunity_Item_Update_Last_Sync__c field
						log.info("records[0].getField ==>> "+records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_ITEM_UPDATE_LAST_SYNC));
						qbSynchControl = records[0].getField(SFDCSynchControlConstants.OPPORTUNITY_ITEM_UPDATE_LAST_SYNC).toString();
					}
					log.info("qbSynchControl ==>> "+flag);
				}
			}
			//get modified sfdc opportunity data
			List<SObject> OpportunityList = getModifiedSFDCOpportunityData();
			if(OpportunityList != null && OpportunityList.size() > 0){
				for(SObject sObject : OpportunityList){
					setOpportunityIds.add((String)sObject.getField("Id"));
				}
				//prepare query to get opportunity line items
				if(setOpportunityIds != null && setOpportunityIds.size() > 0){
					String oppLineItemQuery = "SELECT Id,OpportunityId,ProductCode,Quantity, ListPrice,CreatedDate,LastModifiedDate,UnitPrice, PriceBookEntry.UnitPrice, PricebookEntry.Name,Description, PricebookEntry.product2.Family FROM OpportunityLineItem ";					
					if(qbSynchControl != null && qbSynchControl.length() > 0){
						oppLineItemQuery = oppLineItemQuery + " WHERE (CreatedDate > "+qbSynchControl;
						oppLineItemQuery = oppLineItemQuery + " OR LastModifiedDate > "+qbSynchControl+")";
					}
					log.info("@@@ oppLineItemQuery ==>> "+oppLineItemQuery);
					QueryResult objQueryResult = partnerConnection.query(oppLineItemQuery);
					String lastModifiedDate = null;
					int index = 0;

					if (objQueryResult !=null && objQueryResult.getSize()>0) {
						log.info("@@@ objQueryResult ==>> "+objQueryResult);
						SObject[] oppLineItemRecords = objQueryResult.getRecords();
						if(oppLineItemRecords != null && oppLineItemRecords.length > 0){//oppLineItemRecords null check
							log.info("@@@ oppLineItemRecords length ==>> "+oppLineItemRecords.length);
							for(SObject sObject : oppLineItemRecords){//prepare map of opportunity id and sobject
								if(mapOppLineItem.containsKey(sObject.getField("OpportunityId"))){
									mapOppLineItem.get(sObject.getField("OpportunityId")).add(sObject);
								}else{
									List<SObject> invoiceLineItemList = new ArrayList<SObject>();
									invoiceLineItemList.add(sObject);
									mapOppLineItem.put((String)sObject.getField("OpportunityId"), invoiceLineItemList);
								}
								//get value of LastModifiedDate field
								if(sObject.getField("LastModifiedDate") != null && sObject.getField("LastModifiedDate").toString().length() > 0){
									//method that returns sorted lastModifiedDate
									lastModifiedDate = UtilityFunctions.sortAndGetLastModifiedDate(sObject.getField("LastModifiedDate").toString(),lastModifiedDate,index);
								}
							}
							log.info("@@@ mapOppLineItem size ==>> "+mapOppLineItem.size());
						}
					}
				}
			}			
			//OpportunityList null check to update estimate line items in qbsf
			if(OpportunityList != null && OpportunityList.size() > 0){
				log.info("OpportunityList ==>> "+OpportunityList);
				log.info("OpportunityList size ==>> "+OpportunityList.size());
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"12.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">";
				for(SObject sObject : OpportunityList){  
					if((sObject.getChild("Account") != null 
							&& sObject.getChild("Account").getField("ListID__c") != null) && (sObject.getField(OpportunityConstantFields.TXNID__C) != null && 
							sObject.getField(OpportunityConstantFields.EDITSEQUENCE__C) != null)){
						log.info("$$$ Acc List Id ==> "+sObject.getChild("Account").getField("ListID__c"));
						//REF_NUMBER__C
						if(sObject.getField(OpportunityConstantFields.REF_NUMBER__C) != null){
							reqXML = reqXML +"<EstimateModRq requestID=\""+System.currentTimeMillis()+"\""+">"
									+"<EstimateMod>" 
									+ "<TxnID>"+sObject.getField(OpportunityConstantFields.TXNID__C)+"</TxnID>"
									+ "<EditSequence>"+sObject.getField(OpportunityConstantFields.EDITSEQUENCE__C)+"</EditSequence>"
									+"<CustomerRef>"
									+"<ListID>"+sObject.getChild("Account").getField("ListID__c")+"</ListID>"
									+"</CustomerRef>"; 
							reqXML = reqXML +"<TxnDate>"+sObject.getField(OpportunityConstantFields.CLOSEDATE)+"</TxnDate>";
							reqXML = reqXML +"<RefNumber>"+sObject.getField(OpportunityConstantFields.REF_NUMBER__C)+"</RefNumber>"; 
							reqXML = reqXML +"<BillAddress>";
							//ADDRESS__C
							if(sObject.getField(OpportunityConstantFields.ADDRESS__C) != null){
								reqXML = reqXML +"<Addr1>"+sObject.getField(OpportunityConstantFields.ADDRESS__C)+"</Addr1>";
							}else{
								reqXML = reqXML +"<Addr1></Addr1>";
							}
							//City__c
							if(sObject.getField(OpportunityConstantFields.CITY__C) != null){
								reqXML = reqXML +"<City>"+sObject.getField(OpportunityConstantFields.CITY__C)+"</City>";
							}else{
								reqXML = reqXML +"<City></City>";
							}
							//State__c
							if(sObject.getField(OpportunityConstantFields.STATE__C) != null){
								reqXML = reqXML +"<State>"+sObject.getField(OpportunityConstantFields.STATE__C)+"</State>";
							}else{
								reqXML = reqXML +"<State></State>";
							}
							//POSTAL_CODE__C
							if(sObject.getField(OpportunityConstantFields.POSTAL_CODE__C) != null){
								reqXML = reqXML +"<PostalCode>"+sObject.getField(OpportunityConstantFields.POSTAL_CODE__C)+"</PostalCode>";
							}else{
								reqXML = reqXML +"<PostalCode></PostalCode>";
							}
							//COUNTRY__C
							if(sObject.getField(OpportunityConstantFields.COUNTRY__C) != null){
								reqXML = reqXML +"<Country>"+sObject.getField(OpportunityConstantFields.COUNTRY__C)+"</Country>";
							}else{
								reqXML = reqXML +"<Country></Country>";
							}
							reqXML = reqXML +"</BillAddress>";
							List<SObject> OpportunityItemList = mapOppLineItem.get(sObject.getField("Id"));
							if(OpportunityItemList != null && OpportunityItemList.size() > 0){//OpportunityItemList null check
								for(SObject sObj : OpportunityItemList){
									if(sObj.getField("ProductCode") != null){
										reqXML = reqXML + "<EstimateLineMod>" 
												+ "<TxnLineID>-1</TxnLineID>"
												+"<ItemRef>" 
												+"<ListID>"+sObj.getField("ProductCode")+"</ListID>"
												+"</ItemRef>";
										//DESCRIPTION
										if(sObj.getField(OpportunityLineItem.DESCRIPTION) != null){
											reqXML = reqXML + "<Desc>"+sObj.getField(OpportunityLineItem.DESCRIPTION)+"</Desc>";
										}else{
											reqXML = reqXML + "<Desc></Desc>";
										}
										//QUANTITY
										if(sObj.getField(OpportunityLineItem.QUANTITY) != null){
											reqXML = reqXML + "<Quantity>"+sObj.getField(OpportunityLineItem.QUANTITY)+"</Quantity>";
										}else{
											reqXML = reqXML + "<Quantity></Quantity>";
										}
										//UNITPRICE
										if(sObj.getField(OpportunityLineItem.UNITPRICE) != null){
											reqXML = reqXML + "<Rate>"+sObj.getField(OpportunityLineItem.UNITPRICE)+"</Rate>";
										}else{
											reqXML = reqXML + "<Rate></Rate>";
										}
										reqXML = reqXML + "</EstimateLineMod>";
									}
								}
							}else{
								log.info("Opportunity Object did not have Line Items");
							}
							reqXML = reqXML + "</EstimateMod>"
									+ "</EstimateModRq>";
						}
					}
				}
				reqXML = reqXML + "</QBXMLMsgsRq>"
						+"</QBXML>";
				log.info("@@@ reqXML ==> "+reqXML);
			}else{
				reqXML="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"
						+"<?qbxml version="+"\"12.0\""+"?>"
						+"<QBXML>"
						+"<QBXMLMsgsRq onError=\"stopOnError\">"
						+"</QBXMLMsgsRq>"
						+"</QBXML>";
			}
		}catch(Exception e){
			//Exception
		}
		return reqXML;
	}
	/**
	 * updateSFDCOpportunities method
	 * @param response
	 * @return String
	 * update sfdc Opportunities with the ids generated from qbsf when records are inserted in qbsf
	 */
	public java.lang.String updateSFDCOpportunities(java.lang.String response){
		log.info("***************** OPPORTUNITY DETAILS UPDATE SYNC START *****************");
		String isOk = "";
		boolean opportunityFlag = false;
		boolean oppFlag=false;
		Set<String> setOpportunityIds = new HashSet<String>();
		Map<String,List<SObject>> mapOppLineItem = new HashMap<String,List<SObject>>();
		com.appshark.estimate.mod.rs.QBXMLType qbxml = null;
		List<com.appshark.estimate.mod.rs.EstimateModRsType> estimateModRsTypeList = null;
		//jars content
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.estimate.mod.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.estimate.mod.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.estimate.mod.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getEstimateModRs() != null)){
				estimateModRsTypeList = qbxml.getQBXMLMsgsRs().getEstimateModRs();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			e.printStackTrace();
		}catch (Exception ex) {
			log.info("Exception ==>> " + ex.toString());
			ex.printStackTrace();
		}

		try{
			if(estimateModRsTypeList != null && estimateModRsTypeList.size() > 0){//estimateModRsTypeList null check
				List<SObject> lstOpportunities = new ArrayList<SObject>();//list of opportunities
				List<SObject> lstOpportunityItems = new ArrayList<SObject>();//list of opportunity line items
				SObject[] objOpportunityLst = null;//array of sobject to upsert to sfdc
				SObject objOpportunity;//sobject for opportunity to map qb fields 
				SObject objOpportunityItem;//sobject for opportunity line items to map qb fields
				Map<String,String> maprefNumbers = new HashMap<String, String>();
				//preparing query of opportunities
				if((estimateModRsTypeList != null && estimateModRsTypeList.size() > 0)){
					//preparing query for estimate
					String opportunityQuery = SFDCQueryConstants.ESTIMATE_QUERY_INSERT;
					//fields to be checked conditionally for dynamic query building
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(OpportunityConstantFields.REF_NUMBER__C);
					fieldToBeUsed.add(OpportunityConstantFields.OPPORTUNITY_ID);
					//building query
					Map<String,Object> returnedMap = sqlQueryBuilder.buildQueryForEsimateUpdate(opportunityQuery,setOpportunityFields, SFDCObjectConstants.OPPORTUNITY, estimateModRsTypeList,fieldToBeUsed);
					//extracting return result after query building
					if(returnedMap != null && returnedMap.size() > 0){
						oppFlag = (boolean) returnedMap.get("flag");
						opportunityQuery = (String) returnedMap.get("sqlQuery");
						log.info("accQuery ==>> "+opportunityQuery);
						log.info("invflag ==>> "+oppFlag);
					}

					//check flag and prepare a map from query which was returnd from query builder
					if(oppFlag){	
						//Retrieving Account objects from Salesforce
						fieldToBeUsed = new ArrayList<String>();
						fieldToBeUsed.add(OpportunityConstantFields.REF_NUMBER__C);
						fieldToBeUsed.add(OpportunityConstantFields.OPPORTUNITY_ID);
						maprefNumbers = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(opportunityQuery), fieldToBeUsed);	
						setOpportunityIds=QueryResultIterator.prepareSetFromQueryResult(partnerConnection.query(opportunityQuery), "Id");
					}
					//Invoice Line Items Data Preparation
					if(setOpportunityIds != null && setOpportunityIds.size() > 0){
						String condition = "";//holds query string where condition
						String oppLineItemQuery = "SELECT Id,OpportunityId,ProductCode,Quantity, ListPrice, PriceBookEntry.UnitPrice, PricebookEntry.Name,Description, PricebookEntry.product2.Family FROM OpportunityLineItem where OpportunityId in (";
						int cnt = 1;
						for(String OpportunityId : setOpportunityIds){//iteration of set of opportunity ids
							condition = condition+"'"+OpportunityId+"'";
							if(cnt != setOpportunityIds.size())
								condition = condition+",";
							if(cnt == setOpportunityIds.size()){
								condition = condition+")";
							}	
							cnt++;
						}
						if(condition != null && condition.length() > 0){
							oppLineItemQuery = oppLineItemQuery + condition;
							log.info("@@@ oppLineItemQuery ==>> "+oppLineItemQuery);
							QueryResult queryRslt = partnerConnection.query(oppLineItemQuery);
							if (queryRslt != null && queryRslt.getSize() > 0) {//result set null check
								SObject[] oppLineItemRecords = queryRslt.getRecords();
								if(oppLineItemRecords != null && oppLineItemRecords.length > 0){//records from query result
									for(SObject sObject : oppLineItemRecords){//prepares map of opportunity id and sobject
										if(sObject != null && sObject.getField("OpportunityId") != null){
											if(mapOppLineItem.containsKey(sObject.getField("OpportunityId"))){
												mapOppLineItem.get(sObject.getField("OpportunityId")).add(sObject);
											}else{
												List<SObject> oppLineItemList = new ArrayList<SObject>();
												oppLineItemList.add(sObject);
												mapOppLineItem.put((String)sObject.getField("OpportunityId"), oppLineItemList);
											}
										}
									}
								}
							}
						}
					}
				}
				//iteration of estimateModRsTypeList to map sfdc fields with qbsf data
				for(com.appshark.estimate.mod.rs.EstimateModRsType estimateModRsType : estimateModRsTypeList){
					if(estimateModRsType != null && (estimateModRsType.getEstimateRet() != null && estimateModRsType.getEstimateRet().size() > 0)){
						com.appshark.estimate.mod.rs.EstimateRetType estimateRetType = estimateModRsType.getEstimateRet().get(0);
						if((maprefNumbers != null && maprefNumbers.size() > 0) && (estimateRetType != null && (estimateRetType.getCustomerRef() != null && estimateRetType.getCustomerRef().getListID() != null))){
							if((estimateRetType.getRefNumber() != null && maprefNumbers.containsKey(estimateRetType.getRefNumber())) && estimateRetType.getTxnID() != null){
								String productId = maprefNumbers.get(estimateRetType.getRefNumber());
								log.info("invoiceRetType TxnID ==>> "+estimateRetType.getTxnID());
								if(setOpportunityFields != null && setOpportunityFields.size() > 0){
									log.info("setOpportunityFields.size ==>> "+setOpportunityFields.size());
									opportunityFlag = true;
									objOpportunity = new SObject();
									objOpportunity.setType(SFDCObjectConstants.OPPORTUNITY);//setting sobject type to opportunity
									//id
									objOpportunity.setField(OpportunityConstantFields.OPPORTUNITY_ID, maprefNumbers.get(estimateRetType.getRefNumber()));
									//TXNID__C
									if(setOpportunityFields.contains(OpportunityConstantFields.TXNID__C)){
										if(estimateRetType.getTxnID() != null){
											objOpportunity.setField(OpportunityConstantFields.TXNID__C, estimateRetType.getTxnID().trim());
										}
									}
									//SFDC_UPDATE_FLAG__C
									if(estimateRetType.getTxnID() != null){
										if(setOpportunityFields.contains(OpportunityConstantFields.SFDC_UPDATE_FLAG__C)){
											objOpportunity.setField(OpportunityConstantFields.SFDC_UPDATE_FLAG__C,true);
											log.info("SFDC_Update_Flag__c" +objOpportunity.getField(OpportunityConstantFields.SFDC_UPDATE_FLAG__C));
										}
									}
									//SFDC_FLAG__C
									if(estimateRetType.getTxnID() != null){
										if(setOpportunityFields.contains(OpportunityConstantFields.SFDC_FLAG__C)){
											objOpportunity.setField(OpportunityConstantFields.SFDC_FLAG__C,true);
											log.info("SFDC_Update_Flag__c" +objOpportunity.getField(OpportunityConstantFields.SFDC_FLAG__C));
										}
									}
									//EditSequence__c
									if(setOpportunityFields.contains(OpportunityConstantFields.EDITSEQUENCE__C)){
										if(estimateRetType.getEditSequence() != null){
											objOpportunity.setField(OpportunityConstantFields.EDITSEQUENCE__C, estimateRetType.getEditSequence());
										}else{
											objOpportunity.setField(OpportunityConstantFields.EDITSEQUENCE__C, "");
										}
									}
									lstOpportunities.add(objOpportunity);
									//Preparing opportunity Line Items
									Map<String,String> mapOppLineItemIds = new HashMap<String,String>(); 
									List<SObject> oppLineItemList = mapOppLineItem.get(productId);
									if(oppLineItemList != null && oppLineItemList.size() > 0){
										for(SObject oppLineItem : oppLineItemList){
											if(oppLineItem != null && (oppLineItem.getField("ProductCode") != null && oppLineItem.getField("Id") != null)){
												mapOppLineItemIds.put((String)oppLineItem.getField("ProductCode"),(String)oppLineItem.getField("Id"));
											}
										}
									}
									List<com.appshark.estimate.mod.rs.EstimateLineRetType> estimateLineRetTypeList = estimateRetType.getEstimateLineRet();
									//estimateLineRetTypeList null check
									if((estimateLineRetTypeList != null && estimateLineRetTypeList.size() > 0) && (mapOppLineItemIds != null && mapOppLineItemIds.size() > 0)){
										log.info("$$$ LINE NO ==>> 4687"+estimateLineRetTypeList);
										for(com.appshark.estimate.mod.rs.EstimateLineRetType estimateLineRetType : estimateLineRetTypeList){
											objOpportunityItem = new SObject();
											objOpportunityItem.setType(SFDCObjectConstants.OPPORTUNITYLINEITEM);
											//id
											if(setOppLineItemFields.contains(OpportunityLineItem.OPPORTUNITYLINEITEM_ID)){
												if(estimateLineRetType.getItemRef() != null && estimateLineRetType.getItemRef().getListID() != null)
													objOpportunityItem.setField(OpportunityLineItem.OPPORTUNITYLINEITEM_ID, mapOppLineItemIds.get((String)estimateLineRetType.getItemRef().getListID()));
											}
											//TXN_LINEID__C
											if(setOppLineItemFields.contains(OpportunityLineItem.TXN_LINEID__C)){
												objOpportunityItem.setField(OpportunityLineItem.TXN_LINEID__C, "");
												if(estimateLineRetType.getTxnLineID() != null)
													objOpportunityItem.setField(OpportunityLineItem.TXN_LINEID__C, estimateLineRetType.getTxnLineID().trim());
											}
											//EXTERNID__C
											if(setOppLineItemFields.contains(OpportunityLineItem.EXTERNID__C)){
												objOpportunityItem.setField(OpportunityLineItem.EXTERNID__C, "");
												if(estimateRetType.getTxnID() != null && (estimateLineRetType.getItemRef() != null && estimateLineRetType.getItemRef().getListID() != null))
													objOpportunityItem.setField(OpportunityLineItem.EXTERNID__C, estimateRetType.getTxnID().trim()+" "+estimateLineRetType.getItemRef().getListID().trim());
											}
											lstOpportunityItems.add(objOpportunityItem);
										}
									}
								}
							}
						}
					}
				}
				//list to array
				if(lstOpportunities != null && lstOpportunities.size() > 0){
					objOpportunityLst = new SObject[lstOpportunities.size()];
					lstOpportunities.toArray(objOpportunityLst);
					log.info("objAccLst********"+objOpportunityLst.toString());
				}
				//logic to prepare batches of 100 to over come limit of sfdc update
				if((objOpportunityLst != null && objOpportunityLst.length > 0) && opportunityFlag){
					log.info("objOpportunityLst Size ==>> "+objOpportunityLst.length);
					isOk = QueryExecuter.prepareAndExecuteUpdate(objOpportunityLst,partnerConnection,SFDCObjectConstants.OPPORTUNITY);					
					//Inserting Invoice Items
					if(lstOpportunityItems != null && lstOpportunityItems.size() > 0){
						log.info("lstOpportunityItems Size ==>> "+lstOpportunityItems.size());
						SObject[] opportunityItemList = new SObject[lstOpportunityItems.size()];
						lstOpportunityItems.toArray(opportunityItemList);
						log.info("objAccLst********"+opportunityItemList.toString());
						//logic to overcome the limit of sfdc update by preparing batches
						if(opportunityItemList != null && opportunityItemList.length > 0){
							log.info("opportunityItemList Size ==>> "+opportunityItemList.length);
							isOk = QueryExecuter.prepareAndExecuteUpdate(opportunityItemList,partnerConnection,SFDCObjectConstants.OPPORTUNITYLINEITEM);							
						}
					}
				}
			}
		} catch (Exception e) {
			log.info("Exception while Inserting Opportunity :"+ e.getMessage());
		}
		log.info("***************** OPPORTUNITY DETAILS UPDATE SYNC END *****************");
		return isOk;
	}
	/**
	 * getEstimates method for insert estimates from QuickBooks to SFDC Opportunities
	 * @param response
	 * @return String
	 * @Description this method is used to insert estimates from qbsf to sfdc Opportunities
	 */
	public java.lang.String getEstimates(java.lang.String response){
		log.info("***************** QB Estimates SYNC START *****************");
		String isOk = "";//return string
		boolean flag = false;//check query preparation
		boolean estimateFlag = false;//check query preparation
		boolean productFlag = false;//check query preparation
		boolean pbeFlag = false;//check query preparation
		boolean oppflag=false;//check query preparation
		Map<String,SObject> mapOldListIds = new HashMap<String, SObject>();//contains parent Account Ids
		Map<String,SObject> mapEstimates = new HashMap<String, SObject>();//contains estimates
		Map<String,SObject> mapProducts = new HashMap<String, SObject>();//contains products
		Map<String,SObject> mapPriceBookEntry = new HashMap<String, SObject>();//contains price book entrys
		Map<String,SObject> mapOppLineItem = new HashMap<String, SObject>();//contains opp line items
		Map<String,String> mapTxnIds = new HashMap<String,String>();//contains txnids
		Set<String> setEstimateTxnIds = new HashSet<String>();//contains set of txn ids
		com.appshark.estimatequery.rs.QBXMLType qbxml = null;//QBXMLType
		List<com.appshark.estimatequery.rs.EstimateRetType> estimateRetTypeList = null;//list of estimates from qb
		//jaxb classes
		try {
			javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(com.appshark.estimatequery.rs.ObjectFactory.class);
			javax.xml.bind.Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(response);
			javax.xml.transform.Source source = new javax.xml.transform.stream.StreamSource(reader);
			javax.xml.bind.JAXBElement<com.appshark.estimatequery.rs.QBXMLType> root = unmarshaller.unmarshal(source, com.appshark.estimatequery.rs.QBXMLType.class);
			if(root != null)
				qbxml = root.getValue();
			if(qbxml != null && (qbxml.getQBXMLMsgsRs() != null && qbxml.getQBXMLMsgsRs().getEstimateQueryRs() != null && 
					qbxml.getQBXMLMsgsRs().getEstimateQueryRs().getEstimateRet() != null)){
				estimateRetTypeList = qbxml.getQBXMLMsgsRs().getEstimateQueryRs().getEstimateRet();
			}
		} catch (JAXBException e) {
			log.info("JAXBException ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}catch (Exception e) {
			log.info("Exception ==>> " + e.toString());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		try{
			if(estimateRetTypeList != null && estimateRetTypeList.size() > 0){ //list of estimates null check
				//preparing query for opportunity
				String oppQuery = SFDCQueryConstants.OPPORTUNITY_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(OpportunityConstantFields.SFDC_FLAG__C);
				fieldToBeUsed.add(OpportunityConstantFields.TXNID__C);
				//building query
				Map<String,Object> returnedMap = sqlQueryBuilder.buildQueryForOpportunity(oppQuery,setOpportunityFields, SFDCObjectConstants.OPPORTUNITY, estimateRetTypeList,fieldToBeUsed);
				//extracting return result after query building
				if(returnedMap != null && returnedMap.size() > 0){
					estimateFlag = (boolean) returnedMap.get("flag");
					oppQuery = (String) returnedMap.get("sqlQuery");
					qbEstimateSynchControl = (String)returnedMap.get("lastModifiedDate");//qbEstimateSynchControl
					log.info("accQuery ==>> "+oppQuery);
					log.info("invflag ==>> "+estimateFlag);
					log.info("qbEstimateSynchControl ==>> "+qbEstimateSynchControl);
				}

				//check flag and prepare a map from query which was returnd from query builder
				if(estimateFlag){	
					//Retrieving Account objects from Salesforce
					fieldToBeUsed = new ArrayList<String>();
					fieldToBeUsed.add(OpportunityConstantFields.TXNID__C);
					fieldToBeUsed.add(OpportunityConstantFields.SFDC_FLAG__C);
					mapTxnIds = QueryResultIterator.prepareMapForMultipleParams(partnerConnection.query(oppQuery), fieldToBeUsed);					
				}
				log.info("mapTxnIds ==>> "+mapTxnIds.size());
				//Preparing Query for Product2
				String productQuery = "SELECT Id, Name";
				if(setProduct2Fields.contains("ListID__c")){
					productQuery = productQuery+", ListID__c FROM Product2";
					productFlag = true;
				}
				//Preparing Query for PriceBook Entry 
				String pbeQuery = "SELECT Id, Name";
				if(setPriceBookEntryFields.contains("ProductCode")){
					pbeQuery = pbeQuery+", ProductCode FROM PricebookEntry";
					pbeFlag = true;
				}
				if(productFlag){
					QueryResult qResult = partnerConnection.query(productQuery);
					if (qResult!=null && qResult.getSize() > 0) {//qResult null check
						SObject[] records = qResult.getRecords();//records from query result
						log.info("SFDC PRODUCT RECORDS LENGTH ==>> "+records.length);
						for(SObject sObj : records){//sobject iteration
							log.info("PRODUCT2 SOBJECT ID FROM QUERY ==>> "+sObj.getId()+" <==> "+sObj.getField("ListID__c"));
							if(sObj.getField("ListID__c") != null){
								mapProducts.put(sObj.getField("ListID__c").toString(), sObj);//preparing map of listId and sobject
							}
						}
					}
				}
				if(pbeFlag){
					QueryResult qResult = partnerConnection.query(pbeQuery);
					if (qResult!=null && qResult.getSize() > 0) {//result from query
						SObject[] records = qResult.getRecords();//records from query result
						log.info("SFDC Price Book Entry RECORDS LENGTH ==>> "+records.length);
						for(SObject sObj : records){//sobject iteration
							log.info("Price Book Entry  SOBJECT ID FROM QUERY ==>> "+sObj.getId()+" <==> "+sObj.getField("ProductCode"));
							if(sObj.getField("ProductCode") != null){//productCode null check
								mapPriceBookEntry.put(sObj.getField("ProductCode").toString(), sObj);//preparing map of ProductCode and sobject
							}
						}
					}
				}
				//preparing query for opp line items
				String OpportunityLineItemQuery = "";
				boolean oppLineItemFlag = false;
				if(setOppLineItemFields != null && setOppLineItemFields.contains("ExternID__c")){
					OpportunityLineItemQuery = "SELECT Id, OpportunityId, PricebookEntryId, Product2Id, ProductCode, Name, Quantity, UnitPrice,ExternID__c FROM OpportunityLineItem";
					oppLineItemFlag = true;
				}
				if(oppLineItemFlag){
					QueryResult opportunityLineItemResult = partnerConnection.query(OpportunityLineItemQuery);
					if (opportunityLineItemResult!=null && opportunityLineItemResult.getSize() > 0) {//queryResult null check
						SObject[] opportunityLineItemrecords = opportunityLineItemResult.getRecords();//records from queryResult
						log.info("OpportunityLineItem RECORDS LENGTH ==>> "+opportunityLineItemrecords.length);
						for(SObject sObj : opportunityLineItemrecords){
							if(sObj.getField("ExternID__c") != null){//ExternID__c null check
								mapOppLineItem.put(sObj.getField("ExternID__c").toString(), sObj);//preparing map of ProductCode and sobject
							}
						}
					}
				}
				//Preparing Query for Account
				String accQuery = SFDCQueryConstants.ACCOUNT_QUERY;
				//fields to be checked conditionally for dynamic query building
				fieldToBeUsed = new ArrayList<String>();
				fieldToBeUsed.add(AccountConstantFields.QBLISTID__C);
				fieldToBeUsed.add(AccountConstantFields.LISTID__C);
				returnedMap = sqlQueryBuilder.buildQueryForEstimateAccount(accQuery,setAccountFields, SFDCObjectConstants.ACCOUNT, estimateRetTypeList,fieldToBeUsed);
				if(returnedMap != null && returnedMap.size() > 0){
					flag = (boolean) returnedMap.get("flag");
					accQuery = (String) returnedMap.get("sqlQuery");
					log.info("accQuery ==>> "+accQuery);
					log.info("invflag ==>> "+flag);
				}

				if(flag){
					//Retrieving Account objects from Salesforce
					mapOldListIds = QueryResultIterator.prepareMapFromQueryResult(partnerConnection.query(accQuery), AccountConstantFields.LISTID__C);
				}
				log.info("mapOldListIds ==>> "+mapOldListIds.size());

			}
			if(estimateRetTypeList != null && estimateRetTypeList.size() > 0){//estimateRetTypeList null check
				List<SObject> lstEstimates = new ArrayList<SObject>();//list of estimates sobjects
				List<SObject> lstEstimatesItems = new ArrayList<SObject>();//list of estimateline item sobjects
				SObject[] objOpportunityLst = null;//array to upsert
				SObject[] objOpportunityList = null;//array to upsert
				SObject objOpportunity;//sobject to map fields
				SObject objEstimateItem;//sobject to map fields
				
				for(EstimateRetType estimateRetType : estimateRetTypeList){//estimateRetTypeList iteration
					
					if((mapTxnIds != null && estimateRetType.getTxnID() != null) 
							&& ((mapTxnIds.isEmpty() || (mapTxnIds.size()>0  && !mapTxnIds.containsKey(estimateRetType.getTxnID().trim()))) || (mapTxnIds.size() > 0 && mapTxnIds.containsKey(estimateRetType.getTxnID().trim()) && "false".equalsIgnoreCase(mapTxnIds.get(estimateRetType.getTxnID().trim()))))){
						log.info("estimateRetType TxnID ==>> "+estimateRetType.getTxnID());
						
						if(mapOldListIds != null && mapOldListIds.size() > 0){//accountIds null check
							for(String strLstId : mapOldListIds.keySet()){//iteration of Accountids map
								if(estimateRetType.getCustomerRef().getListID() != null && strLstId.equals(estimateRetType.getCustomerRef().getListID().trim())){
									if(estimateRetType.getTxnID() != null){//TxnID null check
										if((setOpportunityFields != null && setOpportunityFields.size() > 0)/* && compFlag*/){
											estimateFlag = true;
											objOpportunity = new SObject();//sobject declaration
											objOpportunity.setType("Opportunity");//sets sobject to opportunity
											
											//QB_Flag__c
											if(setOpportunityFields.contains(OpportunityConstantFields.QB_FLAG__C)){
												objOpportunity.setField(OpportunityConstantFields.QB_FLAG__C,true);
											}
											
											//SFDC_Flag__c
											if(setOpportunityFields.contains(OpportunityConstantFields.SFDC_FLAG__C)){
												objOpportunity.setField(OpportunityConstantFields.SFDC_FLAG__C,false);
											}
											
											//Account
											if(setOpportunityFields.contains(OpportunityConstantFields.ACCOUNTID)){
												objOpportunity.setField(OpportunityConstantFields.ACCOUNTID, mapOldListIds.get(strLstId).getId());
											}
											
											//Name
											log.info("***** Name ==>> true");
											if(setOpportunityFields.contains(OpportunityConstantFields.OPPORTUNITY_NAME)){
												if(estimateRetType.getRefNumber() != null){
													objOpportunity.setField(OpportunityConstantFields.OPPORTUNITY_NAME,"Opp-"+estimateRetType.getRefNumber());
												}
											}
											
											//TxnID
											if(setOpportunityFields.contains(OpportunityConstantFields.TXNID__C)){
												if(estimateRetType.getTxnID() != null){
													log.info("***** TxnID__c ==>> "+estimateRetType.getTxnID());
													objOpportunity.setField(OpportunityConstantFields.TXNID__C, estimateRetType.getTxnID().trim());
												}
											}
											
											//Edit Sequence
											if(setOpportunityFields.contains(OpportunityConstantFields.EDITSEQUENCE__C)){
												if(estimateRetType.getEditSequence() != null){
													log.info("***** EditSequence__c ==>> "+estimateRetType.getEditSequence());
													objOpportunity.setField(OpportunityConstantFields.EDITSEQUENCE__C, estimateRetType.getEditSequence());
												}
											}
											
											//Ref Number
											if(setOpportunityFields.contains(OpportunityConstantFields.REF_NUMBER__C)){
												if(estimateRetType.getRefNumber() != null){
													log.info("***** Ref_Number__c ==>> "+estimateRetType.getRefNumber());
													objOpportunity.setField(OpportunityConstantFields.REF_NUMBER__C, estimateRetType.getRefNumber());
												}
											}
											
											log.info("***** bill address");
											//bill address
											if(estimateRetType.getBillAddress() != null){
												if(setOpportunityFields.contains(OpportunityConstantFields.ADDRESS__C)){//setFieldsToNull(new String[]{"TotalAmount__c"});
													if(estimateRetType.getBillAddress().getAddr1() != null){
														log.info("***** Address__c ==>> "+estimateRetType.getBillAddress().getAddr1());
														objOpportunity.setField(OpportunityConstantFields.ADDRESS__C, estimateRetType.getBillAddress().getAddr1());
													}else{
														objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.ADDRESS__C});
													}
												}
												
												//CITY__C
												if(setOpportunityFields.contains(OpportunityConstantFields.CITY__C)){
													if(estimateRetType.getBillAddress().getCity() != null){
														log.info("***** City__c ==>> "+estimateRetType.getBillAddress().getCity());
														objOpportunity.setField(OpportunityConstantFields.CITY__C, estimateRetType.getBillAddress().getCity());
													}else{
														objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.CITY__C});
													}
												}
												
												//STATE__C
												if(setOpportunityFields.contains(OpportunityConstantFields.STATE__C)){
													if(estimateRetType.getBillAddress().getState() != null){
														log.info("***** State__c ==>> "+estimateRetType.getBillAddress().getState());
														objOpportunity.setField(OpportunityConstantFields.STATE__C, estimateRetType.getBillAddress().getState());
													}else{
														objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.STATE__C});
													}
												}
												
												//POSTAL_CODE__C
												if(setOpportunityFields.contains(OpportunityConstantFields.POSTAL_CODE__C)){
													if(estimateRetType.getBillAddress().getPostalCode() != null){
														objOpportunity.setField(OpportunityConstantFields.POSTAL_CODE__C, estimateRetType.getBillAddress().getPostalCode());
													}else{
														objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.POSTAL_CODE__C});
													}
												}
												
												//COUNTRY__C
												if(setOpportunityFields.contains(OpportunityConstantFields.COUNTRY__C)){
													if(estimateRetType.getBillAddress().getCountry() != null){
														log.info("***** Country__c ==>> "+estimateRetType.getBillAddress().getCountry());
														objOpportunity.setField(OpportunityConstantFields.COUNTRY__C, estimateRetType.getBillAddress().getCountry());
													}else{
														objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.COUNTRY__C});
													}
												}
											}else{
												objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.ADDRESS__C});
												objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.CITY__C});
												objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.STATE__C});
												objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.POSTAL_CODE__C});
												objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.COUNTRY__C});
											}
											//TxnDate
											if(setOpportunityFields.contains(OpportunityConstantFields.CLOSEDATE)){
												if(estimateRetType.getTxnDate() != null){
													String strTxnDate =estimateRetType.getTxnDate();
													SimpleDateFormat txnDateFormat = new SimpleDateFormat(CommonConstants.COMMON_DATE_FORMAT);
													Date txnDate = txnDateFormat.parse(strTxnDate);
													log.info("***** txnDate ==>> "+txnDate);
													objOpportunity.setField(OpportunityConstantFields.CLOSEDATE, txnDate);
												}else{
													objOpportunity.setFieldsToNull(new String[]{OpportunityConstantFields.CLOSEDATE});
												}
											}
											
											//Stage in opportunities is hard coded
											log.info("***** StageName ==>> Closed Won");
											if(setOpportunityFields.contains(OpportunityConstantFields.STAGENAME)){
												objOpportunity.setField(OpportunityConstantFields.STAGENAME, "Closed Won");	
											}
											
											lstEstimates.add(objOpportunity);

											//Preparing Estimate Line Items
											List<EstimateLineRetType> estimateLineRetTypeList = estimateRetType.getEstimateLineRet();
											log.info("$$$ "+estimateLineRetTypeList);
											String externID = "";
											if(estimateLineRetTypeList != null && estimateLineRetTypeList.size() > 0){//null check for estimate line items
												for(EstimateLineRetType estimateLineRetType : estimateLineRetTypeList){
													setEstimateTxnIds.add(estimateRetType.getTxnID().trim());
													objEstimateItem = new SObject();
													objEstimateItem.setType(SFDCObjectConstants.OPPORTUNITYLINEITEM);//sets sobject type to OPPORTUNITYLINEITEM
													//Txn Line ID
													if(setOppLineItemFields.contains(OpportunityLineItem.TXN_LINEID__C)){
														if(estimateLineRetType.getTxnLineID() != null){
															log.info("***** Txn_LineID__c ==>> "+estimateLineRetType.getTxnLineID());
															objEstimateItem.setField(OpportunityLineItem.TXN_LINEID__C, estimateLineRetType.getTxnLineID().trim());
														}
													}
													
													//ExternID__c
													if(setOppLineItemFields.contains(OpportunityLineItem.EXTERNID__C)){
														objEstimateItem.setField(OpportunityLineItem.EXTERNID__C, "");
														if(estimateRetType.getTxnID() != null && (estimateLineRetType.getItemRef() != null && estimateLineRetType.getItemRef ().getListID() != null)){
															externID = estimateRetType.getTxnID().trim()+" "+estimateLineRetType.getItemRef().getListID().trim();  
															log.info("***** ExternID__c ==>> "+estimateRetType.getTxnID().trim()+" "+estimateLineRetType.getItemRef().getListID().trim());
															objEstimateItem.setField(OpportunityLineItem.EXTERNID__C, estimateRetType.getTxnID().trim()+" "+estimateLineRetType.getItemRef().getListID().trim());
														}
													}
													
													//Txn  ID
													if(setOppLineItemFields.contains(OpportunityLineItem.TXNID__C)){
														if(estimateRetType.getTxnID().trim()!= null){
															log.info("***** TxnID__c in line items==>> "+estimateRetType.getTxnID().trim());
															objEstimateItem.setField(OpportunityLineItem.TXNID__C, estimateRetType.getTxnID().trim());
														}
													}
													
													//Quantity
													if(setOppLineItemFields.contains(OpportunityLineItem.QUANTITY)){
														if(estimateLineRetType.getQuantity() != null){
															log.info("***** Quantity ==>> " +estimateLineRetType.getQuantity());
															objEstimateItem.setField(OpportunityLineItem.QUANTITY, estimateLineRetType.getQuantity());
														}
													}
													
													//UnitPrize
													if(setOppLineItemFields.contains(OpportunityLineItem.UNITPRICE)){
														if(estimateLineRetType.getRate() != null){
															log.info("***** UnitPrice ==>> " +estimateLineRetType.getRate());
															objEstimateItem.setField(OpportunityLineItem.UNITPRICE, estimateLineRetType.getRate());
														}else{
															objEstimateItem.setFieldsToNull(new String[]{OpportunityLineItem.UNITPRICE});
														}
													}
													
													//Description
													if(setOppLineItemFields.contains(OpportunityLineItem.DESCRIPTION)){
														if(estimateLineRetType.getDesc() != null){
															log.info("***** Description ==>> " +estimateLineRetType.getDesc());
															objEstimateItem.setField(OpportunityLineItem.DESCRIPTION, estimateLineRetType.getDesc());
														}else{
															objEstimateItem.setFieldsToNull(new String[]{OpportunityLineItem.DESCRIPTION});
														}
													}
													
													//Products2
													log.info("$$$ LINE NO ==>> 1285");
													//Price Book Entry
													log.info("$$$ LINE NO ==>> 1285");
													if(estimateLineRetType.getItemRef() != null){
														log.info("$$$ LINE NO ==>> 1298 : " +estimateLineRetType.getItemRef().getListID());
														log.info("$$$ LINE NO ==>> 1299 : " +estimateLineRetType.getItemRef().getFullName());
														log.info("$$$ LINE NO ==>> 1300 : " +mapPriceBookEntry.get(estimateLineRetType.getItemRef().getListID()));
														if(mapOppLineItem != null && !mapOppLineItem.containsKey(externID)){
															if(setOppLineItemFields.contains(OpportunityLineItem.PRICEBOOKENTRYID)){
																if(estimateLineRetType.getItemRef().getListID() != null && (mapPriceBookEntry != null && mapPriceBookEntry.get(estimateLineRetType.getItemRef().getListID()) != null)){
																	objEstimateItem.setField(OpportunityLineItem.PRICEBOOKENTRYID, mapPriceBookEntry.get(estimateLineRetType.getItemRef().getListID()).getId());
																}
															}
														}
													}
													
													lstEstimatesItems.add(objEstimateItem);
												}
											}
										}
									}
								}
							}
						}			
					}
				}
				
				//converts list to an array
				if(lstEstimates != null && lstEstimates.size() > 0){
					objOpportunityLst = new SObject[lstEstimates.size()];
					lstEstimates.toArray(objOpportunityLst);
					log.info("objAccLst********"+objOpportunityLst.toString());
				}
				
				//objOpportunityLst null check
				if((objOpportunityLst != null && objOpportunityLst.length > 0) && estimateFlag){
					isOk = QueryExecuter.prepareAndExecute(objOpportunityLst,partnerConnection,OpportunityConstantFields.TXNID__C,SFDCObjectConstants.OPPORTUNITY);
					//Inserting Estimate Items
					if(lstEstimatesItems != null && lstEstimatesItems.size() > 0){
						log.info("lstEstimatesItems Size ==>> "+lstEstimatesItems.size());
						boolean iItemFlag = false;
						SObject[] estimateItemList = new SObject[lstEstimatesItems.size()];
						log.info("estimateOrdQuery first ==>> "+setOppLineItemFields);
						//query to get estimates
						String estimateOrdQuery = "SELECT Id";
						if(setOppLineItemFields.contains("TxnID__c")){
							estimateOrdQuery = estimateOrdQuery+", TxnID__c FROM Opportunity where TxnID__c in (";
							iItemFlag = true;
						}
						
						if(iItemFlag){
							estimateOrdQuery = estimateOrdQuery+"";
							StringBuilder filter = new StringBuilder();
							int count = 1;
							//Preparing set of LISTID's
							log.info("%%% EstimateTxnIds size"+setEstimateTxnIds.size());
							if(setEstimateTxnIds != null && setEstimateTxnIds.size() > 0){
								for(String estimateTxnId : setEstimateTxnIds){
									if(estimateTxnId != null){
										filter.append("\'"+estimateTxnId+"\'");
										if(count != setEstimateTxnIds.size())
											filter.append(" ,");
									}
									count++;
								}
							}
							
							estimateOrdQuery = estimateOrdQuery+filter+")";
							log.info("estimateOrdQuery ==>> "+estimateOrdQuery);
							//Retrieving Opportunity objects from Salesforce
							QueryResult qResult = partnerConnection.query(estimateOrdQuery);
							if (qResult!=null && qResult.getSize() > 0) {//qResult null check
								SObject[] records = qResult.getRecords();//records from qResult
								for(SObject sObj : records){//records iteration
									String strTxnID = sObj.getField("TxnID__c").toString().trim();
									mapEstimates.put(strTxnID, sObj);//map of txnId and sobject
								}
							}
						}
						
						//sets OpportunityId for each Opp line item
						for(int i = 0;i < lstEstimatesItems.size();i++){
							SObject estimateItemObj = lstEstimatesItems.get(i);
							if(estimateItemObj != null){
								if(mapEstimates != null && mapEstimates.size() > 0){
									if(mapEstimates.containsKey(estimateItemObj.getField("TxnID__c").toString())){
										if(setOppLineItemFields.contains("OpportunityId")){
											estimateItemObj.setField("OpportunityId", mapEstimates.get(estimateItemObj.getField("TxnID__c").toString()).getId());
										}
									}
								}
							}
							estimateItemList[i] = estimateItemObj;
						}
						
						//estimateItemList null check
						if(estimateItemList != null && estimateItemList.length > 0){
							log.info("estimateItemList Size ==>> "+estimateItemList.length);
							isOk = QueryExecuter.prepareAndExecute(estimateItemList,partnerConnection,OpportunityLineItem.EXTERNID__C,SFDCObjectConstants.OPPORTUNITYLINEITEM);
						}
					}
				}
			}	
		}catch (Exception e) {
			log.info("Exception while Inserting Estimates :"+ e.getMessage());
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
		log.info("***************** QB Estimates SYNC END *****************");
		return isOk;
	}
	
	/**
	 * describeSfdcObjects method
	 * @param response
	 * @return String
	 * @Description this method is used to get all field api names of sfdc objects
	 */
	public void describeSfdcObjects(){
		setAccountFields = new HashSet<String>();
		setInvoiceFields = new HashSet<String>();
		setInvoiceLineItemFields = new HashSet<String>();
		setOpportunityFields = new HashSet<String>();
		setOppLineItemFields = new HashSet<String>();
		setProduct2Fields = new HashSet<String>();
		setPriceBookEntryFields = new HashSet<String>();
		setVendorFields=new HashSet<String>();
		setItemInvFields=new HashSet<String>();
		setPurchaseOrderFields=new HashSet<String>();
		setPurchaseLineItemFields=new HashSet<String>();
		setBillFields=new HashSet<String>();
		setExpensesLineItemsFields=new HashSet<String>();
		setBillLineItemsFields=new HashSet<String>();
		setQBSynchFileds=new HashSet<String>();
		setSFDCSyncFields=new HashSet<String>();
		try{
			if(partnerConnection != null){
				//all account fields
				setAccountFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.ACCOUNT),SFDCObjectConstants.ACCOUNT);
				
				//all invoice fields
				setInvoiceFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.INVOICE__C),SFDCObjectConstants.INVOICE__C);
				
				//all invoice line item fields
				setInvoiceLineItemFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.INVOICE_LINE_ITEM__C),SFDCObjectConstants.INVOICE_LINE_ITEM__C);
				
				//all opportunity fields
				setOpportunityFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.OPPORTUNITY),SFDCObjectConstants.OPPORTUNITY);
				
				//all opportunity line item fields
				setOppLineItemFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.OPPORTUNITYLINEITEM),SFDCObjectConstants.OPPORTUNITYLINEITEM);
				
				//all product fields
				setProduct2Fields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.PRODUCT2),SFDCObjectConstants.PRODUCT2);
				
				//all price book entry fields
				setPriceBookEntryFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.PRICEBOOKENTRY),SFDCObjectConstants.PRICEBOOKENTRY);
				//all qbsynch control fields
				setQBSynchFileds=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.QBSYNCHCONTROL__C),SFDCObjectConstants.QBSYNCHCONTROL__C);
				
				//all sfdc synch control fields
				setSFDCSyncFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.SFDCSYNCHCONTROL__C),SFDCObjectConstants.SFDCSYNCHCONTROL__C);
				
				//all vendor sobject fields
				setVendorFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.VENDOR__C),SFDCObjectConstants.VENDOR__C);
				
				//all itemInventory sobject fields
				setItemInvFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.ITEM_INVENTORY__C),SFDCObjectConstants.ITEM_INVENTORY__C);
				
				//all purchaseOrder sobject fields
				setPurchaseOrderFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.PURCHASE_ORDER__C),SFDCObjectConstants.PURCHASE_ORDER__C);
				
				//all purchaseOrder line item sobject fields
				setPurchaseLineItemFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.PURCHASE_ORDER_LINE_ITEM__C),SFDCObjectConstants.PURCHASE_ORDER_LINE_ITEM__C);
				
				//all Bill sobject fields
				setBillFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.BILL__C),SFDCObjectConstants.BILL__C);
				
				//all expense line item sobject fields
				setExpensesLineItemsFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.EXPENSE_LINE_ITEM__C),SFDCObjectConstants.EXPENSE_LINE_ITEM__C);
				
				//all bill line items fields
				setBillLineItemsFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.BILL_LINE__ITEM__C),SFDCObjectConstants.BILL_LINE__ITEM__C);
				
				//all Accounting fields
				setAccountingFields=QueryResultIterator.prepareSetFromDescribeSObject(partnerConnection.describeSObject(SFDCObjectConstants.ACCOUNTING__C),SFDCObjectConstants.ACCOUNTING__C);
			}
		}catch(Exception e){
			if(e.getMessage()!=null){
				new MailService().sendMail(adminEmail,"Exception",e.getMessage());
			}
		}
	}
	/**
	 * getAdminEmailFromSFDC method
	 * @return String
	 * @Description This method is to get System Admin Email from salesforce 
	 * user object to send an email with the error logs .
	 */
	public String getAdminEmailFromSFDC(){
		SObject[] records=null;//array of sobjects from query
		String strEmail="";//email to return
		
		//query to get email based on profile name
		String userQuery="select id,name,Profile.name,email from user where Profile.name='System Administrator'";	
		try {
			log.info("userQuery ======>>"+ userQuery);
			QueryResult qResult = partnerConnection.query(userQuery);//query result	
			if(qResult!=null && qResult.getSize()>0){ //query result null check
				records=qResult.getRecords(); //records from qResult;
				if(records!=null && records.length>0){ //records null check
					log.info("records ======>>"+ records.length);
					for(SObject sobj:records){//iteration of sobject array
						log.info("records ======>>"+ sobj.getId()+"email--- " + sobj.getField("Email"));
						if(sobj.getField("email")!=null && sobj.getField("email")!=""){
							strEmail=sobj.getField("email").toString();//gets email field value
							log.info("strEmail ======>>"+ strEmail);
						}//end of if							
					}//end of for
				}//end of if
			}//end of if
			
		} catch (ConnectionException e) {
			log.info("exception from getAdminEmailFromSFDC=====>"+e.getMessage());
		}
		
		return strEmail;
	}

	@WebMethod
	@WebResult(name = "connectionErrorResult", targetNamespace = "http://developer.intuit.com/")
	public java.lang.String connectionError(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket,
			@WebParam(name = "hresult", targetNamespace = "http://developer.intuit.com/") java.lang.String hresult,
			@WebParam(name = "message", targetNamespace = "http://developer.intuit.com/") java.lang.String message) {

		log.info("connectionError : " + ticket + " \n" + message);
		return null;
	}

	@WebMethod
	@WebResult(name = "getLastErrorResult", targetNamespace = "http://developer.intuit.com/")
	public java.lang.String getLastError(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket) {
		return null;
	}

	@WebMethod
	@WebResult(name = "closeConnectionResult", targetNamespace = "http://developer.intuit.com/")
	public java.lang.String closeConnection(@WebParam(name = "ticket", targetNamespace = "http://developer.intuit.com/") java.lang.String ticket) {
		log.info("Closing Connection For Session ==>> " + ticket);
		return ("close with this message");
	}

	@WebMethod
	@WebResult(name = "clientVersionResult", targetNamespace = "http://developer.intuit.com/")
	public String clientVersion(@WebParam(name = "strVersion", targetNamespace = "http://developer.intuit.com/") java.lang.String strVersion) {
		log.info("Client Version ==>> " + strVersion);
		return "";
	}

	@WebMethod
	@WebResult(name = "serverVersionResult", targetNamespace = "http://developer.intuit.com/")
	public java.lang.String serverVersion(@WebParam(name = "strVersion", targetNamespace = "http://developer.intuit.com/") java.lang.String strVersion) {
		log.info("Server Version ==>> " + strVersion);
		return "";
	}

}
