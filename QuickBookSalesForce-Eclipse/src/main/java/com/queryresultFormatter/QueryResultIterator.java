package com.queryresultFormatter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.Field;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;

/**
 * The  program uses query results from sfdc and prepares a map
 * @author  A Gopala krishna reddy
 * @version 1.0
 * @since   2016-06-01 
 */

public class QueryResultIterator {

	static Logger log = Logger.getLogger(QueryResultIterator.class.getName()); // for logging


	/**
	 * This method is used to prepare map from query results 
	 * by dynamic column names supplied
	 * 
	 * @param qResult to be iterated
	 * @param fieldToBeUsed Dynamic fields to be extracted from query result
	 * @return Map<String,String> This returns map for the supplied fields in the param fieldToBeUsed.
	 */
	public static Map<String,String> prepareMapForMultipleParams(QueryResult qResult,List<String> fieldToBeUsed){
		Map<String,String> returnMap = new HashMap<String,String>();
		if (qResult.getSize() > 0) {
			log.info("qResult.getSize()" +qResult.getSize());
			log.info("qResult.getRecords()" +qResult.getRecords());
			SObject[] records = qResult.getRecords();
			if(records.length > 0){
				log.info(" Class : QueryResultIterator ; Method : prepareMapForMultipleParams ;SFDC ACC RECORDS LENGTH ==>> "+records.length);
				for(SObject sObj : records){
					if(sObj != null && sObj.getField(fieldToBeUsed.get(0)) != null && sObj.getField(fieldToBeUsed.get(1))!=null){
						returnMap.put(sObj.getField(fieldToBeUsed.get(0)).toString(), sObj.getField(fieldToBeUsed.get(1)).toString());
						log.info("Class : QueryResultIterator ; Method : prepareMapForMultipleParams ;sObj.getField(fieldToBeUsed.get(1)).toString()"+sObj.getField(fieldToBeUsed.get(1)).toString());
					}
				}
			}

		}
		return returnMap;
	}

	/**
	 * This method is used to prepare map from query results 
	 * by dynamic column names supplied
	 * 
	 * @param qResult to be iterated
	 * @param fieldToBeUsed Dynamic fields to be extracted from query result
	 * @return Map<String,String> This returns map for the supplied fields in the param fieldToBeUsed.
	 */
	public static Map<String,SObject> prepareMapFromQueryResult(QueryResult qResult,String fieldToBeUsed){
		Map<String,SObject> returnMap = new HashMap<String,SObject>();
		if (qResult.getSize() > 0) {
			log.info("qResult.getSize()" +qResult.getSize());
			log.info("qResult.getRecords()" +qResult.getRecords());
			SObject[] records = qResult.getRecords();
			if(records.length > 0){
				log.info("Class : QueryResultIterator ; Method : prepareMapFromQueryResult ;SFDC ACC RECORDS LENGTH ==>> "+records.length);
				for(SObject sObj : records){
					if(sObj != null && sObj.getField(fieldToBeUsed) != null){
						log.info("Class : QueryResultIterator ; Method : prepareMapFromQueryResult ;SOBJECT ID FROM QUERY ==>> "+sObj.getId());
						returnMap.put(sObj.getField(fieldToBeUsed).toString(), sObj);
					}
				}
			}
		}
		return returnMap;
	}
	/**
	 * This method is used to prepare set from query results 
	 * by dynamic column names supplied
	 * 
	 * 	@param qResult to be iterated
	 * @param fieldToBeUsed Dynamic fields to be extracted from query result
	 * @return Set<String> This returns set for the supplied fields in the param fieldToBeUsed.
	 */
	public static Set<String> prepareSetFromQueryResult(QueryResult qResult,String fieldToBeUsed){
		log.info("in set method");
		Set<String> setInvoiceIds = new HashSet<String>();
		if (qResult!=null && qResult.getSize() > 0) {
			log.info("Class : QueryResultIterator ; Method : prepareSetFromQueryResult ;qResult.getSize()" +qResult.getSize());
			log.info("Class : QueryResultIterator ; Method : prepareSetFromQueryResult ;qResult.getRecords()" +qResult.getRecords());
			SObject[] records = qResult.getRecords();
			if(records!=null && records.length > 0){
				log.info("Class : QueryResultIterator ; Method : prepareSetFromQueryResult ;SFDC ACC RECORDS LENGTH ==>> "+records.length);
				for(SObject sObj : records){
					if(sObj != null && sObj.getField(fieldToBeUsed) != null){
						log.info("Class : QueryResultIterator ; Method : prepareSetFromQueryResult ;SOBJECT ID FROM QUERY ==>> "+sObj.getField(fieldToBeUsed));
						setInvoiceIds.add((String)sObj.getField(fieldToBeUsed));
					}
				}
			}
		}
		return setInvoiceIds;
	}

	/**
	 * This method is used to prepare set from DescribeSObjectResult
	 * by dynamic column names supplied
	 * 
	 * @param describeSObjectResult to be iterated
	 * @param sObject Dynamic fields to be extracted from query result
	 * @return Set<String> This returns map for the supplied fields in the param fieldToBeUsed.
	 */
	public static Set<String> prepareSetFromDescribeSObject(DescribeSObjectResult describeSObjectResult,String sObject){
		Set<String> returnSet = new HashSet<String>();
		if(describeSObjectResult != null && sObject.equals(describeSObjectResult.getName())){
			if(describeSObjectResult.getFields() != null && describeSObjectResult.getFields().length > 0){
				Field[] fields = describeSObjectResult.getFields();
				for(Field objField : fields){
					returnSet.add(objField.getName());
				}
			}
		}

		return returnSet;
	}
}

