package com.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;



/**
 * provides cors origin access  and access default page
 * 
 * @author Appshark 
 *
 */
public class FilterDispatcher implements Filter{
		
		@Override
	    public void init(FilterConfig arg0) throws ServletException {
			// just overridding it
		}
	    @Override
	    public void doFilter(ServletRequest req, ServletResponse resp,
	            FilterChain chain) throws IOException, ServletException {
	    	
	    	 HttpServletRequest request;
	    	
	        HttpServletResponse response=(HttpServletResponse) resp;
	        request=(HttpServletRequest) req;
	        
	        response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
	        response.setHeader("Access-Control-Max-Age", "3600"); 
	        response.setHeader("Access-Control-Allow-Headers", "*");

	        String path = request.getRequestURI().substring(request.getContextPath().length());
		       
		       if(StringUtils.isNotEmpty(path)) {
		       	if (path.length() > 2) {
		            chain.doFilter(request, response); // Goes to default servlet.
		        } else {
		            request.getRequestDispatcher( request.getRequestURI()+ "service/resources/index.html").forward(request, response);
		        }
		       }else {
		    	   chain.doFilter(request, response); // Goes to default servlet.
		       }
	       // chain.doFilter(request, response); // Goes to default servlet.
	      }
	 
	    @Override
	    public void destroy() {
	    	// just overridding it
	    }
	    
	   
}
