package com.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.appshark.qb.invoice.InvoiceRetType;
import com.intuit.developer.QBSyncService;

public class SqlQueryBuilder {
	
	StringBuilder sqlQuery = new StringBuilder(); //to hold dynamic select query built by buildQuery function
	boolean flag = false; //to check sql query builds correctly with Object name and fields
	Map<String,Object> returnMap = new HashMap<String, Object>(); //return type holding sqlQuery and flag
	static Logger log = Logger.getLogger(QBSyncService.class.getName()); // for logging
	
	
	public Map<String,Object> buildQuery(Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues){
		int count = 1;
		String filter = "";
		
		log.info("IN buildQuery flag===========>"+ objectName.equals("Invoice__c"));
		if(objectName.equals("Invoice__c")){ //condition to check Object name
			
			sqlQuery.append("SELECT Id, Name");
			
			if(setOfFields.contains("SFDC_Flag__c") && setOfFields.contains("TxnID__c")){
				sqlQuery.append(",TxnID__c,SFDC_Flag__c,QB_Flag__c FROM "+objectName+" where TxnID__c in (");
				flag = true;
			}
			
			log.info("IN buildQuery flag===========>"+ flag);
			if(flag == true){
				
				for(Object invoiceRetTypeObejct : conditionalvalues){
					InvoiceRetType invoiceRetType = (InvoiceRetType)invoiceRetTypeObejct;
					if(invoiceRetType.getTxnID()!= null){
						log.info("IN buildQuery getTxnID===========>"+ invoiceRetType.getTxnID());
						filter = filter+"\'"+invoiceRetType.getTxnID()+"\'";
						
						if(count != conditionalvalues.size())
							filter = filter+" ,";
					}
					count++;
				}
				
				sqlQuery.append(filter+")");
				
			}
			
		}
		log.info("IN buildQuery else sqlQuery===========>"+ sqlQuery);
		
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			returnMap.put("flag", flag);
			returnMap.put("sqlQuery", sqlQuery.toString());

			return returnMap;
		
		}
		
		
	}

}
