package com.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.appshark.purchaseorderquery.rs.PurchaseOrderLineRetType;
import com.appshark.purchaseorderquery.rs.PurchaseOrderRetType;
import com.appshark.qb.invoice.InvoiceLineRetType;
import com.appshark.qb.invoice.InvoiceRetType;
import com.appshark.vendorquery.rs.VendorRetType;
import com.appshark.AccountingQuery.rs.AccountRetType;
import com.appshark.billquery.rs.BillRetType;
import com.appshark.billquery.rs.ExpenseLineRetType;
import com.appshark.customermodrs.CustomerModRsType;
import com.appshark.customutil.cls.CustomerRetType;
import com.appshark.estimate.add.rs.EstimateAddRsType;
import com.appshark.estimate.mod.rs.EstimateModRsType;
import com.appshark.estimatequery.rs.EstimateRetType;
import com.appshark.invoice.add.rs.InvoiceAddRsType;
import com.appshark.invoice.mod.rs.InvoiceModRsType;
import com.appshark.item.query.rs.ItemServiceRetType;
import com.appshark.item.service.add.rs.ItemServiceAddRsType;
import com.appshark.item.service.mod.rs.ItemServiceModRsType;
import com.appshark.itemInventoryquery.rs.ItemInventoryRetType;
import com.constants.CommonConstants;
import com.constants.objectconstants.SFDCObjectConstants;
import com.qb.appshark.CustomerAddRsType;
/**
 * The  program conatins logic to build querys with 
 * where condition and returns the results from query
 * @author  A Gopala krishna reddy
 * @version 1.0
 * @since   2016-06-07 
 */
public class SqlQueryBuilder {

	StringBuilder sqlQuery = new StringBuilder(); //to hold dynamic select query built by buildQuery function
	boolean flag = false; //to check sql query builds correctly with Object name and fields
	Map<String,Object> returnMap = null; //return type holding sqlQuery,lastModifiedDate and flag
	static Logger log = Logger.getLogger(SqlQueryBuilder.class.getName()); // for logging

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for invoice line items
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryInvoiceItems(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){

		log.info(" Class : SqlQueryBuilder ; Method : buildQueryInvoiceItems ; Line No : 43 ---> In buildQueryInvoiceItems method");
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryInvoiceItems ;IN buildQuery flag===========>"+ flag);
		//condition for Invoice_Line_Item__c
		if(objectName.equals(SFDCObjectConstants.INVOICE_LINE_ITEM__C) && flag){ 
			for(Object invoiceRetTypeObejct : conditionalvalues){
				InvoiceRetType invoiceRetType = (InvoiceRetType)invoiceRetTypeObejct;
				List<InvoiceLineRetType> invoiceLineRetTypeList = invoiceRetType.getInvoiceLineRet();
				if(invoiceLineRetTypeList != null && !invoiceLineRetTypeList.isEmpty()){
					for(InvoiceLineRetType invoiceLineRetType : invoiceLineRetTypeList){
						if(invoiceLineRetType.getTxnLineID()!=null &&invoiceLineRetType.getTxnLineID()!=""){//TxnLineID null check
							log.info("Class : SqlQueryBuilder ; Method : buildQueryInvoiceItems ; Line No : 43 ---> invoiceLineRetType.getTxnLineID() " );
							filter.append("\'"+invoiceLineRetType.getTxnLineID()+"\'");
							filter.append(" ,");
						} //end of if
					} //end of for
				} //end of if
				count++;
			}//end of for	
		} 

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			String filtered=filter.substring(0, filter.length()-1);
			sqlQuery.append(filtered+")");
			log.info("Class : SqlQueryBuilder ; Method : buildQueryInvoiceItems ;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;

		}//end of else
	}

	/**
	 * This method is used to get a map of sqlQuery,flag and lastmodifiedDate with the parameters supplied for invoice 
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForInvoice(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for Invoice__c
		if(objectName.equals(SFDCObjectConstants.INVOICE__C) && flag){

			for(Object invoiceRetTypeObejct : conditionalvalues){
				InvoiceRetType invoiceRetType = (InvoiceRetType)invoiceRetTypeObejct;
				if(invoiceRetType.getTxnID()!= null){
					filter.append( (count != conditionalvalues.size() ) ? "\'"+invoiceRetType.getTxnID()+"\' ," : "\'"+invoiceRetType.getTxnID()+"\' )");
				}
				if(invoiceRetType != null && invoiceRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(invoiceRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}			
		}
		
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoice ;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC invoice insert
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForInvoiceInsert(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for Invoice__c
		if(objectName.equals(SFDCObjectConstants.INVOICE__C) && flag){

			for(Object invoiceRetTypeObejct : conditionalvalues){
				InvoiceAddRsType invoiceAddRsType = (InvoiceAddRsType)invoiceRetTypeObejct;
				com.appshark.invoice.add.rs.InvoiceRetType  invoiceRetType = invoiceAddRsType.getInvoiceRet();
				if(invoiceRetType.getRefNumber()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoiceInsert;IN buildQuery getRefNumber===========>"+ invoiceRetType.getRefNumber());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+invoiceRetType.getRefNumber()+"\' ," : "\'"+invoiceRetType.getRefNumber()+"\' )");
				}
				count++;
			}			
		}
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoiceInsert;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC opportunity insert
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForEsimateInsert(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values

		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for OPPORTUNITY
		if(objectName.equals(SFDCObjectConstants.OPPORTUNITY) && flag){
			for(Object EstimateTypeObejct : conditionalvalues){
				EstimateAddRsType estimateAddRsType = (EstimateAddRsType)EstimateTypeObejct;
				com.appshark.estimate.add.rs.EstimateRetType estimateRetType = estimateAddRsType.getEstimateRet().get(0);
				if(estimateRetType.getRefNumber()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForEsimateInsert;IN buildQuery getTxnID===========>"+ estimateRetType.getRefNumber());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+estimateRetType.getRefNumber()+"\' ," : "\'"+estimateRetType.getRefNumber()+"\' )");
				}
				count++;
			}			
		}
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForEsimateInsert;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC opportunity update
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForEsimateUpdate(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for OPPORTUNITY
		if(objectName.equals(SFDCObjectConstants.OPPORTUNITY) && flag){

			for(Object EstimateTypeObejct : conditionalvalues){
				EstimateModRsType estimateModRsType = (EstimateModRsType)EstimateTypeObejct;
				com.appshark.estimate.mod.rs.EstimateRetType estimateRetType = estimateModRsType.getEstimateRet().get(0);
				if(estimateRetType.getRefNumber()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForEsimateUpdate;IN buildQuery getTxnID===========>"+ estimateRetType.getRefNumber());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+estimateRetType.getRefNumber()+"\' ," : "\'"+estimateRetType.getRefNumber()+"\' )");
				}
				count++;
			}			
		}
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForEsimateUpdate;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC invoice update
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForInvoiceUpdate(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		
		//condition for Invoice__c
		if(objectName.equals(SFDCObjectConstants.INVOICE__C) && flag){

			for(Object invoiceRetTypeObejct : conditionalvalues){
				InvoiceModRsType invoiceModRsType = (InvoiceModRsType)invoiceRetTypeObejct;
				com.appshark.invoice.mod.rs.InvoiceRetType  invoiceRetType = invoiceModRsType.getInvoiceRet();
				if(invoiceRetType.getTxnID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoiceUpdate;IN buildQuery getTxnID===========>"+ invoiceRetType.getTxnID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+invoiceRetType.getTxnID().trim()+"\' ," : "\'"+invoiceRetType.getTxnID().trim()+"\' )");
				}
				count++;
			}			
		}
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoiceUpdate;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for Account related invoice
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForInvoiceAccount(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
			if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
				sqlQuery.append(defaultQuery +" IN (");
				flag = true;
			}

			//condition for Account
			if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){

				for(Object invoiceRetTypeObejct : conditionalvalues){
					InvoiceRetType invoiceRetType = (InvoiceRetType)invoiceRetTypeObejct;
					if(invoiceRetType.getTxnID()!= null){
						log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoiceAccount;IN buildQuery getTxnID===========>"+ invoiceRetType.getCustomerRef().getListID());
						filter.append( (count != conditionalvalues.size() ) ? "\'"+invoiceRetType.getCustomerRef().getListID()+"\' ," : "\'"+invoiceRetType.getCustomerRef().getListID()+"\' )");
					}
					count++;
				}					
			}
			if(sqlQuery != null && sqlQuery.length() <= 0){
				return returnMap;
			}else{
				sqlQuery.append(filter);
				log.info("Class : SqlQueryBuilder ; Method : buildQueryForInvoiceAccount;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
				returnMap.put(CommonConstants.FLAG, flag);
				returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
				return returnMap;		
			}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for Account related invoice
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForPurchaseItemAccount(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		log.info(" Class : SqlQueryBuilder ; Method : buildQueryForPurchaseItemAccount ; Line No : 43 ---> In buildQueryInvoiceItems method");
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		String filtered="";
		boolean isLastRecord = false;
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseItemAccount ;IN buildQuery flag===========>"+ flag);
		//condition for ACCOUNT
		try{
			if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){ 
				log.info("conditionalvalues"+conditionalvalues.size());
				for(Object purchaseRetTypeObejct : conditionalvalues){
					int innerrCount = 1;
					PurchaseOrderRetType purchaseRetType = (PurchaseOrderRetType)purchaseRetTypeObejct;
					List<PurchaseOrderLineRetType> purchaseLineRetTypeList = purchaseRetType.getPurchaseOrderLineRet();

					if(purchaseLineRetTypeList != null && purchaseLineRetTypeList.size() > 0){
						log.info("purchaseLineRetTypeList"+purchaseLineRetTypeList.size());
						if(count != conditionalvalues.size())
							isLastRecord = true;						
						for(PurchaseOrderLineRetType purchaseLineRetType : purchaseLineRetTypeList){
							if(purchaseLineRetType.getCustomerRef()!=null && purchaseLineRetType.getCustomerRef().getListID()!=null && purchaseLineRetType.getCustomerRef().getListID()!=""){//TxnLineID null check
								System.out.println("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseItemAccount ; Line No : 43 --->");
								filter.append("\'"+purchaseLineRetType.getCustomerRef().getListID()+"\'");
								filter.append(" ,");
							} //end of if
							innerrCount++;
						} //end of for
					} //end of if
					count++;
				}//end of for	
			} 
		}catch(Exception ex){
			log.info("Exception occured in buildQueryForPurchaseItemAccount"+ex.getMessage());
		}
		if(sqlQuery != null && sqlQuery.length() <= 0){
			log.info("returnMap if"+returnMap);
			return returnMap;
		}else{
			log.info("sqlQuery5---"+sqlQuery);
			if(filter!=null && filter.length()>0){
				 filtered=filter.substring(0, filter.length()-1);
				sqlQuery.append(filtered+")");			
				log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseItemAccount ;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
				returnMap.put(CommonConstants.FLAG, flag);
				returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
				log.info("returnMap else"+returnMap);	
				return returnMap;
			}else{
				return returnMap;
			}		
		}//end of else
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for Account related invoice
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForBillItemAccount(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		log.info(" Class : SqlQueryBuilder ; Method : buildQueryForBillItemAccount ; Line No : 43 ---> In buildQueryInvoiceItems method");
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		String filtered="";
		boolean isLastRecord = false;
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillItemAccount ;IN buildQuery flag===========>"+ flag);
		//condition for ACCOUNT
		try{
			if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){ 
				log.info("conditionalvalues"+conditionalvalues.size());
				for(Object billRetTypeObejct : conditionalvalues){
					int innerrCount = 1;
					BillRetType billRetType = (BillRetType)billRetTypeObejct;
					List<ExpenseLineRetType> billLineRetTypeList = billRetType.getExpenseLineRet();

					if(billLineRetTypeList != null && billLineRetTypeList.size() > 0){
						log.info("purchaseLineRetTypeList"+billLineRetTypeList.size());
						if(count != conditionalvalues.size())
							isLastRecord = true;						
						for(ExpenseLineRetType billExpRetType : billLineRetTypeList){
							if(billExpRetType.getCustomerRef()!=null && billExpRetType.getCustomerRef().getListID()!=null && billExpRetType.getCustomerRef().getListID()!=""){//TxnLineID null check
								System.out.println("Class : SqlQueryBuilder ; Method : buildQueryForBillItemAccount ; Line No : 43 --->");
								filter.append("\'"+billExpRetType.getCustomerRef().getListID()+"\'");
								filter.append(" ,");
							} //end of if
							innerrCount++;
						} //end of for
					} //end of if
					count++;
				}//end of for	
			} 
			log.info("sqlQuery4"+sqlQuery);
		}catch(Exception ex){
			log.info("Exception occured in buildQueryForBillItemAccount"+ex.getMessage());
		}
		if(sqlQuery != null && sqlQuery.length() <= 0){
			log.info("returnMap if"+returnMap);
			return returnMap;
		}else{
			log.info("sqlQuery5---"+sqlQuery);
			if(filter!=null && filter.length()>0){
				 filtered=filter.substring(0, filter.length()-1);
				sqlQuery.append(filtered+")");			
				log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillItemAccount ;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
				returnMap.put(CommonConstants.FLAG, flag);
				returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
				log.info("returnMap else"+returnMap);	
				return returnMap;
			}else{
				return returnMap;
			}		
		}//end of else
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Account related Opportunities
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForEstimateAccount(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for Account
		if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){

			for(Object estimateRetTypeObejct : conditionalvalues){
				EstimateRetType estimateRetType = (EstimateRetType)estimateRetTypeObejct;
				if(estimateRetType.getCustomerRef().getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForEstimateAccount;IN buildQuery getTxnID===========>"+ estimateRetType.getCustomerRef().getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+estimateRetType.getCustomerRef().getListID()+"\' ," : "\'"+estimateRetType.getCustomerRef().getListID()+"\' )");
				}
				count++;
			}					
		}


		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForEstimateAccount;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC vendor__c related Purchase_Orders__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForPurchaseVendor(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for VENDOR__C
		if(objectName.equals(SFDCObjectConstants.VENDOR__C) && flag){

			for(Object purchaseRetTypeObejct : conditionalvalues){
				PurchaseOrderRetType purchaseRetType = (PurchaseOrderRetType)purchaseRetTypeObejct;
				if(purchaseRetType.getVendorRef().getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseVendor;IN buildQuery getListid===========>"+ purchaseRetType.getVendorRef().getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+purchaseRetType.getVendorRef().getListID()+"\' ," : "\'"+purchaseRetType.getVendorRef().getListID()+"\' )");
				}
				count++;
			}					
		}


		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseVendor;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC vendor__c related Bills__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForBillVendor(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for VENDOR__C
		if(objectName.equals(SFDCObjectConstants.VENDOR__C) && flag){

			for(Object billRetTypeObejct : conditionalvalues){
				BillRetType billRetType = (BillRetType)billRetTypeObejct;
				if(billRetType.getVendorRef().getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillVendor;IN buildQuery getListid===========>"+ billRetType.getVendorRef().getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+billRetType.getVendorRef().getListID()+"\' ," : "\'"+billRetType.getVendorRef().getListID()+"\' )");
				}
				count++;
			}					
		}


		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillVendor;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC vendor__c related Bills__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForBillAccounting(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for ACCOUNTING__C
		if(objectName.equals(SFDCObjectConstants.ACCOUNTING__C) && flag){

			for(Object billRetTypeObejct : conditionalvalues){
				BillRetType billRetType = (BillRetType)billRetTypeObejct;
				if(billRetType.getAPAccountRef()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillVendor;IN buildQuery getListid===========>"+ billRetType.getAPAccountRef().getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+billRetType.getAPAccountRef().getListID()+"\' ," : "\'"+billRetType.getAPAccountRef().getListID()+"\' )");
				}
				count++;
			}					
		}


		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillVendor;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Account
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForAccount(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		returnMap = new HashMap<>();
		sqlQuery=new StringBuilder();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for Account
		if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){

			for(Object customerRetTypeObejct : conditionalvalues){
				CustomerRetType customerRetType = (CustomerRetType)customerRetTypeObejct;
				if(customerRetType.getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccount;IN buildQuery getTxnID===========>"+ customerRetType.getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+customerRetType.getListID()+"\' ," : "\'"+customerRetType.getListID()+"\' )");
				}

				if(customerRetType != null && customerRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(customerRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}                    
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccount;lastModifiedDate  ===========> "+ lastModifiedDate);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccount;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);

			return returnMap;        
		}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Account
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForAccounting(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		returnMap = new HashMap<>();
		sqlQuery=new StringBuilder();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for ACCOUNTING__C
		if(objectName.equals(SFDCObjectConstants.ACCOUNTING__C) && flag){

			for(Object accountingRetTypeObejct : conditionalvalues){
				AccountRetType accountRetType = (AccountRetType)accountingRetTypeObejct;
				if(accountRetType.getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccounting;IN buildQuery getTxnID===========>"+ accountRetType.getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+accountRetType.getListID()+"\' ," : "\'"+accountRetType.getListID()+"\' )");
				}

				if(accountRetType != null && accountRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(accountRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				
				index++;
				count++;
			}                    
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccounting;lastModifiedDate  ===========> "+ lastModifiedDate);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccounting;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);

			return returnMap;        
		}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Vendor__C
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForVendor(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		returnMap = new HashMap<>();
		sqlQuery=new StringBuilder();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for VENDOR__C
		if(objectName.equals(SFDCObjectConstants.VENDOR__C) && flag){

			for(Object vendorRetTypeObejct : conditionalvalues){
				VendorRetType vendorRetType = (VendorRetType)vendorRetTypeObejct;
				if(vendorRetType.getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForVendor;IN buildQuery getListId===========>"+ vendorRetType.getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+vendorRetType.getListID()+"\' ," : "\'"+vendorRetType.getListID()+"\' )");
				}

				if(vendorRetType != null && vendorRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(vendorRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}                    
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForVendor;lastModifiedDate  ===========> "+ lastModifiedDate);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForVendor;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			return returnMap;        
		}
	}
	
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Bill__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForBill(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		returnMap = new HashMap<>();
		sqlQuery=new StringBuilder();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for BILL__C
		if(objectName.equals(SFDCObjectConstants.BILL__C) && flag){

			for(Object billRetTypeObejct : conditionalvalues){
				BillRetType billRetType = (BillRetType)billRetTypeObejct;
				if(billRetType.getTxnID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForBill;IN buildQuery getListId===========>"+ billRetType.getTxnID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+billRetType.getTxnID()+"\' ," : "\'"+billRetType.getTxnID()+"\' )");
				}

				if(billRetType != null && billRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(billRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}                    
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForBill;lastModifiedDate  ===========> "+ lastModifiedDate);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForBill;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			return returnMap;        
		}
	}
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Item_Inventory__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForItemInventory(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		returnMap = new HashMap<>();
		sqlQuery=new StringBuilder();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for ITEM_INVENTORY__C
		if(objectName.equals(SFDCObjectConstants.ITEM_INVENTORY__C) && flag){

			for(Object itemInvenRetTypeObejct : conditionalvalues){
				ItemInventoryRetType itemInvenRetType = (ItemInventoryRetType)itemInvenRetTypeObejct;
				if(itemInvenRetType.getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForItemInventory;IN buildQuery getListId===========>"+ itemInvenRetType.getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+itemInvenRetType.getListID()+"\' ," : "\'"+itemInvenRetType.getListID()+"\' )");
				}

				if(itemInvenRetType != null && itemInvenRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(itemInvenRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}                    
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForItemInventory;lastModifiedDate  ===========> "+ lastModifiedDate);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForItemInventory;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			return returnMap;        
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Purchase_order__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForPurchaseOrder(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		returnMap = new HashMap<>();
		sqlQuery=new StringBuilder();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for PURCHASE_ORDER__C
		if(objectName.equals(SFDCObjectConstants.PURCHASE_ORDER__C) && flag){

			for(Object purchaseRetTypeObejct : conditionalvalues){
				PurchaseOrderRetType purchaseOrderRetType = (PurchaseOrderRetType)purchaseRetTypeObejct;
				if(purchaseOrderRetType.getTxnID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseOrder;IN buildQuery getTxnId===========>"+ purchaseOrderRetType.getTxnID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+purchaseOrderRetType.getTxnID()+"\' ," : "\'"+purchaseOrderRetType.getTxnID()+"\' )");
				}

				if(purchaseOrderRetType != null && purchaseOrderRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(purchaseOrderRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}                    
		}
		log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseOrder;lastModifiedDate  ===========> "+ lastModifiedDate);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForPurchaseOrder;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			return returnMap;        
		}
	}
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Account insert
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForAccountNamesInsert(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for Account
		if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){

			for(Object customerRetTypeObejct : conditionalvalues){
				CustomerAddRsType customerAddRsType = (CustomerAddRsType)customerRetTypeObejct;
				com.qb.appshark.CustomerRetType customerRetType = customerAddRsType.getCustomerRet();
				if(customerRetType != null && customerRetType.getFullName() != null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccountNamesInsert;IN buildQuery getTxnID===========>"+ customerRetType.getFullName());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+customerRetType.getFullName().trim()+"\' ," : "\'"+customerRetType.getFullName().trim()+"\' )");
				}
				count++;
			}					
		}

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccountNamesInsert;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Account update
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForAccountNamesUpdate(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for Account
		if(objectName.equals(SFDCObjectConstants.ACCOUNT) && flag){

			for(Object customerRetTypeObejct : conditionalvalues){
				CustomerModRsType customerModRsType = (CustomerModRsType)customerRetTypeObejct;
				com.appshark.customermodrs.CustomerRetType customerRetType = customerModRsType.getCustomerRet();
				if(customerRetType != null && customerRetType.getFullName() != null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccountNamesInsert;IN buildQuery getTxnID===========>"+ customerRetType.getFullName());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+customerRetType.getFullName().trim()+"\' ," : "\'"+customerRetType.getFullName().trim()+"\' )");
				}
				count++;
			}					
		}

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForAccountNamesInsert;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Product insert
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForProductNamesInsert(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for PRODUCT2
		if(objectName.equals(SFDCObjectConstants.PRODUCT2) && flag){

			for(Object ItemRetTypeObejct : conditionalvalues){
				ItemServiceAddRsType itemServiceAddRsType = (ItemServiceAddRsType)ItemRetTypeObejct;
				com.appshark.item.service.add.rs.ItemServiceRetType itemServiceRetType = itemServiceAddRsType.getItemServiceRet().get(0);
				if(itemServiceRetType != null && itemServiceRetType.getFullName() != null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForProductNamesInsert;IN buildQuery getTxnID===========>"+ itemServiceRetType.getFullName());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+itemServiceRetType.getFullName().trim()+"\' ," : "\'"+itemServiceRetType.getFullName().trim()+"\' )");	
				}
				if(itemServiceRetType != null && itemServiceRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(itemServiceRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}				
		}

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForProductNamesInsert;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Product update
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForProductNamesUpdate(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for PRODUCT2
		if(objectName.equals(SFDCObjectConstants.PRODUCT2) && flag){

			for(Object ItemRetTypeObejct : conditionalvalues){
				ItemServiceModRsType itemServiceModRsType = (ItemServiceModRsType)ItemRetTypeObejct;
				com.appshark.item.service.mod.rs.ItemServiceRetType itemServiceRetType = itemServiceModRsType.getItemServiceRet().get(0);
				if(itemServiceRetType != null && itemServiceRetType.getListID() != null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForProductNamesUpdate;IN buildQuery getTxnID===========>"+ itemServiceRetType.getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+itemServiceRetType.getListID()+"\' ," : "\'"+itemServiceRetType.getListID()+"\' )");	
					
				}
				count++;
			}				
		}

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForProductNamesUpdate;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Product 
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForProduct(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for PRODUCT2
		if(objectName.equals(SFDCObjectConstants.PRODUCT2) && flag){

			for(Object itemServiceRetTypeObejct : conditionalvalues){
				ItemServiceRetType itemServiceRetType = (ItemServiceRetType)itemServiceRetTypeObejct;
				if(itemServiceRetType.getListID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForProduct;IN buildQuery getTxnID===========>"+ itemServiceRetType.getListID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+itemServiceRetType.getListID()+"\' ," : "\'"+itemServiceRetType.getListID()+"\' )");	
				}
				if(itemServiceRetType != null && itemServiceRetType.getTimeModified() != null){
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(itemServiceRetType.getTimeModified().toString(),lastModifiedDate,index);
				}
				index++;
				count++;
			}					
		}

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForProduct;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}
	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC vendor__c related Bills__c
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery and flag
	 */
	public Map<String,Object> buildQueryForExpenseAccounting(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1;
		StringBuilder filter = new StringBuilder();//to hold values
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
		}

		//condition for ACCOUNTING__C
		if(objectName.equals(SFDCObjectConstants.ACCOUNTING__C) && flag == true){

			for(Object billRetTypeObejct : conditionalvalues){
				BillRetType billRetType = (BillRetType)billRetTypeObejct;
				List<ExpenseLineRetType> lstExpenses=billRetType.getExpenseLineRet();
				if(lstExpenses!=null && lstExpenses.size()>0){					
					for(ExpenseLineRetType objExpense:lstExpenses){
						if(objExpense.getAccountRef()!=null){
							if(objExpense.getAccountRef().getListID()!=null && objExpense.getAccountRef().getListID()!=""){
								filter.append("\'"+objExpense.getAccountRef().getListID()+"\'");
								 filter.append(" ,");
							}						
						}
					}
				}
			}					
		}

		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			String filtered=filter.substring(0, filter.length()-1);
			sqlQuery.append(filtered+")");
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForBillVendor;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			return returnMap;		
		}
	}

	/**
	 * This method is used to get a map of sqlQuery,flag with the parameters supplied for SFDC Opportunity
	 * @param String default query
	 * @param Set<String> set containing all sobject fields
	 * @param String sobject name
	 * @param List<? extends Object> list from qbsf
	 * @param List<String> fieldsTobeCompared
	 * @return Map<String,Object> containing sqlQuery,lastmodifiedDate and flag
	 */
	public Map<String,Object> buildQueryForOpportunity(String defaultQuery,Set<String> setOfFields,String objectName,List<? extends Object> conditionalvalues,List<String> fieldsTobeCompared){
		//variable declaration
		int count = 1, index = 0;
		StringBuilder filter = new StringBuilder();//to hold values
		String lastModifiedDate ="";
		sqlQuery=new StringBuilder();
		returnMap = new HashMap<>();
		//sqlQuery.append("SELECT Id, Name");//appending default query
		String fieldOne = fieldsTobeCompared.get(0);
		String fieldTwo = fieldsTobeCompared.get(1);
		log.info("fieldOne"+fieldOne);
		log.info("fieldTwo"+fieldTwo);

		if(setOfFields.contains(fieldOne) && setOfFields.contains(fieldTwo)){
			sqlQuery.append(defaultQuery +" IN (");
			flag = true;
			log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		}
		
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		//condition for Opportunity
		if(objectName.equals(SFDCObjectConstants.OPPORTUNITY) && flag){
			for(Object estimateRetTypeObejct : conditionalvalues){
				EstimateRetType estimateRetType = (EstimateRetType)estimateRetTypeObejct;
				if(estimateRetType.getTxnID()!= null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForOpportunity;IN buildQuery getTxnID===========>"+ estimateRetType.getTxnID());
					filter.append( (count != conditionalvalues.size() ) ? "\'"+estimateRetType.getTxnID()+"\' ," : "\'"+estimateRetType.getTxnID()+"\' )");			
				}
				
				if(estimateRetType != null && estimateRetType.getTimeModified() != null){
					log.info("Class : SqlQueryBuilder ; Method : buildQueryForOpportunity;estimateRetType.getTimeModified()"+estimateRetType.getTimeModified());
					lastModifiedDate = UtilityFunctions.sortComonDatesFromQB(estimateRetType.getTimeModified().toString(),lastModifiedDate,index);
				}				
				index++;
				count++;
			}			
		}
		
		log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
		if(sqlQuery != null && sqlQuery.length() <= 0){
			return returnMap;
		}else{
			log.info("filter------>>>>"+filter);
			sqlQuery.append(filter);
			log.info("Class : SqlQueryBuilder ; Method : buildQueryForOpportunity;sqlQuery for Object "+ objectName +" ===========> "+ sqlQuery);
			returnMap.put(CommonConstants.FLAG, flag);
			returnMap.put(CommonConstants.LASTMODIFIEDDATE, lastModifiedDate);
			returnMap.put(CommonConstants.SQLQUERY, sqlQuery.toString());
			log.info(CommonConstants.SQLQUERY+ " - "+sqlQuery);
			return returnMap;		
		}
	}
}
