package com.util;

import java.util.Arrays;
import java.util.logging.Logger;

import com.constants.CommonConstants;
import com.constants.fieldconstnats.ItemInventoryConstantFields;
import com.logging.PartnerConnectionErrorLogging;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.UpsertResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
/**
 * The  program contains logic to insert 100 records at a time 
 * to salesforce as salesforce upsert or 
 * update has a limit that it can only upsert 200 records at a time
 * @author  A Gopala krishna reddy
 * @version 1.0
 * @since   2016-06-07 
 */

public class QueryExecuter {
	
	
	static Logger log = Logger.getLogger(QueryExecuter.class.getName()); // for logging
	
	
	private QueryExecuter(){
		//hide 
	}
	
	/**
	 * This method is used to send a ststus with the array of sobject.
	 * SObject array is been partitioned and upserted 
	 * 
	 * @param arrayOfSobjects to be upserted
	 * @param partnerConnection from authentication
	 * @param sObjectForUpsert external fiel used to upsert
	 * @param sObjectForError sobject type
	 * @return String isOk
	 */
	public static String prepareAndExecute(SObject[] arrayOfSobjects,PartnerConnection partnerConnection,String sObjectForUpsert,String sObjectForError){
		int noOfChunks=(int) Math.ceil((double)arrayOfSobjects.length/100);//arrayOfSobjects divided by 100
		String isOk = "";// status of upsert
		log.info("arrayOfSobjects"+arrayOfSobjects.length);
		log.info("noOfChunks&&&&  " +noOfChunks);
		for(int i = 0; i < noOfChunks ; i++){//noOfChunks iteration
			log.info("i ****I " +i);
			UpsertResult[] results;//contains results after upserting
			try {
				log.info("100*i&&&&  if" +100*i);
				if(noOfChunks==1){//noOfChunks equals 1
					log.info("Arrays  - " +Arrays.copyOfRange(arrayOfSobjects, 100*i, arrayOfSobjects.length));
					results = partnerConnection.upsert(sObjectForUpsert,Arrays.copyOfRange(arrayOfSobjects, 100*i, arrayOfSobjects.length));
				}else if((i == noOfChunks - 1) && arrayOfSobjects.length%100 > 0){//noOfChunks is equal to i
					log.info("100*i&&&&  if" + (i == noOfChunks - 1));
					results = partnerConnection.upsert(sObjectForUpsert,Arrays.copyOfRange(arrayOfSobjects, 100*i,arrayOfSobjects.length));
				}else{
					log.info("elseee last record of chunk  " +(100*(i+1)));
					log.info("arrayOfSobjects.length  - " +arrayOfSobjects.length);
					results = partnerConnection.upsert(sObjectForUpsert,Arrays.copyOfRange(arrayOfSobjects, 100*i, 100*(i+1)));
				}// end of if
				PartnerConnectionErrorLogging.logUpsertErrors(results,sObjectForError);//method to get the show the logs after upsert
			} catch (ConnectionException e) {
				if(e.getMessage()!=null){
					new MailService().sendMail(CommonConstants.ADMIN,"Exception",e.getMessage());
				}
				log.info(" Class : QueryExecutor ; Method : prepareAndExecute ; Line No : 43 ---> Exception while upsert "+ sObjectForUpsert+" : "+ e.getMessage());
			}
			isOk = "ok";
		}
		return isOk;
	}
	/**
	 * This method is used to send a status with the array of sobject.
	 * SObject array is been partitioned and upserted 
	 * 
	 * @param arrayOfSobjects to be updated
	 * @param partnerConnection from authentication
	 * @param sObjectForError sobject type
	 * @return String isOk
	 */
	public static String prepareAndExecuteUpdate(SObject[] arrayOfSobjects,PartnerConnection partnerConnection,String sObjectForError){
		int noOfChunks=(int) Math.ceil((double)arrayOfSobjects.length/100);//arrayOfSobjects divided by 100
		String isOk = "";// status of update
		log.info("arrayOfSobjects"+arrayOfSobjects.length);
		log.info("noOfChunks&&&&  " +noOfChunks);
		for(int i = 0; i < noOfChunks ; i++){//noOfChunks iteration
			log.info("i ****I " +i);
			SaveResult[] results;//contains results after updating
			try {
				log.info("partnerConnection ----->> " +partnerConnection);
				log.info("arrayOfSobjects starting----->> " +arrayOfSobjects.length);
				if(noOfChunks==1){//noOfChunks equals 1
					SObject[] sObjectArrays = Arrays.copyOfRange(arrayOfSobjects, 100*i, arrayOfSobjects.length);					
				  log.info("Arrays  - " +Arrays.copyOfRange(arrayOfSobjects, 100*i, arrayOfSobjects.length).toString());
					results = partnerConnection.update(Arrays.copyOfRange(arrayOfSobjects, 100*i, arrayOfSobjects.length));
				}else if((i == noOfChunks - 1) && arrayOfSobjects.length%100 > 0){//noOfChunks is equal to i
					log.info("arrayOfSobjects----->> " +arrayOfSobjects.length);
					results = partnerConnection.update(Arrays.copyOfRange(arrayOfSobjects, 100*i,arrayOfSobjects.length));
				}else{
					log.info("elseee last record of chunk  " +(100*(i+1)));
					log.info("arrayOfSobjects.length  - " +arrayOfSobjects.length);
					results = partnerConnection.update(Arrays.copyOfRange(arrayOfSobjects, 100*i, 100*(i+1)));
				}// end of if
				PartnerConnectionErrorLogging.logUpsertErrors(results,sObjectForError);//method to get the show the logs after upsert
			} catch (ConnectionException e) {
				if(e.getMessage()!=null){
					new MailService().sendMail(CommonConstants.ADMIN,"Exception",e.getMessage());
				}
				log.info("ConnectionException  Class : QueryExecutor ; Method : prepareAndExecuteUpdate ; Line No : 43 ---> Exception while upsert "+ sObjectForError+" : "+ e.getMessage());
			}catch (NullPointerException e) {
				if(e.getMessage()!=null){
					new MailService().sendMail(CommonConstants.ADMIN,"Exception",e.getMessage());
				}
				log.info("NullPointerException Class : QueryExecutor ; Method : prepareAndExecuteUpdate ; Line No : 43 ---> Exception while upsert "+ sObjectForError+" : "+ e.getMessage());
			}
			
			isOk = "ok";
		}
		return isOk;
	}

}