package com.util;

import java.util.Calendar;
import java.util.logging.Logger;

/**
 * The  program conatins logic to get sorted last modified date from qb and sfdc
 * @author  A Gopala krishna reddy
 * @version 1.0
 * @since   2016-06-07 
 */

public class UtilityFunctions {
	
	private UtilityFunctions(){
		
	}
	
	static Logger log = Logger.getLogger(UtilityFunctions.class.getName());//for logging
	/**
	 * This method is used to get sorted last modified date from sfdc sobjects
	 * 
	 * @param  dateToCompare String
	 * @param  maxDateStr String
	 * @param  index  int
	 * @return String sorted lastModifiedDate
	 */	
	public static String sortAndGetLastModifiedDate(String dateToCompare,String maxDateStr,int index){
		Calendar actualdDate;
		Calendar maxDate;
		if(index==0){
			return dateToCompare;
		}else{

			actualdDate=getSFDCDateTimeByCalendar(dateToCompare);
			maxDate=getSFDCDateTimeByCalendar(maxDateStr);	

			if(maxDate.compareTo(actualdDate)<0){
				return dateToCompare;
			}else{
				return maxDateStr;
			}
		}

	}
	/**
	 * This method is used to get sorted last modified date from qb
	 * 
	 * @param  dateToCompare String
	 * @param  maxDateStr String
	 * @param  index int
	 * @return String sorted lastModifiedDate
	 */	
	public static String sortComonDatesFromQB(String dateToCompare,String maxDateStr,int index){
		Calendar actualdDate;
		Calendar maxDate;
		if(index==0){
			return dateToCompare;
		}else{

			actualdDate=getDateTimeByCalendar(dateToCompare);
			maxDate=getDateTimeByCalendar(maxDateStr);	

			if(maxDate.compareTo(actualdDate)<0){
				return dateToCompare;
			}else{
				return maxDateStr;
			}
		}

	}

	/**
	 * This method is used to convert a string type date to a calender for dates coming from sfdc 
	 * 
	 * @param  strDateTime String
	 * @return Calendar
	 */	
	public static Calendar getSFDCDateTimeByCalendar(String strDateTime){
		String dateStr;
		String timeStr = "";
		String timeStrTmp;
		String[] timeStrTmpArr = null;
		String[] dateStrArray = null;
		String[] timeStrArray = null;
		String[] strArrDateTime = null;
		String year = "";
		String month = "";
		String day = "";
		String hour = "";
		String minutes = "";
		String seconds = "";
		Calendar cal = Calendar.getInstance();
		if(strDateTime != null && strDateTime.trim().contains("T")){
			strArrDateTime = strDateTime.trim().split("T");
		}
		if(strArrDateTime != null && strArrDateTime.length == 2){
			dateStr = strArrDateTime[0];
			timeStrTmp = strArrDateTime[1];
			if(timeStrTmp != null && timeStrTmp.trim().length() > 0){
				if(timeStrTmp.trim().contains(".")){
					timeStrTmpArr = timeStrTmp.trim().split("\\.");
				}
				if(timeStrTmpArr != null && timeStrTmpArr.length == 2){
					timeStr = timeStrTmpArr[0];
				}
			}
			if(dateStr != null && dateStr.trim().length() > 0){
				dateStrArray = dateStr.trim().split("-");
			}
			if(timeStr != null && timeStr.trim().length() > 0){
				timeStrArray = timeStr.trim().split(":");
			}
			if(dateStrArray != null && dateStrArray.length == 3){
				year = dateStrArray[0];
				month = dateStrArray[1];
				day = dateStrArray[2];
			}
			if(timeStrArray != null && timeStrArray.length == 3){
				hour = timeStrArray[0];
				minutes = timeStrArray[1];
				seconds = timeStrArray[2];
			}
			cal.set(Calendar.YEAR, Integer.parseInt(year));
			cal.set(Calendar.MONTH, Integer.parseInt(month)-1);
			cal.set(Calendar.DATE, Integer.parseInt(day));
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
			cal.set(Calendar.MINUTE, Integer.parseInt(minutes));
			cal.set(Calendar.SECOND, Integer.parseInt(seconds));
			cal.set(Calendar.MILLISECOND, 0);
		}
		return cal;
	}
	/**
	 * This method is used to convert a string type date to a calendar for dates coming from qb
	 * 
	 * @param  strDateTime String
	 * @return Calendar
	 */	
	public static Calendar getDateTimeByCalendar(String strDateTime){
		String dateStr;
		String timeStr ="";
		String timeStrTmp;
		String[] timeStrTmpArr = null;
		String[] dateStrArray = null;
		String[] timeStrArray = null;
		String[] strArrDateTime = null;
		String year = "";
		String month = "";
		String day = "";
		String hour = "";
		String minutes = "";
		String seconds = "";
		Calendar cal = Calendar.getInstance();
		if(strDateTime != null && strDateTime.trim().contains("T")){
			strArrDateTime = strDateTime.trim().split("T");
		}
		if(strArrDateTime != null && strArrDateTime.length == 2){
			dateStr = strArrDateTime[0];
			timeStrTmp = strArrDateTime[1];
			if(timeStrTmp != null && timeStrTmp.trim().length() > 0){
				if(timeStrTmp.trim().contains("+")){
					timeStrTmpArr = timeStrTmp.trim().split("\\+");
				}
				if(timeStrTmp.trim().contains("-")){
					timeStrTmpArr = timeStrTmp.trim().split("\\-");
				}
				if(timeStrTmpArr != null && timeStrTmpArr.length == 2){
					timeStr = timeStrTmpArr[0];
				}
			}
			if(dateStr != null && dateStr.trim().length() > 0){
				dateStrArray = dateStr.trim().split("-");
			}
			if(timeStr != null && timeStr.trim().length() > 0){
				timeStrArray = timeStr.trim().split(":");
			}
			if(dateStrArray != null && dateStrArray.length == 3){
				year = dateStrArray[0];
				month = dateStrArray[1];
				day = dateStrArray[2];
			}
			if(timeStrArray != null && timeStrArray.length == 3){
				hour = timeStrArray[0];
				minutes = timeStrArray[1];
				seconds = timeStrArray[2];
			}
			cal.set(Calendar.YEAR, Integer.parseInt(year));
			cal.set(Calendar.MONTH, Integer.parseInt(month)-1);
			cal.set(Calendar.DATE, Integer.parseInt(day));
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
			cal.set(Calendar.MINUTE, Integer.parseInt(minutes));
			cal.set(Calendar.SECOND, Integer.parseInt(seconds));
			cal.set(Calendar.MILLISECOND, 0);
		}
		return cal;
	}
}
