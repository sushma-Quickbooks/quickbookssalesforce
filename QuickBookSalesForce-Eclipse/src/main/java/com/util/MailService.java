package com.util;

import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;


public class MailService {
    static Logger log = Logger.getLogger(MailService.class.getName()); // for logging
    private static MailSender mailSender;
    private static String fromEmail;
    private static String password;
    
    @Autowired
    public void setMailSender(MailSender mailSender) {
        MailService.mailSender = mailSender;
    }
    
    @Autowired
    public void setFromEmail(String fromEmail) {
        MailService.fromEmail = fromEmail;
    }
    
    @Autowired
    public void setPassword(String password) {
        MailService.password = password;
    }
    
    
    public void sendMail(String to, String subject, String msgBody) {
    	log.info("to ----->> " +to);
    	log.info("subject ----->> " +subject);
    	log.info("msgBody ----->> " +msgBody);
             Properties props = new Properties();
                    props.put("mail.smtp.starttls.enable", "true"); // added this line  
                 props.put("mail.smtp.host", "smtp.gmail.com");  
                 props.put("mail.smtp.port", "587");  
                 props.put("mail.smtp.auth", "true"); 
         
                 Session session = Session.getDefaultInstance(props,  
                    new javax.mail.Authenticator() {
                       protected PasswordAuthentication getPasswordAuthentication() {  
                       return new PasswordAuthentication(fromEmail.trim(),password);  
                   }  
                 }); 
            
          
            try {
                Message msg = new MimeMessage(session);
                log.info("fromEmail ----->> " +fromEmail);
                log.info("password ----->> " +password);
                msg.setFrom(new InternetAddress(fromEmail.trim(), "QB Admin"));
                msg.addRecipient(Message.RecipientType.TO,new InternetAddress(to.trim(), "Mr. User"));
                msg.setSubject(subject);
                msg.setText(msgBody);
                Transport.send(msg);
                

            } catch (AddressException e) {
            	log.info("AddressException ----->> " +e.getMessage());
            } catch (MessagingException e) {
            	log.info("MessagingException ----->> " +e.getMessage());
            } catch (Exception e) {
            	log.info("Exception ----->> " +e.getMessage());
            }
        
    }
    
    
    /**
       * Create mail session.
       * 
       * @return mail session, may not be null.
       */
    public Session createSession()
      {
          Properties props = new Properties();
   
         props.put("mail.smtp.starttls.enable", "true"); // added this line  
         props.put("mail.smtp.host", "smtp.gmail.com");  
         props.put("mail.smtp.port", "587");  
         props.put("mail.smtp.auth", "true"); 
    
        Session session = Session.getDefaultInstance(props,  
                new javax.mail.Authenticator() {
                   protected PasswordAuthentication getPasswordAuthentication() {  
                   return new PasswordAuthentication(fromEmail,password);  
               }  
       }); 
        
        return session;
      }
}