package com.constants.RequestXMLConstants;
/**
* The  program contains quickbooks request xml query constants
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class XMLConstants {
	
	private XMLConstants() {
	      //not called
	}
	

	public final static String CUSTOMER_QUERYR_XML_PART_1="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"12.0\""+"?>"+
			"<QBXML> <QBXMLMsgsRq onError="+"\"stopOnError\""+">"+
			"<CustomerQueryRq requestID=\""+System.currentTimeMillis()+"\" iterator=\"Start\"><MaxReturned>300000</MaxReturned>  "+
			" <FromModifiedDate>";
	public final static String CUSTOMER_QUERYR_XML_PART_2="</FromModifiedDate></CustomerQueryRq> </QBXMLMsgsRq></QBXML>";
	
	public final static String VENDOR_QUERYR_XML_PART_1="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"12.0\""+"?>"+
			"<QBXML> <QBXMLMsgsRq onError="+"\"stopOnError\""+">"+
			"<VendorQueryRq requestID=\""+System.currentTimeMillis()+"\" iterator=\"Start\"><MaxReturned>300000</MaxReturned>  "+
			" <FromModifiedDate>";
	
	public final static String VENDOR_QUERYR_XML_PART_2="</FromModifiedDate></VendorQueryRq> </QBXMLMsgsRq></QBXML>";
	
	public final static String ITEM_QUERY_REQUEST_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			+"<?qbxml version=\"7.0\"?>"
			+"<QBXML>"
			+"<QBXMLMsgsRq onError=\"stopOnError\">"
			+"<BillPaymentCheckQueryRq requestID=\"SXRlbVF1ZXJ5fDEyMA==\" >"
			+"</BillPaymentCheckQueryRq>"
			+"</QBXMLMsgsRq>"
			+"</QBXML>";
	
	public final static String ITEM_INVENTORY_QUERY_REQUEST_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			+"<?qbxml version=\"7.0\"?>"
			+"<QBXML>"
			+"<QBXMLMsgsRq onError=\"stopOnError\">"
			+"<ItemInventoryQueryRq requestID=\"SXRlbVF1ZXJ5fDEyMA==\" >"
			+"</ItemInventoryQueryRq>"
			+"</QBXMLMsgsRq>"
			+"</QBXML>";
	
	public final static String INVOICE_QUERY_REQUEST_XML_PART_1 = "<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"12.0\""+"?>"
			+ "<QBXML>"
			+ "<QBXMLMsgsRq onError=\"stopOnError\">"
			+ "<InvoiceQueryRq requestID=\""+System.currentTimeMillis()+"\" > "
			+ "<ModifiedDateRangeFilter> "
			+ "<FromModifiedDate>";
			
	public final static String INVOICE_QUERY_REQUEST_XML_PART_2 = "</FromModifiedDate> "
					+ "</ModifiedDateRangeFilter> "
					+ "<IncludeLineItems>true</IncludeLineItems> "
					+ "<OwnerID>0</OwnerID>"
					+ "</InvoiceQueryRq>"
					+ "</QBXMLMsgsRq>"
					+ "</QBXML>";
	
	public final static String SALESORDER_QUERY_REQUEST_XML_PART_1 = "<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"5.0\""+"?>"
			+ "<QBXML>"
			+ "<QBXMLMsgsRq onError=\"stopOnError\">"
			+ "<SalesOrderQueryRq requestID=\""+System.currentTimeMillis()+"\"  iterator=\"Start\"><MaxReturned>300000</MaxReturned> "
			+ "<ModifiedDateRangeFilter> "
			+ "<FromModifiedDate>";
			
	public final static String SALESORDER_QUERY_REQUEST_XML_PART_2 = "</FromModifiedDate> "
			+ "</ModifiedDateRangeFilter> "
			+ "<IncludeLineItems>true</IncludeLineItems> <OwnerID>0</OwnerID> "
			+ "</SalesOrderQueryRq>"
			+ "</QBXMLMsgsRq>"
			+ "</QBXML>";
	
	public final static String PURCHASEORDER_QUERY_REQUEST_XML_PART_1 = "<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"5.0\""+"?>"
			+ "<QBXML>"
			+ "<QBXMLMsgsRq onError=\"stopOnError\">"
			+ "<PurchaseOrderQueryRq requestID=\""+System.currentTimeMillis()+"\"  iterator=\"Start\"><MaxReturned>300000</MaxReturned> "
			+ "<ModifiedDateRangeFilter> "
			+ "<FromModifiedDate>";
			
	public final static String PURCHASEORDER_QUERY_REQUEST_XML_PART_2 = "</FromModifiedDate> "
			+ "</ModifiedDateRangeFilter> "
			+ "<IncludeLineItems>true</IncludeLineItems> <OwnerID>0</OwnerID> "
			+ "</PurchaseOrderQueryRq>"
			+ "</QBXMLMsgsRq>"
			+ "</QBXML>";
	
	public final static String ESTIMATE_QUERY_REQUEST_XML = "<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"5.0\""+"?>"
			+ "<QBXML>"
			+ "<QBXMLMsgsRq onError=\"continueOnError\">"
			+ "<EstimateQueryRq requestID=\"1\"> "
			+"<IncludeLineItems>true</IncludeLineItems>"
			+ "<OwnerID>0</OwnerID> "
			+ "</EstimateQueryRq> "
            +"<CustomerQueryRq requestID=\""+System.currentTimeMillis()+"\" iterator=\"Start\"><MaxReturned>300000</MaxReturned>"
			+"<IncludeRetElement>Name</IncludeRetElement>"
			+"</CustomerQueryRq>"
			+ "</QBXMLMsgsRq> "
			+ "</QBXML> ";
	public final static String BILL_QUERY_REQUEST_XML_PART_1 = "<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"5.0\""+"?>"
			+ "<QBXML>"
			+ "<QBXMLMsgsRq onError=\"stopOnError\">"
			+ "<BillQueryRq requestID=\""+System.currentTimeMillis()+"\"  iterator=\"Start\"><MaxReturned>300000</MaxReturned> "
			+ "<ModifiedDateRangeFilter> "
			+ "<FromModifiedDate>";
			
	public final static String BILL_QUERY_REQUEST_XML_PART_2 = "</FromModifiedDate> "
			+ "</ModifiedDateRangeFilter> "
			+ "<IncludeLineItems>true</IncludeLineItems> <OwnerID>0</OwnerID> "
			+ "</BillQueryRq>"
			+ "</QBXMLMsgsRq>"
			+ "</QBXML>";
	/*public final static String REPORT_QUERY_REQUEST_XML ="<?xml version="+"\"1.0\""+" encoding="+"\"utf-8\""+"?>"+
			"<?qbxml version="+"\"5.0\""+"?>"
			+"<QBXML>"
			+ "<QBXMLMsgsRq onError=\"stopOnError\">"
			+"<GeneralSummaryReportQueryRq>"
			+"<GeneralSummaryReportType>OpenPurchaseOrders</GeneralSummaryReportType>"
			+"<DisplayReport>false</DisplayReport>"
			+"<ReportPeriod>"
			+"<FromReportDate>2016-06-01</FromReportDate>"
			+" <ToReportDate>2017-07-01</ToReportDate>"
			+" </ReportPeriod>"
			+"</GeneralSummaryReportQueryRq>"
			+" </QBXMLMsgsRq>"
			+"</QBXML>";*/
	public final static String ACCOUNT_QUERY_REQUEST_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			+"<?qbxml version=\"7.0\"?>"
			+"<QBXML>"
			+"<QBXMLMsgsRq onError=\"stopOnError\">"
			+"<AccountQueryRq requestID=\"SXRlbVF1ZXJ5fDEyMA==\" >"
			+"</AccountQueryRq>"
			+"</QBXMLMsgsRq>"
			+"</QBXML>";
}
