package com.constants;
/**
* The  program contains Global common constants
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class CommonConstants {

	 private CommonConstants() {
	      //not called
	   }
	 
	public final static String COMMON_DATE_FORMAT="yyyy-MM-dd";	
	public final static String FLAG = "flag";
	public final static String LASTMODIFIEDDATE = "lastModifiedDate";
	public final static String SQLQUERY = "sqlQuery";
	public final static String ADMIN="app.qb2sf@gmail.com";
	
	
}