package com.constants.fieldconstnats;
/**
* The  program contains salesforce Purchase Order object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-22
*/
public final class PurchaseOrderConstantFields {
	
	

	private PurchaseOrderConstantFields() {
	      //not called
	} 
	
	
	public final static String ID="Id";	
	public final static String PURCHASE_ORDER_NAME="Name";	
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c	";
	
	public final static String DUE_DATE__C="Due_Date__c";
	public final static String EDITSEQUENCE__C="EditSequence__c";		
	public final static String EXPECTED_DATE__C="Expected_Date__c";
	public final static String IS_FULLY_RECEIVED__C="Is_Fully_Received__c";
	
	public final static String IS_MANUALLY_CLOSED="Is_Manually_closed__c";
	public final static String MEMO__C="Memo__c";		
	public final static String REFERRENCE_NUMBER__C="Reference_Number__c";
	public final static String SHIPPING_ADDRESS1__C="Shipping_Address1__c";
	
	public final static String SHIPPING_ADDRESS2__C="Shipping_Address2__c";
	public final static String SHIPPING_ADDRESS3__C="Shipping_Address3__c";		
	public final static String SHIPPING__CITY__C="Shipping_City__c";
	public final static String SHIPPING_POSTAL_CODE__C="Shipping_Postal_Code__c";
	
	public final static String SHIPPING_STATE__C="Shipping_State__c";
	public final static String TIME_CREATED__C="Time_Created__c";		
	public final static String TIME_MODIFIED__C="Time_Modified__c";
	public final static String TOTAL_AMOUNT__C="Total_Amount__c";
	
	public final static String TRANSACTION_DATE__C="Transaction_Date__c";
	public final static String TRANSACTION_NUMBER__C="Transaction_Number__c";		
	public final static String TXN_ID__C="TxnID__c";
	public final static String VENDOR__C="Vendor__c";
}

