package com.constants.fieldconstnats;
/**
* The  program contains salesforce Purchase Order Line Item object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class PurchaseOrderItemsConstantFields {
	
	private PurchaseOrderItemsConstantFields() {
	      //not called
	} 
	
	
	
	public final static String ID="Id";	
	public final static String PURCHASEORDER_LINE_ITEM_NAME="Name";	
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c	";
	
	public final static String ACCOUNT__C="Account__c";
	public final static String AMOUNT__C="Amount__c";		
	public final static String DESCRIPTION__C="Description__c";
	public final static String IS_BILLED__C="Is_Billed__c";
	
	public final static String IS_MANUALLY_CLOSED="Is_Manually_Closed__c";
	public final static String PRODUCT__C="Product__c";		
	public final static String PURCHASE_ORDER__C="Purchase_Order__c";
	public final static String QUANTITY__C="Quantity__c";
	
	public final static String RATE__C="Rate__c";
	public final static String RECEIVED_QUANTITY__C="Received_Quantity__c";		
	public final static String TXN_ID__C="TxnId__c";
	public final static String TXNLINEID__C="TxnLineID__c";
	public final static String EXTERN_ID__C="Extern_ID__c";

}
