package com.constants.fieldconstnats;
/**
* The  program contains salesforce Vendor__c object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-22
*/
public final class VendorConstantFields {
	
	private VendorConstantFields() {
	      //not called
	} 
	
	
	public final static String LISTID__C="ListID__c";
	public final static String ID="Id";	
	public final static String VENDOR_NAME="Name";	
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c	";
	
	public final static String BALANCE__C="Balance__c";
	public final static String EDITSEQUENCE__C="EditSequence__c";		
	public final static String EMAIL__C="Email__c";
	public final static String FAX__C="Fax__c";
	
	public final static String FIRSTNAME__C="FirstName__c";
	public final static String LASTNAME__C="LastName__c";		
	public final static String PHONE__C="Phone__c";
	public final static String SALUTATION__C="Salutation__c";
	
	public final static String TIME_CREATED__C="Time_Created__c";
	public final static String TIME_MODIFIED__C="Time_Modified__c";		
	public final static String VENDOR_ADDRESS1__C="Vendor_Address1__c";
	public final static String VENDOR_ADDRESS2__C="Vendor_Address2__c";
	
	public final static String VENDOR_ADDRESS3__C="Vendor_Address3__c";
	public final static String VENDOR_CITY__C="Vendor_City__c";		
	public final static String VENDOR_POSTAL_CODE__C="Vendor_Postal_Code__c";
	public final static String VENDOR_STATE__C="Vendor_State__c";
}
