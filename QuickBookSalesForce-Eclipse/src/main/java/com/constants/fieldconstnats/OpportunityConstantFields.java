package com.constants.fieldconstnats;
/**
* The  program contains salesforce Opportunity object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final  class OpportunityConstantFields {
	
	
	private OpportunityConstantFields() {
	      //not called
	} 
	public final static String OPPORTUNITY_ID="Id";
	public final static String OPPORTUNITY_NAME="Name";
	public final static String TXNID__C="TxnID__c";
	public final static String REF_NUMBER__C ="Ref_Number__c";
	public final static String CLOSEDATE="CloseDate";
	public final static String ADDRESS__C="Address__c";
	public final static String CITY__C="City__c";
	public final static String STATE__C="State__c";
	public final static String POSTAL_CODE__C="Postal_Code__c";
	public final static String COUNTRY__C="Country__c";
	public final static String STAGENAME="StageName";
	public final static String EDITSEQUENCE__C="EditSequence__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c";
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String ACCOUNTID="AccountId";

}
