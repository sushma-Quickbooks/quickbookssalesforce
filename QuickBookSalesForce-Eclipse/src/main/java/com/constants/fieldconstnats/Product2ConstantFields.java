package com.constants.fieldconstnats;
/**
* The  program contains salesforce Product2 object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final  class Product2ConstantFields {
	
	
	private Product2ConstantFields() {
	      //not called
	} 
	public final static String PRODUCT2_NAME="Name";
	public final static String DESCRIPTION="Description";
	public final static String RATE__C="Rate__c";
	public final static String PRODUCTCODE="ProductCode";
	public final static String ISACTIVE="IsActive";
	public final static String EDIT_SEQUENCE__C="Edit_Sequence__c";
	
	public final static String LISTID__C="ListID__c";
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String FLAG__C="flag__c";
	public final static String ID="Id";
	public final static String PRODUCT_CODE="ProductCode";
}
