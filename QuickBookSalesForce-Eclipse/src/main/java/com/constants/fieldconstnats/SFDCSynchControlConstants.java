package com.constants.fieldconstnats;
/**
* The  program contains salesforce SFDCSynchControl custom object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class SFDCSynchControlConstants {
	
	private SFDCSynchControlConstants() {
	      //not called
	} 
	
	
	public final static String SYNCID__C="SynchId__c";	
	public final static String SFDCLASTSYNCH__C="SFDCLastSynch__c";	
	public final static String PRODUCT_INSERT_LAST_SYNC__C="Product_Insert_Last_Sync__c";
	public final static String PRODUCT_UPDATE_LAST_SYNC__C="Product_Update_Last_Sync__c";
	public final static String OPPORTUNITY_INSERT_LAST_SYNC="Opportunity_Insert_Last_Sync__c";
	public final static String OPPORTUNITY_UPDATE_LAST_SYNC="Opportunity_Update_Last_Sync__c";
	public final static String OPPORTUNITY_ITEM_UPDATE_LAST_SYNC="Opportunity_Item_Update_Last_Sync__c";
	public final static String SFDCUPDATELASTSYNC__C="SFDCUpdateLastSynch__c";
	public final static String SFDCINVOICEINSERTLASTSYNC__C="SFDCInvoiceInsertLastSync__c";
	public final static String INVOICE_ITEM_UPDATE_LAST_SYNC__C="Invoice_Item_Update_Last_Sync__c";
	public final static String SFDCINVOICEUPDATELASTSYNC__C="SFDCInvoiceUpdateLastSync__c";
}
