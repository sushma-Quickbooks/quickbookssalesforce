package com.constants.fieldconstnats;
/**
* The  program contains salesforce  Accounting object names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-30 
*/
public final class AccountingConstantFields {
	
	private AccountingConstantFields() {
	      //not called
	}
	
	public final static String ACCOUNTING_NAME="Name";
	public final static String ACCOUNTING_ID="Id";
	public final static String ACCOUNTING_TYPE__C="Account_Type__c";
	public final static String BALANCE__C="Balance__c";
	public final static String CASHFLOWCLASSIFICATION__C="CashFlowClassification__c";
	public final static String EDIT_SEQUENCE__C="Edit_Sequence__c";
	public final static String FULL_NAME__C="Full_Name__c";
	public final static String IS_ACTIVE__C="Is_Active__c";
	public final static String LIST_ID__C="List_ID__c";
	public final static String PARENT_FULL_NAME__C="Parent_Full_Name__c";
	
	public final static String PARENT_LIST_ID__C="Parent_List_Id__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c";
	public final static String SUB_LEVEL__C="Sub_Level__c";
	
	public final static String TIME_CREATED__C="Time_Created__c";
	public final static String TIME_MODIFIED__C="Time_Modified__c";
	public final static String TOATL_BALANCE__C="Total_Balance__c";
	public final static String PARENT_ACCOUNTING__C="Parent_Accounting__c";
}
