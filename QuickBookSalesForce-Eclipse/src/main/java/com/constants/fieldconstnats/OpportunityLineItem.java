package com.constants.fieldconstnats;
/**
* The  program contains salesforce OpportunityLineItem object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class OpportunityLineItem {
	
	
	private OpportunityLineItem() {
	      //not called
	} 

	public final static String OPPORTUNITYLINEITEM_ID="Id";
	public final static String TXN_LINEID__C="Txn_LineID__c";
	public final static String EXTERNID__C="ExternID__c";
	public final static String QUANTITY="Quantity";
	public final static String UNITPRICE="UnitPrice";
	public final static String DESCRIPTION="Description";
	
	public final static String PRICEBOOKENTRYID="PricebookEntryId";
	public final static String OPPORTUNITYID="OpportunityId";
	public final static String PRODUCT2ID="Product2Id";
	public final static String TXNID__C="TxnID__c";
	public final static String LIST_PRICE="ListPrice";
	public final static String PRODUCTCODE="ProductCode";
	
	
}
