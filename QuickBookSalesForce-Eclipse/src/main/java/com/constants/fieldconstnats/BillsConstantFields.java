package com.constants.fieldconstnats;

public final class BillsConstantFields {
	
	
	private BillsConstantFields() {
	      //not called
	}
	public final static String ID="Id";	
	public final static String BILLS_NAME="Name";	
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c	";
	
	public final static String AMOUNT_DUE__C="Amount_due__c";		
	public final static String APACCOUNT_LISTID__C="ApAccount_ListId__c";
	public final static String APACCOUNT_TYPE__C="AP_Account_Type__c";
	
	public final static String DUE_DATE__C="Due_Date__c";		
	public final static String QUANTITY__C="Quantity__c";
	public final static String REFERRENCE_NUMBER__C="Referrence_Number__c";
	
	public final static String TIME_CREATED__C="Time_Created__c";
	public final static String TIME_MODIFIED__C="Time_Modified__c";	
	public final static String TRANSACTION_DATE__C="Transaction_Date__c";
	public final static String TRANSACTION_NUMBER__C="Transaction_Number__c";
	
	public final static String TXNID__C="TxnID__c";
	public final static String VENDOR__C="Vendor__c";	
	public final static String ACCOUNTING__C="Accounting__c";
}
