package com.constants.fieldconstnats;
/**
* The  program contains salesforce InvoiceLineItems custom object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class InvoiceLineItemsConstantFields {
	
	
	private InvoiceLineItemsConstantFields() {
	      //not called
	} 

	
	public final static String INVOICE_LINE_ITEMS_ID="Id";
	public final static String INVOICE_LINE_ITEMS_NAME="Name";
	
	public final static String QUANTITY__C="Quantity__c";
	public final static String INVOICE__C="Invoice__c";
	public final static String AMOUNT__C="Amount__c";
	public final static String RATE__C="Rate__c";
	public final static String DESCRIPTION__C="Description__c";
	
	public final static String TXNLINEID__C="TxnLineID__c";
	public final static String TXNID__C="TxnID__c";
	public final static String PRODUCT__C="Product__c";
	public final static String EXTERNID__C="ExternID__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c";
	
	public final static String SALESTAXCODEREF_LISTID__C="SalesTaxCodeRef_ListID__c";
	public final static String SALESTAXCODEREF_FULL_NAME__C="SalesTaxCodeRef_Full_Name__c";
	public final static String CLASSREFLISTID__C="ClassRefListID__c";
	public final static String CLASSREFFULLNAME__C="ClassRefFullName__c";

}
