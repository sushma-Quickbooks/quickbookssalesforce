package com.constants.fieldconstnats;
/**
* The  program contains salesforce PricebookEntry object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class PricebookEntryConstantFields {
	
	private PricebookEntryConstantFields() {
	      //not called
	} 

	
	public final static String PRODUCTCODE="ProductCode";
	public final static String UNITPRICE="UnitPrice";
	public final static String LISTID__C="ListID__c";
	public final static String ISACTIVE="IsActive";
	public final static String USESTANDARDPRICE="UseStandardPrice";
	public final static String PRICEBOOK2ID="Pricebook2Id";
}
