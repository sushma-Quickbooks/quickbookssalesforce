package com.constants.fieldconstnats;
/**
* The  program contains salesforce Account object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class AccountConstantFields {
	
	private AccountConstantFields() {
	      //not called
	}
	
	public final static String ACCOUNT_NAME="Name";	
	public final static String COMPANY_NAME__C="Company_Name__c";
	public final static String JOB_TITLE__C ="Job_Title__c";
	
	// Billing Address
	public final static String BILLINGSTREET="BillingStreet";
	public final static String BILLINGCITY="BillingCity";
	public final static String BILLINGSTATE="BillingState";
	public final static String BILLINGPOSTALCODE="BillingPostalCode";
	public final static String BILLINGCOUNTRY="BillingCountry";
	// Shipping Address 
	public final static String SHIPPINGSTREET="ShippingStreet";
	public final static String SHIPPINGCITY="ShippingCity";
	public final static String SHIPPINGSTATE="ShippingState";
	public final static String SHIPPINGPOSTALCODE="ShippingPostalCode";
	public final static String SHIPPINGCOUNTRY="ShippingCountry";
	
	public final static String PHONE="Phone";
	public final static String FAX="Fax";
	public final static String EMAIL__C="Email__c";
	public final static String ACTIVE__C="Active__c";
	public final static String CURRENT_BALANCE__C="Current_Balance__c";
	
	public final static String LASTMODIFIEDDATE="LastModifiedDate";
	public final static String SFDC_UPDATE_FLAG__C="SFDC_Update_Flag__c";
	public final static String TIME_MODIFIED__C="Time_Modified__c";
		
	public final static String LISTID__C="ListID__c";
	public final static String QBLISTID__C="QBListId__c";
	public final static String ID="Id";
		
	public final static String SFDC_FLAG__C="SFDC_Flag__c";
	public final static String QB_FLAG__C="QB_Flag__c";
	
	public final static String MARKET_SEGMENT__C="Market_Segment__c";
	public final static String SALES_REP__C="Sales_Rep__c";
	public final static String TERMS_CODE__C="Terms_Code__c";
	public final static String EDITSEQUENCE__C="EditSequence__c";

}
