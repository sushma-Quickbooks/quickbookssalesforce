package com.constants.fieldconstnats;
/**
* The  program contains salesforce QBSynchControl custom object field api names
* @author  Appsahrk
* @version 1.0
* @since   2016-06-08 
*/
public final class QBSynchControlConstants {
	
	private QBSynchControlConstants() {
	      //not called
	} 
	
	
	
	public final static String SYNCID__C="SynchId__c";	
	public final static String QBSYNCH__C="QBLastSynch__c";	
	
}
